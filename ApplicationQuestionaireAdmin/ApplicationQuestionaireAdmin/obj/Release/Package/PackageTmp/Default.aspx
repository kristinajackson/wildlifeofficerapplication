﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ApplicationQuestionaireAdmin.Default" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <asp:Label ID="lblConfirm" runat="server" Visible="true"></asp:Label>
    <asp:ListView ID="ListView1" runat="server" DataSourceID="SqlDataSource1"  DataKeyNames="ID" Visible="true" >
        <EditItemTemplate>
            <tr style="background-color:#008A8C;color: #666666;">
                <td>
                    <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="Update" class="btn-xs btn-default"  />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" class="btn-xs btn-default"  />
                </td>
                <td>
                    <asp:TextBox ID="UserNameTextBox" runat="server" Text='<%# Bind("UserName") %>' />
                </td>
                <td>
                    <asp:TextBox ID="PasswordTextBox" runat="server" Text='<%# Bind("Password") %>' />
                </td>
                <td>
                    <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' />
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ID") %>' Visible="false" />
                </td>
                <td>
                    <asp:TextBox ID="FirstNameTextbox" runat="server" Text='<%# Bind("Fname") %>' />
                </td>
                <td>
                    <asp:TextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("Lname") %>' />
                </td>
            </tr>
        </EditItemTemplate>
        <InsertItemTemplate>
            <tr>
                <td>
                    <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="Insert" class="btn-xs btn-default"  />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Clear" class="btn-xs btn-default"  />
                </td>
                <td>
                    <asp:TextBox ID="UserNameTextBox" runat="server" Text='<%# Bind("UserName") %>' />
                </td>
                <td>
                    <asp:TextBox ID="PasswordTextBox" runat="server" Text='<%# Bind("Password") %>' />
                </td>
                <td>
                    <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' />
                </td>
                <td>
                    <asp:TextBox ID="FirstNameTextbox" runat="server" Text='<%# Bind("Fname") %>' />
                </td>
                <td>
                    <asp:TextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("Lname") %>' />
                </td>
            </tr>
        </InsertItemTemplate>
        <ItemTemplate>
            <tr class="<%# Convert.ToBoolean(Container.DataItemIndex % 2) ? "ResEvenRowClr" : "ResOddRowClr" %>">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" class="btn-xs btn-default"  CommandName="Delete" Text="Delete" OnClientClick="return confirm('Are You Certain You Want to Delete?');"  />
                    <asp:Button ID="EditButton" runat="server" class="btn-xs btn-default" CommandName="Edit" Text="Edit" />
                </td>
                <td>
                    <asp:Label ID="UserNameLabel" runat="server" Text='<%# Eval("UserName") %>' />
                </td>
                <td>
                    <asp:Label ID="PasswordLabel" runat="server" Text='<%# Eval("Password") %>' />
                </td>
                <td>
                    <asp:Label ID="EmailLabel" runat="server" Text='<%# Eval("Email") %>' />
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ID") %>' Visible="false" />
                </td>
                <td>
                    <asp:Label ID="FirstNameLabel" runat="server" Text='<%# Eval("Fname") %>' />
                </td>
                <td>
                    <asp:Label ID="LastNameLabel" runat="server" Text='<%# Eval("Lname") %>' />
                </td>
            </tr>                                
        </ItemTemplate>
        <LayoutTemplate>
            <table id="Table1" runat="server">
                <tr id="Tr1" runat="server">
                    <td id="Td1" runat="server">
                        <table id="itemPlaceholderContainer" runat="server" border="1" style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;font-family: Verdana, Arial, Helvetica, sans-serif;">
                            <tr id="Tr2" runat="server" style="background-color:#DCDCDC;color: #000000;">
                                <th id="Th1" runat="server"></th>
                                <th id="Th2" runat="server">UserName</th>
                                <th id="Th3" runat="server">Password</th>
                                <th id="Th4" runat="server">Email</th>
                                <th id="Th5" runat="server">FirstName</th>
                                <th id="Th6" runat="server">LastName</th>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="Tr3" runat="server">
                    <td id="Td2" runat="server" style="text-align: center;background-color: #CCCCCC;font-family: Verdana, Arial, Helvetica, sans-serif;color: #000000;">
                        <asp:DataPager ID="DataPager1" runat="server">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowLastPageButton="True" />
                            </Fields>
                        </asp:DataPager>   
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <SelectedItemTemplate>
            <tr style="background-color:#008A8C;font-weight: bold;color: #FFFFFF;">
                <td>
                    <asp:Button ID="DeleteButton" class="btn-xs btn-default"  runat="server" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Are You Certain You Want to Delete?');"  />
                    <asp:Button ID="EditButton" class="btn-xs btn-default"  runat="server" CommandName="Edit" Text="Edit" />
                </td>
                <td>
                    <asp:Label ID="UserNameLabel" runat="server" Text='<%# Eval("UserName") %>' />
                </td>
                <td>
                    <asp:Label ID="PasswordLabel" runat="server" Text='<%# Eval("Password") %>' />
                </td>
                <td>
                    <asp:Label ID="EmailLabel" runat="server" Text='<%# Eval("Email") %>' />
                </td>
                <td>
                    <asp:Label ID="FirstNameLabel" runat="server" Text='<%# Eval("Fname") %>' />
                </td>
                <td>
                    <asp:Label ID="LastNameLabel" runat="server" Text='<%# Eval("Lname") %>' />
                </td>
            </tr>
        </SelectedItemTemplate>
    </asp:ListView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>"
         DeleteCommand="DELETE FROM OAQ.ApplicantQuestionaire WHERE (ID = @ID)" 
        SelectCommand="SELECT UserName, Password, Email, ID, Fname, Lname, CreatedDate FROM OAQ.ApplicantQuestionaire ORDER BY ID ASC" 
        UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET UserName = @UserName, Password = @Password, Email = @Email, Fname = @Fname, Lname = @Lname WHERE (ID = @ID)">
        <DeleteParameters>
            <asp:Parameter Name="ID" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="UserName" />
            <asp:Parameter Name="Password" />
            <asp:Parameter Name="Email" />
            <asp:Parameter Name="Fname" />
            <asp:Parameter Name="Lname" />
            <asp:Parameter Name="ID" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:Button ID="btnGoback" runat="server" Text="  Go Back " class="btn btn-primary"  PostBackUrl="~/CreateUser.aspx"/>
    <asp:Button ID="btnLogout" runat="server" Text="  Logout  "  class="btn btn-danger"  OnClick="btnLogout_Click" OnClientClick="return confirm('Are You Sure you want to Logout?');" />

</asp:Content>
