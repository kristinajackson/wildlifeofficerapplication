﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="CreateUser.aspx.cs" Inherits="ApplicationQuestionaireAdmin.CreateUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <head>
        <title>Create Applicant</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
    </head>
    <style type="text/css">
        .center {
            text-align: center;
        }

        .right {
            text-align: right;
        }

        #right {
            text-align: right;
        }

        .auto-style1 {
            width: 402px;
        }
    </style>
     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">

    <div id="alert" class="alert alert-success" role="alert" runat="server" visible="false">
        <strong>Well done!</strong> Applicant successfully saved.
    </div>
    <div id="danger" class="alert alert-danger" role="alert" runat="server" visible="false">
        <strong>ERROR!</strong> This account already in use!
    </div>

       <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Insert" OnItemInserted="FormView1_ItemInserted" Width="100%">
        <InsertItemTemplate>
            <div class="container">
                <div class="text-center">
                    <h2>Create New Applicant</h2>
                    <h4>Please enter  applicant information:</h4>
                </div>
                <br />
                <div class="form-group row">
                    <label class="col-3 col-form-label" for="UserName">User Name:</label>
                    <div class="col-5">
                        <asp:TextBox ID="UserName" runat="server" Text='<%# Bind("UserName") %>' CssClass="form-control" TabIndex="1"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group row">
                    <asp:Label ID="PasswordLabel" CssClass="col-3 col-form-label" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                    <div class="col-5">
                        <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="form-control" Text='<%# Bind("Password") %>' TabIndex="2"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group row">
                    <asp:Label ID="EmailLabel" runat="server" CssClass="col-3 col-form-label" AssociatedControlID="Email">E-mail:</asp:Label>
                    <div class="col-5">
                        <asp:TextBox ID="Email" runat="server" Text='<%# Bind("Email") %>' TextMode="Email" CssClass="form-control" TabIndex="3"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="EmailRegular" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="Email" ErrorMessage="E-mail is required." ToolTip="E-mail is required." ValidationGroup="CreateUserWizard1">*</asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="form-group row">
                    <asp:Label ID="FirstNameLabel" runat="server" CssClass="col-3 col-form-label" AssociatedControlID="FirstName">First Name:</asp:Label>
                    <div class="col-5">
                        <asp:TextBox ID="FirstName" runat="server" Text='<%# Bind("Fname") %>' CssClass="form-control" TabIndex="4"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="FirstNameRequired" runat="server" ControlToValidate="FirstName" ErrorMessage="First name is required." ToolTip="First Name is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group row">
                    <asp:Label ID="LastNameLabel" runat="server" CssClass="col-3 col-form-label" AssociatedControlID="LastName">Last Name:</asp:Label>
                    <div class="col-5">
                        <asp:TextBox ID="LastName" runat="server" Text='<%# Bind("Lname") %>' CssClass="form-control" TabIndex="5"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="LastName" ErrorMessage="Last name is required." ToolTip="Last Name is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                    </div>
                </div>
                <asp:Button ID="InsertButton" runat="server" CausesValidation="True"
                    CommandName="Insert" Text="   Submit   " CssClass="btn btn-success" ValidationGroup="CreateUserWizard1" />
                <button type="button" class="btn btn-primary" id="ViewDtaBtn" name="ViewDtaBtn" runat="server" title="  View Applicant  " onserverclick="Button1_Click">View Applicants </button>
                <asp:Button ID="Exitbtn" CssClass="btn btn-danger" runat="server" Text="  Logout  " OnClick="Button2_Click" OnClientClick="return confirm('Are You Sure you want to Logout?');" />

                <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>



        </InsertItemTemplate>
    </asp:FormView>
    <br />
    <br />
    <br />
    <br />

    &nbsp;<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>"
        InsertCommand="INSERT INTO OAQ.ApplicantQuestionaire(UserName, Password,  Email,Fname,Lname) VALUES (@UserName, @Password, @Email,@Fname,@Lname)"
        SelectCommand="SELECT UserName, Password, Email, Fname, MiddleInt, Lname FROM OAQ.ApplicantQuestionaire ORDER BY CreatedDate DESC">
        <InsertParameters>
            <asp:Parameter Name="UserName" />
            <asp:Parameter Name="Password" />
            <asp:Parameter Name="Email" />
            <asp:Parameter Name="Fname" />
            <asp:Parameter Name="Lname" />
        </InsertParameters>
    </asp:SqlDataSource>
    <br />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</asp:Content>
