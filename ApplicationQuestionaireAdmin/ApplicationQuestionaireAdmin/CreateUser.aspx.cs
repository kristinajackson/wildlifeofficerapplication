﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Runtime;
using System.Diagnostics;
using System.Data.SqlClient;

namespace ApplicationQuestionaireAdmin
{
    public partial class CreateUser : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void FormView1_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            Label lbltop = (Label)FormView1.FindControl("lbltop");

            if (e.Exception == null)
            {
                if (e.AffectedRows == 1)
                {   // Show success popup when applicant is saved and removes popup after 3 seconds
                    alert.Visible = true;
                    danger.Visible = false;
                    Response.AddHeader("Refresh", "3");
                  
                }
            }
            else
            { // Display the Error Message if the Record Not Saved
                if (e.Exception.Message.Contains("UNIQUE KEY"))
                    danger.Visible = true;
                else
                    lbltop.Text = e.Exception.Message;
                e.ExceptionHandled = true;
                e.KeepInInsertMode = true;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("loginpage.aspx");
        }

    


    }
}


      