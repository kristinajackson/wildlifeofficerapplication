﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="ApplicationQuestionaireAdmin.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <asp:ListView ID="ListView1" runat="server" DataKeyNames="ID" DataSourceID="SqlDataSource1" GroupItemCount="3" >
        <AlternatingItemTemplate>
            <td runat="server" style="">ID:
                <asp:DynamicControl runat="server" DataField="ID" Mode="ReadOnly" />
                <br />
                UserName:
                <asp:DynamicControl runat="server" DataField="UserName" Mode="ReadOnly" />
                <br />
                Password:
                <asp:DynamicControl runat="server" DataField="Password" Mode="ReadOnly" />
                <br />
                Email:
                <asp:DynamicControl runat="server" DataField="Email" Mode="ReadOnly" />
                <br />
                Fname:
                <asp:DynamicControl runat="server" DataField="Fname" Mode="ReadOnly" />
                <br />
                MiddleInt:
                <asp:DynamicControl runat="server" DataField="MiddleInt" Mode="ReadOnly" />
                <br />
                Lname:
                <asp:DynamicControl runat="server" DataField="Lname" Mode="ReadOnly" />
                <br />
                InterviewedbyTWRA:
                <asp:DynamicControl runat="server" DataField="InterviewedbyTWRA" Mode="ReadOnly" />
                <br />
                When_Interviewed:
                <asp:DynamicControl runat="server" DataField="When_Interviewed" Mode="ReadOnly" />
                <br />
                Relatives:
                <asp:DynamicControl runat="server" DataField="Relatives" Mode="ReadOnly" />
                <br />
                Explain_Relatives:
                <asp:DynamicControl runat="server" DataField="Explain_Relatives" Mode="ReadOnly" />
                <br />
                YrTNResidence:
                <asp:DynamicControl runat="server" DataField="YrTNResidence" Mode="ReadOnly" />
                <br />
                List_Residence:
                <asp:DynamicControl runat="server" DataField="List_Residence" Mode="ReadOnly" />
                <br />
                FishingHuntingTypeNo:
                <asp:DynamicControl runat="server" DataField="FishingHuntingTypeNo" Mode="ReadOnly" />
                <br />
                Enforcement_Exper:
                <asp:DynamicControl runat="server" DataField="Enforcement_Exper" Mode="ReadOnly" />
                <br />
                Explain_Enforcement:
                <asp:DynamicControl runat="server" DataField="Explain_Enforcement" Mode="ReadOnly" />
                <br />
                Enforcement_Train:
                <asp:DynamicControl runat="server" DataField="Enforcement_Train" Mode="ReadOnly" />
                <br />
                Explain_EnforcementTrain:
                <asp:DynamicControl runat="server" DataField="Explain_EnforcementTrain" Mode="ReadOnly" />
                <br />
                Advanced_Courses:
                <asp:DynamicControl runat="server" DataField="Advanced_Courses" Mode="ReadOnly" />
                <br />
                Benefical_Courses:
                <asp:DynamicControl runat="server" DataField="Benefical_Courses" Mode="ReadOnly" />
                <br />
                Hobbies:
                <asp:DynamicControl runat="server" DataField="Hobbies" Mode="ReadOnly" />
                <br />
                Farming:
                <asp:DynamicControl runat="server" DataField="Farming" Mode="ReadOnly" />
                <br />
                Forestry:
                <asp:DynamicControl runat="server" DataField="Forestry" Mode="ReadOnly" />
                <br />
                AnimalHusb:
                <asp:DynamicControl runat="server" DataField="AnimalHusb" Mode="ReadOnly" />
                <br />
                Comm_Fish:
                <asp:DynamicControl runat="server" DataField="Comm_Fish" Mode="ReadOnly" />
                <br />
                Sm_Gas_Eng:
                <asp:DynamicControl runat="server" DataField="Sm_Gas_Eng" Mode="ReadOnly" />
                <br />
                AutoRepair:
                <asp:DynamicControl runat="server" DataField="AutoRepair" Mode="ReadOnly" />
                <br />
                BoatOper:
                <asp:DynamicControl runat="server" DataField="BoatOper" Mode="ReadOnly" />
                <br />
                Hunting:
                <asp:DynamicControl runat="server" DataField="Hunting" Mode="ReadOnly" />
                <br />
                Fishing:
                <asp:DynamicControl runat="server" DataField="Fishing" Mode="ReadOnly" />
                <br />
                Camping:
                <asp:DynamicControl runat="server" DataField="Camping" Mode="ReadOnly" />
                <br />
                Swimming:
                <asp:DynamicControl runat="server" DataField="Swimming" Mode="ReadOnly" />
                <br />
                Speci_Animal_Inv:
                <asp:DynamicControl runat="server" DataField="Speci_Animal_Inv" Mode="ReadOnly" />
                <br />
                FishID:
                <asp:DynamicControl runat="server" DataField="FishID" Mode="ReadOnly" />
                <br />
                NavBirdsID:
                <asp:DynamicControl runat="server" DataField="NavBirdsID" Mode="ReadOnly" />
                <br />
                NavMamID:
                <asp:DynamicControl runat="server" DataField="NavMamID" Mode="ReadOnly" />
                <br />
                NavRepID:
                <asp:DynamicControl runat="server" DataField="NavRepID" Mode="ReadOnly" />
                <br />
                NavAmp:
                <asp:DynamicControl runat="server" DataField="NavAmp" Mode="ReadOnly" />
                <br />
                AirBoat:
                <asp:DynamicControl runat="server" DataField="AirBoat" Mode="ReadOnly" />
                <br />
                OutBrdMotors:
                <asp:DynamicControl runat="server" DataField="OutBrdMotors" Mode="ReadOnly" />
                <br />
                FourWheelDri:
                <asp:DynamicControl runat="server" DataField="FourWheelDri" Mode="ReadOnly" />
                <br />
                FarmTrac:
                <asp:DynamicControl runat="server" DataField="FarmTrac" Mode="ReadOnly" />
                <br />
                Bulldozer:
                <asp:DynamicControl runat="server" DataField="Bulldozer" Mode="ReadOnly" />
                <br />
                ChainSaw:
                <asp:DynamicControl runat="server" DataField="ChainSaw" Mode="ReadOnly" />
                <br />
                Rifle:
                <asp:DynamicControl runat="server" DataField="Rifle" Mode="ReadOnly" />
                <br />
                Pistol:
                <asp:DynamicControl runat="server" DataField="Pistol" Mode="ReadOnly" />
                <br />
                Shotgun:
                <asp:DynamicControl runat="server" DataField="Shotgun" Mode="ReadOnly" />
                <br />
                Spec_Com_Train:
                <asp:DynamicControl runat="server" DataField="Spec_Com_Train" Mode="ReadOnly" />
                <br />
                SpecialAnimalInventoryEssay:
                <asp:DynamicControl runat="server" DataField="SpecialAnimalInventoryEssay" Mode="ReadOnly" />
                <br />
                NativeAmphibiansEssay:
                <asp:DynamicControl runat="server" DataField="NativeAmphibiansEssay" Mode="ReadOnly" />
                <br />
                SpecialComputerTrainingEssay:
                <asp:DynamicControl runat="server" DataField="SpecialComputerTrainingEssay" Mode="ReadOnly" />
                <br />
                GroupMem:
                <asp:DynamicControl runat="server" DataField="GroupMem" Mode="ReadOnly" />
                <br />
                Outdoor_Exp:
                <asp:DynamicControl runat="server" DataField="Outdoor_Exp" Mode="ReadOnly" />
                <br />
                Responsible_Pos:
                <asp:DynamicControl runat="server" DataField="Responsible_Pos" Mode="ReadOnly" />
                <br />
                Manage_Exp:
                <asp:DynamicControl runat="server" DataField="Manage_Exp" Mode="ReadOnly" />
                <br />
                Convictied:
                <asp:DynamicControl runat="server" DataField="Convictied" Mode="ReadOnly" />
                <br />
                Explain_Charge:
                <asp:DynamicControl runat="server" DataField="Explain_Charge" Mode="ReadOnly" />
                <br />
                Traffic_Cit:
                <asp:DynamicControl runat="server" DataField="Traffic_Cit" Mode="ReadOnly" />
                <br />
                OthStateEmploy:
                <asp:DynamicControl runat="server" DataField="OthStateEmploy" Mode="ReadOnly" />
                <br />
                Reason_WLO:
                <asp:DynamicControl runat="server" DataField="Reason_WLO" Mode="ReadOnly" />
                <br />
                Events_ForDecision:
                <asp:DynamicControl runat="server" DataField="Events_ForDecision" Mode="ReadOnly" />
                <br />
                CourseofAction:
                <asp:DynamicControl runat="server" DataField="CourseofAction" Mode="ReadOnly" />
                <br />
                Qualities:
                <asp:DynamicControl runat="server" DataField="Qualities" Mode="ReadOnly" />
                <br />
                MajorConernforWL:
                <asp:DynamicControl runat="server" DataField="MajorConernforWL" Mode="ReadOnly" />
                <br />
                Address:
                <asp:DynamicControl runat="server" DataField="Address" Mode="ReadOnly" />
                <br />
                HomePhone:
                <asp:DynamicControl runat="server" DataField="HomePhone" Mode="ReadOnly" />
                <br />
                WorkPhone:
                <asp:DynamicControl runat="server" DataField="WorkPhone" Mode="ReadOnly" />
                <br />
                EmpConNo:
                <asp:DynamicControl runat="server" DataField="EmpConNo" Mode="ReadOnly" />
                <br />
                AlternateNo:
                <asp:DynamicControl runat="server" DataField="AlternateNo" Mode="ReadOnly" />
                <br />
                Residence_FromDate1:
                <asp:DynamicControl runat="server" DataField="Residence_FromDate1" Mode="ReadOnly" />
                <br />
                Residence_ToDate1:
                <asp:DynamicControl runat="server" DataField="Residence_ToDate1" Mode="ReadOnly" />
                <br />
                ResidenceAddress1:
                <asp:DynamicControl runat="server" DataField="ResidenceAddress1" Mode="ReadOnly" />
                <br />
                CreatedDate:
                <asp:DynamicControl runat="server" DataField="CreatedDate" Mode="ReadOnly" />
                <br />
                Yr_TN_Residence_YrMon:
                <asp:DynamicControl runat="server" DataField="Yr_TN_Residence_YrMon" Mode="ReadOnly" />
                <br />
                Residence_FromDate2:
                <asp:DynamicControl runat="server" DataField="Residence_FromDate2" Mode="ReadOnly" />
                <br />
                Residence_ToDate2:
                <asp:DynamicControl runat="server" DataField="Residence_ToDate2" Mode="ReadOnly" />
                <br />
                Residence_FromDate3:
                <asp:DynamicControl runat="server" DataField="Residence_FromDate3" Mode="ReadOnly" />
                <br />
                Residence_ToDate3:
                <asp:DynamicControl runat="server" DataField="Residence_ToDate3" Mode="ReadOnly" />
                <br />
                ResidenceAddress2:
                <asp:DynamicControl runat="server" DataField="ResidenceAddress2" Mode="ReadOnly" />
                <br />
                ResidenceAddress3:
                <asp:DynamicControl runat="server" DataField="ResidenceAddress3" Mode="ReadOnly" />
                <br />
                Country:
                <asp:DynamicControl runat="server" DataField="Country" Mode="ReadOnly" />
                <br />
                State:
                <asp:DynamicControl runat="server" DataField="State" Mode="ReadOnly" />
                <br />
                Zipcode:
                <asp:DynamicControl runat="server" DataField="Zipcode" Mode="ReadOnly" />
                <br />
                City:
                <asp:DynamicControl runat="server" DataField="City" Mode="ReadOnly" />
                <br />
            </td>
        </AlternatingItemTemplate>
        <EditItemTemplate>
            <td runat="server" style="">ID:
                <asp:DynamicControl runat="server" DataField="ID" Mode="ReadOnly" />
                <br />
                UserName:
                <asp:DynamicControl runat="server" DataField="UserName" Mode="Edit" />
                <br />
                Password:
                <asp:DynamicControl runat="server" DataField="Password" Mode="Edit" />
                <br />
                Email:
                <asp:DynamicControl runat="server" DataField="Email" Mode="Edit" />
                <br />
                Fname:
                <asp:DynamicControl runat="server" DataField="Fname" Mode="Edit" />
                <br />
                MiddleInt:
                <asp:DynamicControl runat="server" DataField="MiddleInt" Mode="Edit" />
                <br />
                Lname:
                <asp:DynamicControl runat="server" DataField="Lname" Mode="Edit" />
                <br />
                InterviewedbyTWRA:
                <asp:DynamicControl runat="server" DataField="InterviewedbyTWRA" Mode="Edit" />
                <br />
                When_Interviewed:
                <asp:DynamicControl runat="server" DataField="When_Interviewed" Mode="Edit" />
                <br />
                Relatives:
                <asp:DynamicControl runat="server" DataField="Relatives" Mode="Edit" />
                <br />
                Explain_Relatives:
                <asp:DynamicControl runat="server" DataField="Explain_Relatives" Mode="Edit" />
                <br />
                YrTNResidence:
                <asp:DynamicControl runat="server" DataField="YrTNResidence" Mode="Edit" />
                <br />
                List_Residence:
                <asp:DynamicControl runat="server" DataField="List_Residence" Mode="Edit" />
                <br />
                FishingHuntingTypeNo:
                <asp:DynamicControl runat="server" DataField="FishingHuntingTypeNo" Mode="Edit" />
                <br />
                Enforcement_Exper:
                <asp:DynamicControl runat="server" DataField="Enforcement_Exper" Mode="Edit" />
                <br />
                Explain_Enforcement:
                <asp:DynamicControl runat="server" DataField="Explain_Enforcement" Mode="Edit" />
                <br />
                Enforcement_Train:
                <asp:DynamicControl runat="server" DataField="Enforcement_Train" Mode="Edit" />
                <br />
                Explain_EnforcementTrain:
                <asp:DynamicControl runat="server" DataField="Explain_EnforcementTrain" Mode="Edit" />
                <br />
                Advanced_Courses:
                <asp:DynamicControl runat="server" DataField="Advanced_Courses" Mode="Edit" />
                <br />
                Benefical_Courses:
                <asp:DynamicControl runat="server" DataField="Benefical_Courses" Mode="Edit" />
                <br />
                Hobbies:
                <asp:DynamicControl runat="server" DataField="Hobbies" Mode="Edit" />
                <br />
                Farming:
                <asp:DynamicControl runat="server" DataField="Farming" Mode="Edit" />
                <br />
                Forestry:
                <asp:DynamicControl runat="server" DataField="Forestry" Mode="Edit" />
                <br />
                AnimalHusb:
                <asp:DynamicControl runat="server" DataField="AnimalHusb" Mode="Edit" />
                <br />
                Comm_Fish:
                <asp:DynamicControl runat="server" DataField="Comm_Fish" Mode="Edit" />
                <br />
                Sm_Gas_Eng:
                <asp:DynamicControl runat="server" DataField="Sm_Gas_Eng" Mode="Edit" />
                <br />
                AutoRepair:
                <asp:DynamicControl runat="server" DataField="AutoRepair" Mode="Edit" />
                <br />
                BoatOper:
                <asp:DynamicControl runat="server" DataField="BoatOper" Mode="Edit" />
                <br />
                Hunting:
                <asp:DynamicControl runat="server" DataField="Hunting" Mode="Edit" />
                <br />
                Fishing:
                <asp:DynamicControl runat="server" DataField="Fishing" Mode="Edit" />
                <br />
                Camping:
                <asp:DynamicControl runat="server" DataField="Camping" Mode="Edit" />
                <br />
                Swimming:
                <asp:DynamicControl runat="server" DataField="Swimming" Mode="Edit" />
                <br />
                Speci_Animal_Inv:
                <asp:DynamicControl runat="server" DataField="Speci_Animal_Inv" Mode="Edit" />
                <br />
                FishID:
                <asp:DynamicControl runat="server" DataField="FishID" Mode="Edit" />
                <br />
                NavBirdsID:
                <asp:DynamicControl runat="server" DataField="NavBirdsID" Mode="Edit" />
                <br />
                NavMamID:
                <asp:DynamicControl runat="server" DataField="NavMamID" Mode="Edit" />
                <br />
                NavRepID:
                <asp:DynamicControl runat="server" DataField="NavRepID" Mode="Edit" />
                <br />
                NavAmp:
                <asp:DynamicControl runat="server" DataField="NavAmp" Mode="Edit" />
                <br />
                AirBoat:
                <asp:DynamicControl runat="server" DataField="AirBoat" Mode="Edit" />
                <br />
                OutBrdMotors:
                <asp:DynamicControl runat="server" DataField="OutBrdMotors" Mode="Edit" />
                <br />
                FourWheelDri:
                <asp:DynamicControl runat="server" DataField="FourWheelDri" Mode="Edit" />
                <br />
                FarmTrac:
                <asp:DynamicControl runat="server" DataField="FarmTrac" Mode="Edit" />
                <br />
                Bulldozer:
                <asp:DynamicControl runat="server" DataField="Bulldozer" Mode="Edit" />
                <br />
                ChainSaw:
                <asp:DynamicControl runat="server" DataField="ChainSaw" Mode="Edit" />
                <br />
                Rifle:
                <asp:DynamicControl runat="server" DataField="Rifle" Mode="Edit" />
                <br />
                Pistol:
                <asp:DynamicControl runat="server" DataField="Pistol" Mode="Edit" />
                <br />
                Shotgun:
                <asp:DynamicControl runat="server" DataField="Shotgun" Mode="Edit" />
                <br />
                Spec_Com_Train:
                <asp:DynamicControl runat="server" DataField="Spec_Com_Train" Mode="Edit" />
                <br />
                SpecialAnimalInventoryEssay:
                <asp:DynamicControl runat="server" DataField="SpecialAnimalInventoryEssay" Mode="Edit" />
                <br />
                NativeAmphibiansEssay:
                <asp:DynamicControl runat="server" DataField="NativeAmphibiansEssay" Mode="Edit" />
                <br />
                SpecialComputerTrainingEssay:
                <asp:DynamicControl runat="server" DataField="SpecialComputerTrainingEssay" Mode="Edit" />
                <br />
                GroupMem:
                <asp:DynamicControl runat="server" DataField="GroupMem" Mode="Edit" />
                <br />
                Outdoor_Exp:
                <asp:DynamicControl runat="server" DataField="Outdoor_Exp" Mode="Edit" />
                <br />
                Responsible_Pos:
                <asp:DynamicControl runat="server" DataField="Responsible_Pos" Mode="Edit" />
                <br />
                Manage_Exp:
                <asp:DynamicControl runat="server" DataField="Manage_Exp" Mode="Edit" />
                <br />
                Convictied:
                <asp:DynamicControl runat="server" DataField="Convictied" Mode="Edit" />
                <br />
                Explain_Charge:
                <asp:DynamicControl runat="server" DataField="Explain_Charge" Mode="Edit" />
                <br />
                Traffic_Cit:
                <asp:DynamicControl runat="server" DataField="Traffic_Cit" Mode="Edit" />
                <br />
                OthStateEmploy:
                <asp:DynamicControl runat="server" DataField="OthStateEmploy" Mode="Edit" />
                <br />
                Reason_WLO:
                <asp:DynamicControl runat="server" DataField="Reason_WLO" Mode="Edit" />
                <br />
                Events_ForDecision:
                <asp:DynamicControl runat="server" DataField="Events_ForDecision" Mode="Edit" />
                <br />
                CourseofAction:
                <asp:DynamicControl runat="server" DataField="CourseofAction" Mode="Edit" />
                <br />
                Qualities:
                <asp:DynamicControl runat="server" DataField="Qualities" Mode="Edit" />
                <br />
                MajorConernforWL:
                <asp:DynamicControl runat="server" DataField="MajorConernforWL" Mode="Edit" />
                <br />
                Address:
                <asp:DynamicControl runat="server" DataField="Address" Mode="Edit" />
                <br />
                HomePhone:
                <asp:DynamicControl runat="server" DataField="HomePhone" Mode="Edit" />
                <br />
                WorkPhone:
                <asp:DynamicControl runat="server" DataField="WorkPhone" Mode="Edit" />
                <br />
                EmpConNo:
                <asp:DynamicControl runat="server" DataField="EmpConNo" Mode="Edit" />
                <br />
                AlternateNo:
                <asp:DynamicControl runat="server" DataField="AlternateNo" Mode="Edit" />
                <br />
                Residence_FromDate1:
                <asp:DynamicControl runat="server" DataField="Residence_FromDate1" Mode="Edit" />
                <br />
                Residence_ToDate1:
                <asp:DynamicControl runat="server" DataField="Residence_ToDate1" Mode="Edit" />
                <br />
                ResidenceAddress1:
                <asp:DynamicControl runat="server" DataField="ResidenceAddress1" Mode="Edit" />
                <br />
                CreatedDate:
                <asp:DynamicControl runat="server" DataField="CreatedDate" Mode="Edit" />
                <br />
                Yr_TN_Residence_YrMon:
                <asp:DynamicControl runat="server" DataField="Yr_TN_Residence_YrMon" Mode="Edit" />
                <br />
                Residence_FromDate2:
                <asp:DynamicControl runat="server" DataField="Residence_FromDate2" Mode="Edit" />
                <br />
                Residence_ToDate2:
                <asp:DynamicControl runat="server" DataField="Residence_ToDate2" Mode="Edit" />
                <br />
                Residence_FromDate3:
                <asp:DynamicControl runat="server" DataField="Residence_FromDate3" Mode="Edit" />
                <br />
                Residence_ToDate3:
                <asp:DynamicControl runat="server" DataField="Residence_ToDate3" Mode="Edit" />
                <br />
                ResidenceAddress2:
                <asp:DynamicControl runat="server" DataField="ResidenceAddress2" Mode="Edit" />
                <br />
                ResidenceAddress3:
                <asp:DynamicControl runat="server" DataField="ResidenceAddress3" Mode="Edit" />
                <br />
                Country:
                <asp:DynamicControl runat="server" DataField="Country" Mode="Edit" />
                <br />
                State:
                <asp:DynamicControl runat="server" DataField="State" Mode="Edit" />
                <br />
                Zipcode:
                <asp:DynamicControl runat="server" DataField="Zipcode" Mode="Edit" />
                <br />
                City:
                <asp:DynamicControl runat="server" DataField="City" Mode="Edit" />
                <br />
                <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="Update" />
                <br />
                <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" />
                <br />
            </td>
        </EditItemTemplate>
        <EmptyDataTemplate>
            <table runat="server" style="">
                <tr>
                    <td>No data was returned.</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <EmptyItemTemplate>
            <td runat="server" />
        </EmptyItemTemplate>
        <GroupTemplate>
            <tr id="itemPlaceholderContainer" runat="server">
                <td id="itemPlaceholder" runat="server"></td>
            </tr>
        </GroupTemplate>
        <InsertItemTemplate>
            <td runat="server" style="">UserName:
                <asp:DynamicControl runat="server" DataField="UserName" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Password:
                <asp:DynamicControl runat="server" DataField="Password" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Email:
                <asp:DynamicControl runat="server" DataField="Email" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Fname:
                <asp:DynamicControl runat="server" DataField="Fname" Mode="Insert" ValidationGroup="Insert" />
                <br />
                MiddleInt:
                <asp:DynamicControl runat="server" DataField="MiddleInt" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Lname:
                <asp:DynamicControl runat="server" DataField="Lname" Mode="Insert" ValidationGroup="Insert" />
                <br />
                InterviewedbyTWRA:
                <asp:DynamicControl runat="server" DataField="InterviewedbyTWRA" Mode="Insert" ValidationGroup="Insert" />
                <br />
                When_Interviewed:
                <asp:DynamicControl runat="server" DataField="When_Interviewed" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Relatives:
                <asp:DynamicControl runat="server" DataField="Relatives" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Explain_Relatives:
                <asp:DynamicControl runat="server" DataField="Explain_Relatives" Mode="Insert" ValidationGroup="Insert" />
                <br />
                YrTNResidence:
                <asp:DynamicControl runat="server" DataField="YrTNResidence" Mode="Insert" ValidationGroup="Insert" />
                <br />
                List_Residence:
                <asp:DynamicControl runat="server" DataField="List_Residence" Mode="Insert" ValidationGroup="Insert" />
                <br />
                FishingHuntingTypeNo:
                <asp:DynamicControl runat="server" DataField="FishingHuntingTypeNo" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Enforcement_Exper:
                <asp:DynamicControl runat="server" DataField="Enforcement_Exper" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Explain_Enforcement:
                <asp:DynamicControl runat="server" DataField="Explain_Enforcement" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Enforcement_Train:
                <asp:DynamicControl runat="server" DataField="Enforcement_Train" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Explain_EnforcementTrain:
                <asp:DynamicControl runat="server" DataField="Explain_EnforcementTrain" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Advanced_Courses:
                <asp:DynamicControl runat="server" DataField="Advanced_Courses" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Benefical_Courses:
                <asp:DynamicControl runat="server" DataField="Benefical_Courses" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Hobbies:
                <asp:DynamicControl runat="server" DataField="Hobbies" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Farming:
                <asp:DynamicControl runat="server" DataField="Farming" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Forestry:
                <asp:DynamicControl runat="server" DataField="Forestry" Mode="Insert" ValidationGroup="Insert" />
                <br />
                AnimalHusb:
                <asp:DynamicControl runat="server" DataField="AnimalHusb" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Comm_Fish:
                <asp:DynamicControl runat="server" DataField="Comm_Fish" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Sm_Gas_Eng:
                <asp:DynamicControl runat="server" DataField="Sm_Gas_Eng" Mode="Insert" ValidationGroup="Insert" />
                <br />
                AutoRepair:
                <asp:DynamicControl runat="server" DataField="AutoRepair" Mode="Insert" ValidationGroup="Insert" />
                <br />
                BoatOper:
                <asp:DynamicControl runat="server" DataField="BoatOper" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Hunting:
                <asp:DynamicControl runat="server" DataField="Hunting" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Fishing:
                <asp:DynamicControl runat="server" DataField="Fishing" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Camping:
                <asp:DynamicControl runat="server" DataField="Camping" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Swimming:
                <asp:DynamicControl runat="server" DataField="Swimming" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Speci_Animal_Inv:
                <asp:DynamicControl runat="server" DataField="Speci_Animal_Inv" Mode="Insert" ValidationGroup="Insert" />
                <br />
                FishID:
                <asp:DynamicControl runat="server" DataField="FishID" Mode="Insert" ValidationGroup="Insert" />
                <br />
                NavBirdsID:
                <asp:DynamicControl runat="server" DataField="NavBirdsID" Mode="Insert" ValidationGroup="Insert" />
                <br />
                NavMamID:
                <asp:DynamicControl runat="server" DataField="NavMamID" Mode="Insert" ValidationGroup="Insert" />
                <br />
                NavRepID:
                <asp:DynamicControl runat="server" DataField="NavRepID" Mode="Insert" ValidationGroup="Insert" />
                <br />
                NavAmp:
                <asp:DynamicControl runat="server" DataField="NavAmp" Mode="Insert" ValidationGroup="Insert" />
                <br />
                AirBoat:
                <asp:DynamicControl runat="server" DataField="AirBoat" Mode="Insert" ValidationGroup="Insert" />
                <br />
                OutBrdMotors:
                <asp:DynamicControl runat="server" DataField="OutBrdMotors" Mode="Insert" ValidationGroup="Insert" />
                <br />
                FourWheelDri:
                <asp:DynamicControl runat="server" DataField="FourWheelDri" Mode="Insert" ValidationGroup="Insert" />
                <br />
                FarmTrac:
                <asp:DynamicControl runat="server" DataField="FarmTrac" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Bulldozer:
                <asp:DynamicControl runat="server" DataField="Bulldozer" Mode="Insert" ValidationGroup="Insert" />
                <br />
                ChainSaw:
                <asp:DynamicControl runat="server" DataField="ChainSaw" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Rifle:
                <asp:DynamicControl runat="server" DataField="Rifle" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Pistol:
                <asp:DynamicControl runat="server" DataField="Pistol" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Shotgun:
                <asp:DynamicControl runat="server" DataField="Shotgun" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Spec_Com_Train:
                <asp:DynamicControl runat="server" DataField="Spec_Com_Train" Mode="Insert" ValidationGroup="Insert" />
                <br />
                SpecialAnimalInventoryEssay:
                <asp:DynamicControl runat="server" DataField="SpecialAnimalInventoryEssay" Mode="Insert" ValidationGroup="Insert" />
                <br />
                NativeAmphibiansEssay:
                <asp:DynamicControl runat="server" DataField="NativeAmphibiansEssay" Mode="Insert" ValidationGroup="Insert" />
                <br />
                SpecialComputerTrainingEssay:
                <asp:DynamicControl runat="server" DataField="SpecialComputerTrainingEssay" Mode="Insert" ValidationGroup="Insert" />
                <br />
                GroupMem:
                <asp:DynamicControl runat="server" DataField="GroupMem" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Outdoor_Exp:
                <asp:DynamicControl runat="server" DataField="Outdoor_Exp" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Responsible_Pos:
                <asp:DynamicControl runat="server" DataField="Responsible_Pos" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Manage_Exp:
                <asp:DynamicControl runat="server" DataField="Manage_Exp" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Convictied:
                <asp:DynamicControl runat="server" DataField="Convictied" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Explain_Charge:
                <asp:DynamicControl runat="server" DataField="Explain_Charge" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Traffic_Cit:
                <asp:DynamicControl runat="server" DataField="Traffic_Cit" Mode="Insert" ValidationGroup="Insert" />
                <br />
                OthStateEmploy:
                <asp:DynamicControl runat="server" DataField="OthStateEmploy" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Reason_WLO:
                <asp:DynamicControl runat="server" DataField="Reason_WLO" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Events_ForDecision:
                <asp:DynamicControl runat="server" DataField="Events_ForDecision" Mode="Insert" ValidationGroup="Insert" />
                <br />
                CourseofAction:
                <asp:DynamicControl runat="server" DataField="CourseofAction" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Qualities:
                <asp:DynamicControl runat="server" DataField="Qualities" Mode="Insert" ValidationGroup="Insert" />
                <br />
                MajorConernforWL:
                <asp:DynamicControl runat="server" DataField="MajorConernforWL" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Address:
                <asp:DynamicControl runat="server" DataField="Address" Mode="Insert" ValidationGroup="Insert" />
                <br />
                HomePhone:
                <asp:DynamicControl runat="server" DataField="HomePhone" Mode="Insert" ValidationGroup="Insert" />
                <br />
                WorkPhone:
                <asp:DynamicControl runat="server" DataField="WorkPhone" Mode="Insert" ValidationGroup="Insert" />
                <br />
                EmpConNo:
                <asp:DynamicControl runat="server" DataField="EmpConNo" Mode="Insert" ValidationGroup="Insert" />
                <br />
                AlternateNo:
                <asp:DynamicControl runat="server" DataField="AlternateNo" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Residence_FromDate1:
                <asp:DynamicControl runat="server" DataField="Residence_FromDate1" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Residence_ToDate1:
                <asp:DynamicControl runat="server" DataField="Residence_ToDate1" Mode="Insert" ValidationGroup="Insert" />
                <br />
                ResidenceAddress1:
                <asp:DynamicControl runat="server" DataField="ResidenceAddress1" Mode="Insert" ValidationGroup="Insert" />
                <br />
                CreatedDate:
                <asp:DynamicControl runat="server" DataField="CreatedDate" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Yr_TN_Residence_YrMon:
                <asp:DynamicControl runat="server" DataField="Yr_TN_Residence_YrMon" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Residence_FromDate2:
                <asp:DynamicControl runat="server" DataField="Residence_FromDate2" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Residence_ToDate2:
                <asp:DynamicControl runat="server" DataField="Residence_ToDate2" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Residence_FromDate3:
                <asp:DynamicControl runat="server" DataField="Residence_FromDate3" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Residence_ToDate3:
                <asp:DynamicControl runat="server" DataField="Residence_ToDate3" Mode="Insert" ValidationGroup="Insert" />
                <br />
                ResidenceAddress2:
                <asp:DynamicControl runat="server" DataField="ResidenceAddress2" Mode="Insert" ValidationGroup="Insert" />
                <br />
                ResidenceAddress3:
                <asp:DynamicControl runat="server" DataField="ResidenceAddress3" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Country:
                <asp:DynamicControl runat="server" DataField="Country" Mode="Insert" ValidationGroup="Insert" />
                <br />
                State:
                <asp:DynamicControl runat="server" DataField="State" Mode="Insert" ValidationGroup="Insert" />
                <br />
                Zipcode:
                <asp:DynamicControl runat="server" DataField="Zipcode" Mode="Insert" ValidationGroup="Insert" />
                <br />
                City:
                <asp:DynamicControl runat="server" DataField="City" Mode="Insert" ValidationGroup="Insert" />
                <br />
                <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="Insert" ValidationGroup="Insert" />
                <br />
                <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Clear" />
                <br />
            </td>
        </InsertItemTemplate>
        <ItemTemplate>
            <td runat="server" style="">ID:
                <asp:DynamicControl runat="server" DataField="ID" Mode="ReadOnly" />
                <br />
                UserName:
                <asp:DynamicControl runat="server" DataField="UserName" Mode="ReadOnly" />
                <br />
                Password:
                <asp:DynamicControl runat="server" DataField="Password" Mode="ReadOnly" />
                <br />
                Email:
                <asp:DynamicControl runat="server" DataField="Email" Mode="ReadOnly" />
                <br />
                Fname:
                <asp:DynamicControl runat="server" DataField="Fname" Mode="ReadOnly" />
                <br />
                MiddleInt:
                <asp:DynamicControl runat="server" DataField="MiddleInt" Mode="ReadOnly" />
                <br />
                Lname:
                <asp:DynamicControl runat="server" DataField="Lname" Mode="ReadOnly" />
                <br />
                InterviewedbyTWRA:
                <asp:DynamicControl runat="server" DataField="InterviewedbyTWRA" Mode="ReadOnly" />
                <br />
                When_Interviewed:
                <asp:DynamicControl runat="server" DataField="When_Interviewed" Mode="ReadOnly" />
                <br />
                Relatives:
                <asp:DynamicControl runat="server" DataField="Relatives" Mode="ReadOnly" />
                <br />
                Explain_Relatives:
                <asp:DynamicControl runat="server" DataField="Explain_Relatives" Mode="ReadOnly" />
                <br />
                YrTNResidence:
                <asp:DynamicControl runat="server" DataField="YrTNResidence" Mode="ReadOnly" />
                <br />
                List_Residence:
                <asp:DynamicControl runat="server" DataField="List_Residence" Mode="ReadOnly" />
                <br />
                FishingHuntingTypeNo:
                <asp:DynamicControl runat="server" DataField="FishingHuntingTypeNo" Mode="ReadOnly" />
                <br />
                Enforcement_Exper:
                <asp:DynamicControl runat="server" DataField="Enforcement_Exper" Mode="ReadOnly" />
                <br />
                Explain_Enforcement:
                <asp:DynamicControl runat="server" DataField="Explain_Enforcement" Mode="ReadOnly" />
                <br />
                Enforcement_Train:
                <asp:DynamicControl runat="server" DataField="Enforcement_Train" Mode="ReadOnly" />
                <br />
                Explain_EnforcementTrain:
                <asp:DynamicControl runat="server" DataField="Explain_EnforcementTrain" Mode="ReadOnly" />
                <br />
                Advanced_Courses:
                <asp:DynamicControl runat="server" DataField="Advanced_Courses" Mode="ReadOnly" />
                <br />
                Benefical_Courses:
                <asp:DynamicControl runat="server" DataField="Benefical_Courses" Mode="ReadOnly" />
                <br />
                Hobbies:
                <asp:DynamicControl runat="server" DataField="Hobbies" Mode="ReadOnly" />
                <br />
                Farming:
                <asp:DynamicControl runat="server" DataField="Farming" Mode="ReadOnly" />
                <br />
                Forestry:
                <asp:DynamicControl runat="server" DataField="Forestry" Mode="ReadOnly" />
                <br />
                AnimalHusb:
                <asp:DynamicControl runat="server" DataField="AnimalHusb" Mode="ReadOnly" />
                <br />
                Comm_Fish:
                <asp:DynamicControl runat="server" DataField="Comm_Fish" Mode="ReadOnly" />
                <br />
                Sm_Gas_Eng:
                <asp:DynamicControl runat="server" DataField="Sm_Gas_Eng" Mode="ReadOnly" />
                <br />
                AutoRepair:
                <asp:DynamicControl runat="server" DataField="AutoRepair" Mode="ReadOnly" />
                <br />
                BoatOper:
                <asp:DynamicControl runat="server" DataField="BoatOper" Mode="ReadOnly" />
                <br />
                Hunting:
                <asp:DynamicControl runat="server" DataField="Hunting" Mode="ReadOnly" />
                <br />
                Fishing:
                <asp:DynamicControl runat="server" DataField="Fishing" Mode="ReadOnly" />
                <br />
                Camping:
                <asp:DynamicControl runat="server" DataField="Camping" Mode="ReadOnly" />
                <br />
                Swimming:
                <asp:DynamicControl runat="server" DataField="Swimming" Mode="ReadOnly" />
                <br />
                Speci_Animal_Inv:
                <asp:DynamicControl runat="server" DataField="Speci_Animal_Inv" Mode="ReadOnly" />
                <br />
                FishID:
                <asp:DynamicControl runat="server" DataField="FishID" Mode="ReadOnly" />
                <br />
                NavBirdsID:
                <asp:DynamicControl runat="server" DataField="NavBirdsID" Mode="ReadOnly" />
                <br />
                NavMamID:
                <asp:DynamicControl runat="server" DataField="NavMamID" Mode="ReadOnly" />
                <br />
                NavRepID:
                <asp:DynamicControl runat="server" DataField="NavRepID" Mode="ReadOnly" />
                <br />
                NavAmp:
                <asp:DynamicControl runat="server" DataField="NavAmp" Mode="ReadOnly" />
                <br />
                AirBoat:
                <asp:DynamicControl runat="server" DataField="AirBoat" Mode="ReadOnly" />
                <br />
                OutBrdMotors:
                <asp:DynamicControl runat="server" DataField="OutBrdMotors" Mode="ReadOnly" />
                <br />
                FourWheelDri:
                <asp:DynamicControl runat="server" DataField="FourWheelDri" Mode="ReadOnly" />
                <br />
                FarmTrac:
                <asp:DynamicControl runat="server" DataField="FarmTrac" Mode="ReadOnly" />
                <br />
                Bulldozer:
                <asp:DynamicControl runat="server" DataField="Bulldozer" Mode="ReadOnly" />
                <br />
                ChainSaw:
                <asp:DynamicControl runat="server" DataField="ChainSaw" Mode="ReadOnly" />
                <br />
                Rifle:
                <asp:DynamicControl runat="server" DataField="Rifle" Mode="ReadOnly" />
                <br />
                Pistol:
                <asp:DynamicControl runat="server" DataField="Pistol" Mode="ReadOnly" />
                <br />
                Shotgun:
                <asp:DynamicControl runat="server" DataField="Shotgun" Mode="ReadOnly" />
                <br />
                Spec_Com_Train:
                <asp:DynamicControl runat="server" DataField="Spec_Com_Train" Mode="ReadOnly" />
                <br />
                SpecialAnimalInventoryEssay:
                <asp:DynamicControl runat="server" DataField="SpecialAnimalInventoryEssay" Mode="ReadOnly" />
                <br />
                NativeAmphibiansEssay:
                <asp:DynamicControl runat="server" DataField="NativeAmphibiansEssay" Mode="ReadOnly" />
                <br />
                SpecialComputerTrainingEssay:
                <asp:DynamicControl runat="server" DataField="SpecialComputerTrainingEssay" Mode="ReadOnly" />
                <br />
                GroupMem:
                <asp:DynamicControl runat="server" DataField="GroupMem" Mode="ReadOnly" />
                <br />
                Outdoor_Exp:
                <asp:DynamicControl runat="server" DataField="Outdoor_Exp" Mode="ReadOnly" />
                <br />
                Responsible_Pos:
                <asp:DynamicControl runat="server" DataField="Responsible_Pos" Mode="ReadOnly" />
                <br />
                Manage_Exp:
                <asp:DynamicControl runat="server" DataField="Manage_Exp" Mode="ReadOnly" />
                <br />
                Convictied:
                <asp:DynamicControl runat="server" DataField="Convictied" Mode="ReadOnly" />
                <br />
                Explain_Charge:
                <asp:DynamicControl runat="server" DataField="Explain_Charge" Mode="ReadOnly" />
                <br />
                Traffic_Cit:
                <asp:DynamicControl runat="server" DataField="Traffic_Cit" Mode="ReadOnly" />
                <br />
                OthStateEmploy:
                <asp:DynamicControl runat="server" DataField="OthStateEmploy" Mode="ReadOnly" />
                <br />
                Reason_WLO:
                <asp:DynamicControl runat="server" DataField="Reason_WLO" Mode="ReadOnly" />
                <br />
                Events_ForDecision:
                <asp:DynamicControl runat="server" DataField="Events_ForDecision" Mode="ReadOnly" />
                <br />
                CourseofAction:
                <asp:DynamicControl runat="server" DataField="CourseofAction" Mode="ReadOnly" />
                <br />
                Qualities:
                <asp:DynamicControl runat="server" DataField="Qualities" Mode="ReadOnly" />
                <br />
                MajorConernforWL:
                <asp:DynamicControl runat="server" DataField="MajorConernforWL" Mode="ReadOnly" />
                <br />
                Address:
                <asp:DynamicControl runat="server" DataField="Address" Mode="ReadOnly" />
                <br />
                HomePhone:
                <asp:DynamicControl runat="server" DataField="HomePhone" Mode="ReadOnly" />
                <br />
                WorkPhone:
                <asp:DynamicControl runat="server" DataField="WorkPhone" Mode="ReadOnly" />
                <br />
                EmpConNo:
                <asp:DynamicControl runat="server" DataField="EmpConNo" Mode="ReadOnly" />
                <br />
                AlternateNo:
                <asp:DynamicControl runat="server" DataField="AlternateNo" Mode="ReadOnly" />
                <br />
                Residence_FromDate1:
                <asp:DynamicControl runat="server" DataField="Residence_FromDate1" Mode="ReadOnly" />
                <br />
                Residence_ToDate1:
                <asp:DynamicControl runat="server" DataField="Residence_ToDate1" Mode="ReadOnly" />
                <br />
                ResidenceAddress1:
                <asp:DynamicControl runat="server" DataField="ResidenceAddress1" Mode="ReadOnly" />
                <br />
                CreatedDate:
                <asp:DynamicControl runat="server" DataField="CreatedDate" Mode="ReadOnly" />
                <br />
                Yr_TN_Residence_YrMon:
                <asp:DynamicControl runat="server" DataField="Yr_TN_Residence_YrMon" Mode="ReadOnly" />
                <br />
                Residence_FromDate2:
                <asp:DynamicControl runat="server" DataField="Residence_FromDate2" Mode="ReadOnly" />
                <br />
                Residence_ToDate2:
                <asp:DynamicControl runat="server" DataField="Residence_ToDate2" Mode="ReadOnly" />
                <br />
                Residence_FromDate3:
                <asp:DynamicControl runat="server" DataField="Residence_FromDate3" Mode="ReadOnly" />
                <br />
                Residence_ToDate3:
                <asp:DynamicControl runat="server" DataField="Residence_ToDate3" Mode="ReadOnly" />
                <br />
                ResidenceAddress2:
                <asp:DynamicControl runat="server" DataField="ResidenceAddress2" Mode="ReadOnly" />
                <br />
                ResidenceAddress3:
                <asp:DynamicControl runat="server" DataField="ResidenceAddress3" Mode="ReadOnly" />
                <br />
                Country:
                <asp:DynamicControl runat="server" DataField="Country" Mode="ReadOnly" />
                <br />
                State:
                <asp:DynamicControl runat="server" DataField="State" Mode="ReadOnly" />
                <br />
                Zipcode:
                <asp:DynamicControl runat="server" DataField="Zipcode" Mode="ReadOnly" />
                <br />
                City:
                <asp:DynamicControl runat="server" DataField="City" Mode="ReadOnly" />
                <br />
            </td>
        </ItemTemplate>
        <LayoutTemplate>
            <table runat="server">
                <tr runat="server">
                    <td runat="server">
                        <table id="groupPlaceholderContainer" runat="server" border="0" style="">
                            <tr id="groupPlaceholder" runat="server">
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr runat="server">
                    <td runat="server" style="">
                        <asp:DataPager ID="DataPager1" runat="server" PageSize="12">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowLastPageButton="True" />
                            </Fields>
                        </asp:DataPager>
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <SelectedItemTemplate>
            <td runat="server" style="">ID:
                <asp:DynamicControl runat="server" DataField="ID" Mode="ReadOnly" />
                <br />
                UserName:
                <asp:DynamicControl runat="server" DataField="UserName" Mode="ReadOnly" />
                <br />
                Password:
                <asp:DynamicControl runat="server" DataField="Password" Mode="ReadOnly" />
                <br />
                Email:
                <asp:DynamicControl runat="server" DataField="Email" Mode="ReadOnly" />
                <br />
                Fname:
                <asp:DynamicControl runat="server" DataField="Fname" Mode="ReadOnly" />
                <br />
                MiddleInt:
                <asp:DynamicControl runat="server" DataField="MiddleInt" Mode="ReadOnly" />
                <br />
                Lname:
                <asp:DynamicControl runat="server" DataField="Lname" Mode="ReadOnly" />
                <br />
                InterviewedbyTWRA:
                <asp:DynamicControl runat="server" DataField="InterviewedbyTWRA" Mode="ReadOnly" />
                <br />
                When_Interviewed:
                <asp:DynamicControl runat="server" DataField="When_Interviewed" Mode="ReadOnly" />
                <br />
                Relatives:
                <asp:DynamicControl runat="server" DataField="Relatives" Mode="ReadOnly" />
                <br />
                Explain_Relatives:
                <asp:DynamicControl runat="server" DataField="Explain_Relatives" Mode="ReadOnly" />
                <br />
                YrTNResidence:
                <asp:DynamicControl runat="server" DataField="YrTNResidence" Mode="ReadOnly" />
                <br />
                List_Residence:
                <asp:DynamicControl runat="server" DataField="List_Residence" Mode="ReadOnly" />
                <br />
                FishingHuntingTypeNo:
                <asp:DynamicControl runat="server" DataField="FishingHuntingTypeNo" Mode="ReadOnly" />
                <br />
                Enforcement_Exper:
                <asp:DynamicControl runat="server" DataField="Enforcement_Exper" Mode="ReadOnly" />
                <br />
                Explain_Enforcement:
                <asp:DynamicControl runat="server" DataField="Explain_Enforcement" Mode="ReadOnly" />
                <br />
                Enforcement_Train:
                <asp:DynamicControl runat="server" DataField="Enforcement_Train" Mode="ReadOnly" />
                <br />
                Explain_EnforcementTrain:
                <asp:DynamicControl runat="server" DataField="Explain_EnforcementTrain" Mode="ReadOnly" />
                <br />
                Advanced_Courses:
                <asp:DynamicControl runat="server" DataField="Advanced_Courses" Mode="ReadOnly" />
                <br />
                Benefical_Courses:
                <asp:DynamicControl runat="server" DataField="Benefical_Courses" Mode="ReadOnly" />
                <br />
                Hobbies:
                <asp:DynamicControl runat="server" DataField="Hobbies" Mode="ReadOnly" />
                <br />
                Farming:
                <asp:DynamicControl runat="server" DataField="Farming" Mode="ReadOnly" />
                <br />
                Forestry:
                <asp:DynamicControl runat="server" DataField="Forestry" Mode="ReadOnly" />
                <br />
                AnimalHusb:
                <asp:DynamicControl runat="server" DataField="AnimalHusb" Mode="ReadOnly" />
                <br />
                Comm_Fish:
                <asp:DynamicControl runat="server" DataField="Comm_Fish" Mode="ReadOnly" />
                <br />
                Sm_Gas_Eng:
                <asp:DynamicControl runat="server" DataField="Sm_Gas_Eng" Mode="ReadOnly" />
                <br />
                AutoRepair:
                <asp:DynamicControl runat="server" DataField="AutoRepair" Mode="ReadOnly" />
                <br />
                BoatOper:
                <asp:DynamicControl runat="server" DataField="BoatOper" Mode="ReadOnly" />
                <br />
                Hunting:
                <asp:DynamicControl runat="server" DataField="Hunting" Mode="ReadOnly" />
                <br />
                Fishing:
                <asp:DynamicControl runat="server" DataField="Fishing" Mode="ReadOnly" />
                <br />
                Camping:
                <asp:DynamicControl runat="server" DataField="Camping" Mode="ReadOnly" />
                <br />
                Swimming:
                <asp:DynamicControl runat="server" DataField="Swimming" Mode="ReadOnly" />
                <br />
                Speci_Animal_Inv:
                <asp:DynamicControl runat="server" DataField="Speci_Animal_Inv" Mode="ReadOnly" />
                <br />
                FishID:
                <asp:DynamicControl runat="server" DataField="FishID" Mode="ReadOnly" />
                <br />
                NavBirdsID:
                <asp:DynamicControl runat="server" DataField="NavBirdsID" Mode="ReadOnly" />
                <br />
                NavMamID:
                <asp:DynamicControl runat="server" DataField="NavMamID" Mode="ReadOnly" />
                <br />
                NavRepID:
                <asp:DynamicControl runat="server" DataField="NavRepID" Mode="ReadOnly" />
                <br />
                NavAmp:
                <asp:DynamicControl runat="server" DataField="NavAmp" Mode="ReadOnly" />
                <br />
                AirBoat:
                <asp:DynamicControl runat="server" DataField="AirBoat" Mode="ReadOnly" />
                <br />
                OutBrdMotors:
                <asp:DynamicControl runat="server" DataField="OutBrdMotors" Mode="ReadOnly" />
                <br />
                FourWheelDri:
                <asp:DynamicControl runat="server" DataField="FourWheelDri" Mode="ReadOnly" />
                <br />
                FarmTrac:
                <asp:DynamicControl runat="server" DataField="FarmTrac" Mode="ReadOnly" />
                <br />
                Bulldozer:
                <asp:DynamicControl runat="server" DataField="Bulldozer" Mode="ReadOnly" />
                <br />
                ChainSaw:
                <asp:DynamicControl runat="server" DataField="ChainSaw" Mode="ReadOnly" />
                <br />
                Rifle:
                <asp:DynamicControl runat="server" DataField="Rifle" Mode="ReadOnly" />
                <br />
                Pistol:
                <asp:DynamicControl runat="server" DataField="Pistol" Mode="ReadOnly" />
                <br />
                Shotgun:
                <asp:DynamicControl runat="server" DataField="Shotgun" Mode="ReadOnly" />
                <br />
                Spec_Com_Train:
                <asp:DynamicControl runat="server" DataField="Spec_Com_Train" Mode="ReadOnly" />
                <br />
                SpecialAnimalInventoryEssay:
                <asp:DynamicControl runat="server" DataField="SpecialAnimalInventoryEssay" Mode="ReadOnly" />
                <br />
                NativeAmphibiansEssay:
                <asp:DynamicControl runat="server" DataField="NativeAmphibiansEssay" Mode="ReadOnly" />
                <br />
                SpecialComputerTrainingEssay:
                <asp:DynamicControl runat="server" DataField="SpecialComputerTrainingEssay" Mode="ReadOnly" />
                <br />
                GroupMem:
                <asp:DynamicControl runat="server" DataField="GroupMem" Mode="ReadOnly" />
                <br />
                Outdoor_Exp:
                <asp:DynamicControl runat="server" DataField="Outdoor_Exp" Mode="ReadOnly" />
                <br />
                Responsible_Pos:
                <asp:DynamicControl runat="server" DataField="Responsible_Pos" Mode="ReadOnly" />
                <br />
                Manage_Exp:
                <asp:DynamicControl runat="server" DataField="Manage_Exp" Mode="ReadOnly" />
                <br />
                Convictied:
                <asp:DynamicControl runat="server" DataField="Convictied" Mode="ReadOnly" />
                <br />
                Explain_Charge:
                <asp:DynamicControl runat="server" DataField="Explain_Charge" Mode="ReadOnly" />
                <br />
                Traffic_Cit:
                <asp:DynamicControl runat="server" DataField="Traffic_Cit" Mode="ReadOnly" />
                <br />
                OthStateEmploy:
                <asp:DynamicControl runat="server" DataField="OthStateEmploy" Mode="ReadOnly" />
                <br />
                Reason_WLO:
                <asp:DynamicControl runat="server" DataField="Reason_WLO" Mode="ReadOnly" />
                <br />
                Events_ForDecision:
                <asp:DynamicControl runat="server" DataField="Events_ForDecision" Mode="ReadOnly" />
                <br />
                CourseofAction:
                <asp:DynamicControl runat="server" DataField="CourseofAction" Mode="ReadOnly" />
                <br />
                Qualities:
                <asp:DynamicControl runat="server" DataField="Qualities" Mode="ReadOnly" />
                <br />
                MajorConernforWL:
                <asp:DynamicControl runat="server" DataField="MajorConernforWL" Mode="ReadOnly" />
                <br />
                Address:
                <asp:DynamicControl runat="server" DataField="Address" Mode="ReadOnly" />
                <br />
                HomePhone:
                <asp:DynamicControl runat="server" DataField="HomePhone" Mode="ReadOnly" />
                <br />
                WorkPhone:
                <asp:DynamicControl runat="server" DataField="WorkPhone" Mode="ReadOnly" />
                <br />
                EmpConNo:
                <asp:DynamicControl runat="server" DataField="EmpConNo" Mode="ReadOnly" />
                <br />
                AlternateNo:
                <asp:DynamicControl runat="server" DataField="AlternateNo" Mode="ReadOnly" />
                <br />
                Residence_FromDate1:
                <asp:DynamicControl runat="server" DataField="Residence_FromDate1" Mode="ReadOnly" />
                <br />
                Residence_ToDate1:
                <asp:DynamicControl runat="server" DataField="Residence_ToDate1" Mode="ReadOnly" />
                <br />
                ResidenceAddress1:
                <asp:DynamicControl runat="server" DataField="ResidenceAddress1" Mode="ReadOnly" />
                <br />
                CreatedDate:
                <asp:DynamicControl runat="server" DataField="CreatedDate" Mode="ReadOnly" />
                <br />
                Yr_TN_Residence_YrMon:
                <asp:DynamicControl runat="server" DataField="Yr_TN_Residence_YrMon" Mode="ReadOnly" />
                <br />
                Residence_FromDate2:
                <asp:DynamicControl runat="server" DataField="Residence_FromDate2" Mode="ReadOnly" />
                <br />
                Residence_ToDate2:
                <asp:DynamicControl runat="server" DataField="Residence_ToDate2" Mode="ReadOnly" />
                <br />
                Residence_FromDate3:
                <asp:DynamicControl runat="server" DataField="Residence_FromDate3" Mode="ReadOnly" />
                <br />
                Residence_ToDate3:
                <asp:DynamicControl runat="server" DataField="Residence_ToDate3" Mode="ReadOnly" />
                <br />
                ResidenceAddress2:
                <asp:DynamicControl runat="server" DataField="ResidenceAddress2" Mode="ReadOnly" />
                <br />
                ResidenceAddress3:
                <asp:DynamicControl runat="server" DataField="ResidenceAddress3" Mode="ReadOnly" />
                <br />
                Country:
                <asp:DynamicControl runat="server" DataField="Country" Mode="ReadOnly" />
                <br />
                State:
                <asp:DynamicControl runat="server" DataField="State" Mode="ReadOnly" />
                <br />
                Zipcode:
                <asp:DynamicControl runat="server" DataField="Zipcode" Mode="ReadOnly" />
                <br />
                City:
                <asp:DynamicControl runat="server" DataField="City" Mode="ReadOnly" />
                <br />
            </td>
        </SelectedItemTemplate>
    </asp:ListView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>" SelectCommand="SELECT OAQ.ApplicantQuestionaire.* FROM OAQ.ApplicantQuestionaire"></asp:SqlDataSource>
</asp:Content>
