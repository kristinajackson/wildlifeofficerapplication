﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Residences.aspx.cs" Inherits="Application_Questionaire.Residences" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <style type="text/css">
        .auto-style1 {
            text-align: right;
            width: 14%;
            height: 36px;
        }
            .auto-style2 {
                text-align: left;
                width: 19%;
                font-size: 14px;
                font-family: Arial, Helvetica, sans-serif;
                height: 36px;
            }
            .auto-style3 {
                text-align: right;
                width: 14%;
                height: 23px;
            }
            .auto-style4 {
                height: 23px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" CombineScripts="false" />
    <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Edit" Height="575px" Width="875px" OnItemInserted="FormView1_ItemInserted" OnItemUpdated="FormView1_ItemUpdated">
        <EditItemTemplate>
            <table id="table2" runat="server" style="background-color: #FFFFFF; font-size: 14px; font-family: Arial, Helvetica, sans-serif;" >
                <tr style="vertical-align: bottom;">
                    <td class="auto-style1" style="white-space:nowrap">First Name:
                        </td>
                        <td class="auto-style2">
                        <asp:TextBox ID="FirstnameTextBox" runat="server" Text='<%# Bind("Fname") %>' CssClass="txtitem"></asp:TextBox>
                        <asp:Label ID="IDPer" runat="server" Text='<%# Eval("ID") %>' Visible="true"></asp:Label>
                            </td>
                    <td class="tdbox">
                        <asp:RequiredFieldValidator ID="FirstnameTextBoxReqValidator" runat="server" ErrorMessage="*This field is required"
                            ValidationGroup="ResVG" ControlToValidate="FirstnameTextBox" Display="Dynamic" Font-Bold="True" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="FirstnameTextBoxeRegExpVal" runat="server"
                            ControlToValidate="FirstnameTextBox" Display="Dynamic" ErrorMessage="Invalid Name!"
                            SetFocusOnError="true" ForeColor="Red" Font-Bold="True"
                            ValidationExpression="^[a-zA-Z''-'\s]{1,40}$" ValidationGroup="ResVG">
                        </asp:RegularExpressionValidator>
                    </td>
                  <%--  <td></td>
                    <td></td>--%>
                    <td class="auto-style2" style="white-space:nowrap">Middle Initial:
                    </td> 
                      <td class="auto-style2">  
                          <asp:TextBox ID="txtMidd" runat="server" Text='<%# Bind("MiddleInt") %>' MaxLength="1" CssClass="txtsmitem"></asp:TextBox>
                    </td>
                <%--    <td></td>--%>
                    <td></td>
                    <td class="auto-style1" style="white-space:nowrap">Last Name:
                        </td>
                    <td class="auto-style2">
                        <asp:TextBox ID="txtlname" runat="server" Text='<%# Bind("Lname") %>' CssClass="txtitem"></asp:TextBox>
                        </td>
                    <td class="tdbox">
                        <asp:RequiredFieldValidator ID="txtlnameReqValidator" runat="server"
                            ErrorMessage="*This field is required" ValidationGroup="ResVG"
                            ControlToValidate="txtlname" Display="Dynamic" Font-Bold="True" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="txtlnameRegExpVal" runat="server"
                            ErrorMessage="Invalid Name!" ControlToValidate="txtlname" Display="Dynamic"
                            SetFocusOnError="true" ForeColor="Red" ValidationExpression="^[a-zA-Z''-'\s]{1,40}$"
                            ValidationGroup="ResVG" Font-Bold="true"></asp:RegularExpressionValidator>

                    </td>
                
                
                    <td class="tdtit" style="white-space:nowrap">Street Address:
                        </td>
                    <td class="tdbox" colspan="2">
                        <asp:TextBox ID="txtAddress" runat="server" Text='<%# Bind("Address") %>' CssClass="txtitem" Width="85%"></asp:TextBox>
                    </td>
                        <td class="tdbox">
                        <asp:RequiredFieldValidator ID="txtAddressReqVal" ValidationGroup="ResVG"
                            runat="server" ControlToValidate="txtAddress" ErrorMessage="*Field must be entered"
                            ForeColor="Red" Display="Dynamic" SetFocusOnError="true" Font-Bold="true">
                        </asp:RequiredFieldValidator>
                        </td>
                    <%--<td></td>--%>
                    <%--<td></td>--%>
                    </tr>
                <tr>
                    <td class="tdtit">Country:
                        </td>
                    <td class="tdbox">
                        <asp:DropDownList ID="ddlCountry" runat="server" Text='<%# Bind("Country") %>' CssClass="ddlitem">
                            <asp:ListItem></asp:ListItem>
                            <asp:ListItem Text="Afghanistan" Value="Afghanistan"></asp:ListItem>
                            <asp:ListItem Text="Albania" Value="Albania"></asp:ListItem>
                            <asp:ListItem Text="Algeria" Value="Algeria"></asp:ListItem>
                            <asp:ListItem Text="American Samoa" Value="American Samoa"></asp:ListItem>
                            <asp:ListItem Text="Andorra" Value="Andorra"></asp:ListItem>
                            <asp:ListItem Text="Angola" Value="Angola"></asp:ListItem>
                            <asp:ListItem Text="Anguilla" Value="Anguilla"></asp:ListItem>
                            <asp:ListItem Text="Antarctica" Value="Antarctica"></asp:ListItem>
                            <asp:ListItem Text="Antigua and Barbuda" Value="Antigua and Barbuda"></asp:ListItem>
                            <asp:ListItem Text="Argentina" Value="Argentina"></asp:ListItem>
                            <asp:ListItem Text="Armenia" Value="Armenia"></asp:ListItem>
                            <asp:ListItem Text="Aruba" Value="Aruba"></asp:ListItem>
                            <asp:ListItem Text="Australia" Value="Australia"></asp:ListItem>
                            <asp:ListItem Text="Austria" Value="Austria"></asp:ListItem>
                            <asp:ListItem Text="Azerbaijan" Value="Azerbaijan"></asp:ListItem>
                            <asp:ListItem Text="Bahamas" Value="Bahamas"></asp:ListItem>
                            <asp:ListItem Text="Bahrain" Value="Bahrain"></asp:ListItem>
                            <asp:ListItem Text="Bangladesh" Value="Bangladesh"></asp:ListItem>
                            <asp:ListItem Text="Barbados" Value="Barbados"></asp:ListItem>
                            <asp:ListItem Text="Belarus" Value="Belarus"></asp:ListItem>
                            <asp:ListItem Text="Belgium" Value="Belgium"></asp:ListItem>
                            <asp:ListItem Text="Belize" Value="Belize"></asp:ListItem>
                            <asp:ListItem Text="Benin" Value="Benin"></asp:ListItem>
                            <asp:ListItem Text="Bermuda" Value="Bermuda"></asp:ListItem>
                            <asp:ListItem Text="Bhutan" Value="Bhutan"></asp:ListItem>
                            <asp:ListItem Text="Bolivia" Value="Bolivia"></asp:ListItem>
                            <asp:ListItem Text="Bosnia and Herzegovina" Value="BosniaandHerzegovina"></asp:ListItem>
                            <asp:ListItem Text="Botswana" Value="Botswana"></asp:ListItem>
                            <asp:ListItem Text="Bouvet Island" Value="BouvetIsland"></asp:ListItem>
                            <asp:ListItem Text="Brazil" Value="Brazil"></asp:ListItem>
                            <asp:ListItem Text="British Indian Ocean Territory" Value="BritishIndianOceanTerritory"></asp:ListItem>
                            <asp:ListItem Text="Brunei Darussalam" Value="BruneiDarussalam"></asp:ListItem>
                            <asp:ListItem Text="Bulgaria" Value="Bulgaria"></asp:ListItem>
                            <asp:ListItem Text="Burkina Faso" Value="BurkinaFaso"></asp:ListItem>
                            <asp:ListItem Text="Burundi" Value="Burundi"></asp:ListItem>
                            <asp:ListItem Text="Cambodia" Value="Cambodia"></asp:ListItem>
                            <asp:ListItem Text="Cameroon" Value="Cameroon"></asp:ListItem>
                            <asp:ListItem Text="Canada" Value="Canada"></asp:ListItem>
                            <asp:ListItem Text="Cape Verde" Value="CapeVerde"></asp:ListItem>
                            <asp:ListItem Text="Cayman Islands" Value="CaymanIslands"></asp:ListItem>
                            <asp:ListItem Text="Central African Republic" Value="CentralAfricanRepublic"></asp:ListItem>
                            <asp:ListItem Text="Chad" Value="Chad"></asp:ListItem>
                            <asp:ListItem Text="Chile" Value="Chile"></asp:ListItem>
                            <asp:ListItem Text="China" Value="China"></asp:ListItem>
                            <asp:ListItem Text="Christmas Island" Value="ChristmasIsland"></asp:ListItem>
                            <asp:ListItem Text="Cocos (Keeling Islands)" Value="Cocos"></asp:ListItem>
                            <asp:ListItem Text="Colombia" Value="Colombia"></asp:ListItem>
                            <asp:ListItem Text="Comoros" Value="Comoros"></asp:ListItem>
                            <asp:ListItem Text="Congo" Value="Congo"></asp:ListItem>
                            <asp:ListItem Text="Cook Islands" Value="CookIslands"></asp:ListItem>
                            <asp:ListItem Text="Costa Rica" Value="CostaRica"></asp:ListItem>
                            <asp:ListItem Text="Cote D'Ivoire (Ivory Coast)" Value="CoteDIvoire"></asp:ListItem>
                            <asp:ListItem Text="Croatia (Hrvatska)" Value="Croatia"></asp:ListItem>
                            <asp:ListItem Text="Cuba" Value="Cuba"></asp:ListItem>
                            <asp:ListItem Text="Cyprus" Value="Cyprus"></asp:ListItem>
                            <asp:ListItem Text="Czech Republic" Value="CzechRepublic"></asp:ListItem>
                            <asp:ListItem Text="Denmark" Value="Denmark"></asp:ListItem>
                            <asp:ListItem Text="Djibouti" Value="Djibouti"></asp:ListItem>
                            <asp:ListItem Text="Dominica" Value="Dominica"></asp:ListItem>
                            <asp:ListItem Text="Dominican Republic" Value="DominicanRepublic"></asp:ListItem>
                            <asp:ListItem Text="East Timor" Value="EastTimor"></asp:ListItem>
                            <asp:ListItem Text="Ecuador" Value="Ecuador"></asp:ListItem>
                            <asp:ListItem Text="Egypt" Value="Egypt"></asp:ListItem>
                            <asp:ListItem Text="El Salvador" Value="ElSalvador"></asp:ListItem>
                            <asp:ListItem Text="Equatorial Guinea" Value="EquatorialGuinea"></asp:ListItem>
                            <asp:ListItem Text="Eritrea" Value="Eritrea"></asp:ListItem>
                            <asp:ListItem Text="Estonia" Value="Estonia"></asp:ListItem>
                            <asp:ListItem Text="Ethiopia" Value="Ethiopia"></asp:ListItem>
                            <asp:ListItem Text="Falkland Islands (Malvinas)" Value="FalklandIslands"></asp:ListItem>
                            <asp:ListItem Text="Faroe Islands" Value="FaroeIslands"></asp:ListItem>
                            <asp:ListItem Text="Fiji" Value="Fiji"></asp:ListItem>
                            <asp:ListItem Text="Finland" Value="Finland"></asp:ListItem>
                            <asp:ListItem Text="France" Value="France"></asp:ListItem>
                            <asp:ListItem Text="Gabon" Value="Gabon"></asp:ListItem>
                            <asp:ListItem Text="Gambia" Value="Gambia"></asp:ListItem>
                            <asp:ListItem Text="Georgia" Value="Georgia"></asp:ListItem>
                            <asp:ListItem Text="Germany" Value="Germany"></asp:ListItem>
                            <asp:ListItem Text="Ghana" Value="Ghana"></asp:ListItem>
                            <asp:ListItem Text="Gibraltar" Value="Gibraltar"></asp:ListItem>
                            <asp:ListItem Text="Greece" Value="Greece"></asp:ListItem>
                            <asp:ListItem Text="Greenland" Value="Greenland"></asp:ListItem>
                            <asp:ListItem Text="Grenada" Value="Grenada"></asp:ListItem>
                            <asp:ListItem Text="Guadeloupe" Value="Guadeloupe"></asp:ListItem>
                            <asp:ListItem Text="Guam" Value="Guam"></asp:ListItem>
                            <asp:ListItem Text="Guatemala" Value="Guatemala"></asp:ListItem>
                            <asp:ListItem Text="Guinea" Value="Guinea"></asp:ListItem>
                            <asp:ListItem Text="Guinea-Bissau" Value="GuineaBissau"></asp:ListItem>
                            <asp:ListItem Text="Guyana" Value="Guyana"></asp:ListItem>
                            <asp:ListItem Text="Haiti" Value="Haiti"></asp:ListItem>
                            <asp:ListItem Text="Honduras" Value="Honduras"></asp:ListItem>
                            <asp:ListItem Text="Hong Kong" Value="HongKong"></asp:ListItem>
                            <asp:ListItem Text="Hungary" Value="Hungary"></asp:ListItem>
                            <asp:ListItem Text="Iceland" Value="Iceland"></asp:ListItem>
                            <asp:ListItem Text="India" Value="India"></asp:ListItem>
                            <asp:ListItem Text="Indonesia" Value="Indonesia"></asp:ListItem>
                            <asp:ListItem Text="Iran" Value="Iran"></asp:ListItem>
                            <asp:ListItem Text="Iraq" Value="Iraq"></asp:ListItem>
                            <asp:ListItem Text="Ireland" Value="Ireland"></asp:ListItem>
                            <asp:ListItem Text="Israel" Value="Israel"></asp:ListItem>
                            <asp:ListItem Text="Italy" Value="Italy"></asp:ListItem>
                            <asp:ListItem Text="Jamaica" Value="Jamaica"></asp:ListItem>
                            <asp:ListItem Text="Japan" Value="Japan"></asp:ListItem>
                            <asp:ListItem Text="Jordan" Value="Jordan"></asp:ListItem>
                            <asp:ListItem Text="Kazakhstan" Value="Kazakhstan"></asp:ListItem>
                            <asp:ListItem Text="Kenya" Value="Kenya"></asp:ListItem>
                            <asp:ListItem Text="Kiribati" Value="Kiribati"></asp:ListItem>
                            <asp:ListItem Text="Korea (North)" Value="Korea"></asp:ListItem>
                            <asp:ListItem Text="Kuwait" Value="Kuwait"></asp:ListItem>
                            <asp:ListItem Text="Kyrgyzstan" Value="Kyrgyzstan"></asp:ListItem>
                            <asp:ListItem Text="Laos" Value="Laos"></asp:ListItem>
                            <asp:ListItem Text="Latvia" Value="Latvia"></asp:ListItem>
                            <asp:ListItem Text="Lebanon" Value="Lebanon"></asp:ListItem>
                            <asp:ListItem Text="Lesotho" Value="Lesotho"></asp:ListItem>
                            <asp:ListItem Text="Liberia" Value="Liberia"></asp:ListItem>
                            <asp:ListItem Text="Libya" Value="Libya"></asp:ListItem>
                            <asp:ListItem Text="Liechtenstein" Value="Liechtenstein"></asp:ListItem>
                            <asp:ListItem Text="Lithuania" Value="Lithuania"></asp:ListItem>
                            <asp:ListItem Text="Luxembourg" Value="Luxembourg"></asp:ListItem>
                            <asp:ListItem Text="Macau" Value="Macau"></asp:ListItem>
                            <asp:ListItem Text="Macedonia" Value="Macedonia"></asp:ListItem>
                            <asp:ListItem Text="Madagascar" Value="Madagascar"></asp:ListItem>
                            <asp:ListItem Text="Malawi" Value="Malawi"></asp:ListItem>
                            <asp:ListItem Text="Malaysia" Value="Malaysia"></asp:ListItem>
                            <asp:ListItem Text="Maldives" Value="Maldives"></asp:ListItem>
                            <asp:ListItem Text="Mali" Value="Mali"></asp:ListItem>
                            <asp:ListItem Text="Malta" Value="Malta"></asp:ListItem>
                            <asp:ListItem Text="Marshall Islands" Value="MarshallIslands"></asp:ListItem>
                            <asp:ListItem Text="Martinique" Value="Martinique"></asp:ListItem>
                            <asp:ListItem Text="Mauritania" Value="Mauritania"></asp:ListItem>
                            <asp:ListItem Text="Mauritius" Value="Mauritius"></asp:ListItem>
                            <asp:ListItem Text="Mayotte" Value="Mayotte"></asp:ListItem>
                            <asp:ListItem Text="Mexico" Value="Mexico"></asp:ListItem>
                            <asp:ListItem Text="Micronesia" Value="Micronesia"></asp:ListItem>
                            <asp:ListItem Text="Moldova" Value="Moldova"></asp:ListItem>
                            <asp:ListItem Text="Monaco" Value="Monaco"></asp:ListItem>
                            <asp:ListItem Text="Mongolia" Value="Mongolia"></asp:ListItem>
                            <asp:ListItem Text="Montserrat" Value="Montserrat"></asp:ListItem>
                            <asp:ListItem Text="Morocco" Value="Morocco"></asp:ListItem>
                            <asp:ListItem Text="Mozambique" Value="Mozambique"></asp:ListItem>
                            <asp:ListItem Text="Myanmar" Value="Myanmar"></asp:ListItem>
                            <asp:ListItem Text="Namibia" Value="Namibia"></asp:ListItem>
                            <asp:ListItem Text="Nauru" Value="Nauru"></asp:ListItem>
                            <asp:ListItem Text="Nepal" Value="Nepal"></asp:ListItem>
                            <asp:ListItem Text="Netherlands" Value="Netherlands"></asp:ListItem>
                            <asp:ListItem Text="Netherlands Antilles" Value="NetherlandsAntilles"></asp:ListItem>
                            <asp:ListItem Text="New Caledonia" Value="NewCaledonia"></asp:ListItem>
                            <asp:ListItem Text="New Zealand" Value="NewZealand"></asp:ListItem>
                            <asp:ListItem Text="Nicaragua" Value="Nicaragua"></asp:ListItem>
                            <asp:ListItem Text="Niger" Value="Niger"></asp:ListItem>
                            <asp:ListItem Text="Nigeria" Value="Nigeria"></asp:ListItem>
                            <asp:ListItem Text="Niue" Value="Niue"></asp:ListItem>
                            <asp:ListItem Text="Norfolk Island" Value="NorfolkIsland"></asp:ListItem>
                            <asp:ListItem Text="Northern Mariana Islands" Value="NorthernMarianaIslands"></asp:ListItem>
                            <asp:ListItem Text="Norway" Value="Norway"></asp:ListItem>
                            <asp:ListItem Text="Oman" Value="Oman"></asp:ListItem>
                            <asp:ListItem Text="Pakistan" Value="Pakistan"></asp:ListItem>
                            <asp:ListItem Text="Palau" Value="Palau"></asp:ListItem>
                            <asp:ListItem Text="Panama" Value="Panama"></asp:ListItem>
                            <asp:ListItem Text="Papua New Guinea" Value="PapuaNewGuinea"></asp:ListItem>
                            <asp:ListItem Text="Paraguay" Value="Paraguay"></asp:ListItem>
                            <asp:ListItem Text="Peru" Value="Peru"></asp:ListItem>
                            <asp:ListItem Text="Pitcairn" Value="Pitcairn"></asp:ListItem>
                            <asp:ListItem Text="Poland" Value="Poland"></asp:ListItem>
                            <asp:ListItem Text="Portugal" Value="Portugal"></asp:ListItem>
                            <asp:ListItem Text="Puerto Rico" Value="PuertoRico"></asp:ListItem>
                            <asp:ListItem Text="Qatar" Value="Qatar"></asp:ListItem>
                            <asp:ListItem Text="Reunion" Value="Reunion"></asp:ListItem>
                            <asp:ListItem Text="Romania" Value="Romania"></asp:ListItem>
                            <asp:ListItem Text="Russian Federation" Value="RussianFederation"></asp:ListItem>
                            <asp:ListItem Text="Rwanda" Value="Rwanda"></asp:ListItem>
                            <asp:ListItem Text="Saint Kitts and Nevis" Value="SaintKittsandNevis"></asp:ListItem>
                            <asp:ListItem Text="Saint Lucia" Value="SaintLucia"></asp:ListItem>
                            <asp:ListItem Text="Saint Vincent and The Grenadines" Value="SaintVincentandTheGrenadines"></asp:ListItem>
                            <asp:ListItem Text="Samoa" Value="Samoa"></asp:ListItem>
                            <asp:ListItem Text="San Marino" Value="SanMarino"></asp:ListItem>
                            <asp:ListItem Text="Sao Tome and Principe" Value="SaoTomeandPrincipe"></asp:ListItem>
                            <asp:ListItem Text="Saudi Arabia" Value="SaudiArabia"></asp:ListItem>
                            <asp:ListItem Text="Senegal" Value="Senegal"></asp:ListItem>
                            <asp:ListItem Text="Seychelles" Value="Seychelles"></asp:ListItem>
                            <asp:ListItem Text="Sierra Leone" Value="SierraLeone"></asp:ListItem>
                            <asp:ListItem Text="Singapore" Value="Singapore"></asp:ListItem>
                            <asp:ListItem Text="Slovak Republic" Value="SlovakRepublic"></asp:ListItem>
                            <asp:ListItem Text="Slovenia" Value="Slovenia"></asp:ListItem>
                            <asp:ListItem Text="Solomon Islands" Value="SolomonIslands"></asp:ListItem>
                            <asp:ListItem Text="Somalia" Value="Somalia"></asp:ListItem>
                            <asp:ListItem Text="South Africa" Value="SouthAfrica"></asp:ListItem>
                            <asp:ListItem Text="Spain" Value="Spain"></asp:ListItem>
                            <asp:ListItem Text="Sri Lanka" Value="SriLanka"></asp:ListItem>
                            <asp:ListItem Text="St. Helena" Value="St.Helena"></asp:ListItem>
                            <asp:ListItem Text="St. Pierre and Miquelon" Value="St.PierreandMiquelon"></asp:ListItem>
                            <asp:ListItem Text="Sudan" Value="Sudan"></asp:ListItem>
                            <asp:ListItem Text="Suriname" Value="Suriname"></asp:ListItem>
                            <asp:ListItem Text="Svalbard and Jan Mayen Islands" Value="SvalbardandJanMayenIslands"></asp:ListItem>
                            <asp:ListItem Text="Swaziland" Value="Swaziland"></asp:ListItem>
                            <asp:ListItem Text="Sweden" Value="Sweden"></asp:ListItem>
                            <asp:ListItem Text="Switzerland" Value="Switzerland"></asp:ListItem>
                            <asp:ListItem Text="Syria" Value="Syria"></asp:ListItem>
                            <asp:ListItem Text="Taiwan" Value="Taiwan"></asp:ListItem>
                            <asp:ListItem Text="Tajikistan" Value="Tajikistan"></asp:ListItem>
                            <asp:ListItem Text="Tanzania" Value="Tanzania"></asp:ListItem>
                            <asp:ListItem Text="Thailand" Value="Thailand"></asp:ListItem>
                            <asp:ListItem Text="Togo" Value="Togo"></asp:ListItem>
                            <asp:ListItem Text="Tokelau" Value="Tokelau"></asp:ListItem>
                            <asp:ListItem Text="Tonga" Value="Tonga"></asp:ListItem>
                            <asp:ListItem Text="Trinidad and Tobago" Value="TrinidadandTobago"></asp:ListItem>
                            <asp:ListItem Text="Tunisia" Value="Tunisia"></asp:ListItem>
                            <asp:ListItem Text="Turkey" Value="Turkey"></asp:ListItem>
                            <asp:ListItem Text="Turkmenistan" Value="Turkmenistan"></asp:ListItem>
                            <asp:ListItem Text="Turks and Caicos Islands" Value="TurksandCaicosIslands"></asp:ListItem>
                            <asp:ListItem Text="Tuvalu" Value="Tuvalu"></asp:ListItem>
                            <asp:ListItem Text="Uganda" Value="Uganda"></asp:ListItem>
                            <asp:ListItem Text="Ukraine" Value="Ukraine"></asp:ListItem>
                            <asp:ListItem Text="United Arab Emirates" Value="UnitedArabEmirates"></asp:ListItem>
                            <asp:ListItem Text="United Kingdom" Value="UnitedKingdom"></asp:ListItem>
                            <asp:ListItem Text="United States" Value="UnitedStates" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="US Minor Outlying Islands" Value="USMinorOutlyingIslands"></asp:ListItem>
                            <asp:ListItem Text="Uruguay" Value="Uruguay"></asp:ListItem>
                            <asp:ListItem Text="Uzbekistan" Value="Uzbekistan"></asp:ListItem>
                            <asp:ListItem Text="Vanuatu" Value="Vanuatu"></asp:ListItem>
                            <asp:ListItem Text="Vatican City State (Holy See)" Value="Vatican City State"></asp:ListItem>
                            <asp:ListItem Text="Venezuela" Value="Venezuela"></asp:ListItem>
                            <asp:ListItem Text="Viet Nam" Value="Viet Nam"></asp:ListItem>
                            <asp:ListItem Text="Virgin Islands (British)" Value="Virgin Islands (British)"></asp:ListItem>
                            <asp:ListItem Text="Virgin Islands (US)" Value="Virgin Islands (US)"></asp:ListItem>
                            <asp:ListItem Text="Wallis and Futuna Islands" Value="Wallis and Futuna Islands"></asp:ListItem>
                            <asp:ListItem Text="Western Sahara" Value="Western Sahara"></asp:ListItem>
                            <asp:ListItem Text="Yemen" Value="Yemen"></asp:ListItem>
                            <asp:ListItem Text="Yugoslavia" Value="Yugoslavia"></asp:ListItem>
                            <asp:ListItem Text="Zaire" Value="Zaire"></asp:ListItem>
                            <asp:ListItem Text="Zambia" Value="Zambia"></asp:ListItem>
                            <asp:ListItem Text="Zimbabwe" Value="Zimbabwe"></asp:ListItem>
                        </asp:DropDownList>
                        </td>
                    <td class="tdbox">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="ResVG" runat="server"
                            ControlToValidate="ddlCountry" ErrorMessage="*Field must be entered" ForeColor="Red"
                            Display="Dynamic" SetFocusOnError="true" Font-Bold="true"></asp:RequiredFieldValidator>
                    </td>
                    <%--<td></td>--%>
                    <%--<td ></td>--%>
                    <td class="tdtit">City:
                        </td>
                    <td class="tdbox">
                    <asp:TextBox ID="txtCity" runat="server" Text='<%# Bind("City") %>' CssClass="txtitem"></asp:TextBox>
                        </td>
                    <td class="tdbox">
                        <asp:RequiredFieldValidator ID="CityReqVal" ValidationGroup="ResVG" runat="server"
                            ControlToValidate="txtCity" ErrorMessage="*Field must be entered" ForeColor="Red"
                            Display="Dynamic" SetFocusOnError="true" Font-Bold="true"></asp:RequiredFieldValidator>
                    </td>

                    <%--<td></td>
                    <td></td>--%>
                    <td class="tdtit">State:
                        </td>
                    <td class="tdbox">
                         <asp:DropDownList ID="ddlState" runat="server" CssClass="ddlitem" Text='<%# Bind("State") %>' SelectedValue='<%# Bind("State") %>'>
                            <asp:ListItem></asp:ListItem>
                            <asp:ListItem>AL</asp:ListItem>
                            <asp:ListItem>AK</asp:ListItem>
                            <asp:ListItem>AZ</asp:ListItem>
                            <asp:ListItem>AR</asp:ListItem>
                            <asp:ListItem>CA</asp:ListItem>
                            <asp:ListItem>CO</asp:ListItem>
                            <asp:ListItem>CT</asp:ListItem>
                            <asp:ListItem>DC</asp:ListItem>
                            <asp:ListItem>DE</asp:ListItem>
                            <asp:ListItem>FL</asp:ListItem>
                            <asp:ListItem>GA</asp:ListItem>
                            <asp:ListItem>HI</asp:ListItem>
                            <asp:ListItem>ID</asp:ListItem>
                            <asp:ListItem>IL</asp:ListItem>
                            <asp:ListItem>IN</asp:ListItem>
                            <asp:ListItem>IA</asp:ListItem>
                            <asp:ListItem>KS</asp:ListItem>
                            <asp:ListItem>KY</asp:ListItem>
                            <asp:ListItem>LA</asp:ListItem>
                            <asp:ListItem>ME</asp:ListItem>
                            <asp:ListItem>MD</asp:ListItem>
                            <asp:ListItem>MA</asp:ListItem>
                            <asp:ListItem>MI</asp:ListItem>
                            <asp:ListItem>MN</asp:ListItem>
                            <asp:ListItem>MS</asp:ListItem>
                            <asp:ListItem>MO</asp:ListItem>
                            <asp:ListItem>MT</asp:ListItem>
                            <asp:ListItem>NE</asp:ListItem>
                            <asp:ListItem>NV</asp:ListItem>
                            <asp:ListItem>NH</asp:ListItem>
                            <asp:ListItem>NJ</asp:ListItem>
                            <asp:ListItem>NM</asp:ListItem>
                            <asp:ListItem>NY</asp:ListItem>
                            <asp:ListItem>NC</asp:ListItem>
                            <asp:ListItem>ND</asp:ListItem>
                            <asp:ListItem>OH</asp:ListItem>
                            <asp:ListItem>OK</asp:ListItem>
                            <asp:ListItem>OR</asp:ListItem>
                            <asp:ListItem>PA</asp:ListItem>
                            <asp:ListItem>RI</asp:ListItem>
                            <asp:ListItem>SC</asp:ListItem>
                            <asp:ListItem>SD</asp:ListItem>
                            <asp:ListItem Selected="True">TN</asp:ListItem>
                            <asp:ListItem>TX</asp:ListItem>
                            <asp:ListItem>UT</asp:ListItem>
                            <asp:ListItem>VT</asp:ListItem>
                            <asp:ListItem>VA</asp:ListItem>
                            <asp:ListItem>WA</asp:ListItem>
                            <asp:ListItem>WV</asp:ListItem>
                            <asp:ListItem>WI</asp:ListItem>
                            <asp:ListItem>WY</asp:ListItem>
                            <asp:ListItem>  </asp:ListItem>
                        </asp:DropDownList>
                        </td>
                    <td class="tdbox">
                        <asp:RequiredFieldValidator ID="StateReqVal" ValidationGroup="ResVG" runat="server"
                            ControlToValidate="ddlState" ErrorMessage="*Field must be entered" ForeColor="Red"
                            Display="Dynamic" SetFocusOnError="true" Font-Bold="true"></asp:RequiredFieldValidator>

                    </td>
                    <%--<td></td>
                    <td></td>--%>
                    <td class="tdtit">Zipcode:
                        </td>
                    <td class="tdbox">
                        <asp:TextBox ID="ZipCodeTextBox" runat="server" CssClass="txtsmitem" Text='<%# Bind("Zipcode") %>' />
                        <asp:Label ID="lblZip" Text="99999" runat="server" Style="font-size: x-small"></asp:Label>
                        </td>
                    <td class="tdbox">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ValidationGroup="ResVG" runat="server"
                            ControlToValidate="ZipCodeTextBox" ErrorMessage="*Field must be entered" ForeColor="Red"
                            Display="Dynamic" SetFocusOnError="true" Font-Bold="true"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="tdtit" style="white-space:nowrap">Home Phone:
                        </td>
                    <td class="tdbox">
                        <asp:TextBox ID="HomePhoneTextBox" runat="server" Text='<%# Bind("HomePhone") %>' CssClass="txtitem"></asp:TextBox>
                        <asp:MaskedEditExtender ID="HomePhoneMEE" runat="server" TargetControlID="HomePhoneTextBox"
                            AutoComplete="False" ClearMaskOnLostFocus="False" Mask="999-999-9999" MaskType="Number" />
                        <asp:Label ID="Label1" Text="999-999-9999" runat="server" Style="font-size: x-small"></asp:Label>
                        </td>
                    <td class="tdbox">
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="ResVG" runat="server"
                            ControlToValidate="HomePhoneTextBox" ErrorMessage="*Field must be entered"
                            ForeColor="Red" Display="Dynamic" SetFocusOnError="true" Font-Bold="True"></asp:RequiredFieldValidator>--%>
                         <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ValidationGroup="ResVG" runat="server"
                            ControlToValidate="HomePhoneTextBox" ErrorMessage="Invalid phone number" ValidationExpression="^[0-9]{10}|^[0-9]{3}-[0-9]{3}-[0-9]{4}"
                            ForeColor="Red" Display="Dynamic" SetFocusOnError="true" Font-Bold="True" />
                    </td>
                    <%--<td></td>
                    <td></td>--%>
                    <td class="tdtit" style="white-space:nowrap">Work Phone:
                        </td>
                    <td class="tdbox"> 
                        <asp:TextBox ID="WorkPhoneTextBox" runat="server" Text='<%# Bind("WorkPhone") %>' CssClass="txtitem"></asp:TextBox>
                        <asp:MaskedEditExtender ID="WorkPhoneMEE" runat="server" TargetControlID="WorkPhoneTextBox" AutoComplete="False" ClearMaskOnLostFocus="False" Mask="999-999-9999" MaskType="Number" /> 
                        <asp:Label ID="Label2" Text="999-999-9999" runat="server" Style="font-size: x-small"></asp:Label>
                        </td>
                    <td class="tdbox">
                       <%-- <asp:RequiredFieldValidator ID="PhoneReqVal" ValidationGroup="ResVG" runat="server"
                            ControlToValidate="WorkPhoneTextBox" ErrorMessage="*Field must be entered" ForeColor="Red"
                            Display="Dynamic" SetFocusOnError="true" Font-Bold="True"></asp:RequiredFieldValidator>--%>
                        <asp:RegularExpressionValidator ID="PhoneRE" ValidationGroup="ResVG" runat="server"
                            ControlToValidate="WorkPhoneTextBox" ErrorMessage="Invalid phone number" ValidationExpression="^[0-9]{10}|^[0-9]{3}-[0-9]{3}-[0-9]{4}"
                            ForeColor="Red" Display="Dynamic" SetFocusOnError="true" Font-Bold="True" />
                    </td>
                   <%-- <td></td>--%>
                    <%--<td ></td>--%>
                    <td class="tdtit" style="white-space:nowrap">Employment Contact No:
                        </td>
                    <td class="tdbox">
                        <asp:TextBox ID="EmpContactTextBox" runat="server" Text='<%# Bind("EmpConNo") %>'></asp:TextBox>
                        <asp:MaskedEditExtender ID="EmpContactTextBox_MaskedEditExtender" runat="server" BehaviorID="EmpContactTextBox_MaskedEditExtender"
                            Mask="999-999-9999" MaskType="Number" ClearMaskOnLostFocus="False" ErrorTooltipEnabled="true" AutoComplete="False" TargetControlID="EmpContactTextBox" />
                        <asp:Label ID="Label3" Text="999-999-9999" runat="server" Style="font-size: x-small"></asp:Label>
                        </td>
                    <td class="tdbox">
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="ResVG" runat="server"
                            ControlToValidate="EmpContactTextBox" ErrorMessage="*Field must be entered"
                            ForeColor="Red" Display="Dynamic" SetFocusOnError="true" Font-Bold="True"></asp:RequiredFieldValidator>--%>
                   <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="ResVG" runat="server"
                            ControlToValidate="EmpContactTextBox" ErrorMessage="Invalid phone number" ValidationExpression="^[0-9]{10}|^[0-9]{3}-[0-9]{3}-[0-9]{4}"
                            ForeColor="Red" Display="Dynamic" SetFocusOnError="true" Font-Bold="True" />
                         </td>
                    <%--<td></td>
                    <td></td>--%>
                    <td class="tdtit" style="white-space:nowrap">Alternate No:
                        </td>
                    <td class="tdbox">
                        <asp:TextBox ID="AlternateNoTextBox" runat="server" Text='<%# Bind("AlternateNo") %>' Width="130px"></asp:TextBox>
                        <asp:MaskedEditExtender ID="AlternateNoTextBox_MaskedEditExtender" runat="server" BehaviorID="AlternateNoTextBox_MaskedEditExtender"
                            Mask="999-999-9999" MaskType="Number" ClearMaskOnLostFocus="False"
                            ErrorTooltipEnabled="true" AutoComplete="False" TargetControlID="AlternateNoTextBox" />
                        <asp:Label ID="Label4" Text="999-999-9999" runat="server" Style="font-size: x-small"></asp:Label>
                        </td>
                    <td class="tdbox">
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="ResVG" runat="server"
                            ControlToValidate="AlternateNoTextBox" ErrorMessage="*Field must be entered"
                            ForeColor="Red" Display="Dynamic" SetFocusOnError="true" Font-Bold="True"></asp:RequiredFieldValidator>--%>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ValidationGroup="ResVG" runat="server"
                            ControlToValidate="AlternateNoTextBox" ErrorMessage="Invalid phone number" ValidationExpression="^[0-9]{10}|^[0-9]{3}-[0-9]{3}-[0-9]{4}"
                            ForeColor="Red" Display="Dynamic" SetFocusOnError="true" Font-Bold="True" />
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <table id="Table1" runat="server">
                <tr>
                 <td>List chronologically your residences for the last three years(including address while attending school).<br></br></td>
               </tr>
                     <tr id="Tr1" runat="server">
                    <td id="Td4" runat="server" >Dates From</td>
                    <td id="TableCell12" runat="server"></td>
                    <td id="Td7" runat="server" >Dates To</td>
                    <td id="Td8" runat="server" ></td>
                    <td id="Td5" runat="server" >Address</td>
                </tr>
                <tr id="Tr2" runat="server">
                    <td class="tdbox">
                        <asp:TextBox ID="txtDtFrom1" runat="server" Text='<%# Bind("Residence_FromDate1") %>' CssClass="txtitem"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server"
                            ErrorMessage="*This field is required" ValidationGroup="ResVG"
                            ControlToValidate="txtDtFrom1" Display="Dynamic" Font-Bold="True" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                        <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" BehaviorID="txtDtFrom1_MaskedEditExtender"
                            Mask=" 99/99/9999" MaskType="Date" ClearMaskOnLostFocus="False"
                            ErrorTooltipEnabled="true" AutoComplete="False" TargetControlID="txtDtFrom1" />
                        <asp:RangeValidator ID="rvDate" runat="server"
                            ControlToValidate="txtDtFrom1" ErrorMessage="Invalid Date"
                            Type="Date" MinimumValue="01/01/1900" MaximumValue="01/01/2100"
                            Display="Dynamic" ForeColor="Red" Font-Bold="true"></asp:RangeValidator>
                    </td>
                    <td class="tdbox">
                        <asp:TextBox ID="txtDtTo1" runat="server" Text='<%# Bind("Residence_ToDate1") %>' CssClass="txtitem"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server"
                            ErrorMessage="*This field is required" ValidationGroup="ResVG"
                            ControlToValidate="txtDtTo1" Display="Dynamic" Font-Bold="True" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                        <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" BehaviorID="txtDtTo1_MaskedEditExtender"
                            Mask=" 99/99/9999" MaskType="Date" ClearMaskOnLostFocus="False"
                            ErrorTooltipEnabled="true" AutoComplete="False" TargetControlID="txtDtTo1" />
                        <asp:RangeValidator ID="RangeValidator1" runat="server"
                            ControlToValidate="txtDtTo1" ErrorMessage="Invalid Date"
                            Type="Date" MinimumValue="01/01/1900" MaximumValue="01/01/2100"
                            Display="Dynamic" ForeColor="Red" Font-Bold="true"></asp:RangeValidator>
                    </td>
                    <td class="tdbox">
                        <asp:TextBox ID="txtAddress1" runat="server" Text='<%# Bind("ResidenceAddress1") %>' CssClass="txtitem"></asp:TextBox></td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server"
                            ErrorMessage="*This field is required" ValidationGroup="ResVG"
                            ControlToValidate="txtAddress1" Display="Dynamic" Font-Bold="True" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="TableRow1" runat="server">
                    <td class="tdbox">
                        <asp:TextBox ID="txtDtFrom2" runat="server" Text='<%# Bind("Residence_FromDate2") %>' CssClass="txtitem"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server"
                            ErrorMessage="*This field is required" ValidationGroup="ResVG"
                            ControlToValidate="txtDtFrom2" Display="Dynamic" Font-Bold="True" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                        <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" BehaviorID="txtDtFrom2_MaskedEditExtender"
                            Mask=" 99/99/9999" MaskType="Date" ClearMaskOnLostFocus="False"
                            ErrorTooltipEnabled="true" AutoComplete="False" TargetControlID="txtDtFrom2" />
                        <asp:RangeValidator ID="RangeValidator2" runat="server"
                            ControlToValidate="txtDtFrom2" ErrorMessage="Invalid Date"
                            Type="Date" MinimumValue="01/01/1900" MaximumValue="01/01/2100"
                            Display="Dynamic" ForeColor="Red" Font-Bold="true"></asp:RangeValidator>
                    </td>
                    <td class="tdbox">
                        <asp:TextBox ID="txtDtTo2" runat="server" Text='<%# Bind("Residence_ToDate2") %>' CssClass="txtitem"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server"
                            ErrorMessage="*This field is required" ValidationGroup="ResVG"
                            ControlToValidate="txtDtTo2" Display="Dynamic" Font-Bold="True" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                        <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" BehaviorID="txtDtTo2_MaskedEditExtender"
                            Mask=" 99/99/9999" MaskType="Date" ClearMaskOnLostFocus="False"
                            ErrorTooltipEnabled="true" AutoComplete="False" TargetControlID="txtDtTo2" />
                        <asp:RangeValidator ID="RangeValidator3" runat="server"
                            ControlToValidate="txtDtTo2" ErrorMessage="Invalid Date"
                            Type="Date" MinimumValue="01/01/1900" MaximumValue="01/01/2100"
                            Display="Dynamic" ForeColor="Red" Font-Bold="true"></asp:RangeValidator>
                    </td>
                    <td class="tdbox">
                        <asp:TextBox ID="txtAddress2" runat="server" Text='<%# Bind("ResidenceAddress2") %>' CssClass="txtitem"></asp:TextBox></td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server"
                            ErrorMessage="*This field is required" ValidationGroup="ResVG"
                            ControlToValidate="txtAddress2" Display="Dynamic" Font-Bold="True" ForeColor="Red">
                        </asp:RequiredFieldValidator></td>
                </tr>
                <tr id="TableRow2" runat="server">
                    <td class="tdbox">
                        <asp:TextBox ID="txtDtFrom3" runat="server" Text='<%# Bind("Residence_FromDate3") %>' CssClass="txtitem"></asp:TextBox></td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server"
                            ErrorMessage="*This field is required" ValidationGroup="ResVG"
                            ControlToValidate="txtDtFrom3" Display="Dynamic" Font-Bold="True" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                        <asp:MaskedEditExtender ID="MaskedEditExtender5" runat="server" BehaviorID="txtDtFrom3_MaskedEditExtender"
                            Mask=" 99/99/9999" MaskType="Date" ClearMaskOnLostFocus="False"
                            ErrorTooltipEnabled="true" AutoComplete="False" TargetControlID="txtDtFrom3" />
                        <asp:RangeValidator ID="RangeValidator4" runat="server"
                            ControlToValidate="txtDtFrom3" ErrorMessage="Invalid Date"
                            Type="Date" MinimumValue="01/01/1900" MaximumValue="01/01/2100"
                            Display="Dynamic" ForeColor="Red" Font-Bold="true"></asp:RangeValidator>
                    </td>
                    <td class="tdbox">
                        <asp:TextBox ID="txtDtTo3" runat="server" Text='<%# Bind("Residence_ToDate3") %>' CssClass="txtitem"></asp:TextBox></td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server"
                            ErrorMessage="*This field is required" ValidationGroup="ResVG"
                            ControlToValidate="txtDtTo3" Display="Dynamic" Font-Bold="True" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                        <asp:MaskedEditExtender ID="MaskedEditExtender6" runat="server" BehaviorID="txtDtTo3_MaskedEditExtender"
                            Mask=" 99/99/9999" MaskType="Date" ClearMaskOnLostFocus="False"
                            ErrorTooltipEnabled="true" AutoComplete="False" TargetControlID="txtDtTo3" />
                        <asp:RangeValidator ID="RangeValidator5" runat="server"
                            ControlToValidate="txtDtTo3" ErrorMessage="Invalid Date"
                            Type="Date" MinimumValue="01/01/1900" MaximumValue="01/01/2100"
                            Display="Dynamic" ForeColor="Red" Font-Bold="true"></asp:RangeValidator>
                    </td>
                    <td class="tdbox">
                        <asp:TextBox ID="txtAddress3" runat="server" Text='<%# Bind("ResidenceAddress3") %>' CssClass="txtitem"></asp:TextBox></td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server"
                            ErrorMessage="*This field is required" ValidationGroup="ResVG"
                            ControlToValidate="txtAddress3" Display="Dynamic" Font-Bold="True" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
            <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Save and Continue " ValidationGroup="ResVG" OnClick="btnSvForLater_Click" />
            &nbsp;<asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Finish Later" OnClientClick="return confirm('Are you sure you want to quit?');" OnClick="InsertCancelButton_Click" />
        </EditItemTemplate>
        
    </asp:FormView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>"
        SelectCommand="SELECT ID, Address, HomePhone, WorkPhone, EmpConNo, AlternateNo, Residence_FromDate1, Residence_ToDate1, ResidenceAddress1, CreatedDate, Yr_TN_Residence_YrMon, Residence_FromDate2, Residence_ToDate2, Residence_FromDate3, Residence_ToDate3, ResidenceAddress2, ResidenceAddress3, Country, State, Zipcode, Fname, MiddleInt, Lname, City, UserName, Password, CreatedDate FROM OAQ.ApplicantQuestionaire WHERE (ID = @PerID)"
        UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET Fname = @Fname, MiddleInt = @MiddleInt, Lname = @Lname, Address = @Address, HomePhone = @HOMEPHONE, WorkPhone = @WorkPhone, EmpConNo = @EmpConNo, AlternateNo = @AlternateNo, Residence_FromDate1 = @Residence_FromDate1, Residence_ToDate1 = @Residence_ToDate1, ResidenceAddress1 = @ResidenceAddress1, Residence_FromDate2 = @Residence_FromDate2, Residence_ToDate2 = @Residence_ToDate2, Residence_FromDate3 = @Residence_FromDate3, Residence_ToDate3 = @Residence_ToDate3, ResidenceAddress2 = @ResidenceAddress2, ResidenceAddress3 = @ResidenceAddress3, Country = @Country, State = @State, Zipcode = @Zipcode, City = @City, CreatedDate=CONVERT (date, SYSDATETIME()) WHERE (ID = @PerID)">
<%--        InsertCommand="INSERT INTO OAQ.ApplicantQuestionaire(Address, HomePhone, WorkPhone, EmpConNo, AlternateNo, Residence_FromDate1, Residence_ToDate1, ResidenceAddress1, CreatedDate, Yr_TN_Residence_YrMon, Residence_FromDate2, Residence_ToDate2, Residence_FromDate3, Residence_ToDate3, ResidenceAddress2, ResidenceAddress3, Country, State, Zipcode, Fname, MiddleInt, Lname, City) VALUES (@Address, @HomePhone, @WorkPhone, @EmpConNo, @AlternateNo, @Residence_FromDate1, @Residence_ToDate1, @ResidenceAddress1, @CreatedDate, @Yr_TN_Residence_YrMon, @Residence_FromDate2, @Residence_ToDate2, @Residence_FromDate3, @Residence_ToDate3, @ResidenceAddress2, @ResidenceAddress3, @Country, @State, @Zipcode, @Fname, @MiddleInt, @Lname, @City)"--%>
       <%-- <InsertParameters>
            <asp:Parameter Name="Address" />
            <asp:Parameter Name="HomePhone" />
            <asp:Parameter Name="WorkPhone" />
            <asp:Parameter Name="EmpConNo" />
            <asp:Parameter Name="AlternateNo" />
            <asp:Parameter Name="Residence_FromDate1" />
            <asp:Parameter Name="Residence_ToDate1" />
            <asp:Parameter Name="ResidenceAddress1" />
            <asp:Parameter Name="CreatedDate" />
            <asp:Parameter Name="Yr_TN_Residence_YrMon" />
            <asp:Parameter Name="Residence_FromDate2" />
            <asp:Parameter Name="Residence_ToDate2" />
            <asp:Parameter Name="Residence_FromDate3" />
            <asp:Parameter Name="Residence_ToDate3" />
            <asp:Parameter Name="ResidenceAddress2" />
            <asp:Parameter Name="ResidenceAddress3" />
            <asp:Parameter Name="Country" />
            <asp:Parameter Name="State" />
            <asp:Parameter Name="Zipcode" />
            <asp:Parameter Name="Fname" />
            <asp:Parameter Name="MiddleInt" />
            <asp:Parameter Name="Lname" />
            <asp:Parameter Name="City" />
        </InsertParameters>--%>
        <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Fname" />
            <asp:Parameter Name="MiddleInt" />
            <asp:Parameter Name="Lname" />
            <asp:Parameter Name="Address" />
            <asp:Parameter Name="HOMEPHONE" />
            <asp:Parameter Name="WorkPhone" />
            <asp:Parameter Name="EmpConNo" />
            <asp:Parameter Name="AlternateNo" />
            <asp:Parameter Name="Residence_FromDate1" />
            <asp:Parameter Name="Residence_ToDate1" />
            <asp:Parameter Name="ResidenceAddress1" />
            <asp:Parameter Name="Residence_FromDate2" />
            <asp:Parameter Name="Residence_ToDate2" />
            <asp:Parameter Name="Residence_FromDate3" />
            <asp:Parameter Name="Residence_ToDate3" />
            <asp:Parameter Name="ResidenceAddress2" />
            <asp:Parameter Name="ResidenceAddress3" />
            <asp:Parameter Name="Country" />
            <asp:Parameter Name="State" />
            <asp:Parameter Name="Zipcode" />
            <asp:Parameter Name="City" />
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
    <br />
    <asp:Label ID="lblMsg" runat="server" Text="Label"></asp:Label>
</asp:Content>
<%--<ItemTemplate>
            <table id="table3" runat="server">
                <tr>
                    <td id="Td1" runat="server">First Name:
                        <asp:Label ID="FirstnameTextBox" runat="server" Text='<%# Eval("Fname") %>'></asp:Label>
                        <asp:Label ID="IDPer" runat="server" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>
                    </td>
                    <td></td>
                    <td></td>
                    <td id="Td2" runat="server">Middle Int:
                        <asp:Label ID="txtMidd" runat="server" Text='<%# Eval("[Middle Int]") %>'></asp:Label>
                    </td>
                    <td></td>
                    <td></td>
                    <td id="Td3" runat="server">Last Name:
                        <asp:Label ID="txtlname" runat="server" Text='<%# Eval("Lname") %>'></asp:Label>

                    </td>
                </tr>
                <tr>
                    <td>Street Address
                        <asp:Label ID="txtAddress" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                    </td>
                    <td></td>
                    <td></td>
                    <td>Country
                        <asp:Label ID="ddlCountry" runat="server" Text='<%# Eval("Country") %>'>
                        </asp:Label>
                    </td>
                    <td></td>
                    <td></td>
                    <td>City<asp:Label ID="txtCity" runat="server" Text='<%# Eval("City") %>'></asp:Label>
                    </td>
                    <td></td>
                    <td></td>
                    <td>State
                        <asp:Label ID="ddlState" runat="server" CssClass="ddlitem" Text='<%# Eval("State") %>'>
                        </asp:Label>
                    </td>
                    <td></td>
                    <td></td>
                    <td>Zipcode:
                        <asp:Label ID="ZipCodeTextBox" runat="server" CssClass="txtsmitem" Text='<%# Eval("Zipcode") %>' />
                    </td>
                </tr>
                <tr>
                    <td>Home Phone<asp:Label ID="HomePhoneTextBox" runat="server" Text='<%# Eval("HomePhone") %>'></asp:Label>
                    </td>
                    <td></td>
                    <td></td>
                    <td>Work Phone<asp:Label ID="TextBox3" runat="server" Text='<%# Eval("WorkPhone") %>'></asp:Label>
                    </td>
                    <td></td>
                    <td></td>
                    <td>Employment Contact No.<asp:Label ID="TextBox4" runat="server" Text='<%# Eval("EmpConNo") %>'></asp:Label>
                    </td>
                    <td></td>
                    <td></td>
                    <td>Alternate No<asp:Label ID="TextBox5" runat="server" Text='<%# Eval("AlternateNo") %>' Width="130px"></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            
            <table id="Table4" runat="server">
                <tr>
                <td style="white-space:nowrap">List chronologically your residences for the last three years(including address while attending school).</td>
                    </tr>
                <tr id="Tr3" runat="server">
                    <td id="Td6" runat="server">Dates From/To</td>
                    <td id="TableCell13" runat="server"></td>
                    <td id="Td9" runat="server">Address</td>
                </tr>
                <tr id="Tr4" runat="server">
                    <td id="Td10" runat="server">
                        <asp:Label ID="txtDtFrom1" runat="server" Text='<%# Eval("Residence_FromDate1") %>'></asp:Label>
                    </td>
                    <td></td>
                    <td id="TableCell14" runat="server">
                        <asp:Label ID="txtDtTo1" runat="server" Text='<%# Eval("Residence_ToDate1") %>'></asp:Label>
                    </td>
                    <td></td>
                    <td id="TableCell15" runat="server">
                        <asp:Label ID="txtAddress1" runat="server" Text='<%# Eval("ResidenceAddress1") %>'></asp:Label>
                    </td>
                </tr>
                <tr id="TableRow3" runat="server">
                    <td id="TableCell16" runat="server">
                        <asp:Label ID="txtDtFrom2" runat="server" Text='<%# Eval("Residence_FromDate2") %>'></asp:Label>
                    </td>
                    <td></td>
                    <td id="TableCell17" runat="server">
                        <asp:Label ID="txtDtTo2" runat="server" Text='<%# Eval("Residence_ToDate2") %>'></asp:Label>
                    </td>
                    <td></td>
                    <td id="TableCell18" runat="server">
                        <asp:Label ID="txtAddress2" runat="server" Text='<%# Eval("ResidenceAddress2") %>'></asp:Label>
                    </td>
                </tr>
                <tr id="TableRow4" runat="server">
                    <td id="TableCell19" runat="server">
                        <asp:Label ID="txtDtFrom3" runat="server" Text='<%# Eval("Residence_FromDate3") %>'></asp:Label>
                    </td>
                    <td></td>
                    <td id="TableCell20" runat="server">
                        <asp:Label ID="txtDtTo3" runat="server" Text='<%# Eval("Residence_ToDate3") %>'></asp:Label>
                    </td>
                    <td></td>
                    <td id="TableCell21" runat="server">
                        <asp:Label ID="txtAddress3" runat="server" Text='<%# Eval("ResidenceAddress3") %>'></asp:Label>
                    </td>
                </tr>
            </table>
            <asp:Button ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit" />
            &nbsp;
        </ItemTemplate>--%>