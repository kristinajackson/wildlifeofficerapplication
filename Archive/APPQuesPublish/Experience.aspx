﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Experience.aspx.cs" Inherits="Application_Questionaire.Experience" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <meta http-equiv="Content-Type" content="text/html" />
    <p>
        <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Edit" DataKeyNames="ID" OnItemInserted="FormView1_ItemInserted" OnItemUpdated="FormView1_ItemUpdated">
            <EditItemTemplate>
                <asp:Label ID="IDLabel1" runat="server" Text='<%# Eval("ID") %>' Visible="false" />
                <p>
                    <table id="Table1" runat="server"> 
                        
                        <tr>
                            <td>
                    If you are a member of an organized wildlife or conservaton group(s), please list below. If none, then type N/A:
                              <%--  </p>--%>
               <%-- <p>--%>
                                <br />
                    <asp:TextBox ID="txtquesMem" runat="server" TextMode="MultiLine" CssClass="txtbox" Text='<%# Bind("GroupMem") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="txtquesMemReqValidator" runat="server" ErrorMessage="*This field is required" Font-Bold="true" ForeColor="Red" ValidationGroup="EXVG" Display="Dynamic" ControlToValidate="txtquesMem">
                    </asp:RequiredFieldValidator>
                    </td>
                            </tr>
<%--                </p>
                <p>--%>
                        <tr>
                <td>
                    10. List those experiences that have associated you with the out-of-doors. If none, then type N/A:
                   <%-- </p>--%>
                <%--<p>--%>
                    <br />
                    <asp:TextBox ID="txtques10" runat="server" TextMode="MultiLine" CssClass="txtbox" Text='<%# Bind("Outdoor_Exp") %>'></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtques10ReqValidator" runat="server" ErrorMessage="*This field is required" Font-Bold="true" ForeColor="Red" ValidationGroup="EXVG" Display="Dynamic" ControlToValidate="txtques10">
                    </asp:RequiredFieldValidator>
                    </td>
                            </tr>
               <%-- </p>
                <p>--%>
                        <tr>
                <td>
                    11.What is the most responsible position that you may have held and how do you think it assisted you 
                    in preparation for the position for which you are applying? If none, then type N/A
                    <%--</p>--%>
                <%--<p>--%>
                    <br />
                    <asp:TextBox ID="txtques11" runat="server" TextMode="MultiLine" CssClass="txtbox" Text='<%# Bind("Responsible_Pos") %>'></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtques11ReqValidator" runat="server" ErrorMessage="*This field is required" Font-Bold="true" ForeColor="Red" ValidationGroup="EXVG" Display="Dynamic" ControlToValidate="txtques11">
                    </asp:RequiredFieldValidator>
                    </td>
                            </tr>
              <%--  </p>
                <p>--%>
                        <tr>
                <td>
                    12.Do you have any experience in wildlife management, either during or after college? If none, then type N/A
                    <%--</p>--%>
                <%--<p>--%>
                    <br />
                    <asp:TextBox ID="txtques12" runat="server" TextMode="MultiLine" CssClass="txtbox" Text='<%# Bind("Manage_Exp") %>'></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtques12ReqValidator" runat="server" ErrorMessage="*This field is required" Font-Bold="true" ForeColor="Red" ValidationGroup="EXVG" Display="Dynamic" ControlToValidate="txtques12">
                    </asp:RequiredFieldValidator>
                    </td>
                            </tr>
               <%-- </p>--%>
                 <%--<br /> &nbsp;--%>
                
                </table>
                <asp:Button ID="btnPrevious" runat="server" Text="Go Back"   PostBackUrl="~/CategoriesEssay.aspx"  />
                         <%--<asp:Button ID="btnNext" runat="server" Text="Save and Continue" ValidationGroup="EXVG"  PostBackUrl="~/Wildlife Officer Reason.aspx" OnClick="btnNext_Click" />
                <asp:Button ID="btnSvForLater" runat="server" Text="Finish Later"  />
                 <asp:Button ID="btnQuit" runat="server" Text="Quit" />--%>
                <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Save and Continue" ValidationGroup="EXVG" />
                &nbsp;<asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Finish Later" OnClientClick="return confirm('Are you sure you want to quit?');" OnClick="UpdateCancelButton_Click" />
            </EditItemTemplate>
            <%--<InsertItemTemplate>
                <p>
                    If you are a member of an organized wildlife or conservaton group(s), please list below:</p>
                <p>
                    <asp:TextBox ID="txtquesMem" runat="server" Height="53px" Width="512px" Text='<%# Bind("GroupMem") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="txtquesMemReqValidator" runat="server" ErrorMessage="*This field is required" Font-Bold="true" ForeColor="Red" ValidationGroup="EXVG" Display="Dynamic" ControlToValidate="txtquesMem">
                    </asp:RequiredFieldValidator>
                </p>
                <p>
                    10. List those experiences that have associated you with the out-of-doors:</p>
                <p>
                    <asp:TextBox ID="txtques10" runat="server" Height="53px" Width="512px" Text='<%# Bind("Outdoor_Exp") %>'></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtques10ReqValidator" runat="server" ErrorMessage="*This field is required" Font-Bold="true" ForeColor="Red" ValidationGroup="EXVG" Display="Dynamic" ControlToValidate="txtques10">
                    </asp:RequiredFieldValidator>
                </p>
                <p>
                    11.What is the most responsible position that you may have held and how do you think it assisted you in preparation for the position for which you are applying?</p>
                <p>
                    <asp:TextBox ID="txtques11" runat="server" Height="53px" Width="512px" Text='<%# Bind("Responsible_Pos") %>'></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtques11ReqValidator" runat="server" ErrorMessage="*This field is required" Font-Bold="true" ForeColor="Red" ValidationGroup="EXVG" Display="Dynamic" ControlToValidate="txtques11">
                    </asp:RequiredFieldValidator>
                </p>
                <p>
                    12.Do you have any experience in wildlife management, either during or after college?</p>
                <p>
                    <asp:TextBox ID="txtques12" runat="server" Height="53px" Width="512px" Text='<%# Bind("Manage_Exp") %>'></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtques12ReqValidator" runat="server" ErrorMessage="*This field is required" Font-Bold="true" ForeColor="Red" ValidationGroup="EXVG" Display="Dynamic" ControlToValidate="txtques12">
                    </asp:RequiredFieldValidator>
                </p>
                 <br /> &nbsp;
                <asp:Button ID="btnPrevious" runat="server" Text="Go Back"   PostBackUrl="~/CategoriesEssay.aspx"  />
                <asp:Button ID="btnNext" runat="server" Text="Save and Continue" ValidationGroup="EXVG"  PostBackUrl="~/Wildlife Officer Reason.aspx"  />
                <asp:Button ID="btnSvForLater" runat="server" Text="Finish Later"  />
                 <asp:Button ID="btnQuit" runat="server" Text="Quit" />
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
                &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
            </InsertItemTemplate>--%>
            <ItemTemplate>
                ID:
                <asp:Label ID="IDLabel" runat="server" Text='<%# Eval("ID") %>' />
                <br />
                GroupMem:
                <asp:Label ID="GroupMemLabel" runat="server" Text='<%# Eval("GroupMem") %>' />
                <br />
                Outdoor_Exp:
                <asp:Label ID="Outdoor_ExpLabel" runat="server" Text='<%# Eval("Outdoor_Exp") %>' />
                <br />
                Responsible_Pos:
                <asp:Label ID="Responsible_PosLabel" runat="server" Text='<%# Eval("Responsible_Pos") %>' />
                <br />
                Manage_Exp:
                <asp:Label ID="Manage_ExpLabel" runat="server" Text='<%# Eval("Manage_Exp") %>' />
                <br />
                <asp:Button ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit" />
                &nbsp;<asp:Button ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New" />
            </ItemTemplate>
        </asp:FormView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>" 
            SelectCommand="SELECT ID, GroupMem, Outdoor_Exp, Responsible_Pos, Manage_Exp FROM OAQ.ApplicantQuestionaire  WHERE (ID = @PerID)" 
            UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET GroupMem = @GroupMem, Outdoor_Exp = @Outdoor_Exp, Responsible_Pos = @Responsible_Pos, Manage_Exp = @Manage_Exp WHERE ID=@PerID">
           <%-- InsertCommand="INSERT INTO OAQ.ApplicantQuestionaire(GroupMem, Outdoor_Exp, Responsible_Pos, Manage_Exp) VALUES (@GroupMem, @Outdoor_Exp, @Responsible_Pos, @Manage_Exp)" --%>
           <%-- <InsertParameters>
                <asp:Parameter Name="GroupMem" />
                <asp:Parameter Name="Outdoor_Exp" />
                <asp:Parameter Name="Responsible_Pos" />
                <asp:Parameter Name="Manage_Exp" />
            </InsertParameters>--%>
            <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="GroupMem" />
                <asp:Parameter Name="Outdoor_Exp" />
                <asp:Parameter Name="Responsible_Pos" />
                <asp:Parameter Name="Manage_Exp" />
                <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>
</asp:Content>
