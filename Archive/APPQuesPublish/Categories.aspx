﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Categories.aspx.cs" Inherits="Application_Questionaire.Categories" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="main" ContentPlaceHolderID="main" runat="server">
    <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Edit" DataKeyNames="ID" OnItemInserted="FormView1_ItemInserted" OnItemUpdated="FormView1_ItemUpdated">
        <EditItemTemplate>
      Please Check your experience or ability in the following categories:<br />
            <br />
            Farming<asp:RequiredFieldValidator ID="rboFarmReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboFarm" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboFarm" runat="server" CellSpacing="50"  RepeatLayout="Flow" Text='<%# Bind("Farming") %>' >
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            Forestry<asp:RequiredFieldValidator ID="rboForestReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboForest" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboForest" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("Forestry") %>' >
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            Animal Husbandry <asp:RequiredFieldValidator ID="rboAnihusReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboAnihus" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />   
         <asp:RadioButtonList ID="rboAnihus" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("AnimalHusb") %>' SelectedValue='<%# Bind("AnimalHusb") %>'>
               <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
       
            <br />
            <br />
            Commercial Fishing<asp:RequiredFieldValidator ID="rboComFishReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboComFish" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />   
         <asp:RadioButtonList ID="rboComFish" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("Comm_Fish") %>' SelectedValue='<%# Bind("Comm_Fish") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            Small Gas Engines<asp:RequiredFieldValidator ID="rboSmGsEngReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboSmGsEng" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
         <br />    
        <asp:RadioButtonList ID="rboSmGsEng" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("Sm_Gas_Eng") %>' SelectedValue='<%# Bind("Sm_Gas_Eng") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            Automotive Repair<asp:RequiredFieldValidator ID="rboAutoRepReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboAutoRep" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboAutoRep" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("AutoRepair") %>' SelectedValue='<%# Bind("AutoRepair") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            Boat Operation<asp:RequiredFieldValidator ID="rboBtOpReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboBtOp" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboBtOp" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("BoatOper") %>' SelectedValue='<%# Bind("BoatOper") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            Hunting<asp:RequiredFieldValidator ID="rboHuntReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboHunt" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboHunt" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("Hunting") %>' SelectedValue='<%# Bind("Hunting") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            Fishing <asp:RequiredFieldValidator ID="rboFishReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboFish" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboFish" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("Fishing") %>' SelectedValue='<%# Bind("Fishing") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
       
            <br />
            <br />
            Camping<asp:RequiredFieldValidator ID="rboCampReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboCamp" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboCamp" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("Camping") %>' SelectedValue='<%# Bind("Camping") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            Swimming<asp:RequiredFieldValidator ID="rboSwimRequiredFieldValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboSwim" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboSwim" runat="server" CellSpacing="50"  RepeatLayout="Flow" Text='<%# Bind("Swimming") %>' SelectedValue='<%# Bind("Swimming") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
        Special Animal Inventory<asp:RequiredFieldValidator ID="rboSpAnInvReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboSpAnInv" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator><br />
            <asp:RadioButtonList ID="rboSpAnInv" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("Speci_Animal_Inv") %>' SelectedValue='<%# Bind("Speci_Animal_Inv") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            Fish Identification(ID)<asp:RequiredFieldValidator ID="rboFTDReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboFTD" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboFTD" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("FishID") %>' SelectedValue='<%# Bind("FishID") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            Native Birds(ID) <asp:RequiredFieldValidator ID="rboNatBrdReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboNatBrd" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboNatBrd" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("NavBirdsID") %>' SelectedValue='<%# Bind("NavBirdsID") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
       
            <br />
            <br />
            Native Mammls(ID)<asp:RequiredFieldValidator ID="rboNatMamReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboNatMam" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboNatMam" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("NavMamID") %>' SelectedValue='<%# Bind("NavMamID") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            Native Reptiles(ID)<asp:RequiredFieldValidator ID="rboRepReqValidator1" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboRep" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboRep" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("NavRepID") %>' SelectedValue='<%# Bind("NavRepID") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
        Native Amphibians(ID)<asp:RequiredFieldValidator ID="rboNatAmpReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboNatAmp" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboNatAmp" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("NavAmp") %>' SelectedValue='<%# Bind("NavAmp") %>'>
               <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            Air Boat<asp:RequiredFieldValidator ID="rboAirbtReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboAirbt" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboAirbt" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("AirBoat") %>' SelectedValue='<%# Bind("AirBoat") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            Outboard Motors<asp:RequiredFieldValidator ID="rBOBMtrsReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rBOBMtrs" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rBOBMtrs" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("OutBrdMotors") %>' SelectedValue='<%# Bind("OutBrdMotors") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            4-Wheel Drive Vehicle<asp:RequiredFieldValidator ID="rbwheelReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rbwheel" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rbwheel" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("FourWheelDri") %>' SelectedValue='<%# Bind("FourWheelDri") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            Farm Tractor<asp:RequiredFieldValidator ID="rboFrmTracReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboFrmTrac" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboFrmTrac" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("FarmTrac") %>' SelectedValue='<%# Bind("FarmTrac") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            Bulldozer<asp:RequiredFieldValidator ID="rboBulldozReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboBulldoz" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboBulldoz" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("Bulldozer") %>' SelectedValue='<%# Bind("Bulldozer") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            Chain Saw<asp:RequiredFieldValidator ID="rboChnSawReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboChnSaw" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br /> 
        <asp:RadioButtonList ID="rboChnSaw" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("ChainSaw") %>' SelectedValue='<%# Bind("ChainSaw") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            Rifle<asp:RequiredFieldValidator ID="rboRifleReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboRifle" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboRifle" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("Rifle") %>' SelectedValue='<%# Bind("Rifle") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            Pistol<asp:RequiredFieldValidator ID="rboPistolReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboPistol" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboPistol" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("Pistol") %>' SelectedValue='<%# Bind("Pistol") %>'>
               <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
            Shotgun<asp:RequiredFieldValidator ID="rboStGunReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboStGun" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboStGun" runat="server" CellSpacing="50" RepeatLayout="Flow"  Text='<%# Bind("ShotGun") %>' SelectedValue='<%# Bind("ShotGun") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
            <br />
            <br />
        Special Computer Training<asp:RequiredFieldValidator ID="rboSpComTrnReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboSpComTrn" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
        <br />
            <asp:RadioButtonList ID="rboSpComTrn" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("Spec_Com_Train") %>' SelectedValue='<%# Bind("Spec_Com_Train") %>'>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem  Value="0">None</asp:ListItem>
                <asp:ListItem  Value="1">Some</asp:ListItem>
                <asp:ListItem  Value="2">Avg</asp:ListItem>
                <asp:ListItem  Value="3">Above Avg</asp:ListItem>
                <asp:ListItem  Value="4">Expert</asp:ListItem>
            </asp:RadioButtonList>
        
           <br /> 
            <br />
        <asp:Button ID="Button1" runat="server" Text="Go Back"   PostBackUrl="~/Applicant.aspx" OnClick="btnPrevious_Click" />
<%--        <asp:Button ID="Button2" runat="server" Text="Save and Continue" ValidationGroup="APCG"  PostBackUrl="~/CategoriesEssay.aspx" />
        <asp:Button ID="Button3" runat="server" Text="Finish Later"  />  --%>  
<%--        <asp:Button ID="btnQuit" runat="server" Text="Quit"  />--%>
            <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Save and Continue" ValidationGroup="APCG" />
&nbsp;<asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Finish Later" OnClientClick="return confirm('Are you sure you want to quit?');" OnClick="UpdateCancelButton_Click" />
    </EditItemTemplate>
        <ItemTemplate>
            Farming:
            <asp:Label ID="FarmingLabel" runat="server" Text='<%# Eval("Farming") %>' />
            <br />
            Forestry:
            <asp:Label ID="ForestryLabel" runat="server" Text='<%# Eval("Forestry") %>' />
            <br />
            AnimalHusb:
            <asp:Label ID="AnimalHusbLabel" runat="server" Text='<%# Eval("AnimalHusb") %>' />
            <br />
            Comm_Fish:
            <asp:Label ID="Comm_FishLabel" runat="server" Text='<%# Eval("Comm_Fish") %>' />
            <br />
            Sm_Gas_Eng:
            <asp:Label ID="Sm_Gas_EngLabel" runat="server" Text='<%# Eval("Sm_Gas_Eng") %>' />
            <br />
            AutoRepair:
            <asp:Label ID="AutoRepairLabel" runat="server" Text='<%# Eval("AutoRepair") %>' />
            <br />
            BoatOper:
            <asp:Label ID="BoatOperLabel" runat="server" Text='<%# Eval("BoatOper") %>' />
            <br />
            Hunting:
            <asp:Label ID="HuntingLabel" runat="server" Text='<%# Eval("Hunting") %>' />
            <br />
            Fishing:
            <asp:Label ID="FishingLabel" runat="server" Text='<%# Eval("Fishing") %>' />
            <br />
            Camping:
            <asp:Label ID="CampingLabel" runat="server" Text='<%# Eval("Camping") %>' />
            <br />
            Swimming:
            <asp:Label ID="SwimmingLabel" runat="server" Text='<%# Eval("Swimming") %>' />
            <br />
            Speci_Animal_Inv:
            <asp:Label ID="Speci_Animal_InvLabel" runat="server" Text='<%# Eval("Speci_Animal_Inv") %>' />
            <br />
            FishID:
            <asp:Label ID="FishIDLabel" runat="server" Text='<%# Eval("FishID") %>' />
            <br />
            NavBirdsID:
            <asp:Label ID="NavBirdsIDLabel" runat="server" Text='<%# Eval("NavBirdsID") %>' />
            <br />
            NavMamID:
            <asp:Label ID="NavMamIDLabel" runat="server" Text='<%# Eval("NavMamID") %>' />
            <br />
            NavRepID:
            <asp:Label ID="NavRepIDLabel" runat="server" Text='<%# Eval("NavRepID") %>' />
            <br />
            NavAmp:
            <asp:Label ID="NavAmpLabel" runat="server" Text='<%# Eval("NavAmp") %>' />
            <br />
            AirBoat:
            <asp:Label ID="AirBoatLabel" runat="server" Text='<%# Eval("AirBoat") %>' />
            <br />
            OutBrdMotors:
            <asp:Label ID="OutBrdMotorsLabel" runat="server" Text='<%# Eval("OutBrdMotors") %>' />
            <br />
            FourWheelDri:
            <asp:Label ID="FourWheelDriLabel" runat="server" Text='<%# Eval("FourWheelDri") %>' />
            <br />
            FarmTrac:
            <asp:Label ID="FarmTracLabel" runat="server" Text='<%# Eval("FarmTrac") %>' />
            <br />
            Bulldozer:
            <asp:Label ID="BulldozerLabel" runat="server" Text='<%# Eval("Bulldozer") %>' />
            <br />
            ChainSaw:
            <asp:Label ID="ChainSawLabel" runat="server" Text='<%# Eval("ChainSaw") %>' />
            <br />
            Rifle:
            <asp:Label ID="RifleLabel" runat="server" Text='<%# Eval("Rifle") %>' />
            <br />
            Pistol:
            <asp:Label ID="PistolLabel" runat="server" Text='<%# Eval("Pistol") %>' />
            <br />
            Shotgun:
            <asp:Label ID="ShotgunLabel" runat="server" Text='<%# Eval("Shotgun") %>' />
            <br />
            Spec_Com_Train:
            <asp:Label ID="Spec_Com_TrainLabel" runat="server" Text='<%# Eval("Spec_Com_Train") %>' />
            <br />
            ID:
           <asp:Label ID="IDPer" runat="server" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit" />
            &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New" />
        </ItemTemplate>
    </asp:FormView>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>"  
    InsertCommand="INSERT INTO OAQ.ApplicantQuestionaire(Farming, Forestry, AnimalHusb, Comm_Fish, Sm_Gas_Eng, AutoRepair, BoatOper, Hunting, Fishing, Camping, Swimming, Speci_Animal_Inv, FishID, NavMamID, NavBirdsID, NavRepID, NavAmp, AirBoat, OutBrdMotors, FourWheelDri, FarmTrac, Bulldozer, ChainSaw, Rifle, Pistol, Shotgun, @Spec_Com_Train) VALUES (@Farming, @Forestry, N'@AnimalHusb', @Comm_Fish, @Sm_Gas_Eng, @AutoRepair, @BoatOper, @Hunting, @Fishing, @Camping, @Swimming, @Speci_Animal_Inv, @FishID, @NavMamID, @NavBirdsID, @NavRepID, @NavAmp, @AirBoat, @OutBrdMotors, N'@FourWheelDri', @FarmTrac, @Bulldozer, @ChainSaw, @Rifle, @Pistol, @Shotgun, @Spec_Com_Train)"
     SelectCommand="SELECT Farming, Forestry, AnimalHusb, Comm_Fish, Sm_Gas_Eng, AutoRepair, BoatOper, Hunting, Fishing, Camping, Swimming, Speci_Animal_Inv, FishID, NavBirdsID, NavMamID, NavRepID, NavAmp, AirBoat, OutBrdMotors, FourWheelDri, FarmTrac, Bulldozer, ChainSaw, Rifle, Pistol, Shotgun, Spec_Com_Train, ID FROM OAQ.ApplicantQuestionaire WHERE (ID = @PerID)"
     UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET Farming = @Farming, Forestry = @Forestry, AnimalHusb = @AnimalHusb, Comm_Fish = @Comm_Fish, Sm_Gas_Eng = @Sm_Gas_Eng, AutoRepair = @AutoRepair, BoatOper = @BoatOper, Hunting = @Hunting, Fishing = @Fishing, Camping = @Camping, Swimming = @Swimming, Speci_Animal_Inv = @Speci_Animal_Inv, FishID = @FishID, NavBirdsID = @NavBirdsID, NavMamID = @NavMamID, NavRepID = @NavRepID, NavAmp = @NavAmp, AirBoat = @AirBoat, OutBrdMotors = @OutBrdMotors, FourWheelDri = @FourWheelDri, FarmTrac = @FarmTrac, Bulldozer = @Bulldozer, ChainSaw = @ChainSaw, Rifle = @Rifle, Pistol = @Pistol, Shotgun = @Shotgun, Spec_Com_Train = @Spec_Com_Train WHERE ID=@PerID">
    <InsertParameters>
        <asp:Parameter Name="Spec_Com_Train" />
        <asp:Parameter Name="Farming" />
        <asp:Parameter Name="Forestry" />
        <asp:Parameter Name="Comm_Fish" />
        <asp:Parameter Name="Sm_Gas_Eng" />
        <asp:Parameter Name="AutoRepair" />
        <asp:Parameter Name="BoatOper" />
        <asp:Parameter Name="Hunting" />
        <asp:Parameter Name="Fishing" />
        <asp:Parameter Name="Camping" />
        <asp:Parameter Name="Swimming" />
        <asp:Parameter Name="Speci_Animal_Inv" />
        <asp:Parameter Name="FishID" />
        <asp:Parameter Name="NavMamID" />
        <asp:Parameter Name="NavBirdsID" />
        <asp:Parameter Name="NavRepID" />
        <asp:Parameter Name="NavAmp" />
        <asp:Parameter Name="AirBoat" />
        <asp:Parameter Name="OutBrdMotors" />
        <asp:Parameter Name="FarmTrac" />
        <asp:Parameter Name="Bulldozer" />
        <asp:Parameter Name="ChainSaw" />
        <asp:Parameter Name="Rifle" />
        <asp:Parameter Name="Pistol" />
        <asp:Parameter Name="Shotgun" />
    </InsertParameters>
    <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
    <UpdateParameters>
        <asp:Parameter Name="Farming" />
        <asp:Parameter Name="Forestry" />
        <asp:Parameter Name="AnimalHusb" />
        <asp:Parameter Name="Comm_Fish" />
        <asp:Parameter Name="Sm_Gas_Eng" />
        <asp:Parameter Name="AutoRepair" />
        <asp:Parameter Name="BoatOper" />
        <asp:Parameter Name="Hunting" />
        <asp:Parameter Name="Fishing" />
        <asp:Parameter Name="Camping" />
        <asp:Parameter Name="Swimming" />
        <asp:Parameter Name="Speci_Animal_Inv" />
        <asp:Parameter Name="FishID" />
        <asp:Parameter Name="NavBirdsID" />
        <asp:Parameter Name="NavMamID" />
        <asp:Parameter Name="NavRepID" />
        <asp:Parameter Name="NavAmp" />
        <asp:Parameter Name="AirBoat" />
        <asp:Parameter Name="OutBrdMotors" />
        <asp:Parameter Name="FourWheelDri" />
        <asp:Parameter Name="FarmTrac" />
        <asp:Parameter Name="Bulldozer" />
        <asp:Parameter Name="ChainSaw" />
        <asp:Parameter Name="Rifle" />
        <asp:Parameter Name="Pistol" />
        <asp:Parameter Name="Shotgun" />
        <asp:Parameter Name="Spec_Com_Train" />
        <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
    </UpdateParameters>
</asp:SqlDataSource>
</asp:Content>
