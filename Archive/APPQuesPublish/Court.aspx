﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Court.aspx.cs" Inherits="Application_Questionaire.Court" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <p>
        <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Edit" DataKeyNames="ID" OnItemInserted="FormView1_ItemInserted" OnItemUpdated="FormView1_ItemUpdated">
            <EditItemTemplate>
                ID:
                <asp:Label ID="IDLabel1" runat="server" Text='<%# Eval("ID") %>' />
                <br />
                Have you ever been <u><b>convicted</b></u> of any crimes or infractions?
                <asp:DropDownList ID="rboConvict" runat="server" SelectedValue='<%# Bind("Convictied") %>'>
                    <asp:ListItem Enabled="False"></asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
                <%--<asp:RadioButtonList ID="rboConvict" runat="server" >
                    <asp:ListItem Enabled="False"></asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:RadioButtonList>--%>
                <asp:RequiredFieldValidator ID="rboConvictReqValidator" runat="server" ErrorMessage="*This field is required" Font-Bold="true" ForeColor="Red" ControlToValidate="rboConvict" ValidationGroup="CTVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                <br />
                If yes, please list below. List all such matters, even if there was no court appearance or the matter was settled by payment of fines or forfeiture of collateral.
                <br />
                <br />
                Please list the dates, places, Agency involved, the charge, final disposition and the details on each. If none, then please type N/A.<br />
                <asp:TextBox ID="txtCharge" runat="server" Height="52px" Text='<%# Bind("Explain_Charge") %>' Width="407px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtChargeReqValidator" runat="server" ErrorMessage="*This field is required" Font-Bold="true" ForeColor="Red" ControlToValidate="txtCharge" ValidationGroup="CTVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                <br />
                <br />
                List all traffic citations, but not parking tickets. If none, then please type N/A.<br />
                <asp:TextBox ID="txtCitation" runat="server" Height="52px" Text='<%# Bind("Traffic_Cit") %>' Width="407px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtCitationReqValidator" runat="server" ErrorMessage="*This field is required" Font-Bold="true" ForeColor="Red" ControlToValidate="txtCitation" ValidationGroup="CTVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                <br />
                <br />
                <br />
                Have you previously applied to any other state, county or other municipal law enforement agency or private security organization and if so, please list below. If none, the please type N/A.<br />
                <asp:TextBox ID="txtOthState" runat="server" Height="52px" Text='<%# Bind("OthStateEmploy") %>' Width="407px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtOthStateReqValidator" runat="server" ErrorMessage="*This field is required" Font-Bold="true" ForeColor="Red" ControlToValidate="txtOthState" ValidationGroup="CTVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                <br /> &nbsp;
                <asp:Button ID="BackButton" runat="server" Text="Go Back" PostBackUrl="~/Experience.aspx" />
                <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Save and Continue" ValidationGroup="CTVG" />
                &nbsp;<asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Finish Later" OnClientClick="return confirm('Are you sure you want to quit?');" OnClick="UpdateCancelButton_Click" />
            </EditItemTemplate>
            <ItemTemplate>
                ID:
                <asp:Label ID="IDLabel" runat="server" Text='<%# Eval("ID") %>' />
                <br />
                Convictied:
                <asp:Label ID="ConvictiedLabel" runat="server" Text='<%# Eval("Convictied") %>' />
                <br />
                Explain_Charge:
                <asp:Label ID="Explain_ChargeLabel" runat="server" Text='<%# Eval("Explain_Charge") %>' />
                <br />
                Traffic_Cit:
                <asp:Label ID="Traffic_CitLabel" runat="server" Text='<%# Eval("Traffic_Cit") %>' />
                <br />
                OthStateEmploy:
                <asp:Label ID="OthStateEmployLabel" runat="server" Text='<%# Eval("OthStateEmploy") %>' />
                <br />
                <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit" />
                &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New" />
            </ItemTemplate>
        </asp:FormView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>" 
            SelectCommand="SELECT ID, Convictied, Explain_Charge, Traffic_Cit, OthStateEmploy FROM OAQ.ApplicantQuestionaire WHERE (ID = @PerID)" 
            UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET Convictied = @Convictied, Explain_Charge = @Explain_Charge, Traffic_Cit = @Traffic_Cit, OthStateEmploy = @OthStateEmploy WHERE ID=@PerID">
            <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="Convictied" />
                <asp:Parameter Name="Explain_Charge" />
                <asp:Parameter Name="Traffic_Cit" />
                <asp:Parameter Name="OthStateEmploy" />
                 <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>

</asp:Content>
