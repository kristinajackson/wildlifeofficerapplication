﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="CategoriesEssay.aspx.cs" Inherits="Application_Questionaire1.CategoriesEssay" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
        <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Edit" DataKeyNames="ID" OnItemInserted="FormView1_ItemInserted" OnItemUpdated="FormView1_ItemUpdated">
            <EditItemTemplate>
                  </p>

    <p>
                    Identify the specific info with the experience or ability of the following. If none, then type N/A</p>
                <p>
                    Special Animal Inventory:</p>
                <p>
                    <asp:TextBox ID="txtSpcAnInv" runat="server" TextMode="MultiLine" CssClass="txtbox" Text='<%# Bind("SpecialAnimalInventoryEssay") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="txtSpcAnInvReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="txtSpcAnInv" SetFocusOnError="true" Font-Bold="true" ValidationGroup="CEVG"> 
        </asp:RequiredFieldValidator>
                </p>
                <p>
                    Native Amphibians:</p>
                <p>
                    <asp:TextBox ID="txtNativeAmp" runat="server" TextMode="MultiLine" CssClass="txtbox" Text='<%# Bind("NativeAmphibiansEssay") %>'></asp:TextBox>
                 <asp:RequiredFieldValidator ID="txtNativeAmpReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="txtNativeAmp" SetFocusOnError="true" Font-Bold="true" ValidationGroup="CEVG"> 
        </asp:RequiredFieldValidator>
                </p>
                <p>
                    &nbsp;</p>
                <p>
                    Special Computer Training:</p>
                <p>
                    <asp:TextBox ID="txtSpComTrain" runat="server" TextMode="MultiLine" CssClass="txtbox" Text='<%# Bind("SpecialComputerTrainingEssay") %>'></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtSpComTrainReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="txtSpComTrain" SetFocusOnError="true" Font-Bold="true" ValidationGroup="CEVG"> 
        </asp:RequiredFieldValidator>
                </p>
                <br /> &nbsp;
                <asp:Button ID="btnPrevious" runat="server" Text="Go Back" PostBackUrl="~/Categories.aspx" />
               <%--<asp:Button ID="btnNext" runat="server" Text="Save and Continue" ValidationGroup="CEVG" PostBackUrl="~/Court.aspx"   />--%>
            <%--     <asp:Button ID="btnSvForLater" runat="server" Text="Finish Later" />
            <asp:Button ID="btnQuit" runat="server" Text="Quit" /--%>
                <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Save and Continue"  ValidationGroup="CEVG" />
                &nbsp;<asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Finish Later" OnClientClick="return confirm('Are you sure you want to quit?');" OnClick="UpdateCancelButton_Click" />
            </EditItemTemplate>
          
            <ItemTemplate>
                SpecialAnimalInventoryEssay:
                <asp:Label ID="SpecialAnimalInventoryEssayLabel" runat="server" Text='<%# Eval("SpecialAnimalInventoryEssay") %>' />
                <br />
                NativeAmphibiansEssay:
                <asp:Label ID="NativeAmphibiansEssayLabel" runat="server" Text='<%# Eval("NativeAmphibiansEssay") %>' />
                <br />
                SpecialComputerTrainingEssay:
                <asp:Label ID="SpecialComputerTrainingEssayLabel" runat="server" Text='<%# Eval("SpecialComputerTrainingEssay") %>' />
                <br />
                ID:
                <asp:Label ID="IDLabel" runat="server" Text='<%# Eval("ID") %>' />
                <br />
                <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit" />
                &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New" />
            </ItemTemplate>
        </asp:FormView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>" 
            SelectCommand="SELECT SpecialAnimalInventoryEssay, NativeAmphibiansEssay, SpecialComputerTrainingEssay, ID FROM OAQ.ApplicantQuestionaire WHERE (ID = @PerID)" 
             UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET SpecialAnimalInventoryEssay = @SpecialAnimalInventoryEssay, NativeAmphibiansEssay = @NativeAmphibiansEssay, SpecialComputerTrainingEssay = @SpecialComputerTrainingEssay WHERE ID=@PerID">
           <%-- InsertCommand="INSERT INTO OAQ.ApplicantQuestionaire(SpecialAnimalInventoryEssay, NativeAmphibiansEssay, SpecialComputerTrainingEssay) VALUES (@SpecialAnimalInventoryEssay, @NativeAmphibiansEssay, @SpecialComputerTrainingEssay)"--%>
          <%--  <InsertParameters>
                <asp:Parameter Name="SpecialAnimalInventoryEssay" />
                <asp:Parameter Name="NativeAmphibiansEssay" />
                <asp:Parameter Name="SpecialComputerTrainingEssay" />
            </InsertParameters>--%>
                   <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="SpecialAnimalInventoryEssay" />
                <asp:Parameter Name="NativeAmphibiansEssay" />
                <asp:Parameter Name="SpecialComputerTrainingEssay" />
                <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
</asp:Content>
