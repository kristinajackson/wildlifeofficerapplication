<%@ Page Language="C#" MasterPageFile="~/Site2.master" AutoEventWireup="true" CodeFile="Logout.aspx.cs" Inherits="Logout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" Runat="Server">
    <h1>
        Logout</h1>
    <p>
        You have been logged out of the system. To log in, please return to the <a href="Login.aspx">
            login page</a>.</p>
</asp:Content>
