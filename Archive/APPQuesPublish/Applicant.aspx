﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Applicant.aspx.cs" Inherits="Application_Questionaire.Applicant" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="main" ContentPlaceHolderID="main" runat="server">

    <asp:FormView ID="FormView1" runat="server" DataKeyNames="ID" DataSourceID="SqlDataSource1" DefaultMode="Edit" OnItemInserted="FormView1_ItemInserted" OnItemUpdated="FormView1_ItemUpdated">
        <EditItemTemplate>
            <b>INSTRUCTIONS:</b> Answer all questions. If you need additional space to properly answer a question, you may attach addtional pages.If none, type N/A: Please sign the statement at the end of the questionaire.
           <br />
           
            <br />
            <asp:Label ID="IDPer" runat="server" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>
            <br />
            <table>
                <tr>
                    <td>
            1. Have you ever been interviewed by TWRA? 
               <asp:Dropdownlist ID="Dropdownlist1" runat="server"  SelectedValue='<%# Bind("InterviewedbyTWRA") %>'>
               <asp:ListItem></asp:ListItem>
               <asp:ListItem Text="Yes"></asp:ListItem>
               <asp:ListItem Text="No"></asp:ListItem>
           </asp:Dropdownlist>
                        If yes, where and when?&nbsp; If none, type N/A:<br /> <br />
            <asp:TextBox ID="txtques1" CssClass="txtbox" runat="server" Text='<%# Bind("When_Interviewed") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques1ReqValidator" runat="server" ErrorMessage="*This field is required*"
                ValidationGroup="INSVG" ControlToValidate="txtques1" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
                        </td>
                    </tr>
                <tr>
                    <td>
            2. Do you have any relatives presently employed by TWRA? <asp:Dropdownlist ID="rboques2" runat="server"  SelectedValue='<%# Bind("Relatives") %>'>
               <asp:ListItem></asp:ListItem>
               <asp:ListItem Text="Yes"></asp:ListItem>
               <asp:ListItem Text="No"></asp:ListItem>
           </asp:Dropdownlist>If yes, please list name(s) and relationship(s). If none, type N/A:
           
            <asp:RequiredFieldValidator ID="rboques2ReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="rboques2" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            &nbsp;<br />
            <br />
            <asp:TextBox ID="txtques2" CssClass="txtbox" runat="server" Text='<%# Bind("Explain_Relatives") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques2ReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="txtques2" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            <br />
                        </td>
                    </tr>
                <tr>
                    <td>
            3. Length of Tennessee residence?&nbsp;<asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("YrTNResidence") %>' Height="16px" Width="46px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*This field is required*"
                ValidationGroup="INSVG" ControlToValidate="TextBox3" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="*This field must be an Integer"
                ValidationGroup="INSVG" ControlToValidate="TextBox3" Display="Dynamic" Font-Bold="True" ForeColor="Red" ValidationExpression="\d+">
            </asp:RegularExpressionValidator>
           <asp:DropDownList ID="ddlyr" runat="server" SelectedValue='<%# Bind("Yr_TN_Residence_YrMon") %>'>
               <asp:ListItem></asp:ListItem>
               <asp:ListItem>Years</asp:ListItem>
               <asp:ListItem>Months</asp:ListItem>
           </asp:DropDownList>
            <asp:RequiredFieldValidator ID="ddlyrReqValidator" runat="server" ErrorMessage="*This field is required*"
                ValidationGroup="INSVG" ControlToValidate="ddlyr" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            If you do not reside in Tennessee, list your current State of residence.&nbsp; If none, type N/A:
           <br />
            <br />
            <asp:TextBox ID="txtlength" CssClass="txtbox" runat="server" Text='<%# Bind("List_Residence") %>' Font-Names="Rod" TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtlengthReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="txtlength" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            <br />
                        </td>
                    </tr>
                <tr>
                    <td>
            4.If you cureently hold a hunting/fishing license in Tennessee, please give the License Type and Identifying No.&nbsp; If none, type N/A:
            <br /> 
            <br />
           <asp:TextBox ID="txtques3" CssClass="txtbox" runat="server" Text='<%# Bind("[FishingHuntingTypeNo]") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques3ReqValidator" runat="server" ErrorMessage="*This field is required*"
                ValidationGroup="INSVG" ControlToValidate="txtques3" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            <br />
                        </td>
                    </tr>
                <tr>
                    <td>
            5. Do you have prior law enforcement experience? If none, type N/A:
           <asp:Dropdownlist ID="rboques5" runat="server" Text='<%# Bind("Enforcement_Exper") %>' SelectedValue='<%# Bind("Enforcement_Exper") %>'>
              <asp:ListItem></asp:ListItem>
               <asp:ListItem  Text="Yes"></asp:ListItem>
               <asp:ListItem  Text="No"></asp:ListItem>
           </asp:Dropdownlist>
            <asp:RequiredFieldValidator ID="rboques5ReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="rboques5" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            <br />
            <asp:TextBox ID="txtques5" CssClass="txtbox" runat="server" Text='<%# Bind("Explain_Enforcement") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques5ReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="txtques5" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
                        </td>
                    </tr>
                <tr>
                    <td>
            6. Do you have prior law enforcement training? If none, type N/A:
           <asp:DropDownList ID="rboques6" runat="server"  SelectedValue='<%# Bind("Enforcement_Train") %>'>
               <asp:ListItem></asp:ListItem>
               <asp:ListItem  Text="Yes"></asp:ListItem>
               <asp:ListItem  Text="No"></asp:ListItem>
           </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rboques6ReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="rboques6" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            <br />
            <asp:TextBox ID="txtques6" CssClass="txtbox" runat="server" Text='<%# Bind("Explain_EnforcementTrain") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques6ReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="txtques6" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
                        </td>
                    </tr>
                <tr>
                    <td>
            7. Other than regular college courses, list any advanced career development or law enforcement courses that you have taken.&nbsp;&nbsp; If none, type N/A:<br />
            <br />
            <asp:TextBox ID="txtques7" CssClass="txtbox" runat="server" Text='<%# Bind("Advanced_Courses") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques7ReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="txtques7" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
                        </td>
                    </tr>
                <tr>
                    <td>
            8. What college courses did you take that you feel would be beneficial to you as a Wildlife Officer?&nbsp; If none, type N/A:<br />
            <br />
            <asp:TextBox ID="txtques8" CssClass="txtbox" runat="server" Text='<%# Bind("Benefical_Courses") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques8ReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="txtques8" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
                          </td>
                    </tr>
                <tr>
                    <td>
            9. If you have hobbies, please list in order of preference.If none, type N/A:<br />
            <br />       
            &nbsp;<asp:TextBox ID="txtques9" CssClass="txtbox" runat="server" Text='<%# Bind("Hobbies") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques9ReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="txtques9" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            &nbsp;
            <br />
                        </td>
                    </tr>
                </table>
            <asp:Button ID="btnPrevious" runat="server" Text="Go Back" PostBackUrl="~/Residences.aspx" />
            <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Save and Continue " ValidationGroup="INSVG" OnClick="Button2_Click" />
            &nbsp;<asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Finish Later" OnClientClick="return confirm('Are you sure you want to quit?');" OnClick="btnQuit_Click"  />
        </EditItemTemplate>
        <ItemTemplate>
            ID:
            <asp:Label ID="IDLabel" runat="server" Text='<%# Eval("ID") %>' />
            <br />
            InterviewedbyTWRA:
            <asp:CheckBox ID="InterviewedbyTWRACheckBox" runat="server" Checked='<%# Bind("InterviewedbyTWRA") %>' Enabled="false" />
            <br />
            When_Interviewed:
            <asp:Label ID="When_InterviewedLabel" runat="server" Text='<%# Bind("When_Interviewed") %>' />
            <br />
            Relatives:
            <asp:CheckBox ID="RelativesCheckBox" runat="server" Checked='<%# Bind("Relatives") %>' Enabled="false" />
            <br />
            Explain_Relatives:
            <asp:Label ID="Explain_RelativesLabel" runat="server" Text='<%# Bind("Explain_Relatives") %>' />
            <br />
            YrTNResidence:
            <asp:Label ID="YrTNResidenceLabel" runat="server" Text='<%# Bind("YrTNResidence") %>' />
            <br />
            List_Residence:
            <asp:Label ID="List_ResidenceLabel" runat="server" Text='<%# Bind("List_Residence") %>' />
            <br />
            FishingHuntingTypeNo:
            <asp:Label ID="FishingHuntingTypeNoLabel" runat="server" Text='<%# Bind("FishingHuntingTypeNo") %>' />
            <br />
            Enforcement_Exper:
            <asp:CheckBox ID="Enforcement_ExperCheckBox" runat="server" Checked='<%# Bind("Enforcement_Exper") %>' Enabled="false" />
            <br />
            Explain_Enforcement:
            <asp:Label ID="Explain_EnforcementLabel" runat="server" Text='<%# Bind("Explain_Enforcement") %>' />
            <br />
            Enforcement_Train:
            <asp:CheckBox ID="Enforcement_TrainCheckBox" runat="server" Checked='<%# Bind("Enforcement_Train") %>' Enabled="false" />
            <br />
            Explain_EnforcementTrain:
            <asp:Label ID="Explain_EnforcementTrainLabel" runat="server" Text='<%# Bind("Explain_EnforcementTrain") %>' />
            <br />
            Advanced_Courses:
            <asp:Label ID="Advanced_CoursesLabel" runat="server" Text='<%# Bind("Advanced_Courses") %>' />
            <br />
            Benefical_Courses:
            <asp:Label ID="Benefical_CoursesLabel" runat="server" Text='<%# Bind("Benefical_Courses") %>' />
            <br />
            Hobbies:
            <asp:Label ID="HobbiesLabel" runat="server" Text='<%# Bind("Hobbies") %>' />
            <br />
            <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit" />
        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>" 
        SelectCommand="SELECT ID, InterviewedbyTWRA, When_Interviewed, Relatives, Explain_Relatives, YrTNResidence, List_Residence, FishingHuntingTypeNo, Enforcement_Exper, Explain_Enforcement, Enforcement_Train, Explain_EnforcementTrain, Advanced_Courses, Benefical_Courses, Hobbies, Yr_TN_Residence_YrMon FROM OAQ.ApplicantQuestionaire WHERE (ID = @PerID)" 
        UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET InterviewedbyTWRA = @InterviewedbyTWRA, When_Interviewed = @When_Interviewed, Relatives = @Relatives, YrTNResidence = @YrTNResidence, Explain_Relatives = @Explain_Relatives, List_Residence = @List_Residence, Enforcement_Exper = @Enforcement_Exper, FishingHuntingTypeNo = @FishingHuntingTypeNo, Explain_Enforcement = @Explain_Enforcement, Hobbies = @Hobbies, Benefical_Courses = @Benefical_Courses, Advanced_Courses = @Advanced_Courses, Enforcement_Train = @Enforcement_Train, Explain_EnforcementTrain = @Explain_EnforcementTrain, Yr_TN_Residence_YrMon = @Yr_TN_Residence_YrMon WHERE (ID = @PerID)">
<%--       --%>
        <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="InterviewedbyTWRA" />
            <asp:Parameter Name="When_Interviewed" />
            <asp:Parameter Name="Relatives" />
            <asp:Parameter Name="YrTNResidence" />
            <asp:Parameter Name="Explain_Relatives" />
            <asp:Parameter Name="List_Residence" />
            <asp:Parameter Name="Enforcement_Exper" />
            <asp:Parameter Name="FishingHuntingTypeNo" />
            <asp:Parameter Name="Explain_Enforcement" />
            <asp:Parameter Name="Hobbies" />
            <asp:Parameter Name="Benefical_Courses" />
            <asp:Parameter Name="Advanced_Courses" />
            <asp:Parameter Name="Enforcement_Train" />
            <asp:Parameter Name="Explain_EnforcementTrain" />
            <asp:Parameter Name="Yr_TN_Residence_YrMon" />
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <asp:Label ID="lblMsg" runat="server" Text="Label"></asp:Label>
      <%-- <b>INSTRUCTIONS:</b> Answer all questions. If you need additional space to properly answer a question, you may attach addtional pages.If none, type N/A: Please sign the statement at the end of the questionaire.
           <br />
            <br />
            <asp:Label ID="IDPer" runat="server" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>
            <br />--%>
 <%--           1. Have you ever been interviewed by TWRA? If yes, where and when?
            <asp:DropDownList ID="rboques7" runat="server" SelectedValue='<%# Bind("InterviewedbyTWRA") %>' Text='<%# Bind("InterviewedbyTWRA") %>' >
                <asp:ListItem></asp:ListItem>
                <asp:ListItem Value="1">Yes</asp:ListItem>
                <asp:ListItem Value="0">No</asp:ListItem>
            </asp:DropDownList>
           <asp:Dropdownlist ID="Dropdownlist1" runat="server" Text='<%# Bind("InterviewedbyTWRA") %>' SelectedValue='<%# Bind("InterviewedbyTWRA") %>'>
              <asp:ListItem></asp:ListItem>
               <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
               <asp:ListItem Value="0" Text="No"></asp:ListItem>
           </asp:Dropdownlist>
            <asp:RequiredFieldValidator ID="rboques7ReqValidator1" runat="server" ErrorMessage="*This field is required*"
                ValidationGroup="INSVG" ControlToValidate="rboques7" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            &nbsp;&nbsp;<br />
            <asp:TextBox ID="txtques1" CssClass="txtbox" runat="server" Text='<%# Bind("When_Interviewed") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques1ReqValidator" runat="server" ErrorMessage="*This field is required*"
                ValidationGroup="INSVG" ControlToValidate="txtques1" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            2. Do you have any relatives presently employed by TWRA? If yes, please list name(s) and relationship(s). If none, type N/A:
           <asp:Dropdownlist ID="rboques2" runat="server"  SelectedValue='<%# Bind("Relatives") %>' Text='<%# Bind("Relatives") %>'>
               <asp:ListItem Enabled="False"></asp:ListItem>
               <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
               <asp:ListItem Value="0" Text="No"></asp:ListItem>
           </asp:Dropdownlist>
            <asp:RequiredFieldValidator ID="rboques2ReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="rboques2" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            &nbsp;<br />
            <asp:TextBox ID="txtques2" CssClass="txtbox" runat="server" Text='<%# Bind("Explain_Relatives") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques2ReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="txtques2" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            3. Length of Tennessee residence?&nbsp;<asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("YrTNResidence") %>'></asp:TextBox>
            &nbsp;
           <asp:DropDownList ID="ddlyr" runat="server" SelectedValue='<%# Bind("Yr_TN_Residence_YrMon") %>' Text='<%# Bind("Yr_TN_Residence_YrMon") %>'>
               <asp:ListItem></asp:ListItem>
               <asp:ListItem>Years</asp:ListItem>
               <asp:ListItem>Months</asp:ListItem>
           </asp:DropDownList>
            <asp:RequiredFieldValidator ID="ddlyrReqValidator" runat="server" ErrorMessage="*This field is required*"
                ValidationGroup="INSVG" ControlToValidate="ddlyr" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            If you do not reside in Tennessee, list your current State of residence.&nbsp; If none, type N/A:
           <asp:TextBox ID="txtlength" CssClass="txtbox" runat="server" Text='<%# Bind("List_Residence") %>'></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtlengthReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="txtlength" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            <br />
            4.If you cureently hold a hunting/fishing license in Tennessee, please give the License Type and Identifying No.&nbsp; If none, type N/A:<br /> &nbsp;
           &nbsp;
           <asp:TextBox ID="txtques3" CssClass="txtbox" runat="server" Text='<%# Bind("[FishingHuntingTypeNo]") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques3ReqValidator" runat="server" ErrorMessage="*This field is required*"
                ValidationGroup="INSVG" ControlToValidate="txtlength" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            <br />
            5. Do you have prior law enforcement experience? If none, type N/A:
           <asp:Dropdownlist ID="rboques5" runat="server" Text='<%# Bind("Enforcement_Exper") %>' SelectedValue='<%# Bind("Enforcement_Exper") %>'>
               <asp:ListItem Enabled="False"></asp:ListItem>
               <asp:ListItem Value="1">Yes</asp:ListItem>
               <asp:ListItem Value="0">No</asp:ListItem>
           </asp:Dropdownlist>
            <asp:RequiredFieldValidator ID="rboques5ReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="rboques5" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            &nbsp;<br />
            <asp:TextBox ID="txtques5" CssClass="txtbox" runat="server" Text='<%# Bind("Explain_Enforcement") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques5ReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="txtques5" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            6. Do you have prior law enforcement training? If none, type N/A:
           <asp:DropDownList ID="rboques6" runat="server"  Text='<%# Bind("Enforcement_Train") %>' SelectedValue='<%# Bind("Enforcement_Train") %>'>
               <asp:ListItem Enabled="False"></asp:ListItem>
               <asp:ListItem Value="1">Yes</asp:ListItem>
               <asp:ListItem Value="0">No</asp:ListItem>
           </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rboques6ReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="rboques6" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            <asp:TextBox ID="txtques6" CssClass="txtbox" runat="server" Text='<%# Bind("Explain_EnforcementTrain") %>'></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques6ReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="txtques6" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            7. Other than regular college courses, list any advanced career development or law enforcement courses that you have taken.&nbsp;&nbsp;<br />
            <br />
            <asp:TextBox ID="txtques7" CssClass="txtbox" runat="server" Text='<%# Bind("Advanced_Courses") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques7ReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="txtques7" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            8. What college courses did you take that you feel would be beneficial to you as a Wildlife Officer?
           <br />
            <br />
            <asp:TextBox ID="txtques8" CssClass="txtbox" runat="server" Text='<%# Bind("Benefical_Courses") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques8ReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="txtques8" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            9. If you have hobbies, please list in order of preference.If none, type N/A:<br />
            <br />
            &nbsp;<asp:TextBox ID="txtques9" CssClass="txtbox" runat="server" Text='<%# Bind("Hobbies") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques9ReqValidator" runat="server" ErrorMessage="*This field is required"
                ValidationGroup="INSVG" ControlToValidate="txtques9" Display="Dynamic" Font-Bold="True" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            &nbsp;
           <asp:Button ID="btnPrevious" runat="server" Text="Go Back" PostBackUrl="~/Residences.aspx" />
            <%--<asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Save and Continue " ValidationGroup="INSVG" />--%>
<%--            &nbsp;<asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Save and Continue " ValidationGroup="INSVG" />
            <asp:Button ID="Button1" runat="server" CausesValidation="False" CommandName="Cancel" Text="Finish Later" />--%>
</asp:Content>
