﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="WildlifeOfficerReason.aspx.cs" Inherits="Application_Questionaire.WildlifeOfficerReason" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <p>
        <meta http-equiv="Content-Type" content="text/html" />
        <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Edit" OnItemInserted="FormView1_ItemInserted1" OnItemUpdated="FormView1_ItemUpdated" >
            <EditItemTemplate>
                ID:
                <asp:Label ID="IDLabel1" runat="server" Text='<%# Eval("ID") %>' />
<br />
     Why do you want to become a Wildlife Officer?<br />
                <asp:TextBox ID="txtWlOff" runat="server" Height="80px" Text='<%# Bind("Reason_WLO") %>' Width="348px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtWlOffReqValidator" runat="server" ErrorMessage="*This field is required" Font-Bold="true" ForeColor="Red" ControlToValidate="txtWlOff" ValidationGroup="WLOVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                <br />
                <br />
                What events or series of events caused you to make this decision?<br />
                <asp:TextBox ID="txtEvent" runat="server" Height="80px" Text='<%# Bind("Events_ForDecision") %>' Width="348px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtEventReqValidator" runat="server" ErrorMessage="*This field is required" Font-Bold="true" ForeColor="Red" ControlToValidate="txtEvent" ValidationGroup="WLOVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                <br />
                <br />
                What specific courses of actions have you taken to prepare yourself for a career as a Wildlife Officer?<br />
                <asp:TextBox ID="txtCourses" runat="server" Height="80px" Text='<%# Bind("CourseofAction") %>' Width="348px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtCoursesReqValidator" runat="server" ErrorMessage="*This field is required" Font-Bold="true" ForeColor="Red" ControlToValidate="txtCourses" ValidationGroup="WLOVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                <br />
                <br />
                   What specific qualities do you possess that would make you better Wildlife Officer than another applicant<br />
            <asp:TextBox ID="txtQualities" runat="server" Text='<%# Bind("Qualities") %>'></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtQualitiesReqValidator" runat="server" ErrorMessage="*This field is required" Font-Bold="true" ForeColor="Red" ControlToValidate="txtQualities" ValidationGroup="WLOVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                <br />
                <br />
                What is the major concer facing all of Tennessee wildlife and enviornmental resources today? As a wildlife officer representing the Agency, what impact do you think you will have in dealing with this concern<br />
                <asp:TextBox ID="txtConcern" runat="server" Text='<%# Bind("MajorConernforWL") %>'></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtConcernReqValidator" runat="server" ControlToValidate="txtConcern" Display="Dynamic" ErrorMessage="*This field is required" Font-Bold="true" ForeColor="Red" ValidationGroup="WLOVG">
                </asp:RequiredFieldValidator>
                <br />
                <asp:Label ID="lblMsg" runat="server" Text="Label"></asp:Label>
                <br /> &nbsp;
                <asp:Button ID="btnPrevious" runat="server" Text="Go Back"   PostBackUrl="~/Court.aspx" />
<%--                <asp:Button ID="btnSvForLater" runat="server" Text="Finish Later" />
                <asp:Button ID="btnQuit" runat="server" Text="Quit" OnClick="btnQuit_Click" />--%>
                <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Submit" ValidationGroup="WLOVG" />
                &nbsp;<asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Finish Later" OnClientClick="return confirm('Are you sure you want to quit?');" OnClick="UpdateCancelButton_Click" />
            </EditItemTemplate>
            <ItemTemplate>
                ID:
                <asp:Label ID="IDLabel" runat="server" Text='<%# Eval("ID") %>' />
                <br />
                Reason_WLO:
                <asp:Label ID="Reason_WLOLabel" runat="server" Text='<%# Eval("Reason_WLO") %>' />
                <br />
                Events_ForDecision:
                <asp:Label ID="Events_ForDecisionLabel" runat="server" Text='<%# Eval("Events_ForDecision") %>' />
                <br />
                Qualities:
                <asp:Label ID="QualitiesLabel" runat="server" Text='<%# Eval("Qualities") %>' />
                <br />
                CourseofAction:
                <asp:Label ID="CourseofActionLabel" runat="server" Text='<%# Eval("CourseofAction") %>' />
                <br />
                MajorConernforWL:
                <asp:Label ID="MajorConernforWLLabel" runat="server" Text='<%# Eval("MajorConernforWL") %>' />
                <br />
                <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit" />
            </ItemTemplate>
        </asp:FormView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>" 
            SelectCommand="SELECT ID, Reason_WLO, Events_ForDecision, Qualities, CourseofAction, MajorConernforWL FROM OAQ.ApplicantQuestionaire WHERE (ID = @PerID)" 
            UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET Reason_WLO = @Reason_WLO, Events_ForDecision = @Events_ForDecision, CourseofAction = @CourseofAction, Qualities = @Qualities, MajorConernforWL = @MajorConernforWL WHERE (ID = @PerID)">
            <UpdateParameters>
                <asp:Parameter Name="Reason_WLO" />
                <asp:Parameter Name="Events_ForDecision" />
                <asp:Parameter Name="CourseofAction" />
                <asp:Parameter Name="Qualities" />
                <asp:Parameter Name="MajorConernforWL" />
                <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
            </UpdateParameters>
            <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
        </asp:SqlDataSource>
    </p>
</asp:Content>
