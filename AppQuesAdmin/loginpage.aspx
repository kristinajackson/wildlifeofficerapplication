﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="loginpage.aspx.cs" Inherits="ApplicationQuestionaireAdmin.loginpage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <head>
        <title>Login</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />        
    </head>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
           <br />
    <br />
    <br /> 
    <div class="form-inline">
        <div class="col-xs-2">
    <label id="lblUserID" runat="server" for="txtUserID" >User ID:</label>  
    <asp:TextBox ID="txtUserID" runat="server" CssClass=" form-control"></asp:TextBox></div>
    <br />
    <br />
    <asp:Button ID="btnLogin" runat="server" Text="Login"  class="btn btn-success btn-md" OnClick="btnLogin_Click" />
    <br />
    <br />
    <asp:Label ID="lblMsg" runat="server" CssClass="txtitem" ForeColor="#CC0000"></asp:Label></div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</asp:Content>
