﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Logout.aspx.cs" Inherits="Application_Questionaire.Logout" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        var elements = document.getElementsByTagName("a");

        function preventBack() { window.history.forward(); }

        setTimeout("preventBack()", 0);

        window.onunload = function () { null };

        jQuery(document).ready(function($) {

            if (window.history && window.history.pushState) {

                window.history.pushState('forward', null, './#forward');

                $(window).on('popstate', function() {
                    alert('You can not go back to the system. Please log back in.');
                });

            }
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <div class="text-center">
<h1>Logout</h1>
        <br />
    <h4>
        You have been logged out of the system. To log in, please return to the <a href="Login.aspx" id="login">
            login page</a>.</h4></div>

</asp:Content>

