﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Experience.aspx.cs" Inherits="Application_Questionaire.Experience" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <style>
        .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        width: 300px;
        border: 3px solid #0DA9D0;
        border-radius: 12px;
        padding:0
      
    }
    .modalPopup .header
    {
        background-color: #2FBDF1;
        height: 30px;
        color: White;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
    }
    .modalPopup .body
    {
        min-height: 50px;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
    }
    .modalPopup .footer
    {
        padding: 6px;
    }
    .modalPopup .yes, .modalPopup .no
    {
        height: 23px;
        color: White;
        line-height: 23px;
        text-align: center;
        font-weight: bold;
        cursor: pointer;
        border-radius: 4px;
    }
    .modalPopup .yes
    {
        background-color: #2FBDF1;
        border: 1px solid #0DA9D0;
    }
    .modalPopup .no
    {
        background-color: #9F9F9F;
        border: 1px solid #5C5C5C;
    }
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
      
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
               <section class="container " id="section1">
                <div class="col-lg-15" style="margin-left: -5%">

                    <div class=" panel panel-default">
                        <br />
                        <div class="panel-heading">
                        </div>
                        <div>
                            <div class="container" style="margin-left: 5%">
                                <div class="panel-body">

                                          <h6 style="color: #FFFFFF">Session Idle:&nbsp;<span id="secondsIdle"></span>&nbsp;seconds.</h6>
                                    <asp:LinkButton ID="lnkFake" runat="server" />
                                    <asp:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkFake"
                                        OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                                    </asp:ModalPopupExtender>
                                    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                                        <div class="header">
                                            Session Expiring!
                                        </div>
                                        <div class="body">
                                            Your Session will expire in&nbsp;<span id="seconds"></span>&nbsp;seconds.<br />
                                            Are you still there?
                                        </div>
                                        <div class="footer" align="right">
                                            <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="yes" />
                                            <asp:Button ID="btnNo" runat="server" Text="No" CssClass="no" />
                                        </div>
                                    </asp:Panel>

            <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Edit" DataKeyNames="ID" OnItemInserted="FormView1_ItemInserted" OnItemUpdated="FormView1_ItemUpdated">
                <EditItemTemplate>

                    <asp:Label ID="IDLabel1" runat="server" Text='<%# Eval("ID") %>' Visible="false" />
                    <p>
                        <form>
                            <div class="container-fluid">
                                <div class="form-group">
                                    <label for="txtquesMem">If you are a member of an organized wildlife or conservaton group(s), please list below:</label>
                                    <asp:TextBox ID="txtquesMem" runat="server" TabIndex="1" Font-Size="Medium" TextMode="MultiLine" CssClass="  form-control" Rows="4" Text='<%# Bind("GroupMem") %>' ></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="txtquesMemReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ValidationGroup="EXVG" Display="Dynamic" ControlToValidate="txtquesMem">
                                    </asp:RequiredFieldValidator>
                                </div>
                                <div class="form-group">
                                    <label for="txtques10">10. List those experiences that have associated you with the out-of-doors:</label>
                                    <asp:TextBox ID="txtques10" runat="server" TabIndex="2" Font-Size="Medium" TextMode="MultiLine" CssClass="  form-control" Rows="4" Text='<%# Bind("Outdoor_Exp") %>' ></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="txtques10ReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ValidationGroup="EXVG" Display="Dynamic" ControlToValidate="txtques10">
                                    </asp:RequiredFieldValidator>
                                </div>
                                <div class="form-group">
                                    <label for="txtques11">
                                        11.What is the most responsible position that you may have held and how do you think it assisted you in preparation for the position for which you are applying?</label>
                                    <asp:TextBox ID="txtques11" runat="server" TabIndex="3" Font-Size="Medium" TextMode="MultiLine" CssClass="  form-control" Rows="4" Text='<%# Bind("Responsible_Pos") %>' ></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="txtques11ReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ValidationGroup="EXVG" Display="Dynamic" ControlToValidate="txtques11">
                                    </asp:RequiredFieldValidator>
                                </div>
                                <div class="form-group">
                                    <label for="txtques12">12.Do you have any experience in wildlife management, either during or after college? </label>
                                    <asp:TextBox ID="txtques12" runat="server" TabIndex="4" Font-Size="Medium" TextMode="MultiLine" Rows="4" CssClass="  form-control" Text='<%# Bind("Manage_Exp") %>' ></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="txtques12ReqValidator" runat="server"
                                        ErrorMessage="*If none, then please type N/A" Font-Bold="true" ForeColor="Red" ValidationGroup="EXVG"
                                        Display="Dynamic" ControlToValidate="txtques12">
                                    </asp:RequiredFieldValidator>
                                </div>
                                <asp:Label ID="div" runat="server" TabIndex="5"></asp:Label>



                                <br />
                                <br />
                                <asp:Button ID="btnPrevious" runat="server" Text="  Go Back  "  CssClass="btn btn-warning btn-lg" OnClick="SessionUpdate" />
                                <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="  Next Page  " ValidationGroup="EXVG" CssClass="btn btn-success btn-lg " OnClick="UpdateButton_Click" />
                                <asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="  Finish Later  " OnClientClick="return confirm('Are you sure you want to quit?');" OnClick="UpdateCancelButton_Click" class="btn btn-danger btn-lg" />

                            </div>
                        </form>
                </EditItemTemplate>
            </asp:FormView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   </section>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>"
        SelectCommand="SELECT ID, GroupMem, Outdoor_Exp, Responsible_Pos, Manage_Exp FROM OAQ.ApplicantQuestionaire  WHERE (ID = @PerID)"
        UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET GroupMem = @GroupMem, Outdoor_Exp = @Outdoor_Exp, Responsible_Pos = @Responsible_Pos, Manage_Exp = @Manage_Exp WHERE ID=@PerID">
        <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="GroupMem" />
            <asp:Parameter Name="Outdoor_Exp" />
            <asp:Parameter Name="Responsible_Pos" />
            <asp:Parameter Name="Manage_Exp" />
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>

     <script type="text/javascript">
         function SessionExpireAlert(timeout) {
             var seconds = timeout / 1000;
             document.getElementsByName("secondsIdle").innerHTML = seconds;
             document.getElementsByName("seconds").innerHTML = seconds;
             setInterval(function () {
                 seconds--;
                 document.getElementById("seconds").innerHTML = seconds;
                 document.getElementById("secondsIdle").innerHTML = seconds;
             }, 1000);
             setTimeout(function () {
                 //Show Popup before 20 seconds of timeout.
                 $find("mpeTimeout").show();
             }, timeout - 20 * 1000);
             setTimeout(function () {
                 window.location = "Login.aspx";
             }, timeout);
         };
         function ResetSession() {
             //Redirect to refresh Session.
             window.location = window.location.href;
         }
    </script>

</asp:Content>
