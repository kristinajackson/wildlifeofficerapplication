﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Categories.aspx.cs" Inherits="Application_Questionaire.Categories" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <style>
        .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        width: 300px;
        border: 3px solid #0DA9D0;
        border-radius: 12px;
        padding:0
      
    }
    .modalPopup .header
    {
        background-color: #2FBDF1;
        height: 30px;
        color: White;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
    }
    .modalPopup .body
    {
        min-height: 50px;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
    }
    .modalPopup .footer
    {
        padding: 6px;
    }
    .modalPopup .yes, .modalPopup .no
    {
        height: 23px;
        color: White;
        line-height: 23px;
        text-align: center;
        font-weight: bold;
        cursor: pointer;
        border-radius: 4px;
    }
    .modalPopup .yes
    {
        background-color: #2FBDF1;
        border: 1px solid #0DA9D0;
    }
    .modalPopup .no
    {
        background-color: #9F9F9F;
        border: 1px solid #5C5C5C;
    }
    </style>
    </asp:Content>
<asp:Content ID="main" ContentPlaceHolderID="main" runat="server">
        
       <section class="container " id="section1">
                <div class="col-lg-11" style="margin-left: -5%">

                    <div class=" panel panel-default">
                        <br />
                        <div class="panel-heading">
                        </div>
                        <div>
                            <div class="container" style="margin-left: 5%">
                                <div class="panel-body">
                                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

                                   <h6 style="color: #FFFFFF">Session Idle:&nbsp;<span id="secondsIdle"></span>&nbsp;seconds.</h6>
<asp:LinkButton ID="lnkFake" runat="server" />
<asp:ModalPopupExtender ID="mpeTimeout" BehaviorID ="mpeTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkFake"
    OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript = "ResetSession()">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
    <div class="header">
        Session Expiring!
    </div>
    <div class="body">
        Your Session will expire in&nbsp;<span id="seconds"></span>&nbsp;seconds.<br />
        Are you still there?
    </div>
    <div class="footer" align="right">
        <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="yes" />
        <asp:Button ID="btnNo" runat="server" Text="No" CssClass="no" />
    </div>
</asp:Panel>


    <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Edit" DataKeyNames="ID" OnItemInserted="FormView1_ItemInserted" OnItemUpdated="FormView1_ItemUpdated" Font-Size="Medium">
        <EditItemTemplate>
            <h2>Please Check your experience or ability in the following categories:</h2>
            <br />
            <br />
            <table>
                <tr>

                    <td class="tdbox" style="white-space: nowrap"><b>Farming</b></td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboFarmReqValidator" runat="server"
                            ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboFarm"
                            SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator></td>
                    <td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboFarm" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("Farming") %>' CssClass="ddlitem" TabIndex="1">
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>

                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Forestry</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboForestReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboForest" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboForest" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("Forestry") %>' CssClass="ddlitem" TabIndex="2">
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Animal Husbandry</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboAnihusReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboAnihus" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboAnihus" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("AnimalHusb") %>' TabIndex="3" SelectedValue='<%# Bind("AnimalHusb") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Commercial Fishing</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboComFishReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboComFish" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboComFish" runat="server" CellSpacing="50" RepeatLayout="Flow" CssClass="ddlitem" Text='<%# Bind("Comm_Fish") %>' TabIndex="4" SelectedValue='<%# Bind("Comm_Fish") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Small Gas Engines</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboSmGsEngReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboSmGsEng" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboSmGsEng" runat="server" CellSpacing="50" RepeatLayout="Flow" CssClass="ddlitem" Text='<%# Bind("Sm_Gas_Eng") %>' TabIndex="5" SelectedValue='<%# Bind("Sm_Gas_Eng") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Automotive Repair</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboAutoRepReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboAutoRep" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboAutoRep" runat="server" CellSpacing="50" RepeatLayout="Flow" CssClass="ddlitem" Text='<%# Bind("AutoRepair") %>' TabIndex="6" SelectedValue='<%# Bind("AutoRepair") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Boat Operation</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboBtOpReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboBtOp" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboBtOp" runat="server" CellSpacing="50" RepeatLayout="Flow" CssClass="ddlitem" Text='<%# Bind("BoatOper") %>' TabIndex="7" SelectedValue='<%# Bind("BoatOper") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Hunting</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboHuntReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboHunt" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboHunt" runat="server" CellSpacing="50" RepeatLayout="Flow" CssClass="ddlitem" Text='<%# Bind("Hunting") %>' TabIndex="8" SelectedValue='<%# Bind("Hunting") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Fishing</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboFishReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboFish" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">

                        <asp:RadioButtonList ID="rboFish" runat="server" CellSpacing="50" RepeatLayout="Flow" CssClass="ddlitem" Text='<%# Bind("Fishing") %>' TabIndex="9" SelectedValue='<%# Bind("Fishing") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Camping</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboCampReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboCamp" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboCamp" runat="server" CellSpacing="50" RepeatLayout="Flow" CssClass="ddlitem" Text='<%# Bind("Camping") %>' TabIndex="10" SelectedValue='<%# Bind("Camping") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Swimming </b>

                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboSwimRequiredFieldValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboSwim" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboSwim" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("Swimming") %>' TabIndex="11" SelectedValue='<%# Bind("Swimming") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Special Animal Inventory</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboSpAnInvReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboSpAnInv" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboSpAnInv" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("Speci_Animal_Inv") %>' TabIndex="12" SelectedValue='<%# Bind("Speci_Animal_Inv") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Fish Identification(ID)</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboFTDReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboFTD" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboFTD" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("FishID") %>' TabIndex="13" SelectedValue='<%# Bind("FishID") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Native Birds(ID)</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboNatBrdReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboNatBrd" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboNatBrd" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("NavBirdsID") %>' TabIndex="14" SelectedValue='<%# Bind("NavBirdsID") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Native Mammals(ID)</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboNatMamReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboNatMam" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboNatMam" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("NavMamID") %>' TabIndex="15" SelectedValue='<%# Bind("NavMamID") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Native Reptiles(ID)</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboRepReqValidator1" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboRep" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboRep" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("NavRepID") %>' TabIndex="16" SelectedValue='<%# Bind("NavRepID") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Native Amphibians(ID)</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboNatAmpReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboNatAmp" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboNatAmp" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("NavAmp") %>' TabIndex="17" SelectedValue='<%# Bind("NavAmp") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Air Boat</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboAirbtReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboAirbt" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboAirbt" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("AirBoat") %>' TabIndex="18" SelectedValue='<%# Bind("AirBoat") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Outboard Motors</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rBOBMtrsReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rBOBMtrs" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rBOBMtrs" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("OutBrdMotors") %>' TabIndex="19" SelectedValue='<%# Bind("OutBrdMotors") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>4-Wheel Drive Vehicle</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rbwheelReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rbwheel" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rbwheel" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("FourWheelDri") %>' TabIndex="20" SelectedValue='<%# Bind("FourWheelDri") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Farm Tractor</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboFrmTracReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboFrmTrac" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboFrmTrac" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("FarmTrac") %>' TabIndex="21" SelectedValue='<%# Bind("FarmTrac") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Bulldozer</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboBulldozReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboBulldoz" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboBulldoz" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("Bulldozer") %>' TabIndex="22" SelectedValue='<%# Bind("Bulldozer") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Chain Saw</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboChnSawReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboChnSaw" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboChnSaw" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("ChainSaw") %>' TabIndex="23" SelectedValue='<%# Bind("ChainSaw") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Rifle</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboRifleReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboRifle" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboRifle" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("Rifle") %>' TabIndex="24" SelectedValue='<%# Bind("Rifle") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Pistol</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboPistolReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboPistol" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboPistol" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("Pistol") %>' TabIndex="25" SelectedValue='<%# Bind("Pistol") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Shotgun</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboStGunReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboStGun" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboStGun" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("ShotGun") %>' TabIndex="26" SelectedValue='<%# Bind("ShotGun") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="tdbox" style="white-space: nowrap">
                        <b>Special Computer Training</b>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboSpComTrnReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboSpComTrn" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="checkbox-inline">
                        <asp:RadioButtonList ID="rboSpComTrn" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("Spec_Com_Train") %>' TabIndex="27" SelectedValue='<%# Bind("Spec_Com_Train") %>'>
                            <asp:ListItem Value="None">None</asp:ListItem>
                            <asp:ListItem Value="Some">Some</asp:ListItem>
                            <asp:ListItem Value="Avg">Avg</asp:ListItem>
                            <asp:ListItem Value="Above Avg">Above Avg</asp:ListItem>
                            <asp:ListItem Value="Expert">Expert</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:TextBox ID="IDtextbox" runat="server" Text='<%# Bind("ID") %>' CssClass="form-control" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="1" Visible="False"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="  Go Back  "  CssClass="btn btn-warning btn-lg" OnClick="Button1_Click" />
            <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="  Next Page  " ValidationGroup="APCG" CssClass="btn btn-success btn-lg " OnClick="SessionUpdate" />
            <asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="  Finish Later  " CssClass="btn btn-danger btn-lg" OnClientClick="return confirm('Are you sure you want to quit?');" OnClick="UpdateCancelButton_Click" />
        </EditItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>"
        SelectCommand="SELECT Farming, Forestry, AnimalHusb, Comm_Fish, Sm_Gas_Eng, AutoRepair, BoatOper, Hunting, Fishing, Camping, Swimming, Speci_Animal_Inv, FishID, NavBirdsID, NavMamID, NavRepID, NavAmp, AirBoat, OutBrdMotors, FourWheelDri, FarmTrac, Bulldozer, ChainSaw, Rifle, Pistol, Shotgun, Spec_Com_Train, ID FROM OAQ.ApplicantQuestionaire WHERE (ID = @PerID)"
        UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET Farming = @Farming, Forestry = @Forestry, AnimalHusb = @AnimalHusb, Comm_Fish = @Comm_Fish, Sm_Gas_Eng = @Sm_Gas_Eng, AutoRepair = @AutoRepair, BoatOper = @BoatOper, Hunting = @Hunting, Fishing = @Fishing, Camping = @Camping, Swimming = @Swimming, Speci_Animal_Inv = @Speci_Animal_Inv, FishID = @FishID, NavBirdsID = @NavBirdsID, NavMamID = @NavMamID, NavRepID = @NavRepID, NavAmp = @NavAmp, AirBoat = @AirBoat, OutBrdMotors = @OutBrdMotors, FourWheelDri = @FourWheelDri, FarmTrac = @FarmTrac, Bulldozer = @Bulldozer, ChainSaw = @ChainSaw, Rifle = @Rifle, Pistol = @Pistol, Shotgun = @Shotgun, Spec_Com_Train = @Spec_Com_Train WHERE ID=@PerID">
        <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Farming" />
            <asp:Parameter Name="Forestry" />
            <asp:Parameter Name="AnimalHusb" />
            <asp:Parameter Name="Comm_Fish" />
            <asp:Parameter Name="Sm_Gas_Eng" />
            <asp:Parameter Name="AutoRepair" />
            <asp:Parameter Name="BoatOper" />
            <asp:Parameter Name="Hunting" />
            <asp:Parameter Name="Fishing" />
            <asp:Parameter Name="Camping" />
            <asp:Parameter Name="Swimming" />
            <asp:Parameter Name="Speci_Animal_Inv" />
            <asp:Parameter Name="FishID" />
            <asp:Parameter Name="NavBirdsID" />
            <asp:Parameter Name="NavMamID" />
            <asp:Parameter Name="NavRepID" />
            <asp:Parameter Name="NavAmp" />
            <asp:Parameter Name="AirBoat" />
            <asp:Parameter Name="OutBrdMotors" />
            <asp:Parameter Name="FourWheelDri" />
            <asp:Parameter Name="FarmTrac" />
            <asp:Parameter Name="Bulldozer" />
            <asp:Parameter Name="ChainSaw" />
            <asp:Parameter Name="Rifle" />
            <asp:Parameter Name="Pistol" />
            <asp:Parameter Name="Shotgun" />
            <asp:Parameter Name="Spec_Com_Train" />
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
           </section>

    <script type="text/javascript">
        function SessionExpireAlert(timeout) {
            var seconds = timeout / 1000;
            document.getElementsByName("secondsIdle").innerHTML = seconds;
            document.getElementsByName("seconds").innerHTML = seconds;
            setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
                document.getElementById("secondsIdle").innerHTML = seconds;
            }, 1000);
            setTimeout(function () {
                //Show Popup before 20 seconds of timeout.
                $find("mpeTimeout").show();
            }, timeout - 20 * 1000);
            setTimeout(function () {
                window.location = "Expired.aspx";
            }, timeout);
        };
        function ResetSession() {
            //Redirect to refresh Session.
            window.location = window.location.href;
        }
</script>

</asp:Content>
