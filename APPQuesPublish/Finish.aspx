﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Finish.aspx.cs" Inherits="Application_Questionaire.Finish" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script>
         var elements = document.getElementsByTagName("a");
         function preventBack() { window.history.forward(); }

         setTimeout("preventBack()", 0);

         window.onunload = function () { null };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
        <meta http-equiv="Refresh" content="7;url=Login.aspx" />
                <div class="text-center">
              <h3 >Your resume has been submitted. You are now finished with this Application. </h3>
                  <br />
             <h5><a href="Login.aspx">  Return to Login</a></h5>

                </div>
</asp:Content>
