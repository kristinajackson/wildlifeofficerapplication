﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="R.aspx.cs" Inherits="Application_Questionaire.R" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        width: 300px;
        border: 3px solid #0DA9D0;
        border-radius: 12px;
        padding:0
      
    }
    .modalPopup .header
    {
        background-color: #2FBDF1;
        height: 30px;
        color: White;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
    }
    .modalPopup .body
    {
        min-height: 50px;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
    }
    .modalPopup .footer
    {
        padding: 6px;
    }
    .modalPopup .yes, .modalPopup .no
    {
        height: 23px;
        color: White;
        line-height: 23px;
        text-align: center;
        font-weight: bold;
        cursor: pointer;
        border-radius: 4px;
    }
    .modalPopup .yes
    {
        background-color: #2FBDF1;
        border: 1px solid #0DA9D0;
    }
    .modalPopup .no
    {
        background-color: #9F9F9F;
        border: 1px solid #5C5C5C;
    }
    </style>
    </asp:Content>
<asp:Content ID="main" ContentPlaceHolderID="main" runat="server">
    

   <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
       <section class="container " id="section1">
                <div class="col-lg-13" style="margin-left: -5%">

                    <div class=" panel panel-default">
                        <br />
                        <div class="panel-heading">
                        </div>
                        <div>
                            <div class="container" style="margin-left: 5%">
                                <div class="panel-body">
                                    <h3>Session Idle:&nbsp;<span id="secondsIdle"></span>&nbsp;seconds.</h3>
<asp:LinkButton ID="lnkFake" runat="server" />
<asp:ModalPopupExtender ID="mpeTimeout" BehaviorID ="mpeTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkFake"
    OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript = "ResetSession()">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
    <div class="header">
        Session Expiring!
    </div>
    <div class="body">
        Your Session will expire in&nbsp;<span id="seconds"></span>&nbsp;seconds.<br />
        Do you want to reset?
    </div>
    <div class="footer" align="right">
        <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="yes" />
        <asp:Button ID="btnNo" runat="server" Text="No" CssClass="no" />
    </div>
</asp:Panel>

    <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Edit" OnItemInserted="FormView1_ItemInserted" OnItemUpdated="FormView1_ItemUpdated" OnItemUpdating="FormView1_ItemUpdating">
        <EditItemTemplate>
            <form class="form-inline" data-toggle="validator" role="form" id="profileForm">
                <br />
                <div id="Div1" class="container-fluid" runat="server" style="text-transform: uppercase">
                    <div class="form-group">
                        <div class="form-group col-sm-7">
                            <label class="col-sm-3 control-label" for="Firstname">First Name:</label>
                            <div class="form-group col-sm-7">
                                <asp:TextBox ID="FirstnameTextBox" runat="server" Text='<%# Bind("Fname") %>' CssClass="form-control" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="1"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*First name is required!"
                                    ValidationGroup="ResVG" ControlToValidate="FirstnameTextBox" Display="Dynamic" ForeColor="Red">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server"
                                    ControlToValidate="FirstnameTextBox" Display="Dynamic" ErrorMessage="Invalid First Name!"
                                    SetFocusOnError="true" ForeColor="Red"
                                    ValidationExpression="^[a-zA-Z''-'\s]{1,40}$" ValidationGroup="ResVG">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="form-group col-sm-7">
                            <label class="col-sm-3 control-label" for="MiddleInitial">Middle Initial:</label>
                            <div class="form-group col-sm-7">
                                <asp:TextBox ID="txtMidd" runat="server" Text='<%# Bind("MiddleInt") %>' MaxLength="1" CssClass="form-control" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="2"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group col-sm-7">
                            <label class="col-sm-3 control-label" for="LastName">Last Name:</label>
                            <div class="form-group col-sm-7">
                                <asp:TextBox ID="txtlname" runat="server" Text='<%# Bind("Lname") %>' CssClass="form-control" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="3"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                    ErrorMessage="*Last name is required!" ValidationGroup="ResVG"
                                    ControlToValidate="txtlname" Display="Dynamic" ForeColor="Red">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                    ErrorMessage="Invalid Last Name!" ControlToValidate="txtlname" Display="Dynamic"
                                    SetFocusOnError="true" ForeColor="Red" ValidationExpression="^[a-zA-Z''-'\s]{1,40}$"
                                    ValidationGroup="ResVG"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="container-fluid">
                    <br />
                    <div class="form-group">
                        <div class="form-group col-sm-7">
                            <label class="col-sm-3 control-label" for="StreetAdderss">Street Address:</label>
                            <div class="form-group col-sm-7">
                                <asp:TextBox ID="StreetAdderssTextBox" runat="server" Text='<%# Bind("Address") %>'
                                    CssClass="form-control" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="4" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                    ErrorMessage="*Street address is required!" ValidationGroup="ResVG"
                                    ControlToValidate="StreetAdderssTextBox" Display="Dynamic" ForeColor="Red">
                                </asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-group col-sm-7">
                            <label class="col-sm-3 control-label" for="City">City:</label>
                            <div class="form-group col-sm-7">
                                <asp:TextBox ID="txtCity" runat="server" Text='<%# Bind("City") %>' CssClass="form-control" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="5"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="CityReqVal" ValidationGroup="ResVG" runat="server"
                                    ControlToValidate="txtCity" ErrorMessage="*City must be entered!" ForeColor="Red"
                                    Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-group col-sm-7">
                            <label for="state" class="col-sm-3 control-label">State</label>
                            <div class="form-group col-sm-7">
                                <asp:DropDownList ID="ddlState" CssClass="form-control" runat="server" SelectedValue='<%# Bind("State") %>' Font-Size="Medium" Style="text-transform: uppercase" TabIndex="6">
                                    <asp:ListItem></asp:ListItem>
                                    <asp:ListItem>AL</asp:ListItem>
                                    <asp:ListItem>AK</asp:ListItem>
                                    <asp:ListItem>AZ</asp:ListItem>
                                    <asp:ListItem>AR</asp:ListItem>
                                    <asp:ListItem>CA</asp:ListItem>
                                    <asp:ListItem>CO</asp:ListItem>
                                    <asp:ListItem>CT</asp:ListItem>
                                    <asp:ListItem>DC</asp:ListItem>
                                    <asp:ListItem>DE</asp:ListItem>
                                    <asp:ListItem>FL</asp:ListItem>
                                    <asp:ListItem>GA</asp:ListItem>
                                    <asp:ListItem>HI</asp:ListItem>
                                    <asp:ListItem>ID</asp:ListItem>
                                    <asp:ListItem>IL</asp:ListItem>
                                    <asp:ListItem>IN</asp:ListItem>
                                    <asp:ListItem>IA</asp:ListItem>
                                    <asp:ListItem>KS</asp:ListItem>
                                    <asp:ListItem>KY</asp:ListItem>
                                    <asp:ListItem>LA</asp:ListItem>
                                    <asp:ListItem>ME</asp:ListItem>
                                    <asp:ListItem>MD</asp:ListItem>
                                    <asp:ListItem>MA</asp:ListItem>
                                    <asp:ListItem>MI</asp:ListItem>
                                    <asp:ListItem>MN</asp:ListItem>
                                    <asp:ListItem>MS</asp:ListItem>
                                    <asp:ListItem>MO</asp:ListItem>
                                    <asp:ListItem>MT</asp:ListItem>
                                    <asp:ListItem>NE</asp:ListItem>
                                    <asp:ListItem>NV</asp:ListItem>
                                    <asp:ListItem>NH</asp:ListItem>
                                    <asp:ListItem>NJ</asp:ListItem>
                                    <asp:ListItem>NM</asp:ListItem>
                                    <asp:ListItem>NY</asp:ListItem>
                                    <asp:ListItem>NC</asp:ListItem>
                                    <asp:ListItem>ND</asp:ListItem>
                                    <asp:ListItem>OH</asp:ListItem>
                                    <asp:ListItem>OK</asp:ListItem>
                                    <asp:ListItem>OR</asp:ListItem>
                                    <asp:ListItem>PA</asp:ListItem>
                                    <asp:ListItem>RI</asp:ListItem>
                                    <asp:ListItem>SC</asp:ListItem>
                                    <asp:ListItem>SD</asp:ListItem>
                                    <asp:ListItem Selected="True">TN</asp:ListItem>
                                    <asp:ListItem>TX</asp:ListItem>
                                    <asp:ListItem>UT</asp:ListItem>
                                    <asp:ListItem>VT</asp:ListItem>
                                    <asp:ListItem>VA</asp:ListItem>
                                    <asp:ListItem>WA</asp:ListItem>
                                    <asp:ListItem>WV</asp:ListItem>
                                    <asp:ListItem>WI</asp:ListItem>
                                    <asp:ListItem>WY</asp:ListItem>
                                    <asp:ListItem>  </asp:ListItem>
                                </asp:DropDownList>

                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="ResVG" runat="server"
                                    ControlToValidate="ddlState" ErrorMessage="*State must be entered!" ForeColor="Red"
                                    Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group col-sm-7">
                            <label for="Zipcode" class="col-sm-3 control-label">Zipcode</label>
                            <div class="form-group col-sm-4">
                                <asp:TextBox ID="ZipCodeTextBox" runat="server" CssClass="form-control" Text='<%# Bind("Zipcode") %>' MaxLength="5" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="7" />
                                <asp:Label ID="lblZip" Text="99999" runat="server" Style="font-size: x-small"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ValidationGroup="ResVG" runat="server"
                                    ControlToValidate="ZipCodeTextBox" ErrorMessage="*Zipcode must be entered!" ForeColor="Red"
                                    Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server"
                                    ErrorMessage="Invalid zip code!" ValidationExpression="^\d{5}(-\d{4})?$"
                                    ForeColor="Red" Display="Dynamic" SetFocusOnError="true" ControlToValidate="ZipCodeTextBox" ValidationGroup="ResVG" />

                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group col-sm-7">
                            <label class="col-sm-3 control-label" for="HomePhone">Home Phone:</label>
                            <div class="form-group col-sm-7">
                                <asp:TextBox ID="HomePhoneTextBox" runat="server" Text='<%# Bind("HomePhone") %>' CssClass="form-control" Font-Size="Medium" TabIndex="8"></asp:TextBox>
                                <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="HomePhoneTextBox" AutoComplete="False" ClearMaskOnLostFocus="true" Mask="999-999-9999" MaskType="Number" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                    ErrorMessage="Invalid home phone number!" ValidationExpression="^[0-9]{10}|^[0-9]{3}-[0-9]{3}-[0-9]{4}"
                                    ForeColor="Red" Display="Dynamic" SetFocusOnError="true" ControlToValidate="HomePhoneTextBox" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group col-sm-7">
                            <label class="col-sm-3 control-label" for="WorkPhone">Work Phone:</label>
                            <div class="form-group col-sm-7">
                                <asp:TextBox ID="WorkPhoneTextBox" runat="server" Text='<%# Bind("WorkPhone") %>' CssClass="form-control" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="9"></asp:TextBox>
                                <asp:MaskedEditExtender ID="WorkPhoneMEE" runat="server" TargetControlID="WorkPhoneTextBox" AutoComplete="False" ClearMaskOnLostFocus="true" Mask="999-999-9999" MaskType="Number" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server"
                                    ErrorMessage="Invalid work phone number!" ValidationExpression="^[0-9]{10}|^[0-9]{3}-[0-9]{3}-[0-9]{4}"
                                    ForeColor="Red" Display="Dynamic" SetFocusOnError="true" ControlToValidate="WorkPhoneTextBox" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-group col-sm-7">
                            <label class="col-sm-3 control-label" for="EmpContact">Employment Contact No:</label>
                            <div class="form-group col-sm-7">
                                <asp:TextBox ID="EmpContactTextBox" runat="server" Text='<%# Bind("EmpConNo") %>' CssClass="form-control" Font-Size="Medium" TabIndex="10"></asp:TextBox>
                                <asp:MaskedEditExtender ID="EmpContactTextBox_MaskedEditExtender" runat="server" BehaviorID="EmpContactTextBox_MaskedEditExtender"
                                    Mask="999-999-9999" MaskType="Number" ClearMaskOnLostFocus="true" ErrorTooltipEnabled="true" AutoComplete="False" TargetControlID="EmpContactTextBox" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                    ErrorMessage="Invalid employment contact number!" ValidationExpression="^[0-9]{10}|^[0-9]{3}-[0-9]{3}-[0-9]{4}"
                                    ForeColor="Red" Display="Dynamic" SetFocusOnError="true" ControlToValidate="EmpContactTextBox" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-group col-sm-7">
                            <label class="col-sm-3 control-label" for="AlternateNo">Alternate No:</label>
                            <div class="form-group col-sm-7">
                                <asp:TextBox ID="AlternateNoTextBox" runat="server" Text='<%# Bind("AlternateNo") %>' CssClass="form-control" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="11"></asp:TextBox>
                                <asp:MaskedEditExtender ID="AlternateNoTextBox_MaskedEditExtender" runat="server" BehaviorID="AlternateNoTextBox_MaskedEditExtender"
                                    Mask="999-999-9999" MaskType="Number" ClearMaskOnLostFocus="true"
                                    ErrorTooltipEnabled="true" AutoComplete="true" TargetControlID="AlternateNoTextBox" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                    ErrorMessage="Invalid alternate phone number!" ValidationExpression="^[0-9]{10}|^[0-9]{3}-[0-9]{3}-[0-9]{4}"
                                    ForeColor="Red" Display="Dynamic" SetFocusOnError="true" ControlToValidate="AlternateNoTextBox" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="form-group">
                        <h4>
                            <label>List chronologically your residences for the last three years(including address while attending school).</label></h4>
                    </div>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Dates From</th>
                            <th>Dates To</th>
                            <th>Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtDtFrom1" runat="server" Text='<%# Bind("Residence_FromDate1") %>' MaxLength="10" CssClass="form-control" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="12"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender7" runat="server" TargetControlID="txtDtFrom1"></asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server"
                                    ErrorMessage="*Please enter your beginning date at residence!"
                                    ControlToValidate="txtDtFrom1" Display="Dynamic" ForeColor="Red" Font-Size="Small" ValidationGroup="ResVG">
                                </asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="RangeValidator6" runat="server"
                                    ControlToValidate="txtDtFrom1" ErrorMessage="Invalid From Date"
                                    Type="Date" MinimumValue="01/01/1900" MaximumValue="01/01/2100"
                                    Display="Dynamic" ForeColor="Red" ValidationGroup="ResVG" Font-Size="Small"></asp:RangeValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtDtTo1" runat="server" Text='<%# Bind("Residence_ToDate1") %>' MaxLength="10" CssClass="form-control" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="13"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender5" runat="server" TargetControlID="txtDtTo1"></asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server"
                                    ErrorMessage="*Please enter your ending date at residence!"
                                    ControlToValidate="txtDtTo1" Display="Dynamic" ForeColor="Red" Font-Size="X-Small" ValidationGroup="ResVG">
                                </asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="RangeValidator7" runat="server"
                                    ControlToValidate="txtDtTo1" ErrorMessage="Invalid To Date"
                                    Type="Date" MinimumValue="01/01/1900" MaximumValue="01/01/2100"
                                    Display="Dynamic" ForeColor="Red" ValidationGroup="ResVG" Font-Size="X-Small"></asp:RangeValidator>
                                <asp:CompareValidator ID="CompareValidator4" runat="server" ErrorMessage="From date should be greater than To date!" ControlToCompare="txtDtFrom1"
                                    ControlToValidate="txtDtTo1" Operator="GreaterThan" Type="Date" ValidationGroup="ResVG" Display="Dynamic" ForeColor="Red" Font-Size="X-Small"></asp:CompareValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtAddress1" runat="server" Text='<%# Bind("ResidenceAddress1") %>' CssClass="form-control" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="14"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server"
                                    ErrorMessage="*Please enter your address!" ValidationGroup="ResVG"
                                    ControlToValidate="txtAddress1" Display="Dynamic" ForeColor="Red" Font-Size="Small">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <td class="auto-style9">
                            <asp:Label ID="Label1" Text="MM/DD/YYYY" runat="server" Style="font-size: x-small"></asp:Label></td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <td>
                          <asp:Label ID="Label2" Text="MM/DD/YYYY" runat="server" Style="font-size: x-small"></asp:Label></td>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtDtFrom2" runat="server" Text='<%# Bind("Residence_FromDate2") %>' CssClass="form-control" MaxLength="10" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="15"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDtFrom2"></asp:CalendarExtender>
                                <asp:RangeValidator ID="RangeValidator1" runat="server"
                                    ErrorMessage="Invalid From Date!"
                                    Type="Date" MinimumValue="01/01/1900" MaximumValue="01/01/2100"
                                    Display="Dynamic" ForeColor="Red" ControlToValidate="txtDtFrom2" ValidationGroup="ResVG"></asp:RangeValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtDtTo2" runat="server" Text='<%# Bind("Residence_ToDate2") %>' CssClass="form-control" MaxLength="10" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="16"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtDtTo2">
                                </asp:CalendarExtender>
                                <asp:RangeValidator ID="RangeValidator3" runat="server"
                                    ErrorMessage="Invalid To Date!"
                                    Type="Date" MinimumValue="01/01/1900" MaximumValue="01/01/2100"
                                    Display="Dynamic" ForeColor="Red" ControlToValidate="txtDtTo2" ValidationGroup="ResVG"></asp:RangeValidator>
                                <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="From date should be greater than To date!" ControlToCompare="txtDtFrom2"
                                    ControlToValidate="txtDtTo2" Operator="GreaterThan" Type="Date" ValidationGroup="ResVG" Display="Dynamic" ForeColor="Red"></asp:CompareValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtAddress2" runat="server" Text='<%# Bind("ResidenceAddress2") %>' CssClass="form-control" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="17"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server"
                                    ErrorMessage="*Please enter your address!" ValidationGroup="ResVG"
                                    ControlToValidate="txtAddress2" Enabled="false" Display="Dynamic" ForeColor="Red" Font-Size="Small">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <td>
                            <asp:Label ID="Label5" Text="MM/DD/YYYY" runat="server" Style="font-size: x-small"></asp:Label></td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       <td>
                           <asp:Label ID="Label6" Text="MM/DD/YYYY" runat="server" Style="font-size: x-small"></asp:Label></td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtDtFrom3" runat="server" Text='<%# Bind("Residence_FromDate3") %>' CssClass="form-control" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="18"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender3" runat="server"
                                        TargetControlID="txtDtFrom3">
                                    </asp:CalendarExtender>
                                    <asp:RangeValidator ID="RangeValidator4" runat="server"
                                        ErrorMessage="Invalid From Date"
                                        Type="Date" MinimumValue="01/01/1900" MaximumValue="01/01/2100"
                                        Display="Dynamic" ForeColor="Red" ControlToValidate="txtDtFrom3" ValidationGroup="ResVG"></asp:RangeValidator>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDtTo3" runat="server" Text='<%# Bind("Residence_ToDate3") %>' CssClass="form-control" MaxLength="10" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="19"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender4" runat="server"
                                        TargetControlID="txtDtTo3">
                                    </asp:CalendarExtender>
                                    <asp:RangeValidator ID="RangeValidator5" runat="server"
                                        ErrorMessage="Invalid To Date"
                                        Type="Date" MinimumValue="01/01/1900" MaximumValue="01/01/2100"
                                        Display="Dynamic" ForeColor="Red" ControlToValidate="txtDtTo3" ValidationGroup="ResVG" Font-Size="X-Small"></asp:RangeValidator>
                                    <asp:CompareValidator ID="CompareValidator3" runat="server" ErrorMessage="From date should be greater than To date!" ControlToCompare="txtDtFrom3"
                                        ControlToValidate="txtDtTo3" Operator="GreaterThan" Type="Date" ValidationGroup="ResVG" Display="Dynamic" ForeColor="Red" Font-Size="XX-Small"></asp:CompareValidator>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAddress3" runat="server" Text='<%# Bind("ResidenceAddress3") %>' CssClass="form-control" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="20"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server"
                                        ErrorMessage="*Please enter your address!" ValidationGroup="ResVG"
                                        ControlToValidate="txtAddress3" Display="Dynamic" ForeColor="Red" Font-Size="Small" Enabled="false">
                                    </asp:RequiredFieldValidator>

                                </td>
                            </tr>
                        <tr>
                            <td class="auto-style9">
                                <asp:Label ID="Label10" Text="MM/DD/YYYY" runat="server" Style="font-size: x-small"></asp:Label>
                            </td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <td>
                          <asp:Label ID="Label11" Text="MM/DD/YYYY" runat="server" Style="font-size: x-small"></asp:Label>
                      </td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </tr>
                    </tbody>
                    <asp:Label ID="lblConfirm" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
                </table>


            </form>
            <asp:Button ID="UpdateButton" runat="server" CssClass="btn btn-success btn-lg " CausesValidation="True" CommandName="Update" Text="  Next Page  " ValidationGroup="ResVG" OnClick="btnSvForLater_Click" />
            <%--<asp:Button ID="UpdateButton" CssClass="btn btn-success btn-lg " runat="server" CausesValidation="True" CommandName="Update" Text="  Next Page  " ValidationGroup="ResVG" OnClick="btnSvForLater_Click" />--%>
            <asp:Button ID="UpdateCancelButton" CssClass="btn btn-danger btn-lg" runat="server" CausesValidation="False" CommandName="Cancel" Text="  Finish Later  " OnClientClick="return confirm('Are you sure you want to quit?');" OnClick="InsertCancelButton_Click" />

             <div id="danger" class="alert alert-danger" role="alert" runat="server" visible="false">
        <strong>ERROR!</strong> This account already in use!
    </div>

        </EditItemTemplate>
    </asp:FormView>
                                       </div>
                            </div>
                        </div>
                    </div>
           </section>
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>"
        SelectCommand="SELECT ID, Address, HomePhone, WorkPhone, EmpConNo, AlternateNo, Residence_FromDate1, Residence_ToDate1, ResidenceAddress1, CreatedDate, Yr_TN_Residence_YrMon, Residence_FromDate2, Residence_ToDate2, Residence_FromDate3, Residence_ToDate3, ResidenceAddress2, ResidenceAddress3, Country, State, Zipcode, Fname, MiddleInt, Lname, City, UserName, Password, CreatedDate FROM OAQ.ApplicantQuestionaire WHERE (ID = @PerID)"
        UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET Fname = @Fname, MiddleInt = @MiddleInt, Lname = @Lname, Address = @Address, HomePhone = @HOMEPHONE, WorkPhone = @WorkPhone, EmpConNo = @EmpConNo, AlternateNo = @AlternateNo, Residence_FromDate1 = @Residence_FromDate1, Residence_ToDate1 = @Residence_ToDate1, ResidenceAddress1 = @ResidenceAddress1, Residence_FromDate2 = @Residence_FromDate2, Residence_ToDate2 = @Residence_ToDate2, Residence_FromDate3 = @Residence_FromDate3, Residence_ToDate3 = @Residence_ToDate3, ResidenceAddress2 = @ResidenceAddress2, ResidenceAddress3 = @ResidenceAddress3, Country = @Country, State = @State, Zipcode = @Zipcode, City = @City, CreatedDate=CONVERT (date, SYSDATETIME()) WHERE (ID = @PerID)">
        <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Fname" />
            <asp:Parameter Name="MiddleInt" />
            <asp:Parameter Name="Lname" />
            <asp:Parameter Name="Address" />
            <asp:Parameter Name="HOMEPHONE" />
            <asp:Parameter Name="WorkPhone" />
            <asp:Parameter Name="EmpConNo" />
            <asp:Parameter Name="AlternateNo" />
            <asp:Parameter Name="Residence_FromDate1" />
            <asp:Parameter Name="Residence_ToDate1" />
            <asp:Parameter Name="ResidenceAddress1" />
            <asp:Parameter Name="Residence_FromDate2" />
            <asp:Parameter Name="Residence_ToDate2" />
            <asp:Parameter Name="Residence_FromDate3" />
            <asp:Parameter Name="Residence_ToDate3" />
            <asp:Parameter Name="ResidenceAddress2" />
            <asp:Parameter Name="ResidenceAddress3" />
            <asp:Parameter Name="Country" />
            <asp:Parameter Name="State" />
            <asp:Parameter Name="Zipcode" />
            <asp:Parameter Name="City" />
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>


</div>
                             
    <script type="text/javascript">
        function SessionExpireAlert(timeout) {
            var seconds = timeout / 1000;
            document.getElementsByName("secondsIdle").innerHTML = seconds;
            document.getElementsByName("seconds").innerHTML = seconds;
            setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
                document.getElementById("secondsIdle").innerHTML = seconds;
            }, 1000);
            setTimeout(function () {
                //Show Popup before 20 seconds of timeout.
                $find("mpeTimeout").show();
            }, timeout - 20 * 1000);
            setTimeout(function () {
                window.location = "Login.aspx";
            }, timeout);
        };
        function ResetSession() {
            //Redirect to refresh Session.
            window.location = window.location.href;
        }
</script>
    </asp:Content>