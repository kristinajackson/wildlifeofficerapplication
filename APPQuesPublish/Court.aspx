﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Court.aspx.cs" Inherits="Application_Questionaire.Court" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
         <style>
        .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        width: 300px;
        border: 3px solid #0DA9D0;
        border-radius: 12px;
        padding:0
      
    }
    .modalPopup .header
    {
        background-color: #2FBDF1;
        height: 30px;
        color: White;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
    }
    .modalPopup .body
    {
        min-height: 50px;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
    }
    .modalPopup .footer
    {
        padding: 6px;
    }
    .modalPopup .yes, .modalPopup .no
    {
        height: 23px;
        color: White;
        line-height: 23px;
        text-align: center;
        font-weight: bold;
        cursor: pointer;
        border-radius: 4px;
    }
    .modalPopup .yes
    {
        background-color: #2FBDF1;
        border: 1px solid #0DA9D0;
    }
    .modalPopup .no
    {
        background-color: #9F9F9F;
        border: 1px solid #5C5C5C;
    }
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
      
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
       <section class="container " id="section1">
                <div class="col-lg-15" >

                    <div class=" panel panel-default">
                        <br />
                        <div class="panel-heading">
                        </div>
                        <div>
                            <div class="container-fluid" style="margin-left: 5%">
                                <div class="panel-body">

                                      <h6 style="color: #FFFFFF">Session Idle:&nbsp;<span id="secondsIdle"></span>&nbsp;seconds.</h6>
<asp:LinkButton ID="lnkFake" runat="server" />
<asp:ModalPopupExtender ID="mpeTimeout" BehaviorID ="mpeTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkFake"
    OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript = "ResetSession()">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
    <div class="header">
        Session Expiring!
    </div>
    <div class="body">
        Your Session will expire in&nbsp;<span id="seconds"></span>&nbsp;seconds.<br />
        Are you still there?
    </div>
    <div class="footer" align="right">
        <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="yes" />
        <asp:Button ID="btnNo" runat="server" Text="No" CssClass="no" />
    </div>
</asp:Panel>

        <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Edit" DataKeyNames="ID" OnItemInserted="FormView1_ItemInserted" OnItemUpdated="FormView1_ItemUpdated">
            <EditItemTemplate>
                <br />
                <form>
                <div class="container-fluid">
                        <div class="form-group">
                            <asp:TextBox ID="IDtextbox" runat="server" Text='<%# Bind("ID") %>' CssClass="form-control" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="1" Visible="False"></asp:TextBox>
                            <label for="rboConvict">Have you ever been <u><b>convicted</b></u> of any crimes or infractions?
                            <asp:DropDownList ID="rboConvict" TabIndex="1" runat="server" SelectedValue='<%# Bind("Convictied") %>' CssClass="ddlitem">
                    <asp:ListItem></asp:ListItem>
                    <asp:ListItem>Yes</asp:ListItem>
                    <asp:ListItem>No</asp:ListItem>
                </asp:DropDownList></label>
                             <asp:RequiredFieldValidator ID="rboConvictReqValidator" runat="server" ErrorMessage="*Please select Yes/No" 
                    Font-Bold="true" ForeColor="Red" ControlToValidate="rboConvict" ValidationGroup="CTVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                        </div>
                     <h5><label > If yes, please list below. List all such matters, even if there was no court appearance or the matter was settled by payment of fines or forfeiture of collateral.</label></h5>
                     <div class="form-group">
                            <label for="txtCharge">Please list the dates, places, Agency involved, the charge, final disposition and the details on each. </label>
                          <asp:TextBox ID="txtCharge" TabIndex="2" runat="server" TextMode="MultiLine" Rows="4" Text='<%# Bind("Explain_Charge") %>'  CssClass="  form-control" Font-Size="Medium" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="txtChargeReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ControlToValidate="txtCharge" ValidationGroup="CTVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                         </div>
                     <div class="form-group">
                            <label for="txtCitation">List all traffic citations, but not parking tickets.  </label>
                           <asp:TextBox ID="txtCitation" TabIndex="3" TextMode="MultiLine" Rows="4" runat="server" Text='<%# Bind("Traffic_Cit") %>' CssClass="  form-control" Font-Size="Medium" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="txtCitationReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ControlToValidate="txtCitation" ValidationGroup="CTVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                         </div>
                     <div class="form-group">
                            <label for="txtOthState">Have you previously applied to any other state, county or other municipal law enforement agency or private security organization and if so, please list below. </label>
                          <asp:TextBox ID="txtOthState" TabIndex="4" runat="server" TextMode="MultiLine" Rows="4" Text='<%# Bind("OthStateEmploy") %>' CssClass="  form-control" Font-Size="Medium"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="txtOthStateReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ControlToValidate="txtOthState" ValidationGroup="CTVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                         </div>
                    
                    
               
                <br />
                <br />
                <asp:Button ID="BackButton" runat="server" Text="  Go Back  " OnClick="SessionUpdate" CssClass="btn btn-warning btn-lg" />
                <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="  Next Page  " ValidationGroup="CTVG" CssClass="btn btn-success btn-lg " OnClick="UpdateButton_Click" />
                <asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="  Finish Later  " OnClientClick="return confirm('Are you sure you want to quit?');" OnClick="UpdateCancelButton_Click" class="btn btn-danger btn-lg" />
                    </div>
                    </form>
            </EditItemTemplate>
        </asp:FormView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>" 
            SelectCommand="SELECT ID, Convictied, Explain_Charge, Traffic_Cit, OthStateEmploy FROM OAQ.ApplicantQuestionaire WHERE (ID = @PerID)" 
            UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET Convictied = @Convictied, Explain_Charge = @Explain_Charge, Traffic_Cit = @Traffic_Cit, OthStateEmploy = @OthStateEmploy WHERE ID=@PerID">
            <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="Convictied" />
                <asp:Parameter Name="Explain_Charge" />
                <asp:Parameter Name="Traffic_Cit" />
                <asp:Parameter Name="OthStateEmploy" />
                 <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
</div>
                                </div>
                            </div>
                        </div>
                    </div>
           </section>
    
    <script type="text/javascript">
        function SessionExpireAlert(timeout) {
            var seconds = timeout / 1000;
            document.getElementsByName("secondsIdle").innerHTML = seconds;
            document.getElementsByName("seconds").innerHTML = seconds;
            setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
                document.getElementById("secondsIdle").innerHTML = seconds;
            }, 1000);
            setTimeout(function () {
                //Show Popup before 20 seconds of timeout.
                $find("mpeTimeout").show();
            }, timeout - 20 * 1000);
            setTimeout(function () {
                window.location = "Login.aspx";
            }, timeout);
        };
        function ResetSession() {
            //Redirect to refresh Session.           
            window.location = window.location.href;
        }
</script>

</asp:Content>
