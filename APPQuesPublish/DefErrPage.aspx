﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="DefErrPage.aspx.cs" Inherits="BoaterEducation.DefErrPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
            <div style="text-align: center">
        <br />
        <br />
        <asp:Label ID="lblerror" runat="server" Text="An Error Has Occured. Please Contact System Administrator!" ForeColor="#990000" Font-Size="Large" Font-Bold="True"></asp:Label>
                <br />
             <h5><a href="Login.aspx">  Return to Login</a></h5>
        <br />
        <br />
    </div>
</asp:Content>
