﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="WildlifeOfficerReason.aspx.cs" Inherits="Application_Questionaire.WildlifeOfficerReason" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var elements = document.getElementsByTagName("a");
        function preventBack() { window.history.forward(); }

        setTimeout("preventBack()", 0);

        window.onunload = function () { null };
    </script>
         <style>
        .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        width: 300px;
        border: 3px solid #0DA9D0;
        border-radius: 12px;
        padding:0
      
    }
    .modalPopup .header
    {
        background-color: #2FBDF1;
        height: 30px;
        color: White;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
    }
    .modalPopup .body
    {
        min-height: 50px;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
    }
    .modalPopup .footer
    {
        padding: 6px;
    }
    .modalPopup .yes, .modalPopup .no
    {
        height: 23px;
        color: White;
        line-height: 23px;
        text-align: center;
        font-weight: bold;
        cursor: pointer;
        border-radius: 4px;
    }
    .modalPopup .yes
    {
        background-color: #2FBDF1;
        border: 1px solid #0DA9D0;
    }
    .modalPopup .no
    {
        background-color: #9F9F9F;
        border: 1px solid #5C5C5C;
    }
    </style>

    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="heartbeat">&hearts;</div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
               <section class="container " id="section1">
                <div class="col-lg-15" style="margin-left: -5%">

                    <div class=" panel panel-default">
                        <br />
                        <div class="panel-heading">
                        </div>
                        <div>
                            <div class="container" style="margin-left: 5%">
                                <div class="panel-body">

                                      <h3 style="color: #FFFFFF">Session Idle:&nbsp;<span id="secondsIdle"></span>&nbsp;seconds.</h3>
<asp:LinkButton ID="lnkFake" runat="server" />
<asp:ModalPopupExtender ID="mpeTimeout" BehaviorID ="mpeTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkFake"
    OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript = "ResetSession()">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
    <div class="header">
        Session Expiring!
    </div>
    <div class="body">
        Your Session will expire in&nbsp;<span id="seconds"></span>&nbsp;seconds.<br />
        Are you still there?
    </div>
    <div class="footer" align="right">
        <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="yes" />
        <asp:Button ID="btnNo" runat="server" Text="No" CssClass="no" />
    </div>
</asp:Panel>


            <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Edit" OnItemInserted="FormView1_ItemInserted1" OnItemUpdated="FormView1_ItemUpdated">
                <EditItemTemplate>
                    <asp:TextBox ID="IDLabel1" runat="server" Text='<%# Eval("ID") %>' Visible="false" />
                    <br />
                    <form>
                        <div class="container-fluid">
                            <div class="form-group">
                                <label for="txtWlOff">Why do you want to become a Wildlife Officer? </label>
                                <asp:TextBox ID="txtWlOff" runat="server" TextMode="MultiLine" Rows="4" TabIndex="1" Text='<%# Bind("Reason_WLO") %>' CssClass="  form-control"  Font-Size="Medium"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="txtWlOffReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ControlToValidate="txtWlOff" ValidationGroup="WLOVG" Display="Dynamic">
                                </asp:RequiredFieldValidator>
                            </div>

                            <div class="form-group">
                                <label for="txtEvent">What events or series of events caused you to make this decision?</label>
                                <asp:TextBox ID="txtEvent" runat="server" TextMode="MultiLine" Rows="4" TabIndex="2" Text='<%# Bind("Events_ForDecision") %>' CssClass="  form-control" Font-Size="Medium"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="txtEventReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ControlToValidate="txtEvent" ValidationGroup="WLOVG" Display="Dynamic">
                                </asp:RequiredFieldValidator>
                            </div>
                            <div class="form-group">
                                <label for="txtCourses">What specific courses of actions have you taken to prepare yourself for a career as a Wildlife Officer?</label>
                                <asp:TextBox ID="txtCourses" runat="server" TextMode="MultiLine" Rows="4" TabIndex="3" Text='<%# Bind("CourseofAction") %>' CssClass="  form-control"  Font-Size="Medium"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="txtCoursesReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ControlToValidate="txtCourses" ValidationGroup="WLOVG" Display="Dynamic">
                                </asp:RequiredFieldValidator>
                            </div>
                            <div class="form-group">
                                <label for="txtQualities">What specific qualities do you possess that would make you better Wildlife Officer than another applicant?</label>
                                <asp:TextBox ID="txtQualities" runat="server" TextMode="MultiLine" Rows="4" TabIndex="4" Text='<%# Bind("Qualities") %>' CssClass="  form-control"  Font-Size="Medium"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="txtQualitiesReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ControlToValidate="txtQualities" ValidationGroup="WLOVG" Display="Dynamic">
                                </asp:RequiredFieldValidator>
                            </div>
                            <div class="form-group">
                                <label for="txtConcern">
                                    What is the major concern facing all of Tennessee wildlife and enviornmental resources today? 
                            As a wildlife officer representing the Agency, what impact do you think you will have in dealing with this concern?</label>
                                <asp:TextBox ID="txtConcern" runat="server" TextMode="MultiLine" Rows="4" TabIndex="5" Text='<%# Bind("MajorConernforWL") %>' CssClass="  form-control"  Font-Size="Medium"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="txtConcernReqValidator" runat="server" ControlToValidate="txtConcern" Display="Dynamic" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ValidationGroup="WLOVG">
                                </asp:RequiredFieldValidator>
                            </div>

                            <br />
                            <br />
                            <asp:Button ID="btnPrevious" runat="server" Text="Go Back" CssClass="btn btn-warning btn-lg" OnClick="btnPrevious_Click" />
                            <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Submit" ValidationGroup="WLOVG" CssClass="btn btn-success btn-lg " OnClick="UpdateButton_Click" />
                            &nbsp;<asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Finish Later" OnClientClick="return confirm('Are you sure you want to quit?');" OnClick="UpdateCancelButton_Click" class="btn btn-danger btn-lg" />
                        </div>

                    </form>
                </EditItemTemplate>
            </asp:FormView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   </section>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>"
        SelectCommand="SELECT ID, Reason_WLO, Events_ForDecision, Qualities, CourseofAction, MajorConernforWL FROM OAQ.ApplicantQuestionaire WHERE (ID = @PerID)"
        UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET Reason_WLO = @Reason_WLO, Events_ForDecision = @Events_ForDecision, CourseofAction = @CourseofAction, Qualities = @Qualities, MajorConernforWL = @MajorConernforWL WHERE (ID = @PerID)">
        <UpdateParameters>
            <asp:Parameter Name="Reason_WLO" />
            <asp:Parameter Name="Events_ForDecision" />
            <asp:Parameter Name="CourseofAction" />
            <asp:Parameter Name="Qualities" />
            <asp:Parameter Name="MajorConernforWL" />
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </UpdateParameters>
        <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

     <script type="text/javascript">
         function SessionExpireAlert(timeout) {
             var seconds = timeout / 1000;
             document.getElementsByName("secondsIdle").innerHTML = seconds;
             document.getElementsByName("seconds").innerHTML = seconds;
             setInterval(function () {
                 seconds--;
                 document.getElementById("seconds").innerHTML = seconds;
                 document.getElementById("secondsIdle").innerHTML = seconds;
             }, 1000);
             setTimeout(function () {
                 //Show Popup before 20 seconds of timeout.
                 $find("mpeTimeout").show();
             }, timeout - 20 * 1000);
             setTimeout(function () {
                 window.location = "Login.aspx";
             }, timeout);
         };
         function ResetSession() {
             //Redirect to refresh Session.
             window.location = window.location.href;
         }
</script>

</asp:Content>
