﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="CategoriesEssay.aspx.cs" Inherits="Application_Questionaire.CategoriesEssay" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <style>
        .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        width: 300px;
        border: 3px solid #0DA9D0;
        border-radius: 12px;
        padding:0
      
    }
    .modalPopup .header
    {
        background-color: #2FBDF1;
        height: 30px;
        color: White;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
    }
    .modalPopup .body
    {
        min-height: 50px;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
    }
    .modalPopup .footer
    {
        padding: 6px;
    }
    .modalPopup .yes, .modalPopup .no
    {
        height: 23px;
        color: White;
        line-height: 23px;
        text-align: center;
        font-weight: bold;
        cursor: pointer;
        border-radius: 4px;
    }
    .modalPopup .yes
    {
        background-color: #2FBDF1;
        border: 1px solid #0DA9D0;
    }
    .modalPopup .no
    {
        background-color: #9F9F9F;
        border: 1px solid #5C5C5C;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">


    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <section class="container " id="section1">
                <div class="col-lg-11" style="margin-left: -5%">

                    <div class=" panel panel-default">
                        <br />
                        <div class="panel-heading">
                        </div>
                        <div>
                            <div class="container" style="margin-left: 5%">
                                <div class="panel-body">

                                    <h6 style="color: #FFFFFF">Session Idle:&nbsp;<span id="secondsIdle"></span>&nbsp;seconds.</h6>
                                    <asp:LinkButton ID="lnkFake" runat="server" />
                                    <asp:ModalPopupExtender ID="mpeTimeout" BehaviorID="mpeTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkFake"
                                        OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript="ResetSession()">
                                    </asp:ModalPopupExtender>
                                    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
                                        <div class="header">
                                            Session Expiring!
                                        </div>
                                        <div class="body">
                                            Your Session will expire in&nbsp;<span id="seconds"></span>&nbsp;seconds.<br />
                                            Are you still there?
                                        </div>
                                        <div class="footer" align="right">
                                            <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="yes" />
                                            <asp:Button ID="btnNo" runat="server" Text="No" CssClass="no" />
                                        </div>
                                    </asp:Panel>
                                    <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Edit" DataKeyNames="ID" OnItemInserted="FormView1_ItemInserted" OnItemUpdated="FormView1_ItemUpdated">
                                        <EditItemTemplate>

                                            <div class="container-fluid">

                                                <div class="form-group">
                                                    <h4>
                                                        <label>Identify the specific information with the experience or ability of the following.</label></h4>
                                                </div>
                                                <div class="form-group">
                                                    <label for="txtSpcAnInv">Special Animal Inventory:</label>
                                                    <asp:TextBox ID="txtSpcAnInv" runat="server" Font-Size="Medium" TabIndex="1" TextMode="MultiLine" Rows="4" CssClass="  form-control" Text='<%# Bind("SpecialAnimalInventoryEssay") %>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="txtquesMemReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ValidationGroup="EXVG" Display="Dynamic" ControlToValidate="txtSpcAnInv">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                                <div class="form-group">
                                                    <label for="txtNativeAmp">Native Amphibians:</label>
                                                    <asp:TextBox ID="txtNativeAmp" runat="server" Font-Size="Medium" TabIndex="2" TextMode="MultiLine" Rows="4" CssClass="  form-control" Text='<%# Bind("NativeAmphibiansEssay") %>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ValidationGroup="EXVG" Display="Dynamic" ControlToValidate="txtNativeAmp">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                                <div class="form-group">
                                                    <label for="txtSpComTrain">Special Computer Training:</label>
                                                    <asp:TextBox ID="txtSpComTrain" runat="server" Font-Size="Medium" TabIndex="3" TextMode="MultiLine" Rows="4" CssClass="  form-control" Text='<%# Bind("SpecialComputerTrainingEssay") %>' ></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ValidationGroup="EXVG" Display="Dynamic" ControlToValidate="txtSpComTrain">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                                <asp:TextBox ID="div" runat="server" Text=" " TabIndex="4" Visible="false"></asp:TextBox>
                                                <asp:TextBox ID="IDtextbox" runat="server" Text='<%# Bind("ID") %>' CssClass="form-control" Font-Size="Medium" Style="text-transform: uppercase" TabIndex="1" Visible="False"></asp:TextBox>
                                                <br />
                                                <br />
                                                <asp:Button ID="btnPrevious" runat="server" Text="  Go Back  " OnClick="btnPrevious_Click" CssClass="btn btn-warning btn-lg" />
                                                <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" ValidationGroup="EXVG" CommandName="Update" Text="  Next Page  " CssClass="btn btn-success btn-lg " OnClick="UpdateButton_Click" />
                                                <asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="  Finish Later  " OnClientClick="return confirm('Are you sure you want to quit?');" OnClick="UpdateCancelButton_Click" CssClass="btn btn-danger btn-lg" />
                                            </div>

                                        </EditItemTemplate>
                                    </asp:FormView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>"
        SelectCommand="SELECT SpecialAnimalInventoryEssay, NativeAmphibiansEssay, SpecialComputerTrainingEssay, ID FROM OAQ.ApplicantQuestionaire WHERE (ID = @PerID)"
        UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET SpecialAnimalInventoryEssay = @SpecialAnimalInventoryEssay, NativeAmphibiansEssay = @NativeAmphibiansEssay, SpecialComputerTrainingEssay = @SpecialComputerTrainingEssay WHERE ID=@PerID">
        <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="SpecialAnimalInventoryEssay" />
            <asp:Parameter Name="NativeAmphibiansEssay" />
            <asp:Parameter Name="SpecialComputerTrainingEssay" />
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <script type="text/javascript">
        function SessionExpireAlert(timeout) {
            var seconds = timeout / 1000;
            document.getElementsByName("secondsIdle").innerHTML = seconds;
            document.getElementsByName("seconds").innerHTML = seconds;
            setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
                document.getElementById("secondsIdle").innerHTML = seconds;
            }, 1000);
            setTimeout(function () {
                //Show Popup before 20 seconds of timeout.
                $find("mpeTimeout").show();
            }, timeout - 20 * 1000);
            setTimeout(function () {
                window.location = "Login.aspx";
            }, timeout);
        };
        function ResetSession() {
            //Redirect to refresh Session.
            window.location = window.location.href;
        }
    </script>

</asp:Content>
