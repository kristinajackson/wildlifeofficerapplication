﻿<%@ Page Title="" Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Applicant.aspx.cs" Inherits="Application_Questionaire.Applicant" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <style>
        .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        width: 300px;
        border: 3px solid #0DA9D0;
        border-radius: 12px;
        padding:0
      
    }
    .modalPopup .header
    {
        background-color: #2FBDF1;
        height: 30px;
        color: White;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
    }
    .modalPopup .body
    {
        min-height: 50px;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
    }
    .modalPopup .footer
    {
        padding: 6px;
    }
    .modalPopup .yes, .modalPopup .no
    {
        height: 23px;
        color: White;
        line-height: 23px;
        text-align: center;
        font-weight: bold;
        cursor: pointer;
        border-radius: 4px;
    }
    .modalPopup .yes
    {
        background-color: #2FBDF1;
        border: 1px solid #0DA9D0;
    }
    .modalPopup .no
    {
        background-color: #9F9F9F;
        border: 1px solid #5C5C5C;
    }
    </style>
    </asp:Content>
<asp:Content ID="main" ContentPlaceHolderID="main" runat="server">
    

    <asp:ScriptManager runat="server" ></asp:ScriptManager>
    <%--<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" CombineScripts="false" />--%>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <section class="container " id="section1">
                <div class="col-lg-11" style="margin-left: -5%">

                    <div class=" panel panel-default">
                        <br />
                        <div class="panel-heading">
                        </div>
                        <div>
                            <div class="container" style="margin-left: 5%">
                                <div class="panel-body">
                                    <h6 style="color: #FFFFFF">Session Idle:&nbsp;<span id="secondsIdle"></span>&nbsp;seconds.</h6>
<asp:LinkButton ID="lnkFake" runat="server" />
<asp:ModalPopupExtender ID="mpeTimeout" BehaviorID ="mpeTimeout" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkFake"
    OkControlID="btnYes" CancelControlID="btnNo" BackgroundCssClass="modalBackground" OnOkScript = "ResetSession()">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
    <div class="header">
        Session Expiring!
    </div>
    <div class="body">
        Your Session will expire in&nbsp;<span id="seconds"></span>&nbsp;seconds.<br />
        Are you still there?
    </div>
    <div class="footer" align="right">
        <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="yes"  />
        <asp:Button ID="btnNo" runat="server" Text="No" CssClass="no" />
    </div>
</asp:Panel>

                                    <asp:FormView ID="FormView1" runat="server" DataKeyNames="ID" DataSourceID="SqlDataSource1" DefaultMode="Edit" OnItemInserted="FormView1_ItemInserted" OnItemUpdated="FormView1_ItemUpdated">
                                        <EditItemTemplate>
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Label ID="IDPer" runat="server" Text='<%# Eval("ID") %>' Visible="False"></asp:Label>
                                                    <br />
                                                    <form>
                                                        <div class="ui form">
                                                            <span class="inline fields">*<label for="Dropdownlist1">1. Have you ever been interviewed by TWRA? If yes, where and when? </label>
                                                                <asp:DropDownList ID="Dropdownlist1" runat="server" SelectedValue='<%# Bind("InterviewedbyTWRA") %>'
                                                                    OnSelectedIndexChanged="Dropdownlist1_SelectedIndexChanged" AutoPostBack="true" CssClass="txtsmitem " TabIndex="1">
                                                                    <asp:ListItem></asp:ListItem>
                                                                    <asp:ListItem Value="Yes" Text="Yes"></asp:ListItem>
                                                                    <asp:ListItem Value="No" Text="No"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="Dropdownlist1ReqValidator" runat="server" ErrorMessage="*Please select Yes/No"
                                                                    ValidationGroup="INSVG" ControlToValidate="Dropdownlist1" Display="Dynamic" ForeColor="Red">
                                                                </asp:RequiredFieldValidator>
                                                            </span>
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtques1" CssClass="form-control" Rows="4" runat="server" Text='<%# Bind("When_Interviewed") %>' TabIndex="2" TextMode="MultiLine" AutoPostBack="true" OnTextChanged="txtques1_TextChanged" Font-Size="Medium"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="txtques1ReqValidator" runat="server" ErrorMessage="*You have selected Yes, please explain"
                                                                ValidationGroup="INSVG" ControlToValidate="txtques1" Display="Static" ForeColor="Red">
                                                            </asp:RequiredFieldValidator>
                                                        </div>

                                                        <div class="ui form">
                                                            <span class="inline fields">*<label for="rboques2">2. Do you have any relatives presently employed by TWRA? If yes, please list name(s) and relationship(s).</label>
                                                                <asp:DropDownList ID="rboques2" runat="server" SelectedValue='<%# Bind("InterviewedbyTWRA") %>'
                                                                    OnSelectedIndexChanged="rboques2_SelectedIndexChanged" AutoPostBack="true" CssClass="txtsmitem " TabIndex="3">
                                                                    <asp:ListItem></asp:ListItem>
                                                                    <asp:ListItem Value="Yes" Text="Yes"></asp:ListItem>
                                                                    <asp:ListItem Value="No" Text="No"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*Please select Yes/No"
                                                                    ValidationGroup="INSVG" ControlToValidate="rboques2" Display="Dynamic" ForeColor="Red">
                                                                </asp:RequiredFieldValidator>
                                                            </span>
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtques2" CssClass=" form-control" Rows="4" runat="server" AutoPostBack="true" OnTextChanged="txtques2_TextChanged" TabIndex="4" Text='<%# Bind("Explain_Relatives") %>' TextMode="MultiLine" Font-Size="Medium"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="txtques2ReqValidator" runat="server" ErrorMessage="*You have selected Yes, please explain"
                                                                ValidationGroup="INSVG" ControlToValidate="txtques2" Display="Dynamic" ForeColor="Red">
                                                            </asp:RequiredFieldValidator>
                                                        </div>

                                                        <div class="ui form">
                                                            <span class="inline fields">*<label for="TextBox3">3. Length of Tennessee residence? </label>
                                                                <asp:TextBox ID="TextBox3" runat="server" AutoPostBack="true" OnTextChanged="TextBox3_TextChanged" Text='<%# Bind("YrTNResidence") %>' TabIndex="5" CssClass="txtsmitem" Font-Size="Medium"></asp:TextBox>
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TextBox3" FilterType="Numbers" Enabled="true"></asp:FilteredTextBoxExtender>
                                                                <asp:DropDownList ID="ddlyr" runat="server" SelectedValue='<%# Bind("Yr_TN_Residence_YrMon") %>' TabIndex="6" CssClass="txtsmitem" Height="23px">
                                                                    <asp:ListItem></asp:ListItem>
                                                                    <asp:ListItem Selected="True">Years</asp:ListItem>
                                                                    <asp:ListItem>Months</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*Please select Years/Months"
                                                                    ValidationGroup="INSVG" ControlToValidate="TextBox3" Display="Dynamic" ForeColor="Red">
                                                                </asp:RequiredFieldValidator>
                                                                <asp:RequiredFieldValidator ID="ddlyrReqValidator" runat="server" ErrorMessage="*Please enter number of Years/Months"
                                                                    ValidationGroup="INSVG" ControlToValidate="ddlyr" Display="Dynamic" ForeColor="Red">
                                                                </asp:RequiredFieldValidator>
                                                            </span>
                                                        </div>
                                                        <br />
                                                        <div class="ui form">
                                                            <span class="inline fields">
                                                                <label for="txtlength">If you do not reside in Tennessee, list your current State of residence.</label>
                                                            </span>
                                                        </div>
                                                        <div class="form-group" style="font-family: Arial, Helvetica, sans-serif;">
                                                            <asp:TextBox ID="txtlength" CssClass="form-control" Rows="4" runat="server" Text='<%# Bind("List_Residence") %>' TabIndex="7" TextMode="MultiLine" Font-Size="Medium" AutoPostBack="true"></asp:TextBox>
                                                        </div>

                                                        <div class="ui form">
                                                            <span class="inline fields">
                                                                <label for="txtques3">4. If you currently hold a hunting/fishing license in Tennessee, please list License Type and your TWRA Identifying No.:</label>
                                                            </span>
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtques3" CssClass=" form-control" Rows="4" runat="server" Text='<%# Bind("[FishingHuntingTypeNo]") %>' TabIndex="8" TextMode="MultiLine" Font-Size="Medium" AutoPostBack="true"></asp:TextBox>
                                                        </div>

                                                        <div class="ui form">
                                                            <span class="inline fields">*<label for="rboques5">5. Do you have prior law enforcement experience? If yes, please explain: </label>
                                                                <asp:DropDownList ID="rboques5" runat="server" Text='<%# Bind("Enforcement_Exper") %>'
                                                                    SelectedValue='<%# Bind("Enforcement_Exper") %>' AutoPostBack="true" OnSelectedIndexChanged="rboques5_SelectedIndexChanged" CssClass="txtsmitem" TabIndex="9">
                                                                    <asp:ListItem></asp:ListItem>
                                                                    <asp:ListItem>Yes</asp:ListItem>
                                                                    <asp:ListItem>No</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rboques5ReqValidator" runat="server" ErrorMessage="*Please select Yes/No"
                                                                    ValidationGroup="INSVG" ControlToValidate="rboques5" Display="Dynamic" ForeColor="Red">
                                                                </asp:RequiredFieldValidator>
                                                            </span>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtques5" CssClass="form-control" Rows="4" runat="server" AutoPostBack="true" Text='<%# Bind("Explain_Enforcement") %>' TabIndex="10" TextMode="MultiLine" Font-Size="Medium" OnTextChanged="Button2_Click"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="txtques5ReqValidator" runat="server" ErrorMessage="*You have selected Yes for question 5, please explain"
                                                                ValidationGroup="INSVG" ControlToValidate="txtques5" Display="Dynamic" ForeColor="Red">
                                                            </asp:RequiredFieldValidator>
                                                        </div>

                                                        <div class="ui form">
                                                            <span class="inline fields">*<label for="rboques6">6. Do you have prior law enforcement training? If yes, please explain:</label>
                                                                <asp:DropDownList ID="rboques6" runat="server" SelectedValue='<%# Bind("Enforcement_Train") %>'
                                                                    OnSelectedIndexChanged="rboques6_SelectedIndexChanged" AutoPostBack="true" CssClass="txtsmitem" TabIndex="11">
                                                                    <asp:ListItem></asp:ListItem>
                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rboques6ReqValidator" runat="server" ErrorMessage="*Please select Yes/No"
                                                                    ValidationGroup="INSVG" ControlToValidate="rboques6" Display="Dynamic" ForeColor="Red">
                                                                </asp:RequiredFieldValidator>
                                                            </span>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtques6" CssClass="form-control" Rows="4" runat="server" Text='<%# Bind("Explain_EnforcementTrain") %>' TabIndex="12" TextMode="MultiLine" Font-Size="Medium" AutoPostBack="true"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="txtques6ReqValidator" runat="server" ErrorMessage="*You have selected Yes for question 6, please explain"
                                                                ValidationGroup="INSVG" ControlToValidate="txtques6" Display="Dynamic" ForeColor="Red" SetFocusOnError="true">
                                                            </asp:RequiredFieldValidator>
                                                        </div>

                                                        <div class="ui form">
                                                            <span class="inline fields">*<label for="txtques7">7. Other than regular college courses, list any advanced career development or law enforcement courses that you have taken. </label>
                                                            </span>
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtques7" CssClass="form-control" Rows="4" runat="server" Text='<%# Bind("Advanced_Courses") %>' TabIndex="13" TextMode="MultiLine" Font-Size="Medium" AutoPostBack="true"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="txtques7ReqValidator" runat="server" ErrorMessage="*Please answer question 7.If none, type N/A"
                                                                ValidationGroup="INSVG" ControlToValidate="txtques7" Display="Dynamic" ForeColor="Red" SetFocusOnError="true">
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="ui form">
                                                            <span class="inline fields">*
                                                                <label for="txtques8">8. What college courses did you take that you feel would be beneficial to you as a Wildlife Officer? </label>
                                                            </span>
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtques8" CssClass="form-control" Rows="4" runat="server" Text='<%# Bind("Benefical_Courses") %>' TabIndex="14" TextMode="MultiLine" Font-Size="Medium" AutoPostBack="true"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="txtques8ReqValidator" runat="server" ErrorMessage="*Please answer question 8. If none, type N/A"
                                                                ValidationGroup="INSVG" ControlToValidate="txtques8" Display="Dynamic" ForeColor="Red" SetFocusOnError="true">
                                                            </asp:RequiredFieldValidator>
                                                        </div>

                                                        <div class="ui form">
                                                            <span class="inline fields">
                                                                <label for="txtques9">9. If you have hobbies, please list in order of preference. </label>
                                                            </span>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtques9" CssClass="form-control" Rows="4" runat="server" Text='<%# Bind("Hobbies") %>' TabIndex="15" TextMode="MultiLine" Font-Size="Medium" AutoPostBack="true"></asp:TextBox>
                                                        </div>
                                                    </form>
                                                    <br />
                                                    <br />

                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="INSVG" ForeColor="Red" HeaderText="Erorr! Required Field " Font-Bold="True" Font-Size="Large" ShowSummary="True" ShowValidationErrors="True" DisplayMode="BulletList" BackColor="#CCCCCC" BorderColor="Black" BorderStyle="Dotted" />
                                                    <asp:Label ID="lblMsg" runat="server" CssClass="txtitem" ForeColor="#CC0000" Font-Bold="True"></asp:Label>
                                                    <asp:Button ID="btnPrevious" runat="server" CssClass="btn btn-warning btn-lg"  Text="  Go Back  " OnClick="Button2_Click" />
                                                    <asp:Button ID="UpdateButton" runat="server" CssClass="btn btn-success btn-lg " OnClick="UpdateButton_Click" Text="  Next Page  " CommandName="Update" CausesValidation="True" ValidationGroup="INSVG" />
                                                    &nbsp;<asp:Button ID="UpdateCancelButton" runat="server" CssClass="btn btn-danger btn-lg" CausesValidation="False" CommandName="Cancel" OnClick="btnQuit_Click" OnClientClick="return confirm('Are you sure you want to quit?');" Text="  Finish Later  " />

                                                    
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </EditItemTemplate>
                                    </asp:FormView>
   </div>
                                </div></div></div></div></section>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>"
        SelectCommand="SELECT ID, InterviewedbyTWRA, When_Interviewed, Relatives, Explain_Relatives, YrTNResidence, List_Residence, FishingHuntingTypeNo, Enforcement_Exper, Explain_Enforcement, Enforcement_Train, Explain_EnforcementTrain, Advanced_Courses, Benefical_Courses, Hobbies, Yr_TN_Residence_YrMon FROM OAQ.ApplicantQuestionaire WHERE (ID = @PerID)"
        UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET InterviewedbyTWRA = @InterviewedbyTWRA, When_Interviewed = @When_Interviewed, Relatives = @Relatives, YrTNResidence = @YrTNResidence, Explain_Relatives = @Explain_Relatives, List_Residence = @List_Residence, Enforcement_Exper = @Enforcement_Exper, FishingHuntingTypeNo = @FishingHuntingTypeNo, Explain_Enforcement = @Explain_Enforcement, Hobbies = @Hobbies, Benefical_Courses = @Benefical_Courses, Advanced_Courses = @Advanced_Courses, Enforcement_Train = @Enforcement_Train, Explain_EnforcementTrain = @Explain_EnforcementTrain, Yr_TN_Residence_YrMon = @Yr_TN_Residence_YrMon WHERE (ID = @PerID)">
        <%--       --%>
        <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="InterviewedbyTWRA" />
            <asp:Parameter Name="When_Interviewed" />
            <asp:Parameter Name="Relatives" />
            <asp:Parameter Name="YrTNResidence" />
            <asp:Parameter Name="Explain_Relatives" />
            <asp:Parameter Name="List_Residence" />
            <asp:Parameter Name="Enforcement_Exper" />
            <asp:Parameter Name="FishingHuntingTypeNo" />
            <asp:Parameter Name="Explain_Enforcement" />
            <asp:Parameter Name="Hobbies" />
            <asp:Parameter Name="Benefical_Courses" />
            <asp:Parameter Name="Advanced_Courses" />
            <asp:Parameter Name="Enforcement_Train" />
            <asp:Parameter Name="Explain_EnforcementTrain" />
            <asp:Parameter Name="Yr_TN_Residence_YrMon" />
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>


    <script type="text/javascript">
        function SessionExpireAlert(timeout) {
            var seconds = timeout / 1000;
            document.getElementsByName("secondsIdle").innerHTML = seconds;
            document.getElementsByName("seconds").innerHTML = seconds;
            setInterval(function () {
                seconds--;
                document.getElementById("seconds").innerHTML = seconds;
                document.getElementById("secondsIdle").innerHTML = seconds;
            }, 1000);
            setTimeout(function () {
                //Show Popup before 20 seconds of timeout.
                $find("mpeTimeout").show();
            }, timeout - 20 * 1000);
            setTimeout(function () {
                window.location = "Login.aspx";
            }, timeout);
        };
        function ResetSession() {
            //Redirect to refresh Session.           
            window.location = window.location.href;
        }
</script>


</asp:Content>
