﻿using System;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.UI.WebControls;

namespace Application_Questionaire.Class
{
    public class AppUtility
    {

        // data procedures *************************************************************************************************

        //public UserApplication GetUsernameandPassword(string UserName, string Password) 
        //{
        //    DataSet ds = new DataSet();
        //    UserApplication uai = new UserApplication();

        //    try
        //    {
        //        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ApplicantConnectionString"].ConnectionString))
        //        {
        //            using (SqlCommand cmd = new SqlCommand("OAQ.GetUsernameandPassword", con))
        //            {
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@UserName", UserName);
        //                cmd.Parameters.AddWithValue("@Password", Password);

        //                using (SqlDataAdapter da = new SqlDataAdapter())
        //                {
        //                    da.SelectCommand = cmd;
        //                    da.Fill(ds);
        //                }
        //            }

        //            DataRow dr = ds.Tables[0].Rows[0];

        //            uai.UserName = (dr["UserName"]).ToString();
        //            uai.Password = (dr["Password"]).ToString();

        //        }
        //    }
        //    catch
        //    {

        //    }
        //    return uai;  // return a populated fci or a null fci
        //}

        public DataSet GetUsernameandPassword(string UserName, string Password)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ApplicantConnectionString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("OAQ.GetUsernameandPassword", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserName", UserName);
                    cmd.Parameters.AddWithValue("@Password", Password);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }
    }
}