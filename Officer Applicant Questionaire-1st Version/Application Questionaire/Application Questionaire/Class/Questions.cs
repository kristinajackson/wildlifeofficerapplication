﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

namespace Application_Questionaire1
{
    public class Questions 
    {
        public Questions() { }

        public string Fname { get; set; }
        public string MiddleInt { get; set; }
        public string Lname { get; set; }
        public string InterviewedbyTWRA { get; set; }
        public string Where_Interviewed { get; set; }
        public string When_Interviewed { get; set; }
        public string Relatives { get; set; }
        public string Explain_Relatives { get; set; }
        public int YrTNResidence { get; set; }
        public string List_Residence { get; set; }
        public string FishingHuntingTypeNo { get; set; }
        public string Enforcement_Exper { get; set; }
        public string Explain_Enforcement { get; set; }
        public string Enforcement_Train { get; set; }
        public string Explain_EnforcementTrain { get; set; }
        public string Advanced_Courses { get; set;}
        public string Benefical_Courses {get; set;}
        public int Hobbies {get; set;}
        public int Farming {get; set;}
        public int Forestry {get; set;}
        public int AnimalHusb { get; set;}
        public int Comm_Fish {get; set;}
        public int Sm_Gas_Eng {get; set;}
        public int AutoRepair {get; set;}
        public int BoatOper {get; set;}
        public int Hunting {get; set;}
        public int Fishing {get; set;}
        public int Camping {get; set;}
        public int Swimming {get; set;}
        public int Speci_Animal_Inv {get; set;}
        public int FishID {get; set;}
        public int NavBirdsID {get; set;}
        public int NavMamID {get; set;}
        public int NavRepID {get; set;}
        public int NavAmp {get; set;}
        public int AirBoat {get; set;}
        public int OutBrdMotors {get; set;}
        public int FourWheelDri {get; set;}
        public int FarmTrac {get; set;}
        public int Bulldozer {get; set;}
        public int ChainSaw {get; set;}
        public int Rifle {get; set;}
        public int Pistol {get; set;}
        public int Shotgun {get; set;}
        public int Spec_Com_Train {get; set;}
        public string GroupMem {get; set;}
        public string Outdoor_Exp {get; set;}
        public string Responsible_Pos {get; set;}
        public string Manage_Exp {get; set;}
        public string Convictied {get; set;}
        public string Explain_Charge {get; set;}
        public string Traffic_Cit {get; set;}
        public string OthStateEmploy {get; set;}
        public string Reason_WLO {get; set;}
        public string Events_ForDecision {get; set;}
        public string CourseofAction {get; set;}
        public string Qualities {get; set;}
        public string MajorConernforWL {get; set;}
        public string Address {get; set;}
        public string HomePhone {get; set;}
        public string WorkPhone {get; set;}
        public string EmpConNo {get; set;}
        public string AlternateNo {get; set;}
        public DateTime Residence_FromDate1 {get; set;}
        public DateTime Residence_ToDate1 {get; set;}
        public string ResidenceAddress1 {get; set;}
        public DateTime CreatedDate {get; set;}
        public string Yr_TN_Residence_YrMon {get; set;}
        public DateTime Residence_FromDate2 {get; set;}
        public DateTime Residence_ToDate2 {get; set;}
        public DateTime Residence_FromDate3 {get; set;}
        public DateTime Residence_ToDate3 {get; set;}
        public string ResidenceAddress2 {get; set;}
        public string ResidenceAddress3 {get; set;}
        public string Country {get; set;}
        public string State {get; set;}
        public string Zipcode { get; set; }
        


        

        public Questions(
         string p_Fname,
         string p_MiddleInt,
        string p_Lname,
        string p_InterviewedbyTWRA,
        string p_Where_Interviewed,
        string p_When_Interviewed,
         string p_Relatives,
         string p_Explain_Relatives,
         int p_YrTNResidence,
         string p_List_Residence,
         string p_FishingHuntingTypeNo,
         string p_Enforcement_Exper,
         string p_Explain_Enforcement,
         string p_Enforcement_Train,
        string p_Explain_EnforcementTrain,
         string p_Advanced_Courses,
         string p_Benefical_Courses,
        int p_Hobbies,
         int p_Farming,
         int p_Forestry,
         int p_AnimalHusb,
         int p_Comm_Fish,
         int p_Sm_Gas_Eng,
         int p_AutoRepair,
         int p_BoatOper,
         int p_Hunting,
         int p_Fishing,
         int p_Camping,
         int p_Swimming,
         int p_Speci_Animal_Inv,
         int p_FishID,
         int p_NavBirdsID,
         int p_NavMamID,
         int p_NavRepID,
         int p_NavAmp,
         int p_AirBoat,
         int p_OutBrdMotors,
         int p_FourWheelDri,
         int p_FarmTrac,
         int p_Bulldozer,
         int p_ChainSaw,
         int p_Rifle,
         int p_Pistol,
         int p_Shotgun,
         int p_Spec_Com_Train,
         string p_GroupMem,
         string p_Outdoor_Exp,
         string p_Responsible_Pos,
         string p_Manage_Exp,
         string p_Convictied,
         string p_Explain_Charge,
         string p_Traffic_Cit,
         string p_OthStateEmploy,
         string p_Reason_WLO,
         string p_Events_ForDecision,
         string p_CourseofAction,
         string p_Qualities,
         string p_MajorConernforWL,
         string p_Address,
         string p_HomePhone,
         string p_WorkPhone,
         string p_EmpConNo,
         string p_AlternateNo,
         DateTime p_Residence_FromDate1,
         DateTime p_Residence_ToDate1,
         string p_ResidenceAddress1,
         DateTime p_CreatedDate,
         string p_Yr_TN_Residence_YrMon,
         DateTime p_Residence_FromDate2,
         DateTime p_Residence_ToDate2,
         DateTime p_Residence_FromDate3,
         DateTime p_Residence_ToDate3,
         string p_ResidenceAddress2,
         string p_ResidenceAddress3,
         string p_Country,
         string p_State,
         string p_Zipcode)

        {
            this.Fname = p_Fname;
            this.MiddleInt = p_MiddleInt;
            this.Lname = p_Lname;
            this.InterviewedbyTWRA = p_InterviewedbyTWRA;
            this.Where_Interviewed = p_Where_Interviewed;
            this.When_Interviewed = p_When_Interviewed;
            this.Relatives = p_Relatives;
            this.Explain_Relatives = p_Explain_Relatives;
            this.YrTNResidence = p_YrTNResidence;
            this.List_Residence = p_List_Residence;
            this.FishingHuntingTypeNo = p_FishingHuntingTypeNo;
            this.Enforcement_Exper = p_Enforcement_Exper;
            this.Explain_Enforcement = p_Explain_Enforcement;
            this.Enforcement_Train = p_Enforcement_Train;
            this.Explain_EnforcementTrain = p_Explain_EnforcementTrain;
            this.Advanced_Courses = p_Advanced_Courses;
            this.Benefical_Courses = p_Benefical_Courses;
            this.Hobbies = p_Hobbies;
            this.Farming = p_Farming;
            this.Forestry = p_Forestry;
            this.AnimalHusb = p_AnimalHusb;
            this.Comm_Fish =  p_Comm_Fish;
            this.Sm_Gas_Eng = p_Sm_Gas_Eng;
            this.AutoRepair =  p_AutoRepair;
            this.BoatOper =  p_BoatOper;
            this.Hunting =  p_Hunting;
            this.Fishing = p_Fishing;
            this.Camping = p_Camping;
            this.Swimming = p_Swimming;
            this.Speci_Animal_Inv = p_Speci_Animal_Inv;
            this.FishID = p_FishID;
            this.NavBirdsID = p_NavBirdsID;
            this.NavMamID = p_NavMamID;
            this.NavRepID = p_NavRepID;
            this.NavAmp = p_NavAmp;
            this.AirBoat = p_AirBoat;
            this.OutBrdMotors = p_OutBrdMotors;
            this.FourWheelDri = p_FourWheelDri;
            this.FarmTrac = p_FarmTrac;
            this.Bulldozer = p_Bulldozer;
            this.ChainSaw = p_ChainSaw;
            this.Rifle = p_Rifle;
            this.Pistol = p_Pistol;
            this.Shotgun = p_Shotgun;
            this.Spec_Com_Train = p_Spec_Com_Train;
            this.GroupMem = p_GroupMem;
            this.Outdoor_Exp = p_Outdoor_Exp;
            this.Responsible_Pos = p_Responsible_Pos;
            this.Manage_Exp = p_Manage_Exp;
            this.Convictied = p_Convictied;
            this.Explain_Charge = p_Explain_Charge;
            this.Traffic_Cit = p_Traffic_Cit;
            this.OthStateEmploy = p_OthStateEmploy;
            this.Reason_WLO = p_Reason_WLO;
            this.Events_ForDecision = p_Events_ForDecision;
            this.CourseofAction = p_CourseofAction;
            this.Qualities = p_Qualities;
            this.MajorConernforWL = p_MajorConernforWL;
            this.Address = p_Address;
            this.HomePhone = p_HomePhone;
            this.WorkPhone = p_WorkPhone;
            this.EmpConNo = p_EmpConNo;
            this.AlternateNo = p_AlternateNo;
            this.Residence_FromDate1 = p_Residence_FromDate1;
            this.Residence_ToDate1 = p_Residence_ToDate1;
            this.ResidenceAddress1 = p_ResidenceAddress1;
            this.CreatedDate = p_CreatedDate;
            this.Yr_TN_Residence_YrMon = p_Yr_TN_Residence_YrMon;
            this.Residence_FromDate2 = p_Residence_FromDate2;
            this.Residence_ToDate2 = p_Residence_ToDate2;
            this.Residence_ToDate3 = p_Residence_FromDate3;
            this.Residence_ToDate3 = p_Residence_ToDate3;
            this.ResidenceAddress2 = p_ResidenceAddress2;
            this.ResidenceAddress3 = p_ResidenceAddress3;
            this.Country = p_Country;
            this.State = p_State;
            this.Zipcode = p_Zipcode;
        }

    }
}