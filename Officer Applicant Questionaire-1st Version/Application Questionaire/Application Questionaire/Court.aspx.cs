﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Application_Questionaire
{
    public partial class Court : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void UpdateCancelButton_Click(object sender, EventArgs e)
        {//Update table and Take user to the logout page when click cancel button
            DropDownList Convictied = (DropDownList)FormView1.FindControl("rboConvict");
            TextBox Explain_Charge = (TextBox)FormView1.FindControl("txtCharge");
            TextBox Traffic_Cit = (TextBox)FormView1.FindControl("txtCitation");
            TextBox OthStateEmploy = (TextBox)FormView1.FindControl("txtOthState");

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ApplicantConnectionString"].ConnectionString))
            {
                String query = "UPDATE OAQ.ApplicantQuestionaire SET Convictied = @Convictied, Explain_Charge = @Explain_Charge, Traffic_Cit = @Traffic_Cit, OthStateEmploy = @OthStateEmploy WHERE ID=@PerID";

                SqlCommand command = new SqlCommand(query, con);
                command.Parameters.AddWithValue("Convictied", Convictied.SelectedItem.ToString());
                command.Parameters.AddWithValue("Explain_Charge", Explain_Charge.Text);
                command.Parameters.AddWithValue("Traffic_Cit", Traffic_Cit.Text);
                command.Parameters.AddWithValue("OthStateEmploy", OthStateEmploy.Text);

                command.Parameters.AddWithValue("@PerID", Convert.ToInt32(Session["PersonID"].ToString()));
                con.Open();

                command.ExecuteNonQuery();
                con.Close();
            }

            Response.Redirect("Logout.aspx");
            }
           

        protected void FormView1_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {//Save user information
            Session["PersonID"] = e.Values["ID"].ToString();
        }

        protected void FormView1_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
             if (e.Exception != null)
            { // update table and send user to next page
                
                e.ExceptionHandled = true;
                e.KeepInEditMode = true;
            }
            else
                 Response.Redirect("WildlifeOfficerReason.aspx");
        }
        }

   
    }
