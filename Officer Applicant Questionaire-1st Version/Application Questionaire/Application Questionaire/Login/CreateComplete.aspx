﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="CreateComplete.aspx.cs" Inherits="Application_Questionaire.CreateComplete" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <style>
        .center {
        text-align:center;
        }
        .right {
        text-align:right;
        }
    </style>
    <asp:Panel ID="userInfoPanel" runat="server">
        <table style="font-size:100%;height:319px;width:599px;">
            <tr>
                <td class="center" colspan="2">Complete</td>
            </tr>
            <tr>
                <td>Your account has been successfully created.</td>
            </tr>
            <tr>
                <td class="right" colspan="2">
                    <asp:Button ID="ContinueButton" runat="server" CausesValidation="False" CommandName="Continue" OnClick="ContinueButton_Click" PostBackUrl="~/Residences.aspx" Text="Continue" ValidationGroup="CreateUserWizard1" />
                </td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>
