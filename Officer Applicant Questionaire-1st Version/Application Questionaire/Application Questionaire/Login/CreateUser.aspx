﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="CreateUser.aspx.cs" Inherits="Application_Questionaire1.CreateUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .center {
            text-align:center;
        }
        .right {
        text-align:right;
        }
        #right {
        text-align:right;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
   <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Insert" OnItemInserted="FormView1_ItemInserted">
        <InsertItemTemplate>
            <asp:Panel ID="enterUserInfoPanel" runat="server" Width="621px">
            <table style="font-size:100%;height:319px;width:599px;">
                        <tr>
                            
                            <td class="center" colspan="2">
                                <br />
                                Sign Up for Your New Account</td>
                        </tr>
                        <tr>
                            <td class="center" colspan="2">Please enter your user information:</td>
                        </tr>
                        <tr>
                            <td class="auto-style2">
                                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                            </td>
                            <td class="auto-style2">
                                <asp:TextBox ID="UserName" runat="server" Text='<%# Bind("UserName") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td  class="auto-style2">
                                <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                            </td>
                            <td class="auto-style2">
                                <asp:TextBox ID="Password" runat="server" TextMode="Password" Text='<%# Bind("Password") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                       <%-- <tr>
                            <td class="auto-style2">
                                <asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword">Confirm Password:</asp:Label>
                            </td>
                            <td class="auto-style2">
                                <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" Text='<%# Bind("ConfirmPassword") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" ControlToValidate="ConfirmPassword" ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                 </td>
                        </tr>--%>
                        <tr>
                            <td class="auto-style2">
                                <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">E-mail:</asp:Label>
                            </td>
                            <td class="auto-style2">
                                <asp:TextBox ID="Email" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email" ErrorMessage="E-mail is required." ToolTip="E-mail is required." ValidationGroup="CreateUserWizard1" >*</asp:RequiredFieldValidator>
                              <%-- <asp:RegularExpressionValidator ID="EmailRegular" runat="server" ControlToValidate="Email"  ErrorMessage="E-mail is invaild." ToolTip="E-mail is invaild." ValidationGroup="CreateUserWizard1" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>--%>
                         <asp:RegularExpressionValidator ID="EmailRegular" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="Email" ErrorMessage="E-mail is required." ToolTip="E-mail is required." ValidationGroup="CreateUserWizard1" >*</asp:RegularExpressionValidator>     
                            </td>
                        </tr>
                        <tr>
                            <%--<td  class="auto-style2">
                                <asp:Label ID="QuestionLabel" runat="server" AssociatedControlID="DropDownList1">Security Question:</asp:Label>
                            </td>
                            <td class="auto-style2"> 
                                <asp:DropDownList ID="DropDownList1" runat="server" SelectedValue='<%# Bind("SecurityQuestion") %>' Height="16px">
                                    <asp:ListItem>What is your favorite color?</asp:ListItem>
                                    <asp:ListItem>What was your first car?</asp:ListItem>
                                    <asp:ListItem>What is your Mom first name?</asp:ListItem>
                                    <asp:ListItem>What is your dad last name?</asp:ListItem>
                                    <asp:ListItem>What is your dog name?</asp:ListItem>
                                    <asp:ListItem>Where is your favorite vacation spot?</asp:ListItem>
                                    <asp:ListItem></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="QuestionRequired" runat="server" ControlToValidate="DropDownList1" ErrorMessage="Security question is required." ToolTip="Security question is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style2">
                                <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer">Security Answer:</asp:Label>
                            </td>
                            <td class="auto-style2">
                                <asp:TextBox ID="Answer" runat="server" Text='<%# Bind("SecurityAnswer") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="AnswerRequired" runat="server" ControlToValidate="Answer" ErrorMessage="Security answer is required." ToolTip="Security answer is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                            </td>--%>
                            <td>
                                <asp:Button ID="InsertButton" runat="server" CausesValidation="True"
                            CommandName="Insert" Text="   SUBMIT   "  ValidationGroup="CreateUserWizard1"  />
                                <%--<asp:Button ID="SubmitButton" runat="server" CommandName="Insert" Text="Submit" ValidationGroup="CreateUserWizard1" PostBackUrl="~/CreateComplete.aspx" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style2" colspan="2">
                             <%--   <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match." ValidationGroup="CreateUserWizard1"></asp:CompareValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style2" colspan="2" style="color:Red;">
                                <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
                                <br />
                                
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
        </InsertItemTemplate>
    </asp:FormView>
    <asp:Label ID="lblmsg" runat="server" Text="Label"></asp:Label>
  &nbsp;<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>" InsertCommand="INSERT INTO OAQ.ApplicantQuestionaire(UserName, Password,  Email) VALUES (@UserName, @Password, @Email)" SelectCommand="SELECT OAQ.ApplicantQuestionaire.* FROM OAQ.ApplicantQuestionaire">
        <InsertParameters>
            <asp:Parameter Name="UserName" />
            <asp:Parameter Name="Password" />
            <asp:Parameter Name="Email" />
        </InsertParameters>
    </asp:SqlDataSource>
    <br />
    <%--<asp:CreateUserWizard ID="CreateUserWizard1" runat="server"  CancelDestinationPageUrl="~/Login.aspx" 
            ContinueDestinationPageUrl="~/Residences.aspx" DisplayCancelButton="True"
            oncreatinguser="CreateUserWizard1_CreatingUser">
        <WizardSteps>
            <asp:CreateUserWizardStep runat="server" />
            <asp:CompleteWizardStep runat="server" />
        </WizardSteps>
    </asp:CreateUserWizard>
    </p>
    <p>
        <asp:Label runat="server" id="InvalidUserNameOrPasswordMessage" Visible="false" EnableViewState="false" ForeColor="Red"></asp:Label>
    </p>
    
    <p>
        Enter a username: 
        <asp:TextBox ID="Username" runat="server" Visible="false"></asp:TextBox>
        <br />
        
        Choose a password:
        <asp:TextBox ID="Password" TextMode="Password" runat="server"></asp:TextBox>        
        <br />
        
        Enter your email address:
        <asp:TextBox ID="Email" runat="server"></asp:TextBox>
        <br />
        
        <asp:Label runat="server" ID="SecurityQuestion"></asp:Label>: 
        <asp:TextBox ID="SecurityAnswer" runat="server"></asp:TextBox>
        <br />
                
        <asp:Button ID="CreateAccountButton" runat="server" 
            Text="Create the User Account" OnClick="CreateAccountButton_Click" />
    </p>
    <p>
        <asp:Label ID="CreateAccountResults" runat="server"></asp:Label>
    </p>    
<br />
    <br />--%>

</asp:Content>
