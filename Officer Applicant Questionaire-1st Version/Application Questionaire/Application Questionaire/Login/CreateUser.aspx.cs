﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Runtime;
using System.Diagnostics;
using System.Data.SqlClient;

namespace Application_Questionaire1
{
    public partial class CreateUser : System.Web.UI.Page
    {
        //const string passwordQuestion = "What is your favorite color";

        protected void Page_Load(object sender, EventArgs e)
        {
            // if (!Page.IsPostBack)
            //SecurityQuestion.Text = passwordQuestion;    
        }

        //protected void SubmitButton_Click(object sender, EventArgs e)
        //{
        //    lblmsg.Text = "User has been created.";

        //    Response.Redirect("Residences.aspx");
        //}

        protected void FormView1_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
        if (e.Exception == null)
            {
                if (e.AffectedRows == 1)
                {   // to prevent save duplicated record when page is refershed
                    lblmsg.Text = "User has been created";
                    Response.AddHeader("Refresh", "5");
                    Response.Redirect("~/Login.aspx");
                    //e.KeepInInsertMode = true;
                    //TextBox tw = (TextBox)FormView1.FindControl("TWRAIDTextBox");
                    //SetFocus(tw);
                }
            }
            else
            { // Display the Error Message if the Record Not Saved
                if (e.Exception.Message.Contains("UNIQUE KEY"))
                    lblmsg.Text = "ERROR: You already have an account!";
                else
                    lblmsg.Text = e.Exception.Message;
                e.ExceptionHandled = true;
                e.KeepInInsertMode = true;
            }
        }
    }


}
        //protected void CreateAccountButton_Click(object sender, EventArgs e)
        //{
        //MembershipCreateStatus createStatus;

        //MembershipUser newUser = 
        //     Membership.CreateUser(Username.Text, Password.Text,
        //                           Email.Text, passwordQuestion,
        //                           SecurityAnswer.Text, true,
        //                           out createStatus);

        //switch (createStatus)
        //{
        //    case MembershipCreateStatus.Success:
        //        CreateAccountResults.Text = "The user account was successfully created!";
        //        break;

        //    case MembershipCreateStatus.DuplicateUserName:
        //        CreateAccountResults.Text = "There already exists a user with this username.";
        //        break;

        //    case MembershipCreateStatus.DuplicateEmail:
        //        CreateAccountResults.Text = "There already exists a user with this email address.";
        //        break;

        //    case MembershipCreateStatus.InvalidEmail:
        //        CreateAccountResults.Text = "There email address you provided in invalid.";
        //        break;

        //    case MembershipCreateStatus.InvalidAnswer:
        //        CreateAccountResults.Text = "There security answer was invalid.";
        //        break;

        //    case MembershipCreateStatus.InvalidPassword:
        //        CreateAccountResults.Text = "The password you provided is invalid. It must be seven characters long and have at least one non-alphanumeric character.";
        //        break;

        //    default:
        //        CreateAccountResults.Text = "There was an unknown error; the user account was NOT created.";
        //        break;
        //}
        //}

        //protected void CreateUserWizard1_CreatingUser(object sender, LoginCancelEventArgs e)
        //{
        // string trimmedUserName = CreateUserWizard1.UserName.Trim();
        //if (CreateUserWizard1.UserName.Length != trimmedUserName.Length)
        //{
        //    // Show the error message
        //    InvalidUserNameOrPasswordMessage.Text = "The username cannot contain leading or trailing spaces.";
        //    InvalidUserNameOrPasswordMessage.Visible = true;

        //    // Cancel the create user workflow
        //    e.Cancel = true;
        //}
        //else
        //{
        //    // Username is valid, make sure that the password does not contain the username
        //    if (CreateUserWizard1.Password.IndexOf(CreateUserWizard1.UserName, StringComparison.OrdinalIgnoreCase) >= 0)
        //    {
        //        // Show the error message
        //        InvalidUserNameOrPasswordMessage.Text = "The username may not appear anywhere in the password.";
        //        InvalidUserNameOrPasswordMessage.Visible = true;

        //        // Cancel the create user workflow
        //        e.Cancel = true;
        //    }
        //}
        //}
            //Panel enterUserInfoPanel = (Panel)FormView1.FindControl("enterUserInfoPanel");
            //Panel userInfoPanel = (Panel)FormView1.FindControl("userInfoPanel");
            //TextBox UserName = (TextBox)FormView1.FindControl("UserName");
            //if (!IsPostBack)
            //{
            //    if (Session["UserName"] == null)
            //    {
            //        enterUserInfoPanel.Visible = true;
            //    }
            //    else
            //    {
            //        enterUserInfoPanel.Visible = false;

                    
            //    }
            //}
        

        //protected void SubmitButton_Click(object sender, EventArgs e)
        //{

        //    Panel enterUserInfoPanel = (Panel)FormView1.FindControl("enterUserInfoPanel");
        //    Panel userInfoPanel = (Panel)FormView1.FindControl("userInfoPanel");

        //    enterUserInfoPanel.Visible = false;
        //    userInfoPanel.Visible = true;

        //    TextBox UserName = (TextBox)FormView1.FindControl("UserName");
        //    TextBox Password = (TextBox)FormView1.FindControl("Password");
        //    TextBox ConfirmPassword = (TextBox)FormView1.FindControl("ConfirmPassword");
        //    TextBox Email = (TextBox)FormView1.FindControl("Email");
        //    DropDownList DropDownList1 = (DropDownList)FormView1.FindControl("DropDownList1");
        //    TextBox Answer = (TextBox)FormView1.FindControl("Answer");

        //    Response.Redirect("CreateComplete.aspx");



//}
    

