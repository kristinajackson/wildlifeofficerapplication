﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;

namespace Application_Questionaire
{
    public partial class Experience : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

      

        protected void UpdateCancelButton_Click(object sender, EventArgs e)
        {//Update table and Take user to the logout page when click cancel button
            TextBox GroupMem = (TextBox)FormView1.FindControl("txtquesMem");
            TextBox Outdoor_Exp = (TextBox)FormView1.FindControl("txtques10");
            TextBox Responsible_Pos = (TextBox)FormView1.FindControl("txtques11");
            TextBox Manage_Exp = (TextBox)FormView1.FindControl("txtques12");

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ApplicantConnectionString"].ConnectionString))
            {
                String query = "UPDATE OAQ.ApplicantQuestionaire SET GroupMem = @GroupMem, Outdoor_Exp = @Outdoor_Exp, Responsible_Pos = @Responsible_Pos, Manage_Exp = @Manage_Exp WHERE ID=@PerID";

                SqlCommand command = new SqlCommand(query, con);
                command.Parameters.AddWithValue("GroupMem", GroupMem.Text);
                command.Parameters.AddWithValue("Outdoor_Exp", Outdoor_Exp.Text);
                command.Parameters.AddWithValue("Responsible_Pos", Responsible_Pos.Text);
                command.Parameters.AddWithValue("Manage_Exp", Manage_Exp.Text);

                command.Parameters.AddWithValue("@PerID", Convert.ToInt32(Session["PersonID"].ToString()));
                con.Open();

                command.ExecuteNonQuery();
                con.Close();
            }

            Response.Redirect("Logout.aspx");
            }

        protected void FormView1_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            Session["PersonID"] = e.Values["ID"].ToString();
        }

        protected void FormView1_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            if (e.Exception != null)
            { // Display the Error Message if the Record Not Saved
                
                e.ExceptionHandled = true;
                e.KeepInEditMode = true;
            }
            else
                Response.Redirect("Court.aspx");
        }


        }

    }
