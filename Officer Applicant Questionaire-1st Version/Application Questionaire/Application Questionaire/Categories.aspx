﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Categories.aspx.cs" Inherits="Application_Questionaire.Categories" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="main" ContentPlaceHolderID="main" runat="server">
    <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Edit" DataKeyNames="ID" OnItemInserted="FormView1_ItemInserted" OnItemUpdated="FormView1_ItemUpdated" Font-Size="Medium">
        <EditItemTemplate>
    <h2>  Please Check your experience or ability in the following categories:</h2><br />
            <br />


            <table>
                <tr>
                    
                <td class="tdbox" style="white-space:nowrap"><b>
                   <%-- <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>--%>
            Farming</b></td>
                    <td><asp:RequiredFieldValidator ID="rboFarmReqValidator" runat="server" 
                        ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboFarm" 
                        SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator></td>
        <%--<br />--%>
                    <td>
                        </tr>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboFarm" runat="server" CellSpacing="50"  RepeatLayout="Flow" Text='<%# Bind("Farming") %>' CssClass="ddlitem" >
               <%-- <asp:ListItem></asp:ListItem>--%>
                <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
        </td>
                    </tr>
        <%--    <br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
            <b>Forestry</b>
                        </td>
                    <td><asp:RequiredFieldValidator ID="rboForestReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboForest" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
        <%--<br />--%>
                    </tr>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboForest" runat="server" CellSpacing="50" RepeatLayout="Flow" Text='<%# Bind("Forestry") %>' CssClass="ddlitem" >
                <%-- <asp:ListItem></asp:ListItem>--%>
                <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
        </td>
</tr>
          <%--  <br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
          <b>  Animal Husbandry</b>
                        </td>
                    <td> <asp:RequiredFieldValidator ID="rboAnihusReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboAnihus" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator> </td>
       <%-- <br />--%>   
                    </tr>
                <tr>
                    <td class="checkbox-inline">
         <asp:RadioButtonList ID="rboAnihus" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("AnimalHusb") %>' SelectedValue='<%# Bind("AnimalHusb") %>'>
               <%--<asp:ListItem Enabled="False"></asp:ListItem>--%>
               <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
                        </td>
                    </tr>
       
            <%--<br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
           <b> Commercial Fishing</b>
                        </td>
                    <td><asp:RequiredFieldValidator ID="rboComFishReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboComFish" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
   <%--     <br /> --%> 
                    </tr>
                <tr>
                    <td class="checkbox-inline"> 
         <asp:RadioButtonList ID="rboComFish" runat="server" CellSpacing="50" RepeatLayout="Flow" CssClass="ddlitem" Text='<%# Bind("Comm_Fish") %>' SelectedValue='<%# Bind("Comm_Fish") %>'>
                <%--<asp:ListItem Enabled="False"></asp:ListItem>--%>
                <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
                        </td>
                    </tr>
        
           <%-- <br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
           <b> Small Gas Engines</b>
                        </td>
                    <td>
                    <asp:RequiredFieldValidator ID="rboSmGsEngReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboSmGsEng" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
         <%--<br /> --%>
                    </tr>
                <tr>
                    <td class="checkbox-inline">   
        <asp:RadioButtonList ID="rboSmGsEng" runat="server" CellSpacing="50" RepeatLayout="Flow" CssClass="ddlitem" Text='<%# Bind("Sm_Gas_Eng") %>' SelectedValue='<%# Bind("Sm_Gas_Eng") %>'>
                <%--<asp:ListItem Enabled="False"></asp:ListItem>--%>
             <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
                        </td>
                    </tr>
        
           <%-- <br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
          <b>  Automotive Repair</b>
                        </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboAutoRepReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboAutoRep" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
                    </tr>
        <%--<br />--%>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboAutoRep" runat="server" CellSpacing="50" RepeatLayout="Flow" CssClass="ddlitem" Text='<%# Bind("AutoRepair") %>' SelectedValue='<%# Bind("AutoRepair") %>'>
                <%--<asp:ListItem Enabled="False"></asp:ListItem>--%>
               <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
                        </td>
                    </tr>
        
            <%--<br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
        <b>    Boat Operation</b>
                        </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rboBtOpReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboBtOp" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
       <%-- <br />--%>
                    </tr>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboBtOp" runat="server" CellSpacing="50" RepeatLayout="Flow" CssClass="ddlitem" Text='<%# Bind("BoatOper") %>' SelectedValue='<%# Bind("BoatOper") %>'>
               <%-- <asp:ListItem Enabled="False"></asp:ListItem>--%>
                <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
        </td>
                    </tr>
           <%-- <br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
         <b>   Hunting</b>
                        </td>
                    <td><asp:RequiredFieldValidator ID="rboHuntReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboHunt" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
                    </tr>
       <%-- <br />--%>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboHunt" runat="server" CellSpacing="50" RepeatLayout="Flow" CssClass="ddlitem" Text='<%# Bind("Hunting") %>' SelectedValue='<%# Bind("Hunting") %>'>
              <%--  <asp:ListItem Enabled="False"></asp:ListItem>--%>
                <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
        </td>
                    </tr>
           <%-- <br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
         <b>   Fishing</b>
                        </td>
                    <td> <asp:RequiredFieldValidator ID="rboFishReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboFish" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
                    </tr>
        <%--<br />--%>
                <tr>
                    <td class="checkbox-inline">

            <asp:RadioButtonList ID="rboFish" runat="server" CellSpacing="50" RepeatLayout="Flow" CssClass="ddlitem" Text='<%# Bind("Fishing") %>' SelectedValue='<%# Bind("Fishing") %>'>
                <%--<asp:ListItem Enabled="False"></asp:ListItem>--%>
               <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
       </td>
                    </tr>
           <%-- <br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
         <b>   Camping</b>
                        </td>
                    <td><asp:RequiredFieldValidator ID="rboCampReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboCamp" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
                    </tr>
       <%-- <br />--%>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboCamp" runat="server" CellSpacing="50" RepeatLayout="Flow" CssClass="ddlitem" Text='<%# Bind("Camping") %>' SelectedValue='<%# Bind("Camping") %>'>
                <%--<asp:ListItem Enabled="False"></asp:ListItem>--%>
                <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
        </td>
                    </tr>
           <%-- <br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
          <b>  Swimming </b>

                    </td>
                    <td><asp:RequiredFieldValidator ID="rboSwimRequiredFieldValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboSwim" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
        <%--<br />--%>
                    </tr>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboSwim" runat="server" CellSpacing="50" CssClass="ddlitem"  RepeatLayout="Flow" Text='<%# Bind("Swimming") %>' SelectedValue='<%# Bind("Swimming") %>'>
                <%--<asp:ListItem Enabled="False"></asp:ListItem>--%>
                <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
        </td>
            <%--<br />
            <br />--%>
                    </tr>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
     <b>   Special Animal Inventory</b>
                        </td>
                    <td><asp:RequiredFieldValidator ID="rboSpAnInvReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboSpAnInv" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        <%--<br />--%>
                        </td>
                    </tr>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboSpAnInv" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("Speci_Animal_Inv") %>' SelectedValue='<%# Bind("Speci_Animal_Inv") %>'>
               <%-- <asp:ListItem Enabled="False"></asp:ListItem>--%>
                <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
                        </td>
                    </tr>
        
            <%--<br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
       <b>     Fish Identification(ID)</b>
                        </td>
                    <td><asp:RequiredFieldValidator ID="rboFTDReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboFTD" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
                    </tr>
        <%--<br />--%>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboFTD" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("FishID") %>' SelectedValue='<%# Bind("FishID") %>'>
                <%--<asp:ListItem Enabled="False"></asp:ListItem>--%>
               <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
                        </td>
                    </tr>
        
            <%--<br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
         <b>   Native Birds(ID)</b>
                        </td>
                    <td> <asp:RequiredFieldValidator ID="rboNatBrdReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboNatBrd" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
       <%-- <br />--%>
                    </tr>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboNatBrd" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("NavBirdsID") %>' SelectedValue='<%# Bind("NavBirdsID") %>'>
                <%--<asp:ListItem Enabled="False"></asp:ListItem>--%>
               <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
       </td>
                    </tr>
            <%--<br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
          <b>  Native Mammals(ID)</b>
                        </td>
                    <td><asp:RequiredFieldValidator ID="rboNatMamReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboNatMam" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
                    </tr>
        <%--<br />--%>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboNatMam" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("NavMamID") %>' SelectedValue='<%# Bind("NavMamID") %>'>
                <%--<asp:ListItem Enabled="False"></asp:ListItem>--%>
               <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
        </td>
                    </tr>
            <%--<br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
          <b>  Native Reptiles(ID)</b>
                        </td>
                    <td><asp:RequiredFieldValidator ID="rboRepReqValidator1" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboRep" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
        <%--<br />--%>
                    </tr>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboRep" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("NavRepID") %>' SelectedValue='<%# Bind("NavRepID") %>'>
                <%--<asp:ListItem Enabled="False"></asp:ListItem>--%>
               <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
        </td>
                    </tr>
            <%--<br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
       <b> Native Amphibians(ID)</b>
                        </td>
                    <td><asp:RequiredFieldValidator ID="rboNatAmpReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboNatAmp" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
       <%-- <br />--%>
                    </tr>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboNatAmp" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("NavAmp") %>' SelectedValue='<%# Bind("NavAmp") %>'>
              <%-- <asp:ListItem Enabled="False"></asp:ListItem>--%>
               <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
        </td>
                    </tr>
            <%--<br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
        <b>    Air Boat</b>
                        </td>
                    <td><asp:RequiredFieldValidator ID="rboAirbtReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboAirbt" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
      <%--  <br />--%>
                    </tr>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboAirbt" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("AirBoat") %>' SelectedValue='<%# Bind("AirBoat") %>'>
                <%--<asp:ListItem Enabled="False"></asp:ListItem>--%>
                <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
        </td>
                    </tr>
            <%--<br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
        <b>    Outboard Motors</b>
                        </td>
                    <td><asp:RequiredFieldValidator ID="rBOBMtrsReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rBOBMtrs" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                            
<%--        <br />--%>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rBOBMtrs" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("OutBrdMotors") %>' SelectedValue='<%# Bind("OutBrdMotors") %>'>
                <%--<asp:ListItem Enabled="False"></asp:ListItem>--%>
               <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
        </td>
                    </tr>
            <%--<br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
        <b>    4-Wheel Drive Vehicle</b>
                        </td>
                    <td><asp:RequiredFieldValidator ID="rbwheelReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rbwheel" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
        <%--<br />--%>
                    </tr>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rbwheel" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("FourWheelDri") %>' SelectedValue='<%# Bind("FourWheelDri") %>'>
               <%-- <asp:ListItem Enabled="False"></asp:ListItem>--%>
              <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
        </td>
                    </tr>
           <%-- <br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
         <b>   Farm Tractor</b>
                        </td>
                    <td><asp:RequiredFieldValidator ID="rboFrmTracReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboFrmTrac" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
                    </tr>
      <%--  <br />--%>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboFrmTrac" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("FarmTrac") %>' SelectedValue='<%# Bind("FarmTrac") %>'>
                <%--<asp:ListItem Enabled="False"></asp:ListItem>--%>
               <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
        </td>
                    </tr>
            <%--<br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
       <b>     Bulldozer</b>
                        </td>
                    <td><asp:RequiredFieldValidator ID="rboBulldozReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboBulldoz" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
        <%--<br />--%>
                    </tr>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboBulldoz" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("Bulldozer") %>' SelectedValue='<%# Bind("Bulldozer") %>'>
                <%--<asp:ListItem Enabled="False"></asp:ListItem>--%>
              <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
        </td>
                        </tr>
           <%-- <br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
       <b>     Chain Saw</b>
                        </td>
                    <td><asp:RequiredFieldValidator ID="rboChnSawReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboChnSaw" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
        <%--<br /> --%>
                    </tr>
                <tr>
                    <td class="checkbox-inline">
        <asp:RadioButtonList ID="rboChnSaw" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("ChainSaw") %>' SelectedValue='<%# Bind("ChainSaw") %>'>
               <%-- <asp:ListItem Enabled="False"></asp:ListItem>--%>
                <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
        </td>
                    </tr>
           <%-- <br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
         <b>   Rifle</b>
                        </td>
                    <td>
                    <asp:RequiredFieldValidator ID="rboRifleReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboRifle" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
        <%--<br />--%>
                    </tr>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboRifle" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("Rifle") %>' SelectedValue='<%# Bind("Rifle") %>'>
               <%-- <asp:ListItem Enabled="False"></asp:ListItem>--%>
             <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
        </td>
                    </tr>
            <%--<br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
      <b>      Pistol</b>
                        </td>
                    <td><asp:RequiredFieldValidator ID="rboPistolReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboPistol" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
      <%--  <br />--%>
                    </tr>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboPistol" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("Pistol") %>' SelectedValue='<%# Bind("Pistol") %>'>
              <%-- <asp:ListItem Enabled="False"></asp:ListItem>--%>
             <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
        </td>
                    </tr>
            <%--<br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
      <b>      Shotgun</b>
                        </td>
                    <td><asp:RequiredFieldValidator ID="rboStGunReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboStGun" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
                    </tr>
      <%--  <br />--%>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboStGun" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow"  Text='<%# Bind("ShotGun") %>' SelectedValue='<%# Bind("ShotGun") %>'>
                <%--<asp:ListItem Enabled="False"></asp:ListItem>--%>
              <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
        </td>
                    </tr>
            <%--<br />
            <br />--%>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
     <b>   Special Computer Training</b>
                        </td>
                    <td><asp:RequiredFieldValidator ID="rboSpComTrnReqValidator" runat="server" ErrorMessage="*This field is required" ForeColor="Red" Display="Dynamic" ControlToValidate="rboSpComTrn" SetFocusOnError="true" Font-Bold="true" ValidationGroup="APCG"> 
        </asp:RequiredFieldValidator>
                        </td>
        <%--<br />--%>
                    </tr>
                <tr>
                    <td class="checkbox-inline">
            <asp:RadioButtonList ID="rboSpComTrn" runat="server" CellSpacing="50" CssClass="ddlitem" RepeatLayout="Flow" Text='<%# Bind("Spec_Com_Train") %>' SelectedValue='<%# Bind("Spec_Com_Train") %>'>
               <%-- <asp:ListItem Enabled="False"></asp:ListItem>--%>
              <asp:ListItem  Value="None">None</asp:ListItem>
                <asp:ListItem  Value="Some">Some</asp:ListItem>
                <asp:ListItem  Value="Avg">Avg</asp:ListItem>
                <asp:ListItem  Value="Above Avg">Above Avg</asp:ListItem>
                <asp:ListItem  Value="Expert">Expert</asp:ListItem>
            </asp:RadioButtonList>
                    </td>
                    </tr>
        </table>
           <br /> 
            <br />
        <asp:Button ID="Button1" runat="server" Text="  Go Back  "   PostBackUrl="~/Applicant.aspx" class="btn btn-warning btn-lg" />
        <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="  Next Page  " ValidationGroup="APCG"  CssClass="btn btn-success btn-lg "  />
        <asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="  Finish Later  "  class="btn btn-danger btn-lg" OnClientClick="return confirm('Are you sure you want to quit?');" OnClick="UpdateCancelButton_Click" />
            <br />
    </EditItemTemplate>
    </asp:FormView>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>"  
     SelectCommand="SELECT Farming, Forestry, AnimalHusb, Comm_Fish, Sm_Gas_Eng, AutoRepair, BoatOper, Hunting, Fishing, Camping, Swimming, Speci_Animal_Inv, FishID, NavBirdsID, NavMamID, NavRepID, NavAmp, AirBoat, OutBrdMotors, FourWheelDri, FarmTrac, Bulldozer, ChainSaw, Rifle, Pistol, Shotgun, Spec_Com_Train, ID FROM OAQ.ApplicantQuestionaire WHERE (ID = @PerID)"
     UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET Farming = @Farming, Forestry = @Forestry, AnimalHusb = @AnimalHusb, Comm_Fish = @Comm_Fish, Sm_Gas_Eng = @Sm_Gas_Eng, AutoRepair = @AutoRepair, BoatOper = @BoatOper, Hunting = @Hunting, Fishing = @Fishing, Camping = @Camping, Swimming = @Swimming, Speci_Animal_Inv = @Speci_Animal_Inv, FishID = @FishID, NavBirdsID = @NavBirdsID, NavMamID = @NavMamID, NavRepID = @NavRepID, NavAmp = @NavAmp, AirBoat = @AirBoat, OutBrdMotors = @OutBrdMotors, FourWheelDri = @FourWheelDri, FarmTrac = @FarmTrac, Bulldozer = @Bulldozer, ChainSaw = @ChainSaw, Rifle = @Rifle, Pistol = @Pistol, Shotgun = @Shotgun, Spec_Com_Train = @Spec_Com_Train WHERE ID=@PerID">
    <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
    <UpdateParameters>
        <asp:Parameter Name="Farming" />
        <asp:Parameter Name="Forestry" />
        <asp:Parameter Name="AnimalHusb" />
        <asp:Parameter Name="Comm_Fish" />
        <asp:Parameter Name="Sm_Gas_Eng" />
        <asp:Parameter Name="AutoRepair" />
        <asp:Parameter Name="BoatOper" />
        <asp:Parameter Name="Hunting" />
        <asp:Parameter Name="Fishing" />
        <asp:Parameter Name="Camping" />
        <asp:Parameter Name="Swimming" />
        <asp:Parameter Name="Speci_Animal_Inv" />
        <asp:Parameter Name="FishID" />
        <asp:Parameter Name="NavBirdsID" />
        <asp:Parameter Name="NavMamID" />
        <asp:Parameter Name="NavRepID" />
        <asp:Parameter Name="NavAmp" />
        <asp:Parameter Name="AirBoat" />
        <asp:Parameter Name="OutBrdMotors" />
        <asp:Parameter Name="FourWheelDri" />
        <asp:Parameter Name="FarmTrac" />
        <asp:Parameter Name="Bulldozer" />
        <asp:Parameter Name="ChainSaw" />
        <asp:Parameter Name="Rifle" />
        <asp:Parameter Name="Pistol" />
        <asp:Parameter Name="Shotgun" />
        <asp:Parameter Name="Spec_Com_Train" />
        <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
    </UpdateParameters>
</asp:SqlDataSource>
</asp:Content>
