﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
//using System.Windows.Forms;
using System.Text;
using System.IO;

namespace Application_Questionaire
{
    public partial class Residences : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
        }

       
        protected void btnSvForLater_Click(object sender, EventArgs e)
        {
            
        }

        protected int CalculateDateDifference(DateTime dtme)
        {
            DateTime now = DateTime.Today;
            int diff = now.Year - dtme.Year;
            return diff;
        }
        


        protected bool checkedDate ()
        {
            TextBox txtDtFrom1 = (TextBox)FormView1.FindControl("txtDtFrom1");
            TextBox txtDtFrom2 = (TextBox)FormView1.FindControl("txtDtFrom2");
            TextBox txtDtFrom3 = (TextBox)FormView1.FindControl("txtDtFrom3");
            Label lblConfirm = (Label)FormView1.FindControl("lblConfirm");

            if (!string.IsNullOrEmpty(txtDtFrom3.Text) && !string.IsNullOrEmpty(txtDtFrom2.Text) && !string.IsNullOrEmpty(txtDtFrom1.Text))
                return true;
            else
            {


                int diff;
                if (!string.IsNullOrEmpty(txtDtFrom3.Text))

                    diff = CalculateDateDifference(Convert.ToDateTime(txtDtFrom3.Text));
                else

                    if (!string.IsNullOrEmpty(txtDtFrom2.Text))

                        diff = CalculateDateDifference(Convert.ToDateTime(txtDtFrom2.Text));

                    else
                        diff = CalculateDateDifference(Convert.ToDateTime(txtDtFrom1.Text));

                if (diff < 3)
                {
                    lblConfirm.Text = "*Please verify that you typed in the correct information for the last 3 years of your residence";
                    return false;
                }
                else
                    return true;
            }
        }

        protected void InsertCancelButton_Click(object sender, EventArgs e)
        {
            



                TextBox Fname = (TextBox)FormView1.FindControl("FirstnameTextBox");
                TextBox MiddleInt = (TextBox)FormView1.FindControl("txtMidd");
                TextBox Lname = (TextBox)FormView1.FindControl("txtlname");
                TextBox Address = (TextBox)FormView1.FindControl("StreetAdderssTextBox");
                TextBox HomePhone = (TextBox)FormView1.FindControl("HomePhoneTextBox");
                TextBox WorkPhone = (TextBox)FormView1.FindControl("WorkPhoneTextBox");
                TextBox EmpConNo = (TextBox)FormView1.FindControl("EmpContactTextBox");
                TextBox AlternateNo = (TextBox)FormView1.FindControl("AlternateNoTextBox");
                TextBox ResidenceFromDate1 = (TextBox)FormView1.FindControl("txtDtFrom1");
                TextBox ResidenceToDate1 = (TextBox)FormView1.FindControl("txtDtTo1");
                TextBox ResidenceAddress1 = (TextBox)FormView1.FindControl("txtAddress1");
                TextBox ResidenceFromDate2 = (TextBox)FormView1.FindControl("txtDtFrom2");
                TextBox ResidenceToDate2 = (TextBox)FormView1.FindControl("txtDtTo2");
                TextBox ResidenceFromDate3 = (TextBox)FormView1.FindControl("txtDtFrom3");
                TextBox ResidenceAddress2 = (TextBox)FormView1.FindControl("txtAddress2");
                TextBox ResidenceAddress3 = (TextBox)FormView1.FindControl("txtAddress3");
                DropDownList State = (DropDownList)FormView1.FindControl("ddlState");
                TextBox Zipcode = (TextBox)FormView1.FindControl("ZipCodeTextBox");
                TextBox City = (TextBox)FormView1.FindControl("txtCity");
                TextBox ResidenceToDate3 = (TextBox)FormView1.FindControl("txtDtTo3");


                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ApplicantConnectionString"].ConnectionString))
                {
                    String query = "UPDATE OAQ.ApplicantQuestionaire SET Fname = @Fname, MiddleInt = @MiddleInt, Lname = @Lname, Address = @Address, HomePhone = @HOMEPHONE, WorkPhone = @WorkPhone, EmpConNo = @EmpConNo, AlternateNo = @AlternateNo, Residence_FromDate1 = @Residence_FromDate1, Residence_ToDate1 = @Residence_ToDate1, ResidenceAddress1 = @ResidenceAddress1, Residence_FromDate2 = @Residence_FromDate2, Residence_ToDate2 = @Residence_ToDate2, Residence_FromDate3 = @Residence_FromDate3, Residence_ToDate3 = @Residence_ToDate3, ResidenceAddress2 = @ResidenceAddress2, ResidenceAddress3 = @ResidenceAddress3, State = @State, Zipcode = @Zipcode, City = @City WHERE ID=@PerID";

                    SqlCommand command = new SqlCommand(query, con);
                    command.Parameters.AddWithValue("Fname", Fname.Text);
                    command.Parameters.AddWithValue("MiddleInt", MiddleInt.Text);
                    command.Parameters.AddWithValue("Lname", Lname.Text);
                    command.Parameters.AddWithValue("Address", Address.Text);
                    command.Parameters.AddWithValue("HomePhone", HomePhone.Text);
                    command.Parameters.AddWithValue("WorkPhone", WorkPhone.Text);
                    command.Parameters.AddWithValue("EmpConNo", EmpConNo.Text);
                    command.Parameters.AddWithValue("AlternateNo", AlternateNo.Text);
                    command.Parameters.AddWithValue("Residence_FromDate1", ResidenceFromDate1.Text);
                    command.Parameters.AddWithValue("Residence_ToDate1", ResidenceToDate1.Text);
                    command.Parameters.AddWithValue("ResidenceAddress1", ResidenceAddress1.Text);
                    command.Parameters.AddWithValue("Residence_FromDate2", ResidenceFromDate2.Text);
                    command.Parameters.AddWithValue("Residence_ToDate2", ResidenceToDate2.Text);
                    command.Parameters.AddWithValue("Residence_FromDate3", ResidenceFromDate3.Text);
                    command.Parameters.AddWithValue("ResidenceAddress2", ResidenceAddress2.Text);
                    command.Parameters.AddWithValue("ResidenceAddress3", ResidenceAddress3.Text);
                    command.Parameters.AddWithValue("State", State.SelectedItem.ToString());
                    command.Parameters.AddWithValue("Zipcode", Zipcode.Text);
                    command.Parameters.AddWithValue("City", City.Text);
                    command.Parameters.AddWithValue("Residence_ToDate3", ResidenceToDate3.Text);

                    command.Parameters.AddWithValue("@PerID", Convert.ToInt32(Session["PersonID"].ToString()));
                    con.Open();

                    command.ExecuteNonQuery();
                    con.Close();
                }

                Response.Redirect("Logout.aspx");


            }

        

        

        protected void FormView1_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            Session["PersonID"] = e.Values["ID"].ToString();
        }

        protected void FormView1_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            if (e.Exception != null)
            { // Display the Error Message if the Record Not Saved

                e.ExceptionHandled = true;
                e.KeepInEditMode = true;
            }
            else
                Response.Redirect("Applicant.aspx");
        }

        protected void FormView1_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {
            if (!checkedDate())
            {
                e.Cancel = true;
            }
        }

        

    }
}