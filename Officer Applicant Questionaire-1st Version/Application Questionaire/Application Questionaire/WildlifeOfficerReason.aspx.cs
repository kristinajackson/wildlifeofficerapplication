﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
//using System.Windows.Forms;


namespace Application_Questionaire
{
    public partial class WildlifeOfficerReason : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
           
            
                }
  

        protected void UpdateCancelButton_Click(object sender, EventArgs e)
        {//Update table and Take user to the logout page when click cancel button
            TextBox Reason_WLO = (TextBox)FormView1.FindControl("txtWlOff");
            TextBox Events_ForDecision = (TextBox)FormView1.FindControl("txtEvent");
            TextBox CourseofAction = (TextBox)FormView1.FindControl("txtCourses");
            TextBox MajorConernforWL = (TextBox)FormView1.FindControl("txtConcern");
            TextBox Qualities = (TextBox)FormView1.FindControl("txtQualities");

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ApplicantConnectionString"].ConnectionString))
            {
                String query = "UPDATE OAQ.ApplicantQuestionaire SET Reason_WLO = @Reason_WLO, Events_ForDecision = @Events_ForDecision, CourseofAction = @CourseofAction, Qualities = @Qualities, MajorConernforWL = @MajorConernforWL WHERE ID=@PerID";

                SqlCommand command = new SqlCommand(query, con);
                command.Parameters.AddWithValue("Reason_WLO", Reason_WLO.Text);
                command.Parameters.AddWithValue("Events_ForDecision", Events_ForDecision.Text);
                command.Parameters.AddWithValue("CourseofAction", CourseofAction.Text);
                command.Parameters.AddWithValue("Qualities", Qualities.Text);
                command.Parameters.AddWithValue("MajorConernforWL", MajorConernforWL.Text);

                command.Parameters.AddWithValue("@PerID", Convert.ToInt32(Session["PersonID"].ToString()));
                con.Open();

                command.ExecuteNonQuery();
                con.Close();
            }

            Response.Redirect("Logout.aspx");
            }

        protected void FormView1_ItemInserted1(object sender, FormViewInsertedEventArgs e)
        {
            //Store user information
            Session["PersonID"] = e.Values["ID"].ToString();
        }

        protected void FormView1_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
             if (e.Exception != null)
            { // update table and send user to next page
                
                e.ExceptionHandled = true;
                e.KeepInEditMode = true;
            }
            else
                 Response.Redirect("Finish.aspx");
        }

        protected void FormView1_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {
        //    TextBox MajorConernforWL = (TextBox)FormView1.FindControl("txtConcern");
        //    TextBox ID = (TextBox)FormView1.FindControl("IDLabel1");
        //    int f = 0;
        //    if (ID.Text.Length != 0)
        //    {
        //    SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["ApplicantConnectionString"].ToString());
        //    try
        //    {
        //            SqlCommand cmd = new SqlCommand("select distinct ID from OAQ.ApplicantQuestionaire where WHERE ID=@PerID and MajorConernforWL=@MajorConernforWL ", cn);
        //            //SqlCommand cmd = new SqlCommand("SELECT distinct TWRAID FROM BHHS.BE.StudentInfo where TWRAID =@twra and ClassID=@clid", cn);
        //            //cmd.Parameters.AddWithValue("twra", Encryption.TripleDESEncrypt(twraid.Text));
        //            //cmd.Parameters.AddWithValue("clid", classid.SelectedValue);
        //            cmd.Parameters.AddWithValue("PerID", ID.Text);
        //            cmd.Parameters.AddWithValue("MajorConernforWL", MajorConernforWL.Text);
        //            cn.Open();
        //            string appques = cmd.ExecuteScalar().ToString();
        //            if (!string.IsNullOrEmpty(appques))
        //                f = 1;
        //            cn.Close();
        //        }
        //        catch
        //        {
        //            cn.Close();
        //        }
        //    }
        //    if (f == 1)
        //    {
        //        lblMsg.Text = "ERROR: Application has already been submitted!";
        //        e.Cancel = true;
        //    }
        //    else
        //    {

       //     }
        }
        }
}

        

       
    
