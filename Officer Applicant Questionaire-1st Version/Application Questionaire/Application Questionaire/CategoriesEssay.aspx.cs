﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;

namespace Application_Questionaire1
{
    public partial class CategoriesEssay : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void UpdateCancelButton_Click(object sender, EventArgs e)
        {
           // Update table and Take user to the logout page when click cancel button
            TextBox SpecialAnimalInventoryEssay = (TextBox)FormView1.FindControl("txtSpcAnInv");
            TextBox NativeAmphibiansEssay = (TextBox)FormView1.FindControl("txtNativeAmp");
            TextBox SpecialComputerTrainingEssay = (TextBox)FormView1.FindControl("txtSpComTrain");

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ApplicantConnectionString"].ConnectionString))
            {
                String query = "UPDATE OAQ.ApplicantQuestionaire SET SpecialAnimalInventoryEssay = @SpecialAnimalInventoryEssay, NativeAmphibiansEssay = @NativeAmphibiansEssay, SpecialComputerTrainingEssay = @SpecialComputerTrainingEssay WHERE ID=@PerID";

                SqlCommand command = new SqlCommand(query, con);
                command.Parameters.AddWithValue("SpecialAnimalInventoryEssay", SpecialAnimalInventoryEssay.Text);
                command.Parameters.AddWithValue("NativeAmphibiansEssay", NativeAmphibiansEssay.Text);
                command.Parameters.AddWithValue("SpecialComputerTrainingEssay", SpecialComputerTrainingEssay.Text);

                command.Parameters.AddWithValue("@PerID", Convert.ToInt32(Session["PersonID"].ToString()));
                con.Open();

                command.ExecuteNonQuery();
                con.Close();
            }

            Response.Redirect("Logout.aspx");
            }


        protected void FormView1_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            Session["PersonID"] = e.Values["ID"].ToString();
        }

        protected void FormView1_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            if (e.Exception != null)
            { // Display the Error Message if the Record Not Saved
                
                e.ExceptionHandled = true;
                e.KeepInEditMode = true;
            }
            else
                Response.Redirect("Experience.aspx");
        }
        }


    }
