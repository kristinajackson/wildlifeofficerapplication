﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Application_Questionaire
{
    public partial class Categories : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void UpdateCancelButton_Click(object sender, EventArgs e)
        {//Take user to the logout page when click cancel button
            RadioButtonList Farming = (RadioButtonList)FormView1.FindControl("rboFarm");
            RadioButtonList Forestry = (RadioButtonList)FormView1.FindControl("rboForest");
            RadioButtonList AnimalHusb = (RadioButtonList)FormView1.FindControl("rboAnihus");
            RadioButtonList Comm_Fish = (RadioButtonList)FormView1.FindControl("rboComFish");
            RadioButtonList Sm_Gas_Eng = (RadioButtonList)FormView1.FindControl("rboSmGsEng");
            RadioButtonList AutoRepair = (RadioButtonList)FormView1.FindControl("rboAutoRep");
            RadioButtonList BoatOper = (RadioButtonList)FormView1.FindControl("rboBtOp");
            RadioButtonList Hunting = (RadioButtonList)FormView1.FindControl("rboHunt");
            RadioButtonList Fishing = (RadioButtonList)FormView1.FindControl("rboFish");
            RadioButtonList Camping = (RadioButtonList)FormView1.FindControl("rboCamp");
            RadioButtonList Swimming = (RadioButtonList)FormView1.FindControl("rboSwim");
            RadioButtonList Speci_Animal_Inv = (RadioButtonList)FormView1.FindControl("rboSpAnInv");
            RadioButtonList FishID = (RadioButtonList)FormView1.FindControl("rboFTD");
            RadioButtonList NavBirdsID = (RadioButtonList)FormView1.FindControl("rboNatBrd");
            RadioButtonList NavMamID = (RadioButtonList)FormView1.FindControl("rboNatMam");
            RadioButtonList NavRepID = (RadioButtonList)FormView1.FindControl("rboRep");
            RadioButtonList NavAmp = (RadioButtonList)FormView1.FindControl("rboNatAmp");
            RadioButtonList AirBoat = (RadioButtonList)FormView1.FindControl("rboAirbt");
            RadioButtonList OutBrdMotors = (RadioButtonList)FormView1.FindControl("rBOBMtrs");
            RadioButtonList FourWheelDri = (RadioButtonList)FormView1.FindControl("rbwheel");
            RadioButtonList FarmTrac = (RadioButtonList)FormView1.FindControl("rboFrmTrac");
            RadioButtonList Bulldozer = (RadioButtonList)FormView1.FindControl("rboBulldoz");
            RadioButtonList ChainSaw = (RadioButtonList)FormView1.FindControl("rboChnSaw");
            RadioButtonList Rifle = (RadioButtonList)FormView1.FindControl("rboRifle");
            RadioButtonList Pistol = (RadioButtonList)FormView1.FindControl("rboPistol");
            RadioButtonList Shotgun = (RadioButtonList)FormView1.FindControl("rboStGun");
            RadioButtonList Spec_Com_Train = (RadioButtonList)FormView1.FindControl("rboSpComTrn");

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ApplicantConnectionString"].ConnectionString))
            {
                String query = "UPDATE OAQ.ApplicantQuestionaire SET Farming = @Farming, Forestry = @Forestry, AnimalHusb = @AnimalHusb, Comm_Fish = @Comm_Fish, Sm_Gas_Eng = @Sm_Gas_Eng, AutoRepair = @AutoRepair, BoatOper = @BoatOper, Hunting = @Hunting, Fishing = @Fishing, Camping = @Camping, Swimming = @Swimming, Speci_Animal_Inv = @Speci_Animal_Inv, FishID = @FishID, NavBirdsID = @NavBirdsID, NavMamID = @NavMamID, NavRepID = @NavRepID, NavAmp = @NavAmp, AirBoat = @AirBoat, OutBrdMotors = @OutBrdMotors, FourWheelDri = @FourWheelDri, FarmTrac = @FarmTrac, Bulldozer = @Bulldozer, ChainSaw = @ChainSaw, Rifle = @Rifle, Pistol = @Pistol, Shotgun = @Shotgun, Spec_Com_Train = @Spec_Com_Train WHERE ID=@PerID";

                SqlCommand command = new SqlCommand(query, con);
                command.Parameters.AddWithValue("Farming", Farming.SelectedItem.ToString());
                command.Parameters.AddWithValue("Forestry", Forestry.SelectedItem.ToString());
                command.Parameters.AddWithValue("AnimalHusb", AnimalHusb.SelectedItem.ToString());
                command.Parameters.AddWithValue("Comm_Fish", Comm_Fish.SelectedItem.ToString());
                command.Parameters.AddWithValue("Sm_Gas_Eng", Sm_Gas_Eng.SelectedItem.ToString());
                command.Parameters.AddWithValue("AutoRepair", AutoRepair.SelectedItem.ToString());
                command.Parameters.AddWithValue("Hunting", Hunting.SelectedItem.ToString());
                command.Parameters.AddWithValue("BoatOper", BoatOper.SelectedItem.ToString());
                command.Parameters.AddWithValue("Fishing", Fishing.SelectedItem.ToString());
                command.Parameters.AddWithValue("Camping", Camping.SelectedItem.ToString());
                command.Parameters.AddWithValue("Swimming", Swimming.SelectedItem.ToString());
                command.Parameters.AddWithValue("Speci_Animal_Inv", Speci_Animal_Inv.SelectedItem.ToString());
                command.Parameters.AddWithValue("NavBirdsID", NavBirdsID.SelectedItem.ToString());
                command.Parameters.AddWithValue("NavMamID", NavMamID.SelectedItem.ToString());
                command.Parameters.AddWithValue("NavAmp", NavAmp.SelectedItem.ToString());
                command.Parameters.AddWithValue("AirBoat", AirBoat.SelectedItem.ToString());
                command.Parameters.AddWithValue("OutBrdMotors", OutBrdMotors.SelectedItem.ToString());
                command.Parameters.AddWithValue("FourWheelDri", FourWheelDri.SelectedItem.ToString());
                command.Parameters.AddWithValue("FarmTrac", FarmTrac.SelectedItem.ToString());
                command.Parameters.AddWithValue("Bulldozer", Bulldozer.SelectedItem.ToString());
                command.Parameters.AddWithValue("ChainSaw", ChainSaw.SelectedItem.ToString());
                command.Parameters.AddWithValue("Rifle", Rifle.SelectedItem.ToString());
                command.Parameters.AddWithValue("Pistol", Pistol.SelectedItem.ToString());
                command.Parameters.AddWithValue("Shotgun", Shotgun.SelectedItem.ToString());
                command.Parameters.AddWithValue("Spec_Com_Train", Spec_Com_Train.SelectedItem.ToString());
                command.Parameters.AddWithValue("FishID", FishID.SelectedItem.ToString());
                command.Parameters.AddWithValue("NavRepID", NavRepID.SelectedItem.ToString());
                
                command.Parameters.AddWithValue("@PerID", Convert.ToInt32(Session["PersonID"].ToString()));
                con.Open();

                command.ExecuteNonQuery();
                con.Close();
            }

            Response.Redirect("Logout.aspx");
            }

        protected void FormView1_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            Session["PersonID"] = e.Values["ID"].ToString();
        }

        protected void FormView1_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
             if (e.Exception != null)
            { // Display the Error Message if the Record Not Saved
                
                e.ExceptionHandled = true;
                e.KeepInEditMode = true;
            }
            else
                 Response.Redirect("CategoriesEssay.aspx");
        }

        }

    }
