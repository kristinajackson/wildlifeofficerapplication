﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="PasswordRecovery.aspx.cs" Inherits="Application_Questionaire1.PasswordRecovery" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <asp:PasswordRecovery ID="PasswordRecovery1" runat="server" OnSendingMail="PasswordRecovery1_SendingMail"
        MailDefinition-BodyFileName="~/Text/PasswordRecovery.txt"  Subject="Your password has been reset...">
    </asp:PasswordRecovery>
    <br />
</asp:Content>

