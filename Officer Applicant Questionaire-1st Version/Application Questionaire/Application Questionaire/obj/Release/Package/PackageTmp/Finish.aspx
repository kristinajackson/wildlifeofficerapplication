﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Finish.aspx.cs" Inherits="Application_Questionaire.Finish" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <p>
        <meta http-equiv="Refresh" content="10;url=Login.aspx" />
        <asp:FormView ID="FormView1" runat="server"  DefaultMode="Insert">
            <InsertItemTemplate>
                <asp:Label ID="Label1" runat="server" Text="Your resume has been submitted. You are now finished with this Application"></asp:Label>
            </InsertItemTemplate>
        </asp:FormView>
    </p>
</asp:Content>
