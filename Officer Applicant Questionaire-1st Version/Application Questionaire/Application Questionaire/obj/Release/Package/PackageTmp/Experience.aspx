﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Experience.aspx.cs" Inherits="Application_Questionaire.Experience" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <meta http-equiv="Content-Type" content="text/html" />
    <p>
        <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Edit" DataKeyNames="ID" OnItemInserted="FormView1_ItemInserted" OnItemUpdated="FormView1_ItemUpdated">
            <EditItemTemplate>
                <asp:Label ID="IDLabel1" runat="server" Text='<%# Eval("ID") %>' Visible="false" />
                <p>
                    <table id="Table1" runat="server"> 
                        
                        <tr>
                            <td class="tdbox" style="white-space:nowrap">
                    If you are a member of an organized wildlife or conservaton group(s), please list below:
                                </td>
                            </tr>
                              <%--  </p>--%>
               <%-- <p>--%>
                                <tr>
                                    <td>
                    <asp:TextBox ID="txtquesMem" runat="server" TextMode="MultiLine" CssClass="txtbox" Text='<%# Bind("GroupMem") %>'></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                    <td>
                    <asp:RequiredFieldValidator ID="txtquesMemReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ValidationGroup="EXVG" Display="Dynamic" ControlToValidate="txtquesMem">
                    </asp:RequiredFieldValidator>
                    </td>
                            </tr>
<%--                </p>
                <p>--%>
                        <tr>
                <td class="tdbox" style="white-space:nowrap">
                    10. List those experiences that have associated you with the out-of-doors:
                    </td>
                   <%-- </p>--%>
                <%--<p>--%>
                    <%--<br />--%>
                            </tr>
                                    <tr>
                                        <td>
                    <asp:TextBox ID="txtques10" runat="server" TextMode="MultiLine" CssClass="txtbox" Text='<%# Bind("Outdoor_Exp") %>'></asp:TextBox>
                                            </td>
                                        </tr>
                                    <tr>
                                        <td>
                <asp:RequiredFieldValidator ID="txtques10ReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ValidationGroup="EXVG" Display="Dynamic" ControlToValidate="txtques10">
                    </asp:RequiredFieldValidator>
                    </td>
                            </tr>
               <%-- </p>
                <p>--%>
                        <tr>
                <td class="tdbox" style="white-space:nowrap">
                    11.What is the most responsible position that you may have held and how do you think it assisted you 
                    in preparation for the position for which you are applying? 
                    </td>
                    <%--</p>--%>
                <%--<p>--%>
                    
                            </tr>
                                    <tr>
                                        <td>
                    <asp:TextBox ID="txtques11" runat="server" TextMode="MultiLine" CssClass="txtbox" Text='<%# Bind("Responsible_Pos") %>'></asp:TextBox>
                </td>
                                        </tr>
                                    <tr>
                                        <td>
                                            <asp:RequiredFieldValidator ID="txtques11ReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ValidationGroup="EXVG" Display="Dynamic" ControlToValidate="txtques11">
                    </asp:RequiredFieldValidator>
                    </td>
                            </tr>
              <%--  </p>
                <p>--%>
                        <tr>
                <td class="tdbox" style="white-space:nowrap">
                    12.Do you have any experience in wildlife management, either during or after college? 
                    </td>
                            </tr>
                    <%--</p>--%>
                <%--<p>--%>
                    <%--<br />--%>
                                    <tr>
                                        <td>
                    <asp:TextBox ID="txtques12" runat="server" TextMode="MultiLine" CssClass="txtbox" Text='<%# Bind("Manage_Exp") %>'></asp:TextBox>
                                            </td>
                                        </tr>
                                    <tr>
                                        <td>
                <asp:RequiredFieldValidator ID="txtques12ReqValidator" runat="server" 
                    ErrorMessage="*If none, then please type N/A" Font-Bold="true" ForeColor="Red" ValidationGroup="EXVG" 
                    Display="Dynamic" ControlToValidate="txtques12">
                    </asp:RequiredFieldValidator>
                    </td>
                            </tr>
               <%-- </p>--%>
                 <%--<br /> &nbsp;--%>
                </table>
                    <br />
                    <br />
                <asp:Button ID="btnPrevious" runat="server" Text="  Go Back  "   PostBackUrl="~/CategoriesEssay.aspx"  />
                <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="  Next Page  " ValidationGroup="EXVG" />
                <asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="  Finish Later  " OnClientClick="return confirm('Are you sure you want to quit?');" OnClick="UpdateCancelButton_Click" />
            </EditItemTemplate>
        </asp:FormView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>" 
            SelectCommand="SELECT ID, GroupMem, Outdoor_Exp, Responsible_Pos, Manage_Exp FROM OAQ.ApplicantQuestionaire  WHERE (ID = @PerID)" 
            UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET GroupMem = @GroupMem, Outdoor_Exp = @Outdoor_Exp, Responsible_Pos = @Responsible_Pos, Manage_Exp = @Manage_Exp WHERE ID=@PerID">
            <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="GroupMem" />
                <asp:Parameter Name="Outdoor_Exp" />
                <asp:Parameter Name="Responsible_Pos" />
                <asp:Parameter Name="Manage_Exp" />
                <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>
</asp:Content>
