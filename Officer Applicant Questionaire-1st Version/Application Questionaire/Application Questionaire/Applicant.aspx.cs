﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using System.Windows.Forms;
using System.Data;
using System.Data.Sql;
using System.Configuration;
using System.Data.SqlClient;

namespace Application_Questionaire
{

    public partial class Applicant : System.Web.UI.Page
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {//Make
            DropDownList Dropdownlist1 = (DropDownList)FormView1.FindControl("Dropdownlist1");
            TextBox txtques1 = (TextBox)FormView1.FindControl("txtques1");
            Label Lblques1 = (Label)FormView1.FindControl("Lblques1");
            RequiredFieldValidator txtques1ReqValidator = (RequiredFieldValidator)FormView1.FindControl("txtques1ReqValidator");
            DropDownList rboques2 = (DropDownList)FormView1.FindControl("rboques2");
            RequiredFieldValidator txtques2ReqValidator = (RequiredFieldValidator)FormView1.FindControl("txtques2ReqValidator");
            DropDownList rboques5 = (DropDownList)FormView1.FindControl("rboques5");
            RequiredFieldValidator txtques5ReqValidator = (RequiredFieldValidator)FormView1.FindControl("txtques5ReqValidator");
            DropDownList rboques6 = (DropDownList)FormView1.FindControl("rboques6");
            RequiredFieldValidator txtques6ReqValidator = (RequiredFieldValidator)FormView1.FindControl("txtques6ReqValidator");


            if (Dropdownlist1.SelectedItem.Value == "No")
            {
                txtques1ReqValidator.Enabled = false;

            }
            if (Dropdownlist1.SelectedItem.Value == "Yes")
            {

                txtques1ReqValidator.Enabled = true;

            }

            if (rboques2.SelectedItem.Value == "No")
            {
                txtques2ReqValidator.Enabled = false;

            }
            if (rboques2.SelectedItem.Value == "Yes")
            {

                txtques2ReqValidator.Enabled = true;


            }

            if (rboques5.SelectedItem.Value == "No")
            {
                txtques5ReqValidator.Enabled = false;

            }
            if (rboques5.SelectedItem.Value == "Yes")
            {

                txtques5ReqValidator.Enabled = true;

            }

            if (rboques6.SelectedItem.Value == "No")
            {
                txtques6ReqValidator.Enabled = false;

            }
            if (rboques6.SelectedItem.Value == "Yes")
            {

                txtques6ReqValidator.Enabled = true;

            }
        }



        protected void FormView1_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {//Retrieve user information
            Session["PersonID"] = e.Values["ID"].ToString();            
                        
        }

        protected void btnQuit_Click(object sender, EventArgs e)
        {//Update table and Take user to the logout page when click cancel button
            System.Web.UI.WebControls.DropDownList InterviewedbyTWRArboques7 = (System.Web.UI.WebControls.DropDownList)FormView1.FindControl("Dropdownlist1");
            System.Web.UI.WebControls.TextBox WhereInterviewedtxtques1 = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtques1");
            System.Web.UI.WebControls.DropDownList Relativesrboques2 = (System.Web.UI.WebControls.DropDownList)FormView1.FindControl("rboques2");
            System.Web.UI.WebControls.TextBox txtques2ExplainRelatives = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtques2");
            System.Web.UI.WebControls.TextBox YrTNResidenceTextBox3 = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("TextBox3");
            System.Web.UI.WebControls.TextBox ListResidencetxtlength = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtlength");
            System.Web.UI.WebControls.DropDownList ddlyr = (System.Web.UI.WebControls.DropDownList)FormView1.FindControl("ddlyr");
            System.Web.UI.WebControls.TextBox txtlength = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtlength");
            System.Web.UI.WebControls.TextBox FishingHuntingTypeNotxtques3 = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtques3");
            System.Web.UI.WebControls.DropDownList EnforcementExperrboques5 = (System.Web.UI.WebControls.DropDownList)FormView1.FindControl("rboques5");
            System.Web.UI.WebControls.TextBox ExplainEnforcementtxtques5 = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtques5");
            System.Web.UI.WebControls.TextBox txtques6ExplainEnforcementTrain = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtques6");
            System.Web.UI.WebControls.TextBox txtques7AdvancedCourses = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtques7");
            System.Web.UI.WebControls.TextBox txtques8BeneficalCourses = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtques8");
            System.Web.UI.WebControls.DropDownList EnforcementTrainrboques6 = (System.Web.UI.WebControls.DropDownList)FormView1.FindControl("rboques6");
            System.Web.UI.WebControls.TextBox txtques9Hobbies = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtques9");

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ApplicantConnectionString"].ConnectionString))
            {
                String query = "UPDATE OAQ.ApplicantQuestionaire SET InterviewedbyTWRA = @InterviewedbyTWRA, Relatives = @Relatives, Enforcement_Exper = @Enforcement_Exper,  Enforcement_Train = @Enforcement_Train, Explain_Relatives = @Explain_Relatives, YrTNResidence = @YrTNResidence, List_Residence = @List_Residence, FishingHuntingTypeNo = @FishingHuntingTypeNo, Explain_Enforcement = @Explain_Enforcement, Advanced_Courses = @Advanced_Courses, Benefical_Courses = @Benefical_Courses, Hobbies = @Hobbies,   When_Interviewed = @When_Interviewed,   Explain_EnforcementTrain = @Explain_EnforcementTrain,Yr_TN_Residence_YrMon =@Yr_TN_Residence_YrMon WHERE ID=@PerID";

                SqlCommand command = new SqlCommand(query, con);
                command.Parameters.AddWithValue("InterviewedbyTWRA", InterviewedbyTWRArboques7.SelectedItem.ToString());
                command.Parameters.AddWithValue("When_Interviewed", WhereInterviewedtxtques1.Text);
                command.Parameters.AddWithValue("Relatives", Relativesrboques2.SelectedItem.ToString());
                command.Parameters.AddWithValue("Explain_Relatives", txtques2ExplainRelatives.Text);
                command.Parameters.AddWithValue("YrTNResidence", YrTNResidenceTextBox3.Text);
                command.Parameters.AddWithValue("List_Residence", ListResidencetxtlength.Text);
                command.Parameters.AddWithValue("FishingHuntingTypeNo", FishingHuntingTypeNotxtques3.Text);
                command.Parameters.AddWithValue("Enforcement_Exper", EnforcementExperrboques5.SelectedItem.ToString());
                command.Parameters.AddWithValue("Explain_Enforcement", ExplainEnforcementtxtques5.Text);
                command.Parameters.AddWithValue("Enforcement_Train", EnforcementTrainrboques6.SelectedItem.ToString());
                command.Parameters.AddWithValue("Explain_EnforcementTrain", txtques6ExplainEnforcementTrain.Text);
                command.Parameters.AddWithValue("Advanced_Courses", txtques7AdvancedCourses.Text);
                command.Parameters.AddWithValue("Benefical_Courses", txtques8BeneficalCourses.Text);
                command.Parameters.AddWithValue("Hobbies", txtques9Hobbies.Text);
                command.Parameters.AddWithValue("Yr_TN_Residence_YrMon", ddlyr.Text);

                command.Parameters.AddWithValue("@PerID", Convert.ToInt32(Session["PersonID"].ToString()));
                con.Open();

                command.ExecuteNonQuery();
                con.Close();
            }

            Response.Redirect("Logout.aspx");
            }
        
            
          


        protected void FormView1_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            if (e.Exception != null)
            { // update table and send user to next page

                e.ExceptionHandled = true;
                e.KeepInEditMode = true;
            }
            else
                Response.Redirect("Categories.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {//Update textboxes
            System.Web.UI.WebControls.DropDownList InterviewedbyTWRArboques7 = (System.Web.UI.WebControls.DropDownList)FormView1.FindControl("Dropdownlist1");
            System.Web.UI.WebControls.TextBox WhereInterviewedtxtques1 = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtques1");
            System.Web.UI.WebControls.DropDownList Relativesrboques2 = (System.Web.UI.WebControls.DropDownList)FormView1.FindControl("rboques2");
            System.Web.UI.WebControls.TextBox txtques2ExplainRelatives = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtques2");
            System.Web.UI.WebControls.TextBox YrTNResidenceTextBox3 = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("TextBox3");
            System.Web.UI.WebControls.TextBox ListResidencetxtlength = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtlength");
            System.Web.UI.WebControls.DropDownList ddlyr = (System.Web.UI.WebControls.DropDownList)FormView1.FindControl("ddlyr");
            System.Web.UI.WebControls.TextBox txtlength = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtlength");
            System.Web.UI.WebControls.TextBox FishingHuntingTypeNotxtques3 = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtques3");
            System.Web.UI.WebControls.DropDownList EnforcementExperrboques5 = (System.Web.UI.WebControls.DropDownList)FormView1.FindControl("rboques5");
            System.Web.UI.WebControls.TextBox ExplainEnforcementtxtques5 = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtques5");
            System.Web.UI.WebControls.TextBox txtques6ExplainEnforcementTrain = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtques6");
            System.Web.UI.WebControls.TextBox txtques7AdvancedCourses = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtques7");
            System.Web.UI.WebControls.TextBox txtques8BeneficalCourses = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtques8");
            System.Web.UI.WebControls.DropDownList EnforcementTrainrboques6 = (System.Web.UI.WebControls.DropDownList)FormView1.FindControl("rboques6");
            System.Web.UI.WebControls.TextBox txtques9Hobbies = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtques9");

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ApplicantConnectionString"].ConnectionString))
            {
                String query = "UPDATE OAQ.ApplicantQuestionaire SET InterviewedbyTWRA = @InterviewedbyTWRA, Relatives = @Relatives, Enforcement_Exper = @Enforcement_Exper,  Enforcement_Train = @Enforcement_Train, Explain_Relatives = @Explain_Relatives, YrTNResidence = @YrTNResidence, List_Residence = @List_Residence, FishingHuntingTypeNo = @FishingHuntingTypeNo, Explain_Enforcement = @Explain_Enforcement, Advanced_Courses = @Advanced_Courses, Benefical_Courses = @Benefical_Courses, Hobbies = @Hobbies,   When_Interviewed = @When_Interviewed,   Explain_EnforcementTrain = @Explain_EnforcementTrain,Yr_TN_Residence_YrMon =@Yr_TN_Residence_YrMon WHERE ID=@PerID";

                SqlCommand command = new SqlCommand(query, con);
               // string s = InterviewedbyTWRArboques7.SelectedItem.ToString();
                command.Parameters.AddWithValue("InterviewedbyTWRA", InterviewedbyTWRArboques7.SelectedItem.ToString());
                command.Parameters.AddWithValue("When_Interviewed", WhereInterviewedtxtques1.Text);
                command.Parameters.AddWithValue("Relatives", Relativesrboques2.SelectedItem.ToString());
                command.Parameters.AddWithValue("Explain_Relatives", txtques2ExplainRelatives.Text);
                command.Parameters.AddWithValue("YrTNResidence", YrTNResidenceTextBox3.Text);
                command.Parameters.AddWithValue("List_Residence", ListResidencetxtlength.Text);
                command.Parameters.AddWithValue("FishingHuntingTypeNo", FishingHuntingTypeNotxtques3.Text);
                command.Parameters.AddWithValue("Enforcement_Exper", EnforcementExperrboques5.SelectedItem.ToString());
                command.Parameters.AddWithValue("Explain_Enforcement", ExplainEnforcementtxtques5.Text);
                command.Parameters.AddWithValue("Enforcement_Train", EnforcementTrainrboques6.SelectedItem.ToString());
                command.Parameters.AddWithValue("Explain_EnforcementTrain", txtques6ExplainEnforcementTrain.Text);
                command.Parameters.AddWithValue("Advanced_Courses", txtques7AdvancedCourses.Text);
                command.Parameters.AddWithValue("Benefical_Courses", txtques8BeneficalCourses.Text);
                command.Parameters.AddWithValue("Hobbies", txtques9Hobbies.Text);
                command.Parameters.AddWithValue("Yr_TN_Residence_YrMon", ddlyr.Text);

                command.Parameters.AddWithValue("@PerID", Convert.ToInt32(Session["PersonID"].ToString()));
                con.Open();

                command.ExecuteNonQuery();
                con.Close();
            }

            Response.Redirect("Categories.aspx");
            }
        


        protected void Dropdownlist1_SelectedIndexChanged (object sender, EventArgs e)
        {
            DropDownList Dropdownlist1 = (DropDownList)FormView1.FindControl("Dropdownlist1");
            TextBox txtques1 = (TextBox)FormView1.FindControl("txtques1");
            Label Lblques1 = (Label)FormView1.FindControl("Lblques1");
            RequiredFieldValidator txtques1ReqValidator = (RequiredFieldValidator)FormView1.FindControl("txtques1ReqValidator");

            if (Dropdownlist1.SelectedItem.Value=="No")
            {
                txtques1ReqValidator.Enabled = false;

            }
            if (Dropdownlist1.SelectedItem.Value == "Yes")
            {

                txtques1ReqValidator.Enabled = true;

            }
        }

        protected void rboques2_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList rboques2 = (DropDownList)FormView1.FindControl("rboques2");
            //System.Web.UI.WebControls.TextBox txtques1 = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtques1");
            Label Lblques1 = (Label)FormView1.FindControl("Lblques1");
            RequiredFieldValidator txtques2ReqValidator = (RequiredFieldValidator)FormView1.FindControl("txtques2ReqValidator");

            if (rboques2.SelectedItem.Value == "No")
            {
                txtques2ReqValidator.Enabled = false;

            }
            if (rboques2.SelectedItem.Value == "Yes")
            {

                txtques2ReqValidator.Enabled = true;


            }
        }

        protected void rboques5_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList rboques5 = (DropDownList)FormView1.FindControl("rboques5");
            //System.Web.UI.WebControls.TextBox txtques1 = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtques1");
            Label Lblques1 = (Label)FormView1.FindControl("Lblques1");
            RequiredFieldValidator txtques5ReqValidator = (RequiredFieldValidator)FormView1.FindControl("txtques5ReqValidator");

            if (rboques5.SelectedItem.Value == "No")
            {
                txtques5ReqValidator.Enabled = false;

            }
            if (rboques5.SelectedItem.Value == "Yes")
            {

                txtques5ReqValidator.Enabled = true;

            }
        }

        protected void rboques6_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList rboques6 = (DropDownList)FormView1.FindControl("rboques6");
            //System.Web.UI.WebControls.TextBox txtques1 = (System.Web.UI.WebControls.TextBox)FormView1.FindControl("txtques1");
            Label Lblques1 = (Label)FormView1.FindControl("Lblques1");
            RequiredFieldValidator txtques6ReqValidator = (RequiredFieldValidator)FormView1.FindControl("txtques6ReqValidator");

            if (rboques6.SelectedItem.Value == "No")
            {
                txtques6ReqValidator.Enabled = false;

            }
            if (rboques6.SelectedItem.Value == "Yes")
            {

                txtques6ReqValidator.Enabled = true;

            }
        }

        

       
    }
}