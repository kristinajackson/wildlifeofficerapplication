﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel;
using System.Drawing;
using System.Configuration;
using System.Text;
using System.Web.Security;
using Application_Questionaire.Class;


namespace Application_Questionaire
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.IsAuthenticated && !string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
                    // This is an unauthorized, authenticated request...
                    Response.Redirect("~/UnauthorizedAccess.aspx");
            }
        }


        protected void LoginButton_Click(object sender, EventArgs e)
        {

            string UserName = login_username.Value;
            string Password = login_password.Value;

            AppUtility apt = new AppUtility();

            DataSet ds2 = new DataSet();

            ds2 = apt.GetUsernameandPassword(UserName, Password);

            if (ds2.Tables[0].Rows.Count > 0)
            {
                string PersonID = ds2.Tables[0].Rows[0].ItemArray[0].ToString();
                //flag = true;
                Session["PersonID"] = PersonID;
                Response.Redirect("~/Residences.aspx");
            }
            else
            {
                lblMsg.Text = "Please Check your Username and Password";
            }  
            //try
            //{
            //    SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["ApplicantConnectionString"].ToString());
            //    SqlCommand cmd = new SqlCommand("select distinct ID from OAQ.ApplicantQuestionaire where UserName=@UserName and Password=@Password", cn);
            //    cmd.Parameters.AddWithValue("@UserName", UserName);
            //    cmd.Parameters.AddWithValue("@Password", Password);
            //    cn.Open();

            //    string PersonID = cmd.ExecuteScalar().ToString();
            //    if (!String.IsNullOrEmpty(PersonID))
            //    {
            //        //flag = true;
            //        Session["PersonID"] = PersonID;
            //        Response.Redirect("~/Residences.aspx");

            //    }
            //    else
            //    {
            //        lblMsg.Text = "Please Check your Username and Password";
            //    }
            //    // If User ID is Empty or Not Found
            //    //if (flag == false)
            //    //{
            //    //    lblMsg.Text = "Please Check your Username and Password";
            //    //}
            //}
            //catch { lblMsg.Text = "Please Check your Username and Password"; }

                

        }
                       

        }
    }



    


    
