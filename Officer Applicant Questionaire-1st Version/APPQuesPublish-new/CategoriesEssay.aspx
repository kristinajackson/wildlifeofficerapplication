﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="CategoriesEssay.aspx.cs" Inherits="Application_Questionaire1.CategoriesEssay" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
        <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Edit" DataKeyNames="ID" OnItemInserted="FormView1_ItemInserted" OnItemUpdated="FormView1_ItemUpdated">
            <EditItemTemplate>
        <table>
            <tr>
                <td class="tdbox" style="white-space:nowrap">
                    Identify the specific info with the experience or ability of the following.
                    </td>
                </tr>
                        <tr>
                            <td class="tdbox" style="white-space:nowrap">

                            
                  <b>  Special Animal Inventory:</b></td>
                            </tr>
                        <tr>
                            <td class="tdbox">
                    <asp:TextBox ID="txtSpcAnInv" runat="server" TextMode="MultiLine" CssClass="txtbox" Text='<%# Bind("SpecialAnimalInventoryEssay") %>'></asp:TextBox>
                                </td>
                            </tr>
                        <tr>
                            <td class="tdbox" style="white-space:nowrap"><b>
                    Native Amphibians:</b>
                                </td>
                            </tr>
                        <tr>
                            <td>
                    <asp:TextBox ID="txtNativeAmp" runat="server" TextMode="MultiLine" CssClass="txtbox" Text='<%# Bind("NativeAmphibiansEssay") %>'></asp:TextBox>
                                </td>
                            </tr>
                        <tr>
                            <td class="tdbox" style="white-space:nowrap">
                   <b> Special Computer Training:</b>
                                </td>
                            </tr>
                        <tr>
                            <td>
                    <asp:TextBox ID="txtSpComTrain" runat="server" TextMode="MultiLine" CssClass="txtbox" Text='<%# Bind("SpecialComputerTrainingEssay") %>'></asp:TextBox>
                </td>
                </tr>
                    </table>
                <br /> 
                <br />
                <asp:Button ID="btnPrevious" runat="server" Text="  Go Back  " PostBackUrl="~/Categories.aspx" />
                <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="  Next Page  "  ValidationGroup="CEVG" />
                <asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="  Finish Later  " OnClientClick="return confirm('Are you sure you want to quit?');" OnClick="UpdateCancelButton_Click" />
            </EditItemTemplate>
        </asp:FormView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>" 
            SelectCommand="SELECT SpecialAnimalInventoryEssay, NativeAmphibiansEssay, SpecialComputerTrainingEssay, ID FROM OAQ.ApplicantQuestionaire WHERE (ID = @PerID)" 
             UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET SpecialAnimalInventoryEssay = @SpecialAnimalInventoryEssay, NativeAmphibiansEssay = @NativeAmphibiansEssay, SpecialComputerTrainingEssay = @SpecialComputerTrainingEssay WHERE ID=@PerID">
                   <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="SpecialAnimalInventoryEssay" />
                <asp:Parameter Name="NativeAmphibiansEssay" />
                <asp:Parameter Name="SpecialComputerTrainingEssay" />
                <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
</asp:Content>
