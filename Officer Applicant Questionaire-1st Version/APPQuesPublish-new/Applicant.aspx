﻿<%@ Page Title="" Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Applicant.aspx.cs" Inherits="Application_Questionaire.Applicant" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <meta http-equiv="default-style" content="revealTrans(Duration=0,Transition=5)"/>

</asp:Content>
<asp:Content ID="main" ContentPlaceHolderID="main" runat="server">

    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" CombineScripts="false" />
    
    
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="ID" DataSourceID="SqlDataSource1" DefaultMode="Edit" OnItemInserted="FormView1_ItemInserted" OnItemUpdated="FormView1_ItemUpdated">
        <EditItemTemplate>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
            <asp:Label ID="IDPer" runat="server" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>
            <br />
            <table>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
            1. Have you ever been interviewed by TWRA? If yes, where and when? 
               <asp:Dropdownlist ID="Dropdownlist1" runat="server"  SelectedValue='<%# Bind("InterviewedbyTWRA") %>' 
                   OnSelectedIndexChanged="Dropdownlist1_SelectedIndexChanged" AutoPostBack="true" CssClass="txtsmitem">
               <asp:ListItem></asp:ListItem>
               <asp:ListItem Value="Yes" Text="Yes"></asp:ListItem>
               <asp:ListItem Value="No" Text="No"></asp:ListItem>
           </asp:Dropdownlist>
            <asp:RequiredFieldValidator ID="Dropdownlist1ReqValidator" runat="server" ErrorMessage="*Please select Yes/No"
                ValidationGroup="INSVG" ControlToValidate="Dropdownlist1" Display="Dynamic" ForeColor="Red">
            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                <tr>
                    <td>
            <asp:TextBox ID="txtques1" CssClass="txtbox" runat="server" Text='<%# Bind("When_Interviewed") %>' TextMode="MultiLine" AutoPostBack="true"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques1ReqValidator" runat="server" ErrorMessage="*You have selected Yes, please explain"
                ValidationGroup="INSVG" ControlToValidate="txtques1" Display="Static" ForeColor="Red">
            </asp:RequiredFieldValidator>

            </td>
                    </tr>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
            2. Do you have any relatives presently employed by TWRA? If yes, please list name(s) and relationship(s).
                         <asp:Dropdownlist ID="rboques2" runat="server"  SelectedValue='<%# Bind("Relatives") %>' 
                             OnSelectedIndexChanged="rboques2_SelectedIndexChanged" AutoPostBack="true" CssClass="txtsmitem">
               <asp:ListItem></asp:ListItem>
               <asp:ListItem Value="Yes" Text="Yes"></asp:ListItem>
               <asp:ListItem Value="No" Text="No"></asp:ListItem>
           </asp:Dropdownlist>
                        <asp:RequiredFieldValidator ID="rboques2ReqValidator" runat="server" ErrorMessage="*Please select Yes/No"
                ValidationGroup="INSVG" ControlToValidate="rboques2" Display="Dynamic" ForeColor="Red">
            </asp:RequiredFieldValidator>
                    </td>
                    </tr>
            <tr>
                <td>
            <asp:TextBox ID="txtques2" CssClass="txtbox" runat="server" AutoPostBack="true" Text='<%# Bind("Explain_Relatives") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques2ReqValidator" runat="server" ErrorMessage="*You have selected Yes, please explain"
                ValidationGroup="INSVG" ControlToValidate="txtques2" Display="Dynamic" ForeColor="Red">
            </asp:RequiredFieldValidator>
            </td>
                    </tr>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
            3. Length of Tennessee residence?

                        <asp:TextBox ID="TextBox3" runat="server" AutoPostBack="true" Text='<%# Bind("YrTNResidence") %>' CssClass="txtsmitem"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TextBox3" FilterType="Numbers" Enabled="true"></asp:FilteredTextBoxExtender>
                    <asp:DropDownList ID="ddlyr" runat="server" SelectedValue='<%# Bind("Yr_TN_Residence_YrMon") %>' CssClass="txtsmitem" Height="23px">
               <asp:ListItem></asp:ListItem>
               <asp:ListItem Selected="True">Years</asp:ListItem>
               <asp:ListItem>Months</asp:ListItem>
           </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*Please select Years/Months"
                ValidationGroup="INSVG" ControlToValidate="TextBox3" Display="Dynamic"  ForeColor="Red">
            </asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="ddlyrReqValidator" runat="server" ErrorMessage="*Please enter number of Years/Months"
                ValidationGroup="INSVG" ControlToValidate="ddlyr" Display="Dynamic" ForeColor="Red">
            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
            If you do not reside in Tennessee, list your current State of residence.
                        </td>
                    </tr>
                <tr>
           <td>
            <asp:TextBox ID="txtlength" CssClass="txtbox" runat="server" Text='<%# Bind("List_Residence") %>' Font-Names="Rod" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
            4. If you currently hold a hunting/fishing license in Tennessee, please list License Type and your TWRA Identifying No.:</td>
                    </tr>
                <tr>
                    <td>
           <asp:TextBox ID="txtques3" CssClass="txtbox" runat="server" Text='<%# Bind("[FishingHuntingTypeNo]") %>' TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
            5. Do you have prior law enforcement experience? If yes, please explain:
           <asp:Dropdownlist ID="rboques5" runat="server" Text='<%# Bind("Enforcement_Exper") %>' 
               SelectedValue='<%# Bind("Enforcement_Exper") %>' AutoPostBack="true" OnSelectedIndexChanged="rboques5_SelectedIndexChanged" CssClass="txtsmitem">
              <asp:ListItem></asp:ListItem>
               <asp:ListItem>Yes</asp:ListItem>
               <asp:ListItem >No</asp:ListItem>
           </asp:Dropdownlist>
            <asp:RequiredFieldValidator ID="rboques5ReqValidator" runat="server" ErrorMessage="*Please select Yes/No"
                ValidationGroup="INSVG" ControlToValidate="rboques5" Display="Dynamic" ForeColor="Red">
            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                <tr>
                    <td>
            <asp:TextBox ID="txtques5" CssClass="txtbox" runat="server" AutoPostBack="true" Text='<%# Bind("Explain_Enforcement") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques5ReqValidator" runat="server" ErrorMessage="*You have selected Yes, please explain"
                ValidationGroup="INSVG" ControlToValidate="txtques5" Display="Dynamic" ForeColor="Red" >
            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
            6. Do you have prior law enforcement training? If yes, please explain:
           <asp:DropDownList ID="rboques6" runat="server"  SelectedValue='<%# Bind("Enforcement_Train") %>' 
               OnSelectedIndexChanged="rboques6_SelectedIndexChanged" AutoPostBack="true" CssClass="txtsmitem">
               <asp:ListItem></asp:ListItem>
               <asp:ListItem Value="Yes">Yes</asp:ListItem>
               <asp:ListItem Value="No">No</asp:ListItem>
           </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rboques6ReqValidator" runat="server" ErrorMessage="*Please select Yes/No"
                ValidationGroup="INSVG" ControlToValidate="rboques6" Display="Dynamic" ForeColor="Red">
            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                <tr>
                    <td>
            <asp:TextBox ID="txtques6" CssClass="txtbox" runat="server" Text='<%# Bind("Explain_EnforcementTrain") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques6ReqValidator" runat="server" ErrorMessage="*You have selected Yes, please explain"
                ValidationGroup="INSVG" ControlToValidate="txtques6" Display="Dynamic" ForeColor="Red">
            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
            7. Other than regular college courses, list any advanced career development or law enforcement courses that you have taken. 
                        </td>
                    </tr>
                <tr>
                    <td>
            <asp:TextBox ID="txtques7" CssClass="txtbox" runat="server" Text='<%# Bind("Advanced_Courses") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques7ReqValidator" runat="server" ErrorMessage="*If none, type N/A"
                ValidationGroup="INSVG" ControlToValidate="txtques7" Display="Dynamic" ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
                        </td>
                    </tr>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
            8. What college courses did you take that you feel would be beneficial to you as a Wildlife Officer?
                        </td>
                    </tr>
                <tr>
                    <td>
            <asp:TextBox ID="txtques8" CssClass="txtbox" runat="server" Text='<%# Bind("Benefical_Courses") %>' TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtques8ReqValidator" runat="server" ErrorMessage="*If none, type N/A"
                ValidationGroup="INSVG" ControlToValidate="txtques8" Display="Dynamic" ForeColor="Red">
            </asp:RequiredFieldValidator>
                          </td>
                    </tr>
                <tr>
                    <td class="tdbox" style="white-space:nowrap">
            9. If you have hobbies, please list in order of preference.
                        </td>
                    </tr>
                <tr>
                    <td>      
            <asp:TextBox ID="txtques9" CssClass="txtbox" runat="server" Text='<%# Bind("Hobbies") %>' TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            <br/>
            <br/>
            <asp:Button ID="btnPrevious" runat="server" PostBackUrl="~/Residences.aspx" Text="  Go Back  " />
            <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" OnClick="Button2_Click" Text="  Next Page  " ValidationGroup="INSVG" />
            &nbsp;<asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" OnClick="btnQuit_Click" OnClientClick="return confirm('Are you sure you want to quit?');" Text="  Finish Later  " />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true" HeaderText="Please correcr the following errors:" ValidationGroup="INSVG" Visible="false" />
        </ContentTemplate>
                </asp:UpdatePanel>
                </EditItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>" 
        SelectCommand="SELECT ID, InterviewedbyTWRA, When_Interviewed, Relatives, Explain_Relatives, YrTNResidence, List_Residence, FishingHuntingTypeNo, Enforcement_Exper, Explain_Enforcement, Enforcement_Train, Explain_EnforcementTrain, Advanced_Courses, Benefical_Courses, Hobbies, Yr_TN_Residence_YrMon FROM OAQ.ApplicantQuestionaire WHERE (ID = @PerID)" 
        UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET InterviewedbyTWRA = @InterviewedbyTWRA, When_Interviewed = @When_Interviewed, Relatives = @Relatives, YrTNResidence = @YrTNResidence, Explain_Relatives = @Explain_Relatives, List_Residence = @List_Residence, Enforcement_Exper = @Enforcement_Exper, FishingHuntingTypeNo = @FishingHuntingTypeNo, Explain_Enforcement = @Explain_Enforcement, Hobbies = @Hobbies, Benefical_Courses = @Benefical_Courses, Advanced_Courses = @Advanced_Courses, Enforcement_Train = @Enforcement_Train, Explain_EnforcementTrain = @Explain_EnforcementTrain, Yr_TN_Residence_YrMon = @Yr_TN_Residence_YrMon WHERE (ID = @PerID)">
<%--       --%>
        <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="InterviewedbyTWRA" />
            <asp:Parameter Name="When_Interviewed" />
            <asp:Parameter Name="Relatives" />
            <asp:Parameter Name="YrTNResidence" />
            <asp:Parameter Name="Explain_Relatives" />
            <asp:Parameter Name="List_Residence" />
            <asp:Parameter Name="Enforcement_Exper" />
            <asp:Parameter Name="FishingHuntingTypeNo" />
            <asp:Parameter Name="Explain_Enforcement" />
            <asp:Parameter Name="Hobbies" />
            <asp:Parameter Name="Benefical_Courses" />
            <asp:Parameter Name="Advanced_Courses" />
            <asp:Parameter Name="Enforcement_Train" />
            <asp:Parameter Name="Explain_EnforcementTrain" />
            <asp:Parameter Name="Yr_TN_Residence_YrMon" />
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>
     
    
</asp:Content>
