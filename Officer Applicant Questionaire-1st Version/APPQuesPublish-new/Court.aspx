﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Court.aspx.cs" Inherits="Application_Questionaire.Court" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <p>
        <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Edit" DataKeyNames="ID" OnItemInserted="FormView1_ItemInserted" OnItemUpdated="FormView1_ItemUpdated">
            <EditItemTemplate>
                <br />
                <table>
                    <tr>
                        <td class="tdbox">
                Have you ever been <u><b>convicted</b></u> of any crimes or infractions?                
                <asp:DropDownList ID="rboConvict" runat="server" SelectedValue='<%# Bind("Convictied") %>' CssClass="ddlitem">
                    <asp:ListItem></asp:ListItem>
                    <asp:ListItem>Yes</asp:ListItem>
                    <asp:ListItem>No</asp:ListItem>
                </asp:DropDownList>
                         </td>
                        </tr>
                    <tr>
                    <td>
                <asp:RequiredFieldValidator ID="rboConvictReqValidator" runat="server" ErrorMessage="*Please select Yes/No" 
                    Font-Bold="true" ForeColor="Red" ControlToValidate="rboConvict" ValidationGroup="CTVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                        </td>
                        </tr>
                    <tr>
                        <td class="tdbox">
                If yes, please list below. List all such matters, even if there was no court appearance or
                             the matter was settled by payment of fines or forfeiture of collateral.
                            </td>
                        </tr>
                    <tr>
                        <td class="tdbox">
                Please list the dates, places, Agency involved, the charge, final disposition 
                            and the details on each. 
                            </td>
                        </tr>
                    <tr>
                        <td>
                <asp:TextBox ID="txtCharge" runat="server" TextMode="MultiLine" Text='<%# Bind("Explain_Charge") %>'  CssClass="txtbox"></asp:TextBox>
                            </td>
                        </tr>
                    <tr>
                        <td>
                <asp:RequiredFieldValidator ID="txtChargeReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ControlToValidate="txtCharge" ValidationGroup="CTVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    <tr>
                        <td class="tdbox">
                List all traffic citations, but not parking tickets. 
                            </td>
                        </tr>
                    <tr>
                        <td>
                <asp:TextBox ID="txtCitation" TextMode="MultiLine" runat="server" Text='<%# Bind("Traffic_Cit") %>' CssClass="txtbox"></asp:TextBox>
                            </td>
                        </tr>
                    <tr>
                        <td>
                <asp:RequiredFieldValidator ID="txtCitationReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ControlToValidate="txtCitation" ValidationGroup="CTVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    <tr>
                        <td class="tdbox">
                Have you previously applied to any other state, county or other municipal law enforement agency or private 
                            security organization and if so, please list below. 
                            </td>
                        </tr>
                    <tr>
                        <td>
                <asp:TextBox ID="txtOthState" runat="server" TextMode="MultiLine" Text='<%# Bind("OthStateEmploy") %>' CssClass="txtbox"></asp:TextBox>
                            </td>
                        </tr>
                    <tr>
                        <td>
                <asp:RequiredFieldValidator ID="txtOthStateReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ControlToValidate="txtOthState" ValidationGroup="CTVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                   </td>
                             </tr>
                        </table>
                <br />
                <br />
                <asp:Button ID="BackButton" runat="server" Text="  Go Back  " PostBackUrl="~/Experience.aspx" />
                <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="  Next Page  " ValidationGroup="CTVG" />
                <asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="  Finish Later  " OnClientClick="return confirm('Are you sure you want to quit?');" OnClick="UpdateCancelButton_Click" />
            </EditItemTemplate>
        </asp:FormView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>" 
            SelectCommand="SELECT ID, Convictied, Explain_Charge, Traffic_Cit, OthStateEmploy FROM OAQ.ApplicantQuestionaire WHERE (ID = @PerID)" 
            UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET Convictied = @Convictied, Explain_Charge = @Explain_Charge, Traffic_Cit = @Traffic_Cit, OthStateEmploy = @OthStateEmploy WHERE ID=@PerID">
            <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="Convictied" />
                <asp:Parameter Name="Explain_Charge" />
                <asp:Parameter Name="Traffic_Cit" />
                <asp:Parameter Name="OthStateEmploy" />
                 <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>

</asp:Content>
