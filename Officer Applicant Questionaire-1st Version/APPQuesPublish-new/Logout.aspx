﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Logout.aspx.cs" Inherits="Application_Questionaire.Logout" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
<h1>
        Logout</h1>
    <p>
        You have been logged out of the system. To log in, please return to the <a href="Login.aspx">
            login page</a>.</p>
</asp:Content>

