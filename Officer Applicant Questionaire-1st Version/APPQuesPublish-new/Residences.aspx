﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Residences.aspx.cs" Inherits="Application_Questionaire.Residences" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style9 {
            height: 16px;
        }
        .auto-style10 {
            width: 37%;
        }
        .auto-style11 {
            text-align: left;
            width: 37%;
            font-size: 14px;
            font-family: Arial, Helvetica, sans-serif;
            /*height: 36px;*/
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" CombineScripts="false" />
    <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Edit" OnItemInserted="FormView1_ItemInserted" OnItemUpdated="FormView1_ItemUpdated" OnItemUpdating="FormView1_ItemUpdating">
        <EditItemTemplate>
            <table id="table2" runat="server" style="background-color: #FFFFFF; font-size: 14px; font-family: Arial, Helvetica, sans-serif;">
                <tr style="vertical-align: bottom;">
                    <td class="tdtit" style="white-space: nowrap">First Name:
                    </td>
                    <td class="tdbox">
                        <asp:TextBox ID="FirstnameTextBox" runat="server" Text='<%# Bind("Fname") %>' CssClass="txtitem"></asp:TextBox>
                    </td>
                    <td class="tdtit" style="white-space: nowrap">Middle Initial:
                    </td>
                    <td class="tdbox">
                        <asp:TextBox ID="txtMidd" runat="server" Text='<%# Bind("MiddleInt") %>' MaxLength="1" CssClass="txtsmitem"></asp:TextBox>
                    </td>
                    <td class="tdtit" style="white-space: nowrap">Last Name:
                    </td>
                    <td class="tdbox">
                        <asp:TextBox ID="txtlname" runat="server" Text='<%# Bind("Lname") %>' CssClass="txtitem"></asp:TextBox>
                    </td>
                </tr>

                <tr style="vertical-align: top;">
                    <td class="tdtit"></td>
                    <td class="tdbox">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*First name is required!"
                            ValidationGroup="ResVG" ControlToValidate="FirstnameTextBox" Display="Dynamic" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                            ControlToValidate="FirstnameTextBox" Display="Dynamic" ErrorMessage="Invalid First Name!"
                            SetFocusOnError="true" ForeColor="Red"
                            ValidationExpression="^[a-zA-Z''-'\s]{1,40}$" ValidationGroup="ResVG">
                        </asp:RegularExpressionValidator>
                    </td>
                    <td class="tdtit"></td>
                    <td class="tdbox"></td>
                    <td class="tdtit"></td>
                    <td class="tdbox">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                            ErrorMessage="*Last name is required!" ValidationGroup="ResVG"
                            ControlToValidate="txtlname" Display="Dynamic" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server"
                            ErrorMessage="Invalid Last Name!" ControlToValidate="txtlname" Display="Dynamic"
                            SetFocusOnError="true" ForeColor="Red" ValidationExpression="^[a-zA-Z''-'\s]{1,40}$"
                            ValidationGroup="ResVG"></asp:RegularExpressionValidator>
                    </td>

                </tr>
                <tr style="vertical-align: bottom;">
                    <td class="tdtit" style="white-space: nowrap">Street Address:
                    </td>
                    <td class="tdbox" colspan="3">
                        <asp:TextBox ID="StreetAdderssTextBox" runat="server" Text='<%# Bind("Address") %>'
                            CssClass="txtitem" Width="80%" />
                    </td>
                    <td class="tdtit">City:
                    </td>
                    <td class="tdbox">
                        <asp:TextBox ID="txtCity" runat="server" Text='<%# Bind("City") %>' CssClass="txtitem"></asp:TextBox>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td class="tdtit"></td>
                    <td class="tdbox">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                            ErrorMessage="*Street address is required!" ValidationGroup="ResVG"
                            ControlToValidate="StreetAdderssTextBox" Display="Dynamic" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                    </td>
                    <td class="tdtit"></td>
                    <td class="tdtit"></td>
                    <td class="tdtit"></td>
                    <td class="tdbox">
                        <asp:RequiredFieldValidator ID="CityReqVal" ValidationGroup="ResVG" runat="server"
                            ControlToValidate="txtCity" ErrorMessage="*City must be entered!" ForeColor="Red"
                            Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr style="vertical-align: bottom;">
                    <td class="tdtit">State:
                    </td>
                    <td class="tdbox">
                        <asp:DropDownList ID="ddlState" runat="server" CssClass="ddlitem" SelectedValue='<%# Bind("State") %>'>
                            <asp:ListItem></asp:ListItem>
                            <asp:ListItem>AL</asp:ListItem>
                            <asp:ListItem>AK</asp:ListItem>
                            <asp:ListItem>AZ</asp:ListItem>
                            <asp:ListItem>AR</asp:ListItem>
                            <asp:ListItem>CA</asp:ListItem>
                            <asp:ListItem>CO</asp:ListItem>
                            <asp:ListItem>CT</asp:ListItem>
                            <asp:ListItem>DC</asp:ListItem>
                            <asp:ListItem>DE</asp:ListItem>
                            <asp:ListItem>FL</asp:ListItem>
                            <asp:ListItem>GA</asp:ListItem>
                            <asp:ListItem>HI</asp:ListItem>
                            <asp:ListItem>ID</asp:ListItem>
                            <asp:ListItem>IL</asp:ListItem>
                            <asp:ListItem>IN</asp:ListItem>
                            <asp:ListItem>IA</asp:ListItem>
                            <asp:ListItem>KS</asp:ListItem>
                            <asp:ListItem>KY</asp:ListItem>
                            <asp:ListItem>LA</asp:ListItem>
                            <asp:ListItem>ME</asp:ListItem>
                            <asp:ListItem>MD</asp:ListItem>
                            <asp:ListItem>MA</asp:ListItem>
                            <asp:ListItem>MI</asp:ListItem>
                            <asp:ListItem>MN</asp:ListItem>
                            <asp:ListItem>MS</asp:ListItem>
                            <asp:ListItem>MO</asp:ListItem>
                            <asp:ListItem>MT</asp:ListItem>
                            <asp:ListItem>NE</asp:ListItem>
                            <asp:ListItem>NV</asp:ListItem>
                            <asp:ListItem>NH</asp:ListItem>
                            <asp:ListItem>NJ</asp:ListItem>
                            <asp:ListItem>NM</asp:ListItem>
                            <asp:ListItem>NY</asp:ListItem>
                            <asp:ListItem>NC</asp:ListItem>
                            <asp:ListItem>ND</asp:ListItem>
                            <asp:ListItem>OH</asp:ListItem>
                            <asp:ListItem>OK</asp:ListItem>
                            <asp:ListItem>OR</asp:ListItem>
                            <asp:ListItem>PA</asp:ListItem>
                            <asp:ListItem>RI</asp:ListItem>
                            <asp:ListItem>SC</asp:ListItem>
                            <asp:ListItem>SD</asp:ListItem>
                            <asp:ListItem Selected="True">TN</asp:ListItem>
                            <asp:ListItem>TX</asp:ListItem>
                            <asp:ListItem>UT</asp:ListItem>
                            <asp:ListItem>VT</asp:ListItem>
                            <asp:ListItem>VA</asp:ListItem>
                            <asp:ListItem>WA</asp:ListItem>
                            <asp:ListItem>WV</asp:ListItem>
                            <asp:ListItem>WI</asp:ListItem>
                            <asp:ListItem>WY</asp:ListItem>
                            <asp:ListItem>  </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="tdtit">Zipcode:
                    </td>
                    <td class="tdbox">
                        <asp:TextBox ID="ZipCodeTextBox" runat="server" CssClass="txtsmitem" Text='<%# Bind("Zipcode") %>' MaxLength="5" />
                        <asp:Label ID="lblZip" Text="99999" runat="server" Style="font-size: x-small"></asp:Label>
                    </td>
                    <td class="tdtit" style="white-space: nowrap">Home Phone:
                    </td>
                    <td class="tdbox">
                        <asp:TextBox ID="HomePhoneTextBox" runat="server" Text='<%# Bind("HomePhone") %>' CssClass="txtitem"></asp:TextBox>
                        <asp:MaskedEditExtender ID="HomePhoneMEE" runat="server" TargetControlID="HomePhoneTextBox" AutoComplete="False" ClearMaskOnLostFocus="true" Mask="999-999-9999" MaskType="Number" />
                    </td>

                </tr>
                <tr style="vertical-align: top;">
                    <td class="tdtit"></td>
                    <td class="tdbox">
                        <asp:RequiredFieldValidator ID="StateReqVal" ValidationGroup="ResVG" runat="server"
                            ControlToValidate="ddlState" ErrorMessage="*State must be entered!" ForeColor="Red"
                            Display="Dynamic" SetFocusOnError="true" ></asp:RequiredFieldValidator>
                    </td>
                    <td class="tdtit">
                    </td>
                    <td class="tdbox">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ValidationGroup="ResVG" runat="server"
                            ControlToValidate="ZipCodeTextBox" ErrorMessage="*Zipcode must be entered!" ForeColor="Red"
                            Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server"
                            ErrorMessage="Invalid zip code!" ValidationExpression="^\d{5}(-\d{4})?$"
                            ForeColor="Red" Display="Dynamic" SetFocusOnError="true" ControlToValidate="ZipCodeTextBox" ValidationGroup="ResVG" />
                    </td>
                    <td class="tdtit"></td>
                    <td class="tdbox">
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                            ErrorMessage="Invalid home phone number!" ValidationExpression="^[0-9]{10}|^[0-9]{3}-[0-9]{3}-[0-9]{4}"
                            ForeColor="Red" Display="Dynamic" SetFocusOnError="true" ControlToValidate="HomePhoneTextBox" />
                    </td>

                </tr>
                <tr style="vertical-align: bottom;">
                    <td class="tdtit" style="white-space: nowrap">Work Phone:
                    </td>
                    <td class="tdbox">
                        <asp:TextBox ID="WorkPhoneTextBox" runat="server" Text='<%# Bind("WorkPhone") %>' CssClass="txtitem"></asp:TextBox>
                        <asp:MaskedEditExtender ID="WorkPhoneMEE" runat="server" TargetControlID="WorkPhoneTextBox" AutoComplete="False" ClearMaskOnLostFocus="true" Mask="999-999-9999" MaskType="Number" />
                    </td>
                    <td class="tdtit" style="white-space: nowrap">Employment Contact No:
                    </td>
                    <td class="tdbox">
                        <asp:TextBox ID="EmpContactTextBox" runat="server" Text='<%# Bind("EmpConNo") %>'></asp:TextBox>
                        <asp:MaskedEditExtender ID="EmpContactTextBox_MaskedEditExtender" runat="server" BehaviorID="EmpContactTextBox_MaskedEditExtender"
                            Mask="999-999-9999" MaskType="Number" ClearMaskOnLostFocus="true" ErrorTooltipEnabled="true" AutoComplete="False" TargetControlID="EmpContactTextBox" />
                    </td>
                    <td class="tdtit" style="white-space: nowrap">Alternate No:
                    </td>
                    <td class="tdbox">
                        <asp:TextBox ID="AlternateNoTextBox" runat="server" Text='<%# Bind("AlternateNo") %>'></asp:TextBox>
                        <asp:MaskedEditExtender ID="AlternateNoTextBox_MaskedEditExtender" runat="server" BehaviorID="AlternateNoTextBox_MaskedEditExtender"
                            Mask="999-999-9999" MaskType="Number" ClearMaskOnLostFocus="true"
                            ErrorTooltipEnabled="true" AutoComplete="true" TargetControlID="AlternateNoTextBox" />
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td class="tdtit"></td>
                    <td class="tdbox">
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server"
                            ErrorMessage="Invalid work phone number!" ValidationExpression="^[0-9]{10}|^[0-9]{3}-[0-9]{3}-[0-9]{4}"
                            ForeColor="Red" Display="Dynamic" SetFocusOnError="true" ControlToValidate="WorkPhoneTextBox" />
                    </td>
                    <td class="tdtit"></td>
                    <td class="tdbox">
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                            ErrorMessage="Invalid employment contact number!" ValidationExpression="^[0-9]{10}|^[0-9]{3}-[0-9]{3}-[0-9]{4}"
                            ForeColor="Red" Display="Dynamic" SetFocusOnError="true" ControlToValidate="EmpContactTextBox" />
                    </td>
                    <td class="tdtit"></td>
                    <td class="tdbox">
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                            ErrorMessage="Invalid alternate phone number!" ValidationExpression="^[0-9]{10}|^[0-9]{3}-[0-9]{3}-[0-9]{4}"
                            ForeColor="Red" Display="Dynamic" SetFocusOnError="true" ControlToValidate="AlternateNoTextBox" />
                    </td>
                </tr>
            </table>
            <table id="Table1" runat="server">
                <tr>
                    <td class="tdbox" style="white-space: nowrap; font-size: medium; font-weight: lighter;">List chronologically your residences for the last three years(including address while attending school).
                    </td>
                </tr>
            </table>
            <table>
                <tr id="Tr1" runat="server">
                    <td id="Td4" runat="server">Dates From/To</td>
                    <td id="Td5" runat="server" class="auto-style10">Address</td>
                </tr>
                <tr id="Tr2" runat="server">
                    <td style="white-space: nowrap;">
                        <asp:TextBox ID="txtDtFrom1" runat="server" Text='<%# Bind("Residence_FromDate1") %>' MaxLength="10" CssClass="txtitem" ></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender7" runat="server" TargetControlID="txtDtFrom1"></asp:CalendarExtender>
                        <asp:TextBox ID="txtDtTo1" runat="server" Text='<%# Bind("Residence_ToDate1") %>' MaxLength="10" CssClass="txtitem"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender5" runat="server" TargetControlID="txtDtTo1"></asp:CalendarExtender>
                    </td>
                    <td class="auto-style11">
                        <asp:TextBox ID="txtAddress1" runat="server" Text='<%# Bind("ResidenceAddress1") %>' CssClass="txtitem" Width="85%"></asp:TextBox>

                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server"
                            ErrorMessage="*Please enter your beginning date at residence!"
                            ControlToValidate="txtDtFrom1" Display="Dynamic" ForeColor="Red" Font-Size="Small" ValidationGroup="ResVG">
                        </asp:RequiredFieldValidator>
                       <asp:RangeValidator ID="RangeValidator6" runat="server"
                            ControlToValidate="txtDtFrom1" ErrorMessage="Invalid From Date"
                            Type="Date" MinimumValue="01/01/1900" MaximumValue="01/01/2100"
                            Display="Dynamic" ForeColor="Red" ValidationGroup="ResVG" Font-Size="Small"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server"
                            ErrorMessage="*Please enter your ending date at residence!"
                            ControlToValidate="txtDtTo1" Display="Dynamic" ForeColor="Red" Font-Size="Small" ValidationGroup="ResVG">
                        </asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator7" runat="server"
                            ControlToValidate="txtDtTo1" ErrorMessage="Invalid To Date"
                            Type="Date" MinimumValue="01/01/1900" MaximumValue="01/01/2100"
                            Display="Dynamic" ForeColor="Red" ValidationGroup="ResVG" Font-Size="Small"></asp:RangeValidator>
                         <asp:CompareValidator ID="CompareValidator4" runat="server" ErrorMessage="From date should be greater than To date!" ControlToCompare="txtDtFrom1" 
                            ControlToValidate="txtDtTo1" Operator="GreaterThan" Type="Date" ValidationGroup="ResVG"  Display="Dynamic" ForeColor="Red" Font-Size="Small"></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Label ID="Label7" Text="MM/DD/YYYY" runat="server" Style="font-size: x-small"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label8" Text="MM/DD/YYYY" runat="server" Style="font-size: x-small"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server"
                            ErrorMessage="*Please enter your address!" ValidationGroup="ResVG"
                            ControlToValidate="txtAddress1" Display="Dynamic" ForeColor="Red" Font-Size="Small">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="TableRow1" runat="server">
                    <td class="tdbox" style="white-space: nowrap;">
                        <asp:TextBox ID="txtDtFrom2" runat="server" Text='<%# Bind("Residence_FromDate2") %>' CssClass="txtitem" MaxLength="10"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDtFrom2"></asp:CalendarExtender>
                        <asp:TextBox ID="txtDtTo2" runat="server" Text='<%# Bind("Residence_ToDate2") %>' CssClass="txtitem" MaxLength="10" ></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtDtTo2">
                        </asp:CalendarExtender>
                    </td>
                    <td class="auto-style11">
                        <asp:TextBox ID="txtAddress2" runat="server" Text='<%# Bind("ResidenceAddress2") %>' CssClass="txtitem" Width="85%"></asp:TextBox></td>
                    <td class="tdbox">
                        <asp:RangeValidator ID="RangeValidator1" runat="server"
                            ErrorMessage="Invalid From Date!"
                            Type="Date" MinimumValue="01/01/1900" MaximumValue="01/01/2100"
                            Display="Dynamic" ForeColor="Red" ControlToValidate="txtDtFrom2" ValidationGroup="ResVG"></asp:RangeValidator>
                          <asp:RangeValidator ID="RangeValidator3" runat="server"
                              ErrorMessage="Invalid To Date!"
                              Type="Date" MinimumValue="01/01/1900" MaximumValue="01/01/2100"
                              Display="Dynamic" ForeColor="Red" ControlToValidate="txtDtTo2" ValidationGroup="ResVG"></asp:RangeValidator>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="From date should be greater than To date!" ControlToCompare="txtDtFrom2" 
                            ControlToValidate="txtDtTo2" Operator="GreaterThan" Type="Date" ValidationGroup="ResVG"  Display="Dynamic" ForeColor="Red"></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label5" Text="MM/DD/YYYY" runat="server" Style="font-size: x-small"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label6" Text="MM/DD/YYYY" runat="server" Style="font-size: x-small"></asp:Label></td>
                </tr>

                <tr id="TableRow2" runat="server">
                    <td class="tdbox">
                        <asp:TextBox ID="txtDtFrom3" runat="server" Text='<%# Bind("Residence_FromDate3") %>' CssClass="txtitem" MaxLength="10"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender3" runat="server"
                            TargetControlID="txtDtFrom3">
                        </asp:CalendarExtender>
                        <asp:TextBox ID="txtDtTo3" runat="server" Text='<%# Bind("Residence_ToDate3") %>' CssClass="txtitem" MaxLength="10"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender4" runat="server"
                            TargetControlID="txtDtTo3">
                        </asp:CalendarExtender>
                    </td>
                    <td class="auto-style11">
                        <asp:TextBox ID="txtAddress3" runat="server" Text='<%# Bind("ResidenceAddress3") %>' CssClass="txtitem" Width="85%"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="Label10" Text="MM/DD/YYYY" runat="server" Style="font-size: x-small"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label11" Text="MM/DD/YYYY" runat="server" Style="font-size: x-small"></asp:Label></td>
                </tr>
                <tr style="vertical-align: top;">
                    <td class="tdbox">
                        <asp:RangeValidator ID="RangeValidator4" runat="server"
                            ErrorMessage="Invalid From Date"
                            Type="Date" MinimumValue="01/01/1900" MaximumValue="01/01/2100"
                            Display="Dynamic" ForeColor="Red" ControlToValidate="txtDtFrom3" ValidationGroup="ResVG"></asp:RangeValidator>
                          &nbsp;<asp:RangeValidator ID="RangeValidator5" runat="server"
                              ErrorMessage="Invalid To Date"
                              Type="Date" MinimumValue="01/01/1900" MaximumValue="01/01/2100"
                              Display="Dynamic" ForeColor="Red" ControlToValidate="txtDtTo3" ValidationGroup="ResVG"></asp:RangeValidator>
                    <asp:CompareValidator ID="CompareValidator3" runat="server" ErrorMessage="From date should be greater than To date!" ControlToCompare="txtDtFrom3" 
                            ControlToValidate="txtDtTo3" Operator="GreaterThan" Type="Date" ValidationGroup="ResVG"  Display="Dynamic" ForeColor="Red"></asp:CompareValidator>
                    </td>
                </tr>
<asp:Label ID="lblConfirm" runat="server" Font-Bold="true" ForeColor="Red" ></asp:Label> 
            </table>
            <br />
            <%--<br />--%>
            <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="  Next Page  " ValidationGroup="ResVG" OnClick="btnSvForLater_Click" />
            <asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="  Finish Later  " OnClientClick="return confirm('Are you sure you want to quit?');" OnClick="InsertCancelButton_Click" />
            
        </EditItemTemplate>
    </asp:FormView>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>"
        SelectCommand="SELECT ID, Address, HomePhone, WorkPhone, EmpConNo, AlternateNo, Residence_FromDate1, Residence_ToDate1, ResidenceAddress1, CreatedDate, Yr_TN_Residence_YrMon, Residence_FromDate2, Residence_ToDate2, Residence_FromDate3, Residence_ToDate3, ResidenceAddress2, ResidenceAddress3, Country, State, Zipcode, Fname, MiddleInt, Lname, City, UserName, Password, CreatedDate FROM OAQ.ApplicantQuestionaire WHERE (ID = @PerID)"
        UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET Fname = @Fname, MiddleInt = @MiddleInt, Lname = @Lname, Address = @Address, HomePhone = @HOMEPHONE, WorkPhone = @WorkPhone, EmpConNo = @EmpConNo, AlternateNo = @AlternateNo, Residence_FromDate1 = @Residence_FromDate1, Residence_ToDate1 = @Residence_ToDate1, ResidenceAddress1 = @ResidenceAddress1, Residence_FromDate2 = @Residence_FromDate2, Residence_ToDate2 = @Residence_ToDate2, Residence_FromDate3 = @Residence_FromDate3, Residence_ToDate3 = @Residence_ToDate3, ResidenceAddress2 = @ResidenceAddress2, ResidenceAddress3 = @ResidenceAddress3, Country = @Country, State = @State, Zipcode = @Zipcode, City = @City, CreatedDate=CONVERT (date, SYSDATETIME()) WHERE (ID = @PerID)">
        <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Fname" />
            <asp:Parameter Name="MiddleInt" />
            <asp:Parameter Name="Lname" />
            <asp:Parameter Name="Address" />
            <asp:Parameter Name="HOMEPHONE" />
            <asp:Parameter Name="WorkPhone" />
            <asp:Parameter Name="EmpConNo" />
            <asp:Parameter Name="AlternateNo" />
            <asp:Parameter Name="Residence_FromDate1" />
            <asp:Parameter Name="Residence_ToDate1" />
            <asp:Parameter Name="ResidenceAddress1" />
            <asp:Parameter Name="Residence_FromDate2" />
            <asp:Parameter Name="Residence_ToDate2" />
            <asp:Parameter Name="Residence_FromDate3" />
            <asp:Parameter Name="Residence_ToDate3" />
            <asp:Parameter Name="ResidenceAddress2" />
            <asp:Parameter Name="ResidenceAddress3" />
            <asp:Parameter Name="Country" />
            <asp:Parameter Name="State" />
            <asp:Parameter Name="Zipcode" />
            <asp:Parameter Name="City" />
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>
       
</asp:Content>
