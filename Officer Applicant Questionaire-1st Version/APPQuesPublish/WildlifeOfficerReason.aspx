﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="WildlifeOfficerReason.aspx.cs" Inherits="Application_Questionaire.WildlifeOfficerReason" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <p>
        <meta http-equiv="Content-Type" content="text/html" />
        <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Edit" OnItemInserted="FormView1_ItemInserted1" OnItemUpdated="FormView1_ItemUpdated">
            <EditItemTemplate>
                <asp:Textbox ID="IDLabel1" runat="server" Text='<%# Eval("ID") %>' Visible="false" />
<br />
                <form>
                <div class="container-fluid">
                     <div class="form-group">
                            <label for="txtWlOff">Why do you want to become a Wildlife Officer? </label>
                         <asp:TextBox ID="txtWlOff" runat="server" TextMode="MultiLine" Rows="4" Text='<%# Bind("Reason_WLO") %>' CssClass="  form-control" AutoPostBack="true" OnTextChanged="txtWlOff_TextChanged" Font-Size="Medium"></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtWlOffReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ControlToValidate="txtWlOff" ValidationGroup="WLOVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                         </div>
                
                 <div class="form-group">
                            <label for="txtEvent">What events or series of events caused you to make this decision?</label>
                          <asp:TextBox ID="txtEvent" runat="server" TextMode="MultiLine" Rows="4" Text='<%# Bind("Events_ForDecision") %>' CssClass="  form-control" AutoPostBack="true" OnTextChanged="txtWlOff_TextChanged" Font-Size="Medium"></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtEventReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ControlToValidate="txtEvent" ValidationGroup="WLOVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                         </div>
                 <div class="form-group">
                            <label for="txtCourses">What specific courses of actions have you taken to prepare yourself for a career as a Wildlife Officer?</label>
                          <asp:TextBox ID="txtCourses" runat="server" TextMode="MultiLine" Rows="4" Text='<%# Bind("CourseofAction") %>' CssClass="  form-control" AutoPostBack="true" OnTextChanged="txtWlOff_TextChanged" Font-Size="Medium"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="txtCoursesReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ControlToValidate="txtCourses" ValidationGroup="WLOVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                         </div>
                 <div class="form-group">
                            <label for="txtQualities">What specific qualities do you possess that would make you better Wildlife Officer than another applicant?</label>
                           <asp:TextBox ID="txtQualities" runat="server" TextMode="MultiLine" Rows="4" Text='<%# Bind("Qualities") %>' CssClass="  form-control" AutoPostBack="true" OnTextChanged="txtWlOff_TextChanged" Font-Size="Medium"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtQualitiesReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ControlToValidate="txtQualities" ValidationGroup="WLOVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                         </div>
                    <div class="form-group">
                            <label for="txtConcern"> What is the major concern facing all of Tennessee wildlife and enviornmental resources today? 
                            As a wildlife officer representing the Agency, what impact do you think you will have in dealing with this concern?</label>
                          <asp:TextBox ID="txtConcern" runat="server" TextMode="MultiLine" Rows="4" Text='<%# Bind("MajorConernforWL") %>' CssClass="  form-control" AutoPostBack="true" OnTextChanged="txtWlOff_TextChanged" Font-Size="Medium"></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtConcernReqValidator" runat="server" ControlToValidate="txtConcern" Display="Dynamic" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ValidationGroup="WLOVG">
                </asp:RequiredFieldValidator>
                         </div>
                <table>
                   <%-- <tr>
                        <td class="tdbox" style="white-space:nowrap">
     Why do you want to become a Wildlife Officer?
                            </td>
                        </tr>--%>
                    <%--<tr>
                        <td class="tdbox">
                <asp:TextBox ID="txtWlOff" runat="server" TextMode="MultiLine" Text='<%# Bind("Reason_WLO") %>' CssClass=" " AutoPostBack="true" OnTextChanged="txtWlOff_TextChanged" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtWlOffReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ControlToValidate="txtWlOff" ValidationGroup="WLOVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    <tr>--%>
                        <%--<td class="tdbox" style="white-space:nowrap">
                What events or series of events caused you to make this decision?
                            </td>
                        </tr>--%>
                   <%-- <tr>
                        <td class="tdbox">
                <asp:TextBox ID="txtEvent" runat="server" TextMode="MultiLine" Text='<%# Bind("Events_ForDecision") %>' CssClass=" " AutoPostBack="true" OnTextChanged="txtWlOff_TextChanged"></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtEventReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ControlToValidate="txtEvent" ValidationGroup="WLOVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                            </td>
                        </tr>--%>
                   <%-- <tr>
                        <td class="tdbox" style="white-space:nowrap">
                What specific courses of actions have you taken to prepare yourself for a career as a Wildlife Officer?
                            </td>
                        </tr>--%>
                    <%--<tr>
                        <td class="tdbox">
                <asp:TextBox ID="txtCourses" runat="server" TextMode="MultiLine" Text='<%# Bind("CourseofAction") %>' CssClass=" " AutoPostBack="true" OnTextChanged="txtWlOff_TextChanged"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="txtCoursesReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ControlToValidate="txtCourses" ValidationGroup="WLOVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                            </td>
                        </tr>--%>
                   <%-- <tr>
                        <td class="tdbox" style="white-space:nowrap">
                   What specific qualities do you possess that would make you better Wildlife Officer than another applicant?
                            </td>
                        </tr>--%>
                    <%--<tr>
                        <td class="tdbox">
            <asp:TextBox ID="txtQualities" runat="server" TextMode="MultiLine" Text='<%# Bind("Qualities") %>' CssClass=" " AutoPostBack="true" OnTextChanged="txtWlOff_TextChanged"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtQualitiesReqValidator" runat="server" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ControlToValidate="txtQualities" ValidationGroup="WLOVG" Display="Dynamic">
                </asp:RequiredFieldValidator>
                            </td>
                        </tr>--%>
                  <%--  <tr>
                        <td class="tdbox">
                What is the major concern facing all of Tennessee wildlife and enviornmental resources today? 
                            As a wildlife officer representing the Agency, what impact do you think you will have in dealing with this concern?
                            </td>
                        </tr>--%>
                   <%-- <tr>
                        <td class="tdbox">
                <asp:TextBox ID="txtConcern" runat="server" TextMode="MultiLine" Text='<%# Bind("MajorConernforWL") %>' CssClass=" " AutoPostBack="true" OnTextChanged="txtWlOff_TextChanged"></asp:TextBox>
                <asp:RequiredFieldValidator ID="txtConcernReqValidator" runat="server" ControlToValidate="txtConcern" Display="Dynamic" ErrorMessage="*If none, please type N/A" Font-Bold="true" ForeColor="Red" ValidationGroup="WLOVG">
                </asp:RequiredFieldValidator>
                      </td>
                              </tr>--%>
                    </table>
                <%--<asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="Red" Text="**You must hit submit to save your data before logging off**"></asp:Label>--%>
                <br />
                <br /> 
                <asp:Button ID="btnPrevious" runat="server" Text="Go Back"   PostBackUrl="~/Court.aspx" CssClass="btn btn-warning btn-lg" />
                <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Submit" ValidationGroup="WLOVG" CssClass="btn btn-success btn-lg " />
                &nbsp;<asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Finish Later" OnClientClick="return confirm('Are you sure you want to quit?');" OnClick="UpdateCancelButton_Click" class="btn btn-danger btn-lg" />
                    </div>
                    </form>
            </EditItemTemplate>
        </asp:FormView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>" 
            SelectCommand="SELECT ID, Reason_WLO, Events_ForDecision, Qualities, CourseofAction, MajorConernforWL FROM OAQ.ApplicantQuestionaire WHERE (ID = @PerID)" 
            UpdateCommand="UPDATE OAQ.ApplicantQuestionaire SET Reason_WLO = @Reason_WLO, Events_ForDecision = @Events_ForDecision, CourseofAction = @CourseofAction, Qualities = @Qualities, MajorConernforWL = @MajorConernforWL WHERE (ID = @PerID)">
            <UpdateParameters>
                <asp:Parameter Name="Reason_WLO" />
                <asp:Parameter Name="Events_ForDecision" />
                <asp:Parameter Name="CourseofAction" />
                <asp:Parameter Name="Qualities" />
                <asp:Parameter Name="MajorConernforWL" />
                <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
            </UpdateParameters>
            <SelectParameters>
            <asp:SessionParameter Name="PerID" SessionField="PersonID" Type="String" />
        </SelectParameters>
        </asp:SqlDataSource>
    </p>
<%--    <asp:Label ID="lblMsg" runat="server" Text="Label"></asp:Label>--%>
</asp:Content>
