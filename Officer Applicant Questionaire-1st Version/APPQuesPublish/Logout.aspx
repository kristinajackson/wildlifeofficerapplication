﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Logout.aspx.cs" Inherits="Application_Questionaire.Logout" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <div class="text-center">
<h1>Logout</h1>
        <br />
    <h4>
        You have been logged out of the system. To log in, please return to the <a href="Login.aspx">
            login page</a>.</h4></div>
</asp:Content>

