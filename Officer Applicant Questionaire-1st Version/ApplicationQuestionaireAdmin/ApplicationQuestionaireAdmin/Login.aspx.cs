﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel;
using System.Drawing;
using System.Configuration;
using System.Text;
//using System.Windows.Forms;
using System.Web.Security;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;


namespace Application_Questionaire
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.IsAuthenticated && !string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
                    // This is an unauthorized, authenticated request...
                    Response.Redirect("~/UnauthorizedAccess.aspx");
            }
        }


        protected void LoginButton_Click(object sender, EventArgs e)
        {
            //bool flag = false;
            //Session["Username"] = UserName.Text.ToUpper();
            //if (UserName.Text == "")
            //    //flag = false;
            //else
            //{
            try
            {
                SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["ApplicantConnectionString"].ToString());
                SqlCommand cmd = new SqlCommand("select distinct ID from OAQ.ApplicantQuestionaire where UserName=@UserName and Password=@Password", cn);
                cmd.Parameters.AddWithValue("@UserName", UserName.Text);
                cmd.Parameters.AddWithValue("@Password", Password.Text);
                cn.Open();

                string PersonID = cmd.ExecuteScalar().ToString();
                if (!String.IsNullOrEmpty(PersonID))
                {
                    //flag = true;
                    Session["PersonID"] = PersonID;
                    Response.Redirect("~/Residences.aspx");

                }
                else
                {
                    lblMsg.Text = "Please Check your Username and Password";
                }
                // If User ID is Empty or Not Found
                //if (flag == false)
                //{
                //    lblMsg.Text = "Please Check your Username and Password";
                //}
            }
            catch { lblMsg.Text = "Please Check your Username and Password"; }

        }
            

            

        }
    }


    //    protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
    //    {
    //        TextBox EmailTextBox = Login1.FindControl("Email") as TextBox;
    //        string email = EmailTextBox.Text.Trim();

    //        if (Membership.ValidateUser(Login1.UserName, Login1.Password))
    //    {
    //        // Username/password are valid, check email
    //        MembershipUser usrInfo = Membership.GetUser(Login1.UserName);
    //        if (usrInfo != null && string.Compare(usrInfo.Email, email, true) == 0)
    //        {
    //            // Email matches, the credentials are valid
    //            e.Authenticated = true;
    //        }
    //        else
    //        {
    //            // Email address is invalid...
    //            e.Authenticated = false;
    //        }
    //    }
    //    else
    //    {
    //        // Username/password are not valid...
    //        e.Authenticated = false;
    //    }
    //}

    //    protected void Login1_LoginError(object sender, EventArgs e)
    //    {
    //     Login1.FailureText = "Your login attempt was not successful. Please try again.";

    //    // Does there exist a User account for this user?
    //    MembershipUser usrInfo = Membership.GetUser(Login1.UserName);
    //    if (usrInfo != null)
    //    {
    //        // Is this user locked out?
    //        if (usrInfo.IsLockedOut)
    //        {
    //            Login1.FailureText = "Your account has been locked out because of too many invalid login attempts. Please contact the administrator to have your account unlocked.";
    //        }
    //        else if (!usrInfo.IsApproved)
    //        {
    //            Login1.FailureText = "Your account has not yet been approved. You cannot login until an administrator has approved your account.";
    //        }
    //    } 
    //    }

    //    protected void LoginButton_Click(object sender, EventArgs e)
    //    {
    //    //{
    //    //    SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());
    //    //    SqlCommand cmd = new SqlCommand("select distinct ID from OAQ.ApplicantQuestionaire where UserName=@UserName and Password=@Password", cn);
    //    //            cmd.Parameters.AddWithValue("@UserName", UserName.Text);
    //    //            cmd.Parameters.AddWithValue("@Password", Password.Text);
    //    //    Session["PersonID"]=
    //    }

       
                
        //    SqlConnection con = new SqlConnection("Data Source=Testsql2012ndc.tn.gov,1589;database=BHHS;uid=BHHS_user;pwd=Bh%81hS5");
        //    SqlDataAdapter sda = new SqlDataAdapter("Select Count (*) From OAQ.CreateUser wher Username='" + UserName.Text + "' and Password ='" + Password.Text + "'", con);
        //    DataTable dt = new DataTable();
        //    //sda.Fill(dt);
        //    if (dt.Rows[0][0].ToString() == "1")
        //    {
        //        this.Visible = false;

        //        Login ss = new Login();
        //        ss.Visible = true;
        //    }
        //    else
        //    {
        //        MessageBox.Show("Please Check your Username and Password");
        //    }
            
        //}
    


    
