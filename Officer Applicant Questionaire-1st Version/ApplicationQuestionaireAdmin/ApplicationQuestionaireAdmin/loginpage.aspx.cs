﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Data;

namespace ApplicationQuestionaireAdmin
{
    public partial class loginpage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtUserID.Focus();
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
            //bool flag = false;
            // Save the UserID to Session suser to use it in other pages
            Session["suser"] = txtUserID.Text.ToUpper();
            //if (txtUserID.Text == "")
            //    flag = false; 
            //Check the User ID to only let Denise Briggs to login
            //else if (txtUserID.Text.ToUpper() == "BH01700" || txtUserID.Text.ToUpper() == "BH05066" || txtUserID.Text.ToUpper() == "BH05023" || txtUserID.Text.ToUpper() == "BH01516" || txtUserID.Text.ToUpper() == "BH05067")
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ApplicantConnectionString"].ConnectionString);
            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand("SELECT distinct BHnumber FROM OAQ.CreateUser WHERE BHnumber=@BHnumber", conn);
             cmd.Parameters.AddWithValue("@BHnumber",txtUserID.Text);
                conn.Open();
                string dbpassc = cmd.ExecuteScalar().ToString();
                if (txtUserID.Text.ToUpper().Trim() == dbpassc.ToUpper().Trim())
                {
                    Response.Redirect("~/CreateUser.aspx");
                }
                else
                    lblMsg.Text = "INVALID USER ID!";
            }
            catch
            {
                lblMsg.Text = "INVALID USER ID!";
            }

            //{
            //    flag = true;
            //    Response.Redirect("CreateUser.aspx");
            //}

            //        // If User ID is Empty or Not Found
            //if (flag == false)
            //{
            //    lblMsg.Text = "INVALID USER ID!";
            //}
            //else
            //{
            //    { lblMsg.Text = "INVALID USER ID!"; }
            //}


           
        }
    }
}