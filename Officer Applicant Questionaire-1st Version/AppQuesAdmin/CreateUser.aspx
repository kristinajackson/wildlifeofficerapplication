﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="CreateUser.aspx.cs" Inherits="ApplicationQuestionaireAdmin.CreateUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .center {
            text-align:center;
        }
        .right {
        text-align:right;
        }
        #right {
        text-align:right;
        }

        .auto-style1 {
            width: 402px;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DefaultMode="Insert" OnItemInserted="FormView1_ItemInserted">
        <InsertItemTemplate>
            <table style="font-size:100%;height:319px;width:599px;">
                        <tr>
                            
                            <td class="center" colspan="2" style="font-size: large; font-variant: small-caps">
                                <br />
                                <b>Sign Up for New Account</b></td>
                        </tr>
                        <tr>
                            <td class="center" colspan="2" style=" font-variant: small-caps">Please enter  user information:</td>
                        </tr>
                        <tr>
                            <td class="auto-style1">
                                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                            </td>
                            <td>
                                
                                <asp:TextBox ID="UserName" runat="server" Text='<%# Bind("UserName") %>' ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td  class="auto-style1">
                                <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="Password" runat="server" TextMode="Password" Text='<%# Bind("Password") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">
                                <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">E-mail:</asp:Label>
                            </td>
                            <td class="auto-style2">
                                <asp:TextBox ID="Email" runat="server" Text='<%# Bind("Email") %>' TextMode="Email"></asp:TextBox>
<%--                                <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email" ErrorMessage="E-mail is required." ToolTip="E-mail is required." ValidationGroup="CreateUserWizard1" ></asp:RequiredFieldValidator>--%>
                         <asp:RegularExpressionValidator ID="EmailRegular" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="Email" ErrorMessage="E-mail is required." ToolTip="E-mail is required." ValidationGroup="CreateUserWizard1" >*</asp:RegularExpressionValidator>     
                            </td>
                            </tr>
                <tr>
                            <td class="auto-style1">
                                <asp:Label ID="FirstNameLabel" runat="server" AssociatedControlID="FirstName">First Name:</asp:Label>
                                </td>
                            <td>
                                 <asp:TextBox ID="FirstName" runat="server" Text='<%# Bind("Fname") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="FirstNameRequired" runat="server" ControlToValidate="FirstName" ErrorMessage="First name is required." ToolTip="First Name is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                    </td>
                        </tr>
                <tr>
                            <td class="auto-style1">
                                <asp:Label ID="LastNameLabel" runat="server" AssociatedControlID="LastName">Last Name:</asp:Label>
                                </td>
                            <td>
                                 <asp:TextBox ID="LastName" runat="server" Text='<%# Bind("Lname") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="LastName" ErrorMessage="Last name is required." ToolTip="Last Name is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                    </td>
                        </tr>
                        <tr class="center">
                            <td class="auto-style1" colspan="2">
                                <asp:Button ID="InsertButton" runat="server" CausesValidation="True"
                            CommandName="Insert" Text="   Submit   "  ValidationGroup="CreateUserWizard1"   /> 
                                <asp:Button ID="ViewDtaBtn" runat="server" Text="  View Data  " OnClick="Button1_Click"/>
                                <asp:Button ID="Exitbtn" runat="server" Text="  Logout  " OnClick="Button2_Click" OnClientClick="return confirm('Are You Sure you want to Logout?');"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style2" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style2" colspan="2" style="color:Red;">
                                <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
                                <br />
                                
                            </td>
                        </tr>
                    </table>
        </InsertItemTemplate>
    </asp:FormView>
    <asp:Label ID="lblmsg" runat="server" Font-Bold="True" Font-Size="Medium"></asp:Label>
  &nbsp;<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicantConnectionString %>" 
      InsertCommand="INSERT INTO OAQ.ApplicantQuestionaire(UserName, Password,  Email,Fname,Lname) VALUES (@UserName, @Password, @Email,@Fname,@Lname)" 
      SelectCommand="SELECT UserName, Password,  Email,Fname,Lname) FROM OAQ.ApplicantQuestionaire">
        <InsertParameters>
            <asp:Parameter Name="UserName" />
            <asp:Parameter Name="Password" />
            <asp:Parameter Name="Email" />
            <asp:Parameter Name="Fname" />
            <asp:Parameter Name="Lname" />
        </InsertParameters>
    </asp:SqlDataSource>
    <br />

</asp:Content>
